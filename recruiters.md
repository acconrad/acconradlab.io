---
title: Thanks for reaching out
layout: page
class: 'post'
navigation: 'True'
current: recruiters
---

_Updated Mar 2022_

I’m always up for meeting new people with exciting challenges to face. I know your time is valuable so to connect as efficiently as possible, please review this page and provide the following information if you decide to reach out:

* Job Title
* Salary (exact amount or range in $ USD)
* Job Description
* Company Overview
* Company Location & Remote Status
* Steps in Hiring Process

To messages that include this information and the keyword at the bottom of this page, I will reply within 24 hours (usually sooner).

## About me

* Software engineer and manager with 13 years professional experience in consumer and B2B
* Works daily with React, Flow/TypeScript, JavaScript, Java, Python, and SQL
* Prefers a [manager of managers](https://knowyourteam.com/blog/2019/05/01/managing-managers-hows-it-different/) position
* Can commute to Boston; remote preferred
* Minimum cash salary (combination of base and bonus): $350,000
* Contact by email, not phone
* Current r&eacute;sum&eacute; is available [here](/img/resume-adamconrad.pdf)

## Work samples

* [GitHub](https://github.com/acconrad) - personal and open source projects
* [GetToKnow](http://gettoknowapp.com/) - Elixir application in the Slack marketplace
* [My consulting firm](https://anonconsulting.com/) - Custom web applications for select clients

## Status

* US citizen
* BS & MS Brown University
* Location: Boston
* Relocate: No
* Remote: Yes

## Expectations

* Full-time position; for contract positions, please inquire [here](https://anonconsulting.com/work-together/)
* Medical, Dental, 401K
* 4 weeks vacation (or 5 weeks paid time off)
* Minimum cash compensation: $350,000/yr (Salary + Bonus)

## Online profiles

* [LinkedIn](https://www.linkedin.com/in/acconrad)
* [GitHub](https://github.com/acconrad)
* [StackOverflow](https://stackoverflow.com/cv/acconrad)

## To connect

I receive a number of emails and voicemails from recruiters promoting jobs for which I would be a bad fit. If you have a job opening which you think would appeal to me, email me with the word **MoM** at the beginning of the subject line. This will allow me to give your message the special attention it deserves. My email is available on my r&eacute;sum&eacute;. No phone calls, please.
