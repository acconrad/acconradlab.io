<?xml version="1.0" encoding="UTF-8" ?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
   
      <title>https://adamconrad.dev</title>
   
   <link>https://adamconrad.dev</link>
   <description>The website and home of engineering leader Adam C. Conrad.</description>
   <language>en-us</language>
   <managingEditor> Adam C. Conrad</managingEditor>
   <atom:link href="rss" rel="self" type="application/rss+xml" />
   
	<item>
	  <title>What you need to know about WCAG 2.1</title>
	  <link>//blog/what-you-need-to-know-about-wcag-21/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2018-06-27T14:11:00-04:00</pubDate>
	  <guid>//blog/what-you-need-to-know-about-wcag-21/</guid>
	  <description><![CDATA[
	     <p>The W3C just released <a href="https://www.w3.org/TR/WCAG21/">an updated version of the Web Content Accessibility Guidelines</a> a month ago. It’s a pretty massive document with lots of new success criteria and recommendations. Fortunately, you don’t need to read the whole thing to understand what changes you need to make to your websites.</p>

<p>Want to jump right to the new success criteria? Here’s the full list along with tips on how to pass:</p>

<ul>
  <li><a href="#2-1-4">2.1.4 Character Key Shortcuts (A)</a></li>
  <li><a href="#2-5-1">2.5.1 Pointer Gestures (A)</a></li>
  <li><a href="#2-5-2">2.5.2 Pointer Cancellation (A)</a></li>
  <li><a href="#2-5-3">2.5.3 Label in Name (A)</a></li>
  <li><a href="#2-5-4">2.5.4 Motion Actuation (A)</a></li>
  <li><a href="#1-3-4">1.3.4 Orientation (AA)</a></li>
  <li><a href="#1-3-5">1.3.5 Identify Input Purpose (AA)</a></li>
  <li><a href="#1-4-10">1.4.10 Reflow (AA)</a></li>
  <li><a href="#1-4-11">1.4.11 Non-text Contrast (AA)</a></li>
  <li><a href="#1-4-12">1.4.12 Text Spacing (AA)</a></li>
  <li><a href="#1-4-13">1.4.13 Content on Hover or Focus (AA)</a></li>
  <li><a href="#4-1-3">4.1.3 Status Messages (AA)</a></li>
  <li><a href="#1-3-6">1.3.6 Identify Purpose (AAA)</a></li>
  <li><a href="#2-2-6">2.2.6 Timeouts (AAA)</a></li>
  <li><a href="#2-3-3">2.3.3 Animation from Interactions (AAA)</a></li>
  <li><a href="#2-5-5">2.5.5 Target Size (AAA)</a></li>
  <li><a href="#2-5-6">2.5.6 Concurrent Input Mechanisms (AAA)</a></li>
</ul>

<h2 id="what-do-the-different-conformance-levels-a-aa-aaa-mean">What do the different conformance levels (A, AA, AAA) mean?</h2>
<p>As seen in the WCAG 2.1 document, there are a large number of recommendations for accessibility. It would be unrealistic to expect site owners to adhere to them all, so <strong>each success criterion is given a priority for how important it is to satisfy</strong>.</p>

<p><img src="/assets/images/blur-codes-coding-577585.jpg" alt="Accessibility is important" /></p>

<p>Level A criteria are the most important, followed by AA, and then finally AAA criteria. Each level builds on the other, so it is necessary to satisfy all of the Level A requirements first before trying to satisfy AA or AAA requirements. This is because the W3C <a href="https://www.w3.org/TR/UNDERSTANDING-WCAG20/conformance.html#uc-conf-req1-head">requires full conformance at each level to be recognized as compliant</a>.</p>

<blockquote>
  <p>For Level AA conformance, the Web page satisfies <strong>all the Level A and Level AA Success Criteria</strong>, or a Level AA conforming alternate version is provided.</p>
</blockquote>

<p>Each section starts with Level A requirements, and builds up to Level AAA, with each requirement adding onto the previous one. For example, Section 3.3 is on input assistance. 3.3.1 and 3.3.2 are Level A requirements. 3.3.3 builds on 3.3.1, which is Level AA. The highest level, 3.3.5 and 3.3.6, base success around much more involved criteria, and thus achieve a rating of Level AAA.</p>

<h2 id="what-conformance-levels-do-i-need-to-pass">What conformance levels do I need to pass?</h2>
<p>The obvious answer would be to aim for AAA compliance, which is the strictest and most complete standard. The Level AAA standard will create an environment that maximizes your audience reach by providing support for the widest audience of disabled persons.</p>

<p>Practically speaking, <strong>aim for Level AA conformance</strong>. This is the highest realistic conformance level to achieve. The W3C recommends this level as well:</p>

<blockquote>
  <p>It is not recommended that Level AAA conformance be required as a general policy for entire sites because it is not possible to satisfy all Level AAA Success Criteria for some content.</p>
</blockquote>

<p>One example of Level AAA conformance that cannot be satisfied is <a href="https://www.w3.org/TR/WCAG21/#keyboard-no-exception">WCAG criterion 2.1.3 for keyboard shortcuts</a>. If your application is only available on mobile, such as the Uber app, there is no real keyboard, so it cannot be navigated by keyboard shortcuts, and will not pass this success metric. This, of course, is not a practical requirement because Uber is designed for use on mobile and should not have to try and cater their strategy towards a requirement like this.</p>

<p><img src="/assets/images/keyboard-events.jpg" alt="Keyboard events" /></p>

<p>Now that we have an idea of what level we need to aim for, here are the new success criteria in the updated guidelines and how to pass them.</p>

<h2 id="new-wcag-success-criteria">New WCAG success criteria</h2>

<h3 id="level-a-the-bare-minimum">Level A: The bare minimum</h3>

<h4 id="214-character-key-shortcuts-source"><span id="2-1-4">2.1.4 Character Key Shortcuts</span> (<a href="https://www.w3.org/TR/WCAG21/#character-key-shortcuts">source</a>)</h4>
<h5 id="what-it-means">What it means</h5>
<p>Keyboard shortcuts aren’t just for power users like vim developers. They are also super useful for the blind, who have no reason for using a monitor or mouse. Therefore, it is a good idea to implement some basic keyboard shortcuts to navigate your site without the need for a mouse.</p>

<h5 id="how-to-pass">How to pass</h5>
<ul>
  <li><strong>If you have shortcuts, make sure you can disable them</strong></li>
  <li><strong>The shortcut can be mapped to a different key than you provided by default</strong></li>
  <li><strong>The shortcut can only be used on a component on in focus.</strong> You can’t accidentally trigger the shortcut if it isn’t actively on the part of the page that should receive that shortcut.</li>
</ul>

<h4 id="251-pointer-gestures-source"><span id="2-5-1">2.5.1 Pointer Gestures</span> (<a href="https://www.w3.org/TR/WCAG21/#pointer-gestures">source</a>)</h4>
<h5 id="what-it-means-1">What it means</h5>
<p>Things like touch gestures have many ways of interacting. Single point gestures include tapping or clicking. Multipoint gestures include things like pinch-to-zoom. Path-based gestures include things like swiping or sliding. Your application should account for all of these.</p>

<p><img src="/assets/images/touch-interaction.jpg" alt="touch gestures need fallbacks as well" /></p>

<h5 id="how-to-pass-1">How to pass</h5>
<p>If you use multipoint or path-based gestures, the <strong>actions that are triggered by those gestures should have a single point fallback</strong>. In other words, if you use the swipe event to mark something as complete, you should also have a touch or click gesture to mark that item complete as well.</p>

<h4 id="252-pointer-cancellation-source"><span id="2-5-2">2.5.2 Pointer Cancellation</span> (<a href="https://www.w3.org/TR/WCAG21/#pointer-cancellation">source</a>)</h4>
<h5 id="what-it-means-2">What it means</h5>
<p>Actions can be triggered with, for example, a click. Those actions should be reversible.</p>

<h5 id="how-to-pass-2">How to pass</h5>
<ul>
  <li><strong>Don’t use the down-event</strong> (e.g. <code class="highlighter-rouge">keydown</code> or <code class="highlighter-rouge">mousepress</code>) unless it is essential</li>
  <li><strong>Show a popup or alert to undo the previous action</strong></li>
  <li><strong>If you use a down-event, an equivalent up-event should be available to undo that action</strong></li>
</ul>

<h4 id="253-label-in-name-source"><span id="2-5-3">2.5.3 Label in Name</span> (<a href="https://www.w3.org/TR/WCAG21/#label-in-name">source</a>)</h4>
<h5 id="what-it-means-3">What it means</h5>
<p>Images can be used inside of a label to describe content. The component housing those images should have a text fallback of that image content.</p>

<h5 id="how-to-pass-3">How to pass</h5>
<ul>
  <li><strong>If you use an image to label something, the naming attribute should have the text of that image or a description of that image as a fallback.</strong> <a href="https://www.w3.org/TR/accname-1.1/#accessible-name-and-description-mapping">Accessible name and description mappings</a> include attributes like <code class="highlighter-rouge">alt</code> for <code class="highlighter-rouge">&lt;img&gt;</code> tags, <a href="https://webaim.org/techniques/captions/">captions, transcriptions, and descriptions</a> for <code class="highlighter-rouge">&lt;audio&gt;</code> &amp; <code class="highlighter-rouge">&lt;video&gt;</code> content, and <code class="highlighter-rouge">aria-label</code>, <code class="highlighter-rouge">aria-labelledby</code>, or <code class="highlighter-rouge">aria-describedby</code> for all other tags.</li>
</ul>

<h4 id="254-motion-actuation-source"><span id="2-5-4">2.5.4 Motion Actuation</span> (<a href="https://www.w3.org/TR/WCAG21/#motion-actuation">source</a>)</h4>
<h5 id="what-it-means-4">What it means</h5>
<p>Actions can be triggered by device motion, such as shaking, tilting, or turning orientation. Motion actions should have the option of being disabled.</p>

<h5 id="how-to-pass-4">How to pass</h5>
<ul>
  <li><strong>Provide a menu item or setting to disable motion events</strong></li>
  <li><strong>Don’t use motion events unless they are essential to the functionality of the activity</strong></li>
  <li><strong>Don’t use motion events unless the device is a supported accessibility device with motion in mind</strong> (e.g. haptic response for a Braille reader)</li>
</ul>

<h3 id="level-aa-the-gold-standard">Level AA: The gold standard</h3>

<h4 id="134-orientation-source"><span id="1-3-4">1.3.4 Orientation</span> (<a href="https://www.w3.org/TR/WCAG21/#orientation">source</a>)</h4>
<h5 id="what-it-means-5">What it means</h5>
<p>Content can be viewed in a variety of orientations, such as landscape or portrait.</p>

<h5 id="how-to-pass-5">How to pass</h5>
<ul>
  <li><strong>Don’t limit your content to be viewed in only 1 orientation</strong></li>
</ul>

<h4 id="135-identify-input-purpose-source"><span id="1-3-5">1.3.5 Identify Input Purpose</span> (<a href="https://www.w3.org/TR/WCAG21/#identify-input-purpose">source</a>)</h4>
<h5 id="what-it-means-6">What it means</h5>
<p>You can programmatically explain the purpose of any input element.</p>

<h5 id="how-to-pass-6">How to pass</h5>
<ul>
  <li><strong>Use available attributes when possible to explain why that input is being used.</strong> For example, use the <code class="highlighter-rouge">autocomplete</code> attribute on text fields that will take advantage of this functionality.</li>
</ul>

<h4 id="1410-reflow-source"><span id="1-4-10">1.4.10 Reflow</span> (<a href="https://www.w3.org/TR/WCAG21/#reflow">source</a>)</h4>
<h5 id="what-it-means-7">What it means</h5>
<p>Software like browsers allow you to zoom in and out of the content they display. This causes content to reflow and change how it is laid out on the screen, similar to media queries in responsive design.</p>

<h5 id="how-to-pass-7">How to pass</h5>
<ul>
  <li><strong>Use responsive design meta tags to make the content fit your device.</strong> If content reflows, it shouldn’t start requiring you to horizontally scroll.</li>
  <li><strong>If reflows happen, that should not restrict functionality.</strong> Again, make sure you use the correct meta tags to prevent content from going off of the screen, particularly buttons, links, or other components that provide functionality to the system.</li>
</ul>

<h4 id="1411-non-text-contrast-source"><span id="1-4-11">1.4.11 Non-text Contrast</span> (<a href="https://www.w3.org/TR/WCAG21/#non-text-contrast">source</a>)</h4>
<h5 id="what-it-means-8">What it means</h5>
<p>Contrast exists in all things, not just text. People should be able to interpret and read buttons, select dropdowns, and other components that are not primarily identified as text.</p>

<p><img src="/assets/images/reader.jpg" alt="black and white readers require high contrast" /></p>

<h5 id="how-to-pass-8">How to pass</h5>
<ul>
  <li><strong>Use a <a href="https://webaim.org/resources/contrastchecker/">color contrast checker</a></strong> to identify colors that are <strong>greater than a 3:1 ratio of contrast</strong>. For example, use a sufficiently contrasting blue background on a button with white text.</li>
</ul>

<h4 id="1412-text-spacing-source"><span id="1-4-12">1.4.12 Text Spacing</span> (<a href="https://www.w3.org/TR/WCAG21/#text-spacing">source</a>)</h4>
<h5 id="what-it-means-9">What it means</h5>
<p>Text needs to breathe. If it’s too close together, it’s hard to read, especially for those with impaired eyesight.</p>

<h5 id="how-to-pass-9">How to pass</h5>
<ul>
  <li><code class="highlighter-rouge">line-height</code> &gt; 1.5</li>
  <li><code class="highlighter-rouge">margin-top</code>/<code class="highlighter-rouge">margin-bottom</code> &gt; 2 * font size</li>
  <li><code class="highlighter-rouge">letter-spacing</code> &gt; .12 * font size</li>
  <li><code class="highlighter-rouge">word-spacing</code> &gt; .16 * font size</li>
</ul>

<h4 id="1413-content-on-hover-or-focus-source"><span id="1-4-13">1.4.13 Content on Hover or Focus</span> (<a href="https://www.w3.org/TR/WCAG21/#content-on-hover-or-focus">source</a>)</h4>
<h5 id="what-it-means-10">What it means</h5>
<p>Some consideration is required for content that is only available on hover/focus events, such as popups and tooltips.</p>

<h5 id="how-to-pass-10">How to pass</h5>
<ul>
  <li><strong>You can dismiss this content without having to lose hover or focus.</strong> For example, use the <code class="highlighter-rouge">esc</code> key to dismiss something.</li>
  <li><strong>If something is hoverable, you should be able to pass over that content without losing that content.</strong> For example, if you have a tooltip on hover of an acronym like HTML, and you move the mouse onto that tooltip, the tooltip should not disappear because you moved your mouse onto tooltip and off of the acronym.</li>
</ul>

<h4 id="413-status-messagesa-source"><span id="4-1-3">4.1.3 Status Messages&lt;/a&gt; (<a href="https://www.w3.org/TR/WCAG21/#status-messages">source</a>)</span></h4>
<h5 id="what-it-means-11">What it means</h5>
<p>A status message is dynamic content like a warning or error. These kinds of messages usually show up at the top of the document and usually break the normal flow of reading the document.</p>

<h5 id="how-to-pass-11">How to pass</h5>
<ul>
  <li><strong>If you issue a status message, an assistive technology like a screen reader should be able to read this message without having to receive focus.</strong> So if you have a blog with a “Read More” button, and the button fails to load the additional content, the status message should be readable by the screen reader without having to use keyboard shortcuts or some maneuver to change where you are in the content flow in order to read what has happened.</li>
</ul>

<h3 id="level-aaa-extra-credit">Level AAA: Extra credit</h3>

<h4 id="136-identify-purpose-source"><span id="1-3-6">1.3.6 Identify Purpose</span> (<a href="https://www.w3.org/TR/WCAG21/#identify-purpose">source</a>)</h4>
<h5 id="what-it-means-12">What it means</h5>
<p>Similar to <a href="#1-3-5">1.3.5</a>, you can explain the purpose of all tags in a markup language, not just input elements.</p>

<h5 id="how-to-pass-12">How to pass</h5>
<ul>
  <li>Make liberal use of the <a href="https://www.w3.org/TR/2017/NOTE-wai-aria-practices-1.1-20171214/examples/landmarks/index.html">ARIA properties</a></li>
  <li><strong>Provide keyboard shortcuts</strong></li>
  <li><strong>Keep pages simple.</strong> Prefer more pages that are simpler in focus than fewer pages with lots of functionality.</li>
  <li><strong>Use symbols and iconography that users are familiar with</strong> (e.g. right triangle for playing audio or hamburger for menus)</li>
</ul>

<h4 id="226-timeouts-source"><span id="2-2-6">2.2.6 Timeouts</span> (<a href="https://www.w3.org/TR/WCAG21/#timeouts">source</a>)</h4>
<h5 id="what-it-means-13">What it means</h5>
<p>When a user loses connectivity to a site, such as poor cellular service, popups show things called timeouts which inform the user that service has been interrupted. People like the elderly sometimes cannot complete these kinds of forms in one sitting and may have to revisit this content multiple times.</p>

<h5 id="how-to-pass-13">How to pass</h5>
<ul>
  <li>If you use a timeout, <strong>inform the user that there is data loss</strong> and <strong>how long the service will be down for</strong>.</li>
  <li><strong>Store data for 20 hours or longer.</strong> For example, if you have a checkout form, it should autosave the progress on the form for at least this long.</li>
</ul>

<h4 id="233-animation-from-interactions-source"><span id="2-3-3">2.3.3 Animation from Interactions</span> (<a href="https://www.w3.org/TR/WCAG21/#animation-from-interactions">source</a>)</h4>
<h5 id="what-it-means-14">What it means</h5>
<p>Motion animation can be disabled unless it is essential to the functionality of the component. This is important because high speed or repetitive animations could induce seizures or other physical maladies.</p>

<h5 id="how-to-pass-14">How to pass</h5>
<ul>
  <li>Animations that are triggered by some kind of interaction can be disabled unless the animation is required for the component’s functionality. <strong>Allow animations to be disabled via a setting or shortcut.</strong></li>
  <li><strong>Don’t use animations that could cause issues for users.</strong> Police lights, camera flash, repetitive blinking, you get the idea. These kinds of animations are pretty much never needed for an application to work and are just there to add to the design. Avoid them if you can.</li>
</ul>

<h4 id="255-target-size-source"><span id="2-5-5">2.5.5 Target Size</span> (<a href="https://www.w3.org/TR/WCAG21/#target-size">source</a>)</h4>
<h5 id="what-it-means-15">What it means</h5>
<p>The target size is the interactable area of an input. It should be sufficiently large enough to make it easy to identify and interact with.</p>

<h5 id="how-to-pass-15">How to pass</h5>
<ul>
  <li><strong>Make the target at least 44x44 pixels.</strong> If the primary target cannot be that large, there should be a fallback component (such as a link) that is that large.</li>
  <li><strong>Don’t worry about links or inline text.</strong> Those are exempt from this rule.</li>
  <li><strong>Don’t worry about elements controlled by the User Agent.</strong> If you can’t change the component yourself, this criterion won’t hold it against you.</li>
  <li><strong>Don’t worry about elements that are designed to be small on purpose.</strong> For example, an information icon for a tooltip (often portrayed inline as the letter “i” in a circle) is generally presented in a paragraph and would look weird if it were 44x44 pixels in dimension.</li>
</ul>

<h4 id="256-concurrent-input-mechanisms-source"><span id="2-5-6">2.5.6 Concurrent Input Mechanisms</span> (<a href="https://www.w3.org/TR/WCAG21/#concurrent-input-mechanisms">source</a>)</h4>
<h5 id="what-it-means-16">What it means</h5>
<p>Content can be interacted with in multiple ways. You can type in a text box, click on the textbox, or touch the textbox with your thumb. Don’t assume that users want to interact with those elements in a limited subset of those interactions.</p>

<p><img src="/assets/images/taking-notes.jpg" alt="no input should be turned down" /></p>

<h5 id="how-to-pass-16">How to pass</h5>
<ul>
  <li><strong>Don’t restrict any kinds of interactions unless they compromise security or the user specifically disabled them</strong>. For example, if you have a credit card form, just because you’ve enabled keyboard shortcuts to navigate across input elements doesn’t mean you should simultaneously disable touch events because they’re a slower interaction.</li>
</ul>

<h2 id="youve-succeeded">You’ve succeeded!</h2>
<p>You should now be well on your way to providing an accessible experience for your users. Obviously, you’ll need to provide support for all of the other requirements from the previous versions of the WCAG, but now you are able to update your compliance to the latest standards.</p>

<p>If this all feels like too much for you, <a href="https://anonconsulting.com/services/">we offer accessibility compliance as a service</a> so you don’t have to worry about upgrading to the latest standards. Or <a href="https://twitter.com/theadamconrad">send me a question on Twitter</a> and I’d be happy to help answer anything that was confusing or difficult to fix.</p>

	  ]]></description>
	</item>


</channel>
</rss>
