<?xml version="1.0" encoding="UTF-8" ?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
   
      <title>https://adamconrad.dev</title>
   
   <link>https://adamconrad.dev</link>
   <description>The website and home of engineering leader Adam C. Conrad.</description>
   <language>en-us</language>
   <managingEditor> Adam C. Conrad</managingEditor>
   <atom:link href="rss" rel="self" type="application/rss+xml" />
   
	<item>
	  <title>Hitting deadlines</title>
	  <link>//blog/hitting-deadlines/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2021-03-09T11:04:00-05:00</pubDate>
	  <guid>//blog/hitting-deadlines/</guid>
	  <description><![CDATA[
	     <p>My recent productivity obsession is around <a href="https://www.joelonsoftware.com/2007/10/26/evidence-based-scheduling/">Evidence-Based Scheduling</a>.</p>

<p>Software estimation is hard. But I also want to make sure our stakeholders understand how far we are along in our projects and when we think we can reasonably deliver some software.</p>

<p>While watching <em>The Last Dance</em> this weekend about the ‘98 Chicago Bulls basketball team, I got to thinking about how great sports teams all review tape from their previous performances.</p>

<p>Who does that in software? I don’t think I’ve seen anyone do that. And yet, if you think about the principles behind the book in <em>Deep Work</em>, you have to get as much feedback as possible as to how you are performing at your job.</p>

<p><em>Practicing</em> estimation is a skill and takes effort and time. In speaking with a few colleagues and friends on how they’ve implemented this themselves, there are a few things to keep in mind.</p>

<h2 id="buy-in-and-messaging-is-key">Buy-in and messaging is key</h2>

<p>On the surface, this sounds like micromanagement to the <em>n</em> -th degree. Now my boss is tracking my time? <strong>I’m not tracking your time - YOU are</strong>. It’s really easy for a framework for this to sound like a way to place the lens of Big Brother over your development teams. That is <em>not</em> the goal at all.</p>

<p>Instead, this is about tracking <em>yourself</em> to see how good you are at estimating your own work. Endless articles have demonstrated that we, as an industry, are <em>really</em> bad at estimating how long our work will take. This is putting that into repeated, frequent practice.</p>

<p>There are no points for completing things early and there is no shame in finishing things way off target. The point is not to grade your performance in matching expectations. <strong>The point is to see what is your pattern for estimation in the first place and account for that.</strong></p>

<p>Maybe you are the chronic optimist and everything you do actually takes 20% longer than you expect.</p>

<p>Great! Now we can ensure that if you take a ticket we can reliably add 20% to the estimate to predict when any of your work will arrive. The time to complete doesn’t matter; all that matters is the message to stakeholders about when we think this will most likely arrive.</p>

<h2 id="it-takes-a-lot-of-work-upfront">It takes a lot of work upfront</h2>

<p>A friend of mine mentioned that his team of 8 took about 8 hours <em>a week</em> in meetings breaking down tickets in grooming and providing granular estimates of the work that was to be delivered. That’s a <em>lot</em> of time in meetings per week, particularly for individual contributors.</p>

<p>At first, that seems like the effort is way too high for a team size that small. However, after a few weeks of working through this system, they were able to get these meetings down to about 2.5 hours while also increasing their accuracy and delivery.</p>

<p>Would you spend 5% of your week ensuring your delivery is 90% accurate to stakeholders? I’d take that each and every week without fail!</p>

<h2 id="target-practice">Target practice</h2>

<p>Remember that all of this is not an excuse to let stakeholders pepper your team with deadlines. <strong>It is an anti-pattern to adopt EBS <em>so you can enforce deadlines</em></strong>. Rather, it is a reaction to an anti-pattern of deadline-driven development so you can get your teams out of the corner you’ve already backed into.</p>

<p>The end result of adopting evidence-based scheduling should look something like a bull’s eye target. This analogy I use time and time again with my team leads when one of their stakeholders is adamant about requiring a specific date for something to arrive.</p>

<p>Imagine you are firing an arrow at a target 100 yards away with a 5’ radius. The goal, of course, is to hit the bull’s eye in as few shots as possible. There are two ways to do this:</p>

<ol>
  <li><strong>Increase your accuracy</strong>. Each time you fire an arrow (i.e. deliver an estimate) it should get closer to your target. In other words, if you think something will take 2 days and you’re off by 1 day this week and then off by 2 days next week, your estimation skills are only getting worse.</li>
  <li><strong>Move closer to the target</strong>. It’s a lot easier to hit a target from 50 yards away than 100. In software, this is just time moving forward. The closer you get to the projected date of delivery (i.e. target) the clearer the bull’s eye becomes. You probably know what you hope to accomplish today but who knows what you’re confident to deliver 9 months from now.</li>
</ol>

<p>So if you practice hitting a target regularly (i.e. practicing evidence-based scheduling) while simultaneously moving closer to the target (i.e. building more software every day), you should be moving away from deadlines towards target dates which just get more <em>accurate</em> and <em>predictable</em> over time.</p>

<p>At the beginning of the quarter, your project will arrive in 3 months with 60% confidence. In 2 months the project will arrive in 3 weeks in that month with 80%. In that week, the project will arrive on that Thursday with 95% confidence. This is how we shift away from deadlines to target dates.</p>

<p>Stakeholders still hear a date that makes them happy and you aren’t beholden to a rigid delivery date which makes you happy. Everyone wins. That is why I am so excited to try this system out and if you have any experience with EBS please let me know.</p>

	  ]]></description>
	</item>


</channel>
</rss>
