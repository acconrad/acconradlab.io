<?xml version="1.0" encoding="UTF-8" ?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
   
      <title>https://adamconrad.dev</title>
   
   <link>https://adamconrad.dev</link>
   <description>The website and home of engineering leader Adam C. Conrad.</description>
   <language>en-us</language>
   <managingEditor> Adam C. Conrad</managingEditor>
   <atom:link href="rss" rel="self" type="application/rss+xml" />
   
	<item>
	  <title>Build your network now</title>
	  <link>//blog/build-your-network-now/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2019-04-02T22:49:10-04:00</pubDate>
	  <guid>//blog/build-your-network-now/</guid>
	  <description><![CDATA[
	     <p>My contract got cut short. I’ve got to assemble another client in the next 30 days or the money stops coming in. Thankfully, I’ve built a strong network over the last decade I’ve been in this industry so I think I should be alright.</p>

<p>Would you be okay if this happened to you?</p>

<p>I’ve heard this phrase so many times it has been drilled into my brain when it comes to networking:</p>

<blockquote>
  <p>Dig the well before you get thirsty</p>
</blockquote>

<p>Most people think of networking as a <em>reactionary</em> activity. You network with people at meetups because you need something from someone. No wonder people hate to go to meetups!</p>

<p><strong>Networking should be a <em>proactive</em> activity.</strong> You <em>offer</em> help and connections. You build people up and bring them together <em>just because you can</em>.</p>

<p>What does this get you?</p>

<p>If you’ve asked that question you’ve already lost. Stop only thinking about yourself. <strong>No one wants to network with selfish people.</strong> The question you should be asking is:</p>

<blockquote>
  <p>How can I make the world a better place for my fellow humans?</p>
</blockquote>

<p>When you give back, you get more in return. And I don’t mean this in the touchy-feely validating kind of way. I mean real benefits when your contract runs out and you need more money NOW kind of benefit.</p>

<p>Imagine your friend is looking to start a company. Imagine your other friend is looking to start a company. You see two people and one opportunity to actualize. In that moment it probably has no material impact on your life. But in the future when you are looking for a job and those two are now hiring, who do you think they are going to ask to fill their top spot? Who do you think will have their own network of folks looking for freelancers to help build the MVPs for their startups?</p>

<p>If you’re thinking this imaginative scenario actually happened to me, you’d be right.</p>

<p>I actually helped connect my friends together to start a successful startup. It did nothing for my wallet for years. But eventually I ended up working for their company. And years after that they helped me land some key clients when I went into business for myself. And we’re all still friends!</p>

<p>This stuff works. But it only works if you live with an abundance mentality that <strong>it’s not all about you</strong>. If you only network when you’re desperate and need something, you’re going to have a bad time. Which is no wonder why everyone fails at it and thinks it’s scummy and no fun.</p>

<p>If, instead, you <strong>make networking about genuine, authentic connection</strong>, you’re apt to get everything you could ever want out of networking and way more than you bargained for. I may be a sample size of one but my hope is that you’ll add to that list of anecdotes as well.</p>

	  ]]></description>
	</item>


</channel>
</rss>
