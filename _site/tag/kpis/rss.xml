<?xml version="1.0" encoding="UTF-8" ?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
   
      <title>https://adamconrad.dev</title>
   
   <link>https://adamconrad.dev</link>
   <description>The website and home of engineering leader Adam C. Conrad.</description>
   <language>en-us</language>
   <managingEditor> Adam C. Conrad</managingEditor>
   <atom:link href="rss" rel="self" type="application/rss+xml" />
   
	<item>
	  <title>On business value</title>
	  <link>//blog/on-business-value/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2021-05-17T11:04:00-04:00</pubDate>
	  <guid>//blog/on-business-value/</guid>
	  <description><![CDATA[
	     <p>I did an AMA on Clubhouse a little while ago and there were a lot of awesome, hard-hitting questions on the panel. So for the next several weeks, I’m going to write out my thoughts to each of the questions and how I answered them.</p>

<blockquote>
  <p>How do you showcase the business value of the work that your team is doing?</p>
</blockquote>

<p>There’s something that gives me pause about this question having the adjective <code class="highlighter-rouge">business</code> in there. If you’re building a product, you’re adding value to the business. What value is not business value in this context? Or put it another way: <strong>what is valuable that <em>isn’t</em> business value for professional code?</strong>.</p>

<p>So I will answer this question with the broader aim to <strong>demonstrate value of the work you do</strong>.</p>

<p>The easiest way to do this is to <a href="/blog/how-to-demonstrate-impact">showcase your impact</a> on the organization. The best way to automate this is to set up regular OKRs every quarter that relate to business KPIs (i.e. revenue, monthly active users, customer retention). Showing impact with numbers and figures against business targets is the easiest way to bridge the gap with business folks.</p>

<p>The more difficult, laborious way to do this is to compare the value you and your teams have generated against how much <a href="/blog/how-to-justify-your-cost-to-a-client">you and your teams’ cost</a>.</p>

<p>Really big companies like Facebook know <strong>how much a developer contributes to the company bottom line</strong> - both in revenue and the number of users they impact. That’s a huge draw for some developers: imagine if you alone could make a change that affects <em>millions</em> of people! Now what they don’t tell you is that your changes probably amount to altering a button from blue to a slightly more <a href="https://blog.ultimate-uk.com/googles-41-shades-of-blue">convincing shade of blue</a>. So if you want to work on something that will actually move the needle on fixing the planet we live on, <a href="https://indigoag.com/join-us#openings">we’re hiring</a>.</p>

	  ]]></description>
	</item>


</channel>
</rss>
