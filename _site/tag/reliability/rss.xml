<?xml version="1.0" encoding="UTF-8" ?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
   
      <title>https://adamconrad.dev</title>
   
   <link>https://adamconrad.dev</link>
   <description>The website and home of engineering leader Adam C. Conrad.</description>
   <language>en-us</language>
   <managingEditor> Adam C. Conrad</managingEditor>
   <atom:link href="rss" rel="self" type="application/rss+xml" />
   
	<item>
	  <title>Scalability, Reliability, and Maintainability</title>
	  <link>//blog/scalability-reliability-maintainability/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2021-06-28T15:28:00-04:00</pubDate>
	  <guid>//blog/scalability-reliability-maintainability/</guid>
	  <description><![CDATA[
	     <p>Like my series on <a href="/blog/why-hiring-is-broken-and-how-im-dealing-with-it/">Algorithms</a>, I’ve decided that I need to <em>really</em> understand systems design.</p>

<p>I run hiring for multiple teams. Some of our questions revolve around systems design. How can I possibly ask these questions if I couldn’t answer them perfectly myself?</p>

<p>It’s only fair that I master these concepts. The book that everyone seems to agree with is the best book on this topic is <a href="https://dataintensive.net/"><em>Designing Data-Intensive Applications</em></a> by Martin Kleppmann.</p>

<p>In the spirit of my last educational series, I will transcribe my raw notes over the next 7 weeks or so. I do this both to teach my readers and also to reinforce the concepts myself. As I recall from my course on <a href="https://www.coursera.org/learn/learning-how-to-learn">learning how to learn</a>, one of the best ways to learn is to teach others.</p>

<h2 id="the-syllabus">The syllabus</h2>

<p>I’ve decided to craft a syllabus for myself as if I were both the teacher and the student to help keep me on track and reinforcing the concepts. If you have a similar learning plan or have questions about mine, please feel free to <a href="/newsletter">reach out</a>.</p>

<ul>
  <li>Week 1 (this week)
    <ul>
      <li>Read Chapter 1: Scalability, Reliability, and Maintainability</li>
      <li>Practice: <a href="https://www.youtube.com/watch?v=bUHFg8CZFws">Design a scalable system to count events</a></li>
      <li>Explore, at a high-level, data tools in the space</li>
    </ul>
  </li>
  <li>Week 2
    <ul>
      <li>Read Chapter 2: Data Models &amp; Query Languages</li>
      <li>Practice: <a href="https://www.youtube.com/watch?v=kx-XDoPjoHw">Top K Problem</a></li>
    </ul>
  </li>
  <li>Week 3
    <ul>
      <li>Read Chapter 3: Storage &amp; Retrieval</li>
      <li>Practice: <a href="https://www.youtube.com/watch?v=iuqZvajTOyA">Distributed cache</a></li>
    </ul>
  </li>
  <li>Week 4
    <ul>
      <li>Read Chapter 5: Replication</li>
      <li>Practice: <a href="https://www.youtube.com/watch?v=FU4WlwfS3G0">Rate limiter</a></li>
    </ul>
  </li>
  <li>Week 5
    <ul>
      <li>Read Chapter 6: Partitioning</li>
      <li>Practice: <a href="https://www.youtube.com/watch?v=bBTPZ9NdSk8">Notification service</a></li>
    </ul>
  </li>
  <li>Week 6
    <ul>
      <li>Read Chapter 8: Faults &amp; Reliability</li>
      <li>Practice: <a href="https://www.youtube.com/watch?v=iJLL-KPqBpM">Distributed messaging queue</a></li>
      <li>Read: <a href="http://highscalability.com/blog/2010/11/16/facebooks-new-real-time-messaging-system-hbase-to-store-135.html">Facebook’s messaging queue with HBase</a></li>
    </ul>
  </li>
  <li>Week 7
    <ul>
      <li>Read Chapter 9: Consistency &amp; Consensus</li>
      <li>Practice: Read the <a href="http://highscalability.com/blog/2011/12/19/how-twitter-stores-250-million-tweets-a-day-using-mysql.html">High</a> <a href="http://highscalability.com/scaling-twitter-making-twitter-10000-percent-faster">Scalability</a> <a href="http://highscalability.com/blog/2013/7/8/the-architecture-twitter-uses-to-deal-with-150m-active-users.html">Series</a> on Twitter and implement it</li>
    </ul>
  </li>
  <li>Week 8
    <ul>
      <li>Skim Ch 10 &amp; 11: Stream and Batch Processing</li>
      <li>More possible practice that leverage stream/batch processing like <a href="https://www.youtube.com/watch?v=umWABit-wbk">Uber</a></li>
      <li>Listen: a <a href="https://www.youtube.com/watch?v=grGqCuTcu50">podcast review</a> of the book</li>
    </ul>
  </li>
</ul>

<h2 id="notes-on-chapter-1">Notes on Chapter 1</h2>

<p>Chapter one is really an overview chapter. It’s easy to read and easy to skim. There aren’t too many deep concepts here. I found lots of shortlists/rules of thumb to be all you need to focus on the basics.</p>

<h3 id="data-storage">Data storage</h3>

<p>When thinking of data storage, consider these 5 buckets:</p>

<ol>
  <li>DB engines
    <ul>
      <li><strong>Row:</strong> good for <abbr title="Online Transaction Processing">OLTP</abbr>, random reads, random writes, and write-heavy interactions (Postgres, MySQL)</li>
      <li><strong>Column:</strong> good for <abbr title="Online Analytical Processing">OLAP</abbr>, heavy reads on few columns, and few writes (HBase, Cassandra, Vertica, Bigtable)</li>
      <li><strong>Document:</strong> good for data models with one-to-many relationships such as a <abbr title="Content Management System">CMS</abbr> like a blog or video platform, or when you’re cataloging things like e-commerce products (MongoDB, CouchDB)</li>
      <li><strong>Graph:</strong> good for data models with many-to-many relationships such as fraud detection between financial and purchase transactions or recommendation engines that associate many relationships to recommend products (Neo4J, Amazon Neptune)</li>
      <li><strong>Key-Value:</strong> essentially a giant hash table/dictionary, good for in-memory session storage or caching e-commerce shopping cart data (Redis, DynamoDB)</li>
    </ul>
  </li>
  <li><strong>Search indexes</strong> (ElasticSearch, Solr)</li>
  <li><strong>Caches</strong> (Memcached, Redis)</li>
  <li><strong>Batch processing frameworks:</strong> good if data delay can be several hours or more and you can store all the data and process it later (Storm, Flink, Hadoop)</li>
  <li><strong>Stream processing frameworks:</strong> good if data delay can only be several minutes and you need to store data in aggregate while processing data on-the-fly (Kafka, Samza, Flink)</li>
</ol>

<h3 id="systems-design-in-a-nutshell">Systems design in a nutshell</h3>

<p>Approaching systems design falls into 5 steps:</p>

<ol>
  <li><strong>Functional requirements</strong> are designed to get us to think in APIs where we translate sentences into verbs for function names and nouns for input and return values.</li>
  <li><strong>Non-functional requirements</strong> describe system qualities like scalability, availability, performance, consistency, durability, maintainability, and cost.</li>
  <li><strong>High-level designs</strong> present the inbound and outbound data flow of the system.</li>
  <li><strong>Detailed designs</strong> are for the specific components you want to focus on. Focus on the technologies you want to use for the data you want to store, transfer, process, and access.</li>
  <li><strong>Bottlenecks &amp; tradeoffs</strong> ensure we know how to find the limits of our designs and how we can balance solutions since there is no one singular correct answer to an architecture.</li>
</ol>

<h3 id="requirements-gathering">Requirements gathering</h3>

<p>When asking questions regarding a product spec for a large-scale system, focus on these 5 categories of questions:</p>

<ol>
  <li><strong>Users:</strong> Who are they? What do they do with the data?</li>
  <li><strong>Scale:</strong> How many requests/sec? Reads or writes? Where is the bottleneck? How many users are we supporting? How often/fast do users need data?</li>
  <li><strong>Performance:</strong> When do things need to be returned/confirmed? What are the tolerance and SLAs for constraints?</li>
  <li><strong>Cost:</strong> Are we optimizing for development cost (use OSS) or operational/maintenance cost (use cloud services)?</li>
  <li><strong>CAP theorem:</strong> Partitioning is something you know you’ll have to account for with highly-scalable systems. So it may be easier to ask what is more valuable: consistency or availability?</li>
</ol>

<p><strong>If consistency is most important</strong>, consider an <abbr title="Atomicity Consistency Isolation Durability">ACID</abbr> database like Postgres or even a NewSQL database like CockroachDB or Google Spanner. <strong>If availability is most important</strong>, consider a <abbr title="Basic Availability Soft-state Eventual consistency">BASE</abbr> database like an eventually consistent NoSQL solution such as CouchDB, Cassandra, or MongoDB.</p>

<p>Even better is to <a href="https://blog.nahurst.com/visual-guide-to-nosql-systems">use this diagram</a> to map your concerns onto a pyramid. Given that you can only ever expect 2 of the 3 parts of the CAP theorem to be satisfied it might actually be better to ask <em>which property is least important?</em> If it’s…</p>

<ul>
  <li><strong>Consistency</strong> - most NoSQL solutions will work like Cassandra, CouchDB, and Amazon Dynamo</li>
  <li><strong>Availability</strong> - some NoSQL solutions and some NewSQL solutions like Bigtable, HBase, MongoDB, Google Spanner, and Redis</li>
  <li><strong>Partition tolerance</strong> - any relational or graph solution like Postgres or Neo4j will work since these are notoriously difficult to partition compared to the other solutions</li>
</ul>

<p>Though likely everyone <a href="http://pl.atyp.us/wordpress/?p=2521">misunderstands the CAP theorem</a> so I would read this a few times and internalize the example.</p>

<h3 id="the-three-system-qualities-in-1-line">The three system qualities in 1 line</h3>

<p>This chapter can effectively be summarized in 3 sentences:</p>

<ol>
  <li><strong>Scalability</strong> determines if this system can <em>grow</em> with the growth of your product. The best technique for this is <em>partitioning</em>.</li>
  <li><strong>Reliability</strong> determines if this system produces <em>correct results</em> (nearly) each and every time. The best techniques for this are <em>replication</em> and <em>checkpointing</em>.</li>
  <li><strong>Maintainability</strong> determines if this system can <em>evolve</em> with your team and is easy to understand, write, and extend.</li>
</ol>

<h3 id="further-reading-and-study">Further reading and study</h3>

<p>As I said before, this is a pretty simple chapter. I also watched <a href="https://www.youtube.com/watch?v=bUHFg8CZFws">this systems design walkthrough</a>. This video extended these concepts and informed some of these notes. I like to accompany learnings with practice to seed new questions for our own <a href="https://indigoag.com/join-us#openings">interview process</a>.</p>

<p><a href="http://highscalability.com/youtube-architecture">This article</a> on YouTube’s architecture further reinforces the sample problem on the YouTube video (how meta). You can check your solution against the one that was really used by YouTube.</p>

<p>Finally, you can rifle through a bunch of <a href="https://www.youtube.com/playlist?list=PLMCXHnjXnTnvo6alSjVkgxV-VH6EPyvoX">these videos</a> fairly quickly as each touches on a small subset of system design techniques.</p>

<p>Check-in next week with a summary of Chapter 2 of the book: <em>Data Models &amp; Query Languages</em>!</p>

	  ]]></description>
	</item>


</channel>
</rss>
