<?xml version="1.0" encoding="UTF-8" ?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
   
      <title>https://adamconrad.dev</title>
   
   <link>https://adamconrad.dev</link>
   <description>The website and home of engineering leader Adam C. Conrad.</description>
   <language>en-us</language>
   <managingEditor> Adam C. Conrad</managingEditor>
   <atom:link href="rss" rel="self" type="application/rss+xml" />
   
	<item>
	  <title>How to practice frontend engineering</title>
	  <link>//blog/how-to-practice-frontend-engineering/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2020-06-21T11:38:34-04:00</pubDate>
	  <guid>//blog/how-to-practice-frontend-engineering/</guid>
	  <description><![CDATA[
	     <p>I’m a big fan of Will Larson’s blog so his recent post on <a href="https://lethain.com/how-to-practice-backend-engineering/">practicing backend engineering</a> inspired me to write an equivalent article for frontend given my previous experience in that space before moving into management.</p>

<p>I will say one thing that makes it easier to practice frontend engineering is that everything can happen within the browser. You don’t even need a site with real data to practice! So what does frontend practice look like?</p>

<ul>
  <li><strong>Writing HTML templates.</strong> You need some sort of document to express your design’s content.</li>
  <li><strong>Designing with CSS.</strong> The look, feel, color, and experience is most uniquely expressed via the design controls within your style sheets.</li>
  <li><strong>Making pages interactive with JavaScript.</strong> As clients have become more interactive there has been more and more reliance on frontend JavaScript frameworks to build these client-side application as quickly as possible.</li>
  <li><strong>Building infrastructure to support the above three.</strong> Things like linting, schema generation, UI library building, lossless image compression, and asset packaging are part of the piping required to make the previous three run smoothly.</li>
</ul>

<h2 id="rebuild-the-design-of-your-favorite-site">Rebuild the design of your favorite site</h2>

<p>The absolute easiest, no-brainer way to practice frontend is to just pick your favorite big site like Facebook, Twitter, or Stripe and re-implement the pages you see. See how close you can get to pixel-perfect renditions of your favorite sites. Nail the CSS, the JavaScript interactivity, and the semantic HTML. See what libraries and plugins they use. And the best part, <strong>you can just view source to check all of your work</strong>. Unlike backend engineering, the answers are right in front of you. If you’re ever stuck or wondering how they did something, all of the code is available right in your browser client.</p>

<h2 id="create-a-blog-or-write-your-own-static-site-generator">Create a blog or write your own static site generator</h2>

<p>So many code schools or pet projects have you building a Twitter clone or some hobby toy project you will never use again. If you build your own blog using your own static site generator, <strong>there’s you’re homepage</strong>! What a better way to practice frontend than to actually build something you will really use if for not other reason than to showcase as your homepage your frontend skills!</p>

<p>Make your design as fun and whacky as possible. Use fringe, bleeding-edge CSS tools. Write about your experience building the blog <em>on your blog</em>.</p>

<h2 id="whatever-project-you-choose-make-sure-you-test-yourself-on-these-things">Whatever project you choose, make sure you test yourself on these things</h2>

<p>It’s not enough to simply copy a design or write whatever you want. There are a few ways to separate your code from that of a professional.</p>

<ul>
  <li><strong>Have it pass a bunch of audit tools.</strong> <a href="https://developers.google.com/web/tools/lighthouse/#psi">Google Lighthouse</a>, <a href="http://yslow.org/">YSlow</a>, <a href="https://www.deque.com/axe/">aXe</a>, and <a href="https://reactjs.org/blog/2018/09/10/introducing-the-react-profiler.html">React Developer Tools</a> are all examples of ways to ensure your code is fast, accessible, and easy for crawlers to read.</li>
  <li><strong>Make it responsive for every size.</strong> Use the principles of <a href="https://www.uxpin.com/studio/blog/a-hands-on-guide-to-mobile-first-design/">mobile-first responsive design</a> to create a site that works on any screen size. You should be able to grow and shrink your screen on your desktop (or phone) and have the site look beautiful no matter what random width you have set.</li>
  <li><strong>Run your code against linters and formatters.</strong> Make liberal use of <a href="https://prettier.io/">Prettier</a> and <a href="https://eslint.org/">ESLint</a> to write well-formatted, easy-to-read code. Understand the rules and why you would use them.</li>
  <li><strong>Use a bundler and packaging tool.</strong> Most sites use something like <a href="https://gruntjs.com/">Grunt</a>, <a href="https://gulpjs.com/">Gulp</a>, or <a href="https://webpack.js.org/">Webpack</a> to efficiently package their assets for browsers to consume. Even if it might be overkill for your blog, it is still useful to get practice using it and optimizing your bulids for extreme performance.</li>
  <li><strong>Really understand every tab of Chrome/Firefox DevTools.</strong> What would a memory leak look like? How could you tell if there’s a performance problem on the client or the server? Can you spot <a href="https://developers.google.com/web/tools/chrome-devtools/rendering-tools/">runtime issues</a> in painting, rendering, composing, and scripting? DevTools can give you all of these answers if you know where to look.</li>
</ul>

<p>If you can nail these points, you will be thinking not just like a hobbyist, but a professional frontend developer.</p>

<h3 id="throw-it-all-away-and-do-more-with-less">Throw it all away and do more with less</h3>

<p>And when you managed to get everything passing perfectly with all of the practice above, start over with as little as possible. There’s a reason I love <a href="http://motherfuckingwebsite.com/">this website</a>. It does <em>exactly</em> what it says it will do and nothing else. Sometimes I think we’ve forgotten what the web was really intended for. It is first and foremost a document store and linking tool. We use the web to share information. The hyperlink is the glue that connects these documents to create a literal web of networked pages.</p>

<p>The humble <code class="highlighter-rouge">&lt;form&gt;</code> is the purest representation of interactivity between your browser and the rest of the internet. Between documents and forms, that’s really all we need. Everything else is just a pretty distraction designed to keep you clicking and engaged. If we remember the roots of what we are building we tend to forget the noise of the latest SPA framework or hip new bundler and instead just focus on information architecture and user experience. Don’t lose sight of that.</p>

<h2 id="additional-resources">Additional resources</h2>

<p>Beyond hobby projects, I tell most people they should just build a real SaaS app and scratch their own itch. Push something real to production that you can charge customers for. This will have you not only practicing frontend, but backend engineering as well. More than you bargained for but there is truly no better way to practice your craft.</p>

<p>To create front ends that scale, I’d recommend a few more resources that I’ve found incredibly valuable over the years that stand the test of time:</p>

<ul>
  <li><em><a href="https://www.amazon.com/Frontend-Architecture-Design-Systems-Sustainable/dp/1491926783">Front End Architecture for Design Systems</a></em> - This book is the reference I use to cover architecture for even the biggest production apps I work on. Though some of the tools and technologies are dated, the tenants of architecture remain indelible.</li>
  <li><a href="https://developer.mozilla.org/en-US/docs/Web">MDN Web Docs</a> - The gold standard for understanding everything web technologies. This is easily the most comprehensive guide and reference for all things HTML, CSS, and JavaScript. They have tutorials, reference material for every property and function in all three specs, as well as a host of great examples and polyfills. I probably reference this at least once a week.</li>
  <li><a href="https://developers.google.com/web/fundamentals">Google’s Web Fundamentals</a> - These may be fundamental but they are by no means easy or rudimentary. I still find plenty to learn even with more than a decade of experience. These documents always update to include the latest and greatest web technologies as well.</li>
  <li><a href="https://www.nngroup.com/articles/">Nielsen Norman Group</a> - There is no better team to learn user experience from. This UX firm has authored some of the best selling books on the topic are considered the foremost experts in their field.</li>
  <li><a href="https://refactoringui.com/">Refactoring UI</a> - This book, and their <a href="https://refactoringui.com/">free collection of tweets</a> has single-handedly made me a better designer more than any other resource. If you want to create beautifully-designed sites with the tools you already use, you <em>have</em> to check out this book and site.</li>
</ul>

	  ]]></description>
	</item>


</channel>
</rss>
