<?xml version="1.0" encoding="UTF-8" ?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
   
      <title>https://adamconrad.dev</title>
   
   <link>https://adamconrad.dev</link>
   <description>The website and home of engineering leader Adam C. Conrad.</description>
   <language>en-us</language>
   <managingEditor> Adam C. Conrad</managingEditor>
   <atom:link href="rss" rel="self" type="application/rss+xml" />
   
	<item>
	  <title>Becoming an engineering manager</title>
	  <link>//blog/becoming-an-engineering-manager/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2021-06-09T21:00:00-04:00</pubDate>
	  <guid>//blog/becoming-an-engineering-manager/</guid>
	  <description><![CDATA[
	     <p>The following is a README I give to all of my new managers. Think of it like a step-by-step guide to the broader <a href="/blog/a-90-day-plan-for-managers/">90-day plan</a> you would offer your manager when you start in a new role.</p>

<h2 id="welcome">Welcome!</h2>

<p>Your first few months as a manager here at Indigo are crucial to your success, and we’re here to support you!</p>

<p>This new manager issue is a launchpad, and it connects you with crucial information about being a manager at Indigo. It has been designed to ensure you start your journey as a manager with all the resources and training available.</p>

<p>Items on this issue are identified as either <strong>required</strong> (in bold) or <em>optional</em> (in italics). Optional items may not be applicable to your style or team dynamic. Experiment with them for a few days or weeks. If you discover a useful practice or resources outside of this list, please feel free to leave a comment or edit this file.</p>

<h3 id="your-first-week">Your First Week</h3>

<ul>
  <li><input type="checkbox" /> <strong>Schedule a handover meeting with the current team manager</strong>. Items covered should include recent feedback, projects, individual goals, and any shared documents or performance reviews most recently given.</li>
  <li><input type="checkbox" /> <strong>Schedule 1:1 meetings with team members (<a href="https://getlighthouse.com/blog/one-on-one-meetings-template-great-leaders/">Template</a>) (<a href="https://larahogan.me/blog/first-one-on-one-questions/">First 1-1 Questions</a>)</strong></li>
  <li><input type="checkbox" /> <strong>Update your team and organization wiki pages to reflect the new reporting structure</strong></li>
  <li><input type="checkbox" /> <strong>Request access to any team chat channels you should be in</strong></li>
  <li><input type="checkbox" /> <em>Create a README of how you work</em></li>
  <li><input type="checkbox" /> <em>Read <a href="https://adamconrad.dev/blog/technical-lead-management/">Transitioning to management as a Technical Lead Manager</a></em></li>
</ul>

<h3 id="your-first-month">Your First Month</h3>

<ul>
  <li><input type="checkbox" /> <strong>Focus on 1-1s: ask questions, build rapport, and discuss career development plans with each direct report (<a href="https://getlighthouse.com/blog/one-on-one-meeting-questions-great-managers-ask/">Questions Reference</a>)</strong></li>
  <li><input type="checkbox" /> <strong>Review your hiring process</strong></li>
  <li><input type="checkbox" /> <em>Define or review measurable goals for your management role with your manager in a 1-1</em></li>
  <li><input type="checkbox" /> <em>Modify this document to incorporate learnings you’ve had as a new engineering manager</em></li>
</ul>

<h4 id="recommended-reading-list">Recommended Reading List</h4>

<ul>
  <li><em>The Effective Manager</em> by Mark Horstman (and the accompanying <em>Manager Tools</em> podcast)</li>
  <li><em>Resilient Management</em> by Lara Hogan</li>
  <li><em>High Output Management</em> by Andy Grove</li>
  <li><em>The 27 Challenges Managers Face</em> by Bruce Tulgan (particularly for conflict resolution)</li>
  <li><em>Accelerate</em> by Nicole Forsgren, Jez Humble, and Gene Kim</li>
  <li><em>An Elegant Puzzle</em> by Will Larson</li>
  <li><em>Extreme Ownership</em> by Jocko Willink (and his Jocko Podcast)</li>
  <li><em>The Making of a Manager</em> by Julie Zhuo</li>
  <li><em>The Manager’s Path</em> by Camille Fournier</li>
  <li><em>The Talent Code</em> by Daniel Coyle</li>
</ul>

<h3 id="days-31-60">Days 31-60</h3>

<ul>
  <li><input type="checkbox" /> <em>Schedule a coffee chat with your recruiting team</em></li>
  <li><input type="checkbox" /> <em>Begin incorporating feedback into your 1-1s (<a href="https://www.manager-tools.com/2009/10/management-trinity-part-2">podcast on 1-1s &amp; feedback</a>)</em></li>
</ul>

<h3 id="days-61-90">Days 61-90</h3>

<ul>
  <li><input type="checkbox" /> <em>Begin incorporating coaching &amp; delegation into your 1-1s (<a href="https://www.manager-tools.com/2009/10/management-trinity-part-3">podcast on coaching &amp; delegation</a>)</em></li>
  <li><input type="checkbox" /> <em>Work with your manager on decision-making techniques to effectively delegate work and get more out of your direct reports (<a href="https://www.groupmap.com/map-templates/urgent-important-matrix/">Template</a>)</em></li>
</ul>

<h3 id="3-months-and-beyond">3 Months and Beyond</h3>

<ul>
  <li><input type="checkbox" /> <em>For Technical Lead or Interim Managers: Decide with your manager if you want to continue down technical leadership (e.g. Staff engineer, Principal engineer) or transition to professional leadership (e.g. Engineering Manager)</em></li>
  <li><input type="checkbox" /> <em>Walk through your first quarterly goal setting for your directs with your manager (<a href="https://docs.google.com/document/d/1p7-Jo45VAw-RTUK97p6r9G0y0RtbmRxIlRntrmcfwHM/edit#">Template</a>)</em></li>
  <li><input type="checkbox" /> <em>Learn about the performance review process at your company in your manager 1-1</em></li>
</ul>

	  ]]></description>
	</item>

	<item>
	  <title>Always have the Beginner Mindset</title>
	  <link>//blog/always-have-a-beginners-mindset/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2019-12-13T12:19:06-05:00</pubDate>
	  <guid>//blog/always-have-a-beginners-mindset/</guid>
	  <description><![CDATA[
	     <p>The Beginner’s Mindset is about approaching learning with open arms and the understanding that you don’t know everything. You intuitively have this approach when you actually are a beginner but if you maintain that approach as an expert it will serve you well.</p>

<p>I still keep this mindset with everything I do because staying humble and thirsty to learn keeps you ahead of your peers and you’ll learn faster. Here are a few things I keep in mind when learning with a beginner mindset.</p>

<h2 id="learn-how-to-learn">Learn how to learn</h2>

<p>It sounds like a ridiculous premise given that many folks spend much of their childhood in a classroom learning. Unfortunately, most schools teach but that doesn’t mean students learn. And like anything, learning is a skill that can be acquired (and is not taught in primary school).</p>

<p>The best way to learn this is to <a href="https://www.coursera.org/learn/learning-how-to-learn/">take this Coursera course</a> which is really both a class on how to learn and how to read effectively. And while you’re at it, you might as well learn how to <a href="https://www.youtube.com/watch?v=Unzc731iCUY">speak</a>, <a href="https://www.youtube.com/watch?v=vtIzMaLkCaM">write</a>, and how to <a href="https://www.coursera.org/learn/mindware?">think critically</a>.</p>

<h2 id="throw-out-your-assumptions">Throw out your assumptions</h2>

<p>If you’re starting to practice the beginner’s mindset a decade into your career, you’ll have a lot of knowledge already built up. This will only bias you into thinking like an expert, which will hinder your progress.</p>

<p>Instead, throw out all of your pre-existing assumptions and <strong>assume you might be wrong</strong>. This opens you up to new possibilities that you likely didn’t consider because of all of your inherit biases you’ve built up over the years. Now you can explore ideas and concepts with a whole new chain of connections that you’d previously cut off.</p>

<h2 id="stop-jumping-to-solutions">Stop jumping to solutions</h2>

<p>Another problem with thinking with your experience is when you see a new problem, you rely on your previous solutions to create a novel way to solve this one. Again, if you think like a beginner, you don’t have previous solutions to rely on. What you think is a perfect solution is no longer relevant so you should scrap it.</p>

<p>The beginner’s mindset means <strong>asking why all of the time</strong>. This is so effective there is an entire <a href="https://en.wikipedia.org/wiki/5_Whys">Wikipedia entry on this concept</a> that I would encourage you to investigate further.</p>

<h2 id="beginners-dream">Beginners dream</h2>

<p>Something happens as we gain experience: we also gain cynicism. We see what has succeeded and what has failed and that hurts our ability to think ideal states. We know the world is shades of gray but why should that hinder us from believing beyond our true potential?</p>

<p>My favorite example of this is Airbnb’s <a href="https://uxdesign.cc/applying-airbnbs-11-star-framework-to-the-candidate-experience-3f0b9c4e68a3?gi=88688bfe0579">11-star customer rating framework</a>. Everywhere you go you either see a thumbs-up/thumbs-down rating or a 5-star rating system. Airbnb wanted to know what happens if you break the glass ceiling.</p>

<p>What would a 6-star rating on Airbnb look like? I mean, what does more than 100% look like to you? That must be pretty great.</p>

<p>Well, then what does a 7-star rating look like? Can I even fathom that kind of an amazing experience?</p>

<p>Okay then tell me about a <em>10-star experience</em>. Uuh…what are we, the Four Seasons hotel in my tiny Boston apartment? Hey! That’s interesting; are there any elements of the famous Four Seasons hotel chain that we can replicate even in the smallest apartments we list on Airbnb?</p>

<p>Beginners don’t know where to stop because they’ve never reached the end before. That means they are free to dream the greatest of dreams without limitations. This unshackling of limitations is not only liberating, but idea-generating. What would an 11-star experience look like for <em>your</em> business?</p>

<h2 id="embrace-the-haters">Embrace the haters</h2>

<p>Another issue with cynicism is you know where you might fail. You anticipate criticism and faults so you are quick to deflect criticism with retorts rather than <em>embrace</em> it.</p>

<p>What if that criticism is advice? What if, what sounds like criticism, is actually a cry for help? And finally, what if the critics are right and you are wrong?</p>

<p>When you think like an expert, you assume you are right from the start. When you are a beginner, you assume you are <em>wrong</em> from the start. People who want to learn and grow will crave feedback and help if they think they don’t have all the answers.</p>

<p>Can you see how limiting it might be to think like an expert?</p>

<h2 id="teach-what-you-learn-to-internalize">Teach what you learn to internalize</h2>

<p>Finally, just because you learned something doesn’t mean you know it. How many TED talks have you listened to that went in one ear and out the other? If you took the above course on how to learn and you’re checking back in here, you should know a great way to internalize what you’ve learned is to revisit the material and ideally write about it. And what do you know, a platform like a blog is a perfect way to teach others and engage your mind with active learning techniques.</p>

<p>Trust me, nothing is more gut-wrenching than trying to write a blog post on a technical topic and posting it to the internet. The best defense against looking stupid is to really know the material. Writing is a great incentive to force yourself to learn what you are talking about so you aren’t embarrassed.</p>

<p>The beginners mindset has allowed me to stay humble, stay open to possibilities, and most importantly, stay hungry to learn and grow as I move into the next phase of my career. Hopefully it will do the same for you as well.</p>

	  ]]></description>
	</item>


</channel>
</rss>
