<?xml version="1.0" encoding="UTF-8" ?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
   
      <title>https://adamconrad.dev</title>
   
   <link>https://adamconrad.dev</link>
   <description>The website and home of engineering leader Adam C. Conrad.</description>
   <language>en-us</language>
   <managingEditor> Adam C. Conrad</managingEditor>
   <atom:link href="rss" rel="self" type="application/rss+xml" />
   
	<item>
	  <title>Working Remotely When You Didn't Plan To</title>
	  <link>//blog/working-remotely-when-you-didnt-plan-to/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2020-02-27T23:00:19-05:00</pubDate>
	  <guid>//blog/working-remotely-when-you-didnt-plan-to/</guid>
	  <description><![CDATA[
	     <p>If you’re reading this article at the time of its publishing, your professional world is probably upended with pleas (or demands) to work from home. I’ve embraced remote work culture for years and I believe it is the future of work.</p>

<p>Companies that embrace remote-first can increase the size of their candidate pool, reduce costs in lower cost of living areas, increase employee satisfaction by removing one of the worst things about a job (a commute), and hold a key advantage in flexibility that their competitors cannot leverage.</p>

<p>Because of this, my teams at <a href="https://indigoag.com">Indigo</a> are remote-first and the majority of my team members live outside of the Boston metro area where our headquarters resides. Not only are we ready and able for remote work, we also perform at a very high level within our organization.</p>

<p>How are we able to do this? I’ll share <strong>three key strategies that are essential for remote work to succeed in today’s workplace</strong>.</p>

<h2 id="over-communicate">Over communicate</h2>

<p>This may sound obvious to certain leaders but this cannot be understated: <strong>digital communication requires overcommunication</strong>. There are a few reasons you need to overcommunicate when moving to a remote organization:</p>

<ol>
  <li>Words can be misinterpreted. One’s voice does not only communicate words, but tones, inflections, and emotion. This is all removed when your default mode is text rather than speech. Reinforcing this with multiple takes (and multiple mediums) for the same message will ensure no one gets the wrong message.</li>
  <li>Text can feel restricting. As a rule of thumb, if you don’t feel like your message is getting across with 3 attempts, <strong>get on a video call or phone call with the other people.</strong> Just because you are remote doesn’t mean you can’t fall back on video or audio to emulate face-to-face communication.</li>
  <li>Not everyone will see your message when you send it. With remote comes an increased risk for asynchronous messaging and reduced priority that your message will be heard. The more times you say the same message, the higher the likelihood that everyone will get what you’re saying when you say it.</li>
</ol>

<p>So <strong>say the same thing over, and over, and over, and over, and over again until you are 100% confident that everyone gets it</strong>. They may not have seen it the first time, or interpreted the way you did, or could use the help of a personal chat with a tool like Zoom or Slack.</p>

<h2 id="document-everything">Document everything</h2>

<p>As I mentioned in the previous section, your medium has shifted from real-time, synchronous communication, to asynchronous communication. This is both a blessing and a curse.</p>

<p>It is a blessing because you will are now in control of your communication channels. In the office, if someone comes up to your desk and interrupts you, you have no agency as to whether or not this message is worth your time. Some people want to come by and chat. Others need your undivided attention. Now if eight people bug you on Slack, <em>you</em> get to decide when and how to message them back. This is very freeing and allows you to take better control of your time.</p>

<p>It is also a curse because you now need to be on top of the myriad communication channels that exist. Some people love email, others love Slack, and still, others will use text message or a phone call to reach you. Guess what? You’re now responsible to check all of them which will only continue to fraction your time with distractions.</p>

<p>To combat these problems, you need to <strong>document any notes, action items, or checklists that could be useful to more than one person</strong>. When you shift your mindset towards asynchronous communication, you are using documentation as your key lever. <strong>Documentation is your leverage so you don’t have to repeat yourself</strong>. When you don’t have to repeat yourself, your time doesn’t have to be fractured answering so many questions on Slack or email.</p>

<p>How to get started with documentation is easy: write stuff down! Hopefully, in a centralized place. Unfortunately, in some organizations, there are many different tools used at once. For example, we use Microsoft OneDrive for Office <em>and</em> Google Drive <em>and</em> Confluence <em>and</em> an internal Facebook wiki.</p>

<p>Yikes.</p>

<p>If possible, try to converge on one shared wiki tool and stick with it. For engineering, we use Confluence within the Atlassian ecosystem. So you know that if I wrote an article, it’s on Confluence. <strong>Do your best to write your content on one platform and make it known to help your coworkers reinforce the need for one central system.</strong></p>

<h2 id="trust--transparency">Trust &amp; transparency</h2>

<p>The bedrock that supports all of the above is trust. Another one that seems obvious to many leaders but creeps insidiously on newly-remote organizations. Why? <strong>When you go remote, you lose control over your team’s time.</strong></p>

<p>This may be a hard one for you to hear but if you’re used to showing up in the office you’re likely preconditioned to assume that “butts in seats” equates to productivity. How will I know if my team is doing work and not slacking off? Are they putting in a full effort if they have a TV and a bed close to their desk?</p>

<p>I hate to break it to you, but you need to <code class="highlighter-rouge">l e t   g o</code>. That’s right, we’ve got a control complex. And I get it; the American employment system since the Ford days has conditioned us to believe that productivity is correlated with in-office work.</p>

<p>Let me offer you a quick analogy as to how this changed my whole approach to trust with my organization:</p>

<h3 id="a-quick-aside-on-trust-over-a-lifetime">A quick aside on trust over a lifetime</h3>

<p>When we were children, the world gave us structure and guidance because we were not capable of making sound decisions for ourselves. School starts at a specific time, you must eat the lunch given to you, and your whole world is regimented to the whims of the adults you’re surrounded by.</p>

<p>Once you got to college, that whole paradigm changed. Professors did not care if you did the homework, showed up to class, ate lunch, or really did anything. If you don’t do the work, you simply fail. In that sense, all you have to blame is yourself.</p>

<p>And then something curious happened.</p>

<p>You got a job, congratulations! You’ve now regressed to being treated like a child again: you must show up to work at a specific time, you have a narrow window for lunch, and your boss gives you dirty looks if you leave at 4:58 PM to pick up your kids.</p>

<p>Does it have to be this way?</p>

<h3 id="trust-means-assuming-positive-intent">Trust means assuming positive intent</h3>

<p>The truth is that we can reverse this trend of no trust. <strong>If you assume positive intent, your people will being to trust you and be more compelled to deliver great work.</strong> When you <em>decide</em> that your people are great (because you only hire great people) from the beginning and don’t need to be watched over by you to do their best, you are assuming positive intent.</p>

<p>This new world of remote work may mean they take a longer lunch break. Or a break to feed the baby. Or go to the gym. So what? At the end of the day, if they do the work, who cares if they’re online at 3 pm or 3 am?</p>

<p>It’s certainly helpful to your teammates to know others are online to assist them. It is preferred everyone is online at the same time, but it is not required for success.</p>

<p><strong>With trust comes accountability.</strong> Now that you are assuming positive intent and trust in your teammates, they need to return the favor by holding themselves accountable. When the boss isn’t looking, how do <em>I</em> prove to the world that I’m delivering on my work? How can <em>I</em> demonstrate that I’m worthy of a promotion or raise?</p>

<p>If you’ve got anxieties like this moving to remote work, the focus has shifted from time to results. <em>And that’s a good thing</em>. When we all trust each other to get things done, our value is solely reflected on our output and not on inconsequential metrics like attendance or time on the clock. It’s liberating; let remote work liberate you!</p>

<h2 id="weve-got-a-tough-road-ahead-but-we-can-get-through-it-together">We’ve got a tough road ahead, but we can get through it together</h2>

<p>If you keep these three tactics in mind you’ll be shocked at how much your teams will begin to embrace remote work culture. <strong>Be sure to balance all of this work communication with social communication</strong>. That means creating fun Slack channels like <code class="highlighter-rouge">#music</code>, <code class="highlighter-rouge">#video-games</code>, or <code class="highlighter-rouge">#cute-animals</code>.</p>

<p>With everyone holed away in their home offices things are going to be quite isolating. As humans, we crave social interaction. Recognize this and don’t make it all about business. Lighten up and do your best to support your teammates. We’re all going to make it and having a sense of humor will help us all get through this together.</p>

<p>Best of luck!</p>

	  ]]></description>
	</item>


</channel>
</rss>
