<?xml version="1.0" encoding="UTF-8" ?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
   
      <title>https://adamconrad.dev</title>
   
   <link>https://adamconrad.dev</link>
   <description>The website and home of engineering leader Adam C. Conrad.</description>
   <language>en-us</language>
   <managingEditor> Adam C. Conrad</managingEditor>
   <atom:link href="rss" rel="self" type="application/rss+xml" />
   
	<item>
	  <title>How to Load Images to Cut Page Time</title>
	  <link>//blog/how-to-load-images-to-cut-page-time/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2018-05-24T18:25:00-04:00</pubDate>
	  <guid>//blog/how-to-load-images-to-cut-page-time/</guid>
	  <description><![CDATA[
	     <p>There are lots of ways to improve your site’s performance. Serving your static assets from a CDN, using GZip or Brotli compression, and concatenating your CSS/JS files should be on the top of your list. One thing that may not be on your radar is lazy loading your images.</p>

<h2 id="what-is-lazy-loading">What is lazy loading?</h2>

<p>Lazy loading images involves programmatically inserting images into the DOM only when the image becomes visible within the viewport. It is “lazy” because we choose not to download the image data until it is viewed. This can be extremely beneficial for pages like blogs or landing screens, where only a fraction of your audience will view the entire content.</p>

<p><strong>How beneficial is lazy loading?</strong> We created a before and after page to illustrate how important it can be to lazy load images. Our example is a sample blog post. Blogs are a great example because they often have lots of images, and most of them are below the fold (outside of the viewport).</p>

<p><a href="https://adamconrad.dev/test-lazyload-slow/">Example blog post (before lazy loading)</a></p>

<p><a href="https://adamconrad.dev/test-lazyload-fast/">Example blog post (after lazy loading)</a></p>

<p>Did you notice the difference? If you’re not convinced, just take a look how much a difference our lazy loading script impacted our site’s performance:</p>

<p><img src="https://cdn-images-1.medium.com/max/3998/1*Nt8TzUj5bFOf1m70-5-zgQ.png" alt="" /></p>

<p>With just one tiny script, we went from an F to an A on MachMetric’s performance grade. We reduced our page load time by <strong>over 90%</strong>. Our page size reduced by <strong>over 95%</strong>!</p>

<p><img src="https://cdn-images-1.medium.com/max/2000/1*sZHMARGPNP_DQnsrXy8SDg.png" alt="" /></p>

<p>Ready to start lazy loading images now? Here’s the code, and it’s super simple!</p>

<h2 id="lazy-loading--the-code">Lazy loading — the code</h2>

<p>Lazy loading is super easy to accomplish with JavaScript. Here’s <a href="https://raw.githubusercontent.com/deanhume/lazy-observer-load/master/lazy-load.js">all the code</a> you need to start lazy loading today:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>var images = document.querySelectorAll('.js-lazy-image');
var config = {
  rootMargin: '50px 0px',
  threshold: 0.01
};
var imageCount = images.length;
var observer;

var fetchImage = function(url) {
  return new Promise(function(resolve, reject) {
    var image = new Image();
    image.src = url;
    image.onload = resolve;
    image.onerror = reject;
  });
}

var preloadImage = function(image) {
  var src = image.dataset.src;
  if (!src) {
    return;
  }
  return fetchImage(src).then(function(){
    applyImage(image, src);
  });
}

var loadImagesImmediately = function(images) {
  for (image in images) {
    preloadImage(image);
  }
}

var onIntersection = function(entries) {
  if (imageCount === 0) {
    observer.disconnect();
  }
  for (entry in entries) {
    if (entry.intersectionRatio &gt; 0) {
      imageCount--;
      observer.unobserve(entry.target);
      preloadImage(entry.target);
    }
  }
}

var applyImage = function(img, src) {
  img.classList.add('js-lazy-image--handled');
  img.src = src;
}

if (!('IntersectionObserver' in window)) {
  loadImagesImmediately(images);
} else {
  observer = new IntersectionObserver(onIntersection, config);
  for (image in images) {
    var image = images[i];
    if (image.classList.contains('js-lazy-image--handled')) {
      continue;
    }
    observer.observe(image);
  }
}
</code></pre></div></div>

<h2 id="how-it-works">How it works</h2>

<p>This script works by looking for images that have the class <strong>js-lazy-image</strong>. Those images must have a data attribute called data-src which has the location of the image, just like the normal src attribute would be filled out. The script then counts the number of images with this class, and when the viewport intersects with an observable area that includes the image, the source of the image is loaded from it’s data attributes, and the image renders on the screen! Here’s an example of how you would call lazy loaded images:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>&lt;img alt="Lazy" class="js-lazy-image" data-src="/location/of/my/image.jpg" /&gt;
</code></pre></div></div>

<p>As you can see, this works almost exactly like a regular image, but you just need to make sure to add on the special JavaScript class and add a <strong>data-</strong> prefix in front of your src attribute. And that’s all it takes to go from this:</p>

<p><img src="https://cdn-images-1.medium.com/max/3998/1*ZIW_GhTLsCzE23pXO_7ulg.png" alt="" /></p>

<p>To this:</p>

<p><img src="https://cdn-images-1.medium.com/max/3998/1*jVANxQdyxpl8UQ3gmmLr8Q.png" alt="" /></p>

<h2 id="how-the-script-works--section-by-section">How the script works — section by section</h2>

<p>Still curious as to how the script works? Let’s break it down by section:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>var images = document.querySelectorAll('.js-lazy-image');
var config = {
  rootMargin: '50px 0px',
  threshold: 0.01
};
var imageCount = images.length;
var observer;
</code></pre></div></div>

<p>Here we initialize our variables. Remember that class we added to our images? This is where we collect our images. The configuration will be used later with our observer class (don’t worry if you don’t know what that is, we’ll get to that in a second). Finally, we store the image count because we’ll be using that a few times throughout our script.</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>var fetchImage = function(url) {
  return new Promise(function(resolve, reject) {
    var image = new Image();
    image.src = url;
    image.onload = resolve;
    image.onerror = reject;
  });
}
</code></pre></div></div>

<p>Our first function deals with grabbing our images. We make use of <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises">JavaScript Promises</a> to asynchronously load our images. If the Promise was resolved successfully, we load the image with the URL for our image source, otherwise we display an error. So where is this called from? Glad you asked…</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>var preloadImage = function(image) {
  var src = image.dataset.src;
  if (!src) {
    return;
  }
  return fetchImage(src).then(function(){
    applyImage(image, src);
  });
}
</code></pre></div></div>

<p>This image preload function grabs the image source from the data attribute we tacked onto our image tag. If it doesn’t find a source object, no problem, we just stop right there. Otherwise, we fetch our image, and if things go great, we apply the image to the DOM like so:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>var applyImage = function(img, src) {
  img.classList.add('js-lazy-image--handled');
  img.src = src;
}
</code></pre></div></div>

<p>Our function simply finds the image we’re about to reveal, adds on a class to let our script know it has been handled, and then adds the URL to the image’s src attribute.</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>if (!('IntersectionObserver' in window)) {
  loadImagesImmediately(images);
} else {
  observer = new IntersectionObserver(onIntersection, config);
  for (image in images) {
    var image = images[i];
    if (image.classList.contains('js-lazy-image--handled')) {
      continue;
    }
    observer.observe(image);
  }
}
</code></pre></div></div>

<p>This is the meat-and-potatoes of our script. The core functionality that enables our lazy loading to happen quickly and efficiently revolves around the <a href="https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API">Intersection Observer API</a>. This API allows us to track changes to the viewport against target elements asynchronously.</p>

<p>Traditionally you would have to do something like this with the <a href="https://developer.mozilla.org/en-US/docs/Web/Events/scroll">Scroll Event</a> which is both slow and called constantly. To combat this performance hiccup, you might using <a href="https://css-tricks.com/debouncing-throttling-explained-examples/">debouncing or throttling</a> to limit the number of scroll event requests. But with Intersection Observers, this is all handled for you.</p>

<p>Remember the config variable at the top of our script? This is the configuration for our Intersection Observer. It tells us that when we are within a margin of 50px (vertically) from our image, that is when we want to activate our observation callback. The threshold, as you might guess, is the tolerance for what percentage of the object must be observed in order for the callback to be invoked once the margin is reached. In our case, we chose 1%, which is immediately upon bringing the image tag into view.</p>

<p>So now that we have that background, we can see how this if statement works. If we see that Intersection Observers are a part of the window object, we know we are in a browser that supports this functionality. As of right now, <a href="https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API#Browser_compatibility">Intersection Observers are available on all major browser except IE and Safari</a>. So if you are on IE or Safari, you will load the images immediately. If you aren’t, we create a new Intersection Observer object with the configuration we provided in the beginning, as well as a callback function to trigger when our target observation is reached.</p>

<p>Finally, we have to tell the Observer exactly what it has to observe for the callback to be initialized. In this case, we are observing all of the images that haven’t already been handled, which are the images that haven’t been applied to the DOM yet (via the applyImages function we saw earlier).</p>

<p>So what does loading images and the observation callback look like?</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>var loadImagesImmediately = function(images) {
  for (image in images) {
    preloadImage(image);
  }
}
</code></pre></div></div>

<p>For loading images immediately, it’s pretty straightforward. We simply preload all of the images and put them on the screen like we normally would.</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>var onIntersection = function(entries) {
  if (imageCount === 0) {
    observer.disconnect();
  }
  for (entry in entries) {
    if (entry.intersectionRatio &gt; 0) {
      imageCount--;
      observer.unobserve(entry.target);
      preloadImage(entry.target);
    }
  }
}
</code></pre></div></div>

<p>Our intersection callback is a bit more involved. If we have loaded all of our images that have our lazy loading CSS class, we’re done and we can disconnect from our Observer object. Otherwise, for every observer entry in our IntersectionObserver, we want to activate our images. We do that by ensuring we have reached our threshold. <strong>intersectionRatio</strong> is the property we need to see if our target image element is visible within the threshold we defined in our configuration. If it is, the property returns a 1, otherwise it returns a 0. So if we have landed within our necessary ratio, we have one more image to add to the DOM, which means we can remove 1 from our count of images remaining to load. We can therefore stop observing this image because it’s going to be loaded onto the page. Finally, we use our previously-defined preloadImage to execute our now familiar process of adding the image URL onto the image tag and loading the image into the DOM.</p>

<h2 id="next-steps">Next steps</h2>

<p><strong>Lazy loading images is a quick and painless way to drastically improve the performance of your pages that use a lot of imagery.</strong> From there, be sure to <a href="https://adamconrad.dev/the-fastest-way-to-increase-your-sites-performance-now/">compress your images</a> and use the correct image format so you keep a small footprint. Tools like <a href="https://www.machmetrics.com/">MachMetrics</a> are a great way to track your performance improvements over time, as well as provide additional suggestions on how to continuously improve the performance of your application.</p>

<p>What other quick wins do you have for speeding up your site? Leave a reply in the comments below!</p>

<p><em>Originally published at <a href="https://www.machmetrics.com/speed-blog/how-to-lazy-loading-images-script-slash-page-load-time/">www.machmetrics.com</a> on May 24, 2018.</em></p>

	  ]]></description>
	</item>

	<item>
	  <title>The Fastest Way to Increase Your Site's Performance Now</title>
	  <link>//blog/the-fastest-way-to-increase-your-sites-performance-now/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2018-04-25T04:25:00-04:00</pubDate>
	  <guid>//blog/the-fastest-way-to-increase-your-sites-performance-now/</guid>
	  <description><![CDATA[
	     <p>The web is getting bigger. It is also getting slower. The funny thing is, every single day I’m visiting new sites to try out a new product or read a blog, and I find the same performance issues over and over again. All I have to do is check out DevTools to see that most images are way too large.</p>

<p>What baffles me is how easy it is to fix this. <em>The</em> fastest way to increase the performance of any site is to <strong>losslessly compress images</strong> that are being displayed. In fact, it’s so easy, anyone on your team, regardless of coding experience, can make a dent in improving performance.</p>

<h2 id="compressing-images-should-be-automatic">Compressing images should be automatic</h2>

<p>The <a href="https://www.machmetrics.com/speed-blog/average-page-load-times-websites-2018/">average page size</a> is 1.88 MB, with the majority of that weight coming from images. So if the bulk of a site is images, we should be focusing on image optimization before we focus on things like database indexing, caching, or other more advanced forms of performance optimization.</p>

<p><a href="https://royal.pingdom.com/2018/03/07/web-performance-top-100-e-commerce-sites-in-2018/"><img src="/assets/images/top-100-performance.png" alt="Performance of top 100 e-commerce sites via Pingdom" /></a></p>

<h2 id="the-4-levels-of-image-optimization">The 4 levels of image optimization</h2>

<p>There are four major levels of effort required to reduce the size of image data. Each level builds off of the previous one, so be sure to complete level 1 before moving onto level 2.</p>

<p>The first level requires the least amount of work (which can be accomplished in minutes). The fourth level requires all of the previous steps as well as its own set of tasks. This can take hours, if not days, depending on how image-heavy your content is.</p>

<p>Each level provides diminishing returns. Simply completing one level will do more for your performance than the next level. Which is why it’s important not to skip any steps.</p>

<h3 id="level-1-lossless-compression">Level 1: Lossless compression</h3>

<p>The first thing you should do is grab a copy of <a href="https://imageoptim.com/">ImageOptim</a>. This tiny application is your new best friend. ImageOptim will scan every image within a folder you select, and automatically compress the image, removing all of the useless data.</p>

<p>In the context of this blog, useless data refers to anything that doesn’t contribute to the visual quality of the image being displayed. In fact, the term lossless refers to <a href="https://en.wikipedia.org/wiki/Lossless_compression">the ability to reconstruct the original data from the compressed data</a> without losing any information needed to render that data.</p>

<p>For example, did you know that a photo can store the name of the author of that picture? That comment might be useful in protecting the copyright of the photographer, but it has zero effect on what your application will look like to the world. That metadata is <strong>lossless</strong>, and if you were to run lossless compression on that JPG photograph, it would strip out this and <a href="https://www.photometadata.org/META-Resources-Field-Guide-to-Metadata">dozens</a> of other metadata fields.</p>

<p>What you may not realize is that this kind of metadata makes up a very large part of the image’s total size. This is exactly why lossless compression is so important. <strong>Removing lossless data does not affect the visual quality of your images, but it can drastically reduce their size</strong>. And the smaller the image, the less your browser has to download to retrieve from the server where the image is hosted.</p>

<p>That is why running ImageOptim should be a no-brainer. It only takes a few minutes to run and provides potentially big savings in size without a reduction in quality.</p>

<h3 id="level-2-targeted-lossy-compression">Level 2: Targeted lossy compression</h3>

<p>The next level requires a little more work, but even more savings. At this point, you’ll have to make specific fixes for each image type. We’ll break this down for the 3 major image types (and if you aren’t using one of these three, make sure you check out the next step):</p>

<p><img src="/assets/images/image-optim-metadata.png" alt="Toggle metadata in ImageOptim preferences" /></p>

<h4 id="jpg">JPG</h4>

<p>If you’ve already run a JPG through ImageOptim, you’ll have to download another piece of software. There are additional options for JPGs in ImageOptim preferences, such as removing metadata, but I’ve found that removing color profile metadata actually colorizes images to include a bit more greens and yellows.</p>

<p>So which should you choose? <a href="http://www.jpegmini.com/app">JpegMini</a> handles both lossless and lossy compression and allows you to resize your images.</p>

<p>With regards to image sizing, <strong>use the correct image size</strong>. If you have an image that is 512x512 pixels, but the image is only being used in a 64x64 canvas, scale down that image.  Worried about Retina displays? Check out level 4 for more details.</p>

<p><strong>Another tip: Reduce image quality for JPGs down to 80% by default</strong>. JPG is a lossy format in general, but a very <a href="http://regex.info/blog/lightroom-goodies/jpeg-quality">in-depth analysis of JPG compression quality</a> shows that there is effectively zero difference in visual quality, but <em>significant savings in file size</em>.</p>

<h4 id="png">PNG</h4>

<p>Before you need to download anything, you should strip PNG metadata in ImageOptim. This is safe to do for PNGs and I’ve yet to see a PNG image that looks off by removing metadata.</p>

<p><a href="http://optipng.sourceforge.net/">OptiPNG</a> is the next level PNG optimizer after ImageOptim. It has far more algorithms for compressing PNGs and tries far more options to get a truly optimal PNG file size.</p>

<p><img src="/assets/images/color-table.png" alt="PNG color table" /></p>

<p>In addition to compression, PNGs are far more sensitive to <strong>using the correct color profiles</strong>. If you have a black-and-white PNG, using a colorless profile will save a ton of space. From there, <strong>choose the correct color palette size</strong> to restrict the number of colors to an acceptable range. Applications like Photoshop will show you a preview when saving your image, so feel free to play with color palette sizes until you see something that reduces the number of colors without reducing the image quality.</p>

<h4 id="gif">GIF</h4>

<p>Again, ImageOptim will do a solid job optimizing all image types, including GIFs. However, for a more customized compression technique, take a look at <a href="https://nikkhokkho.sourceforge.io/static.php?page=FileOptimizer">FileOptimizer</a> with the additional plugins for <a href="http://www.lcdf.org/gifsicle/">GifSicle</a> and <a href="https://kornel.ski/lossygif">GifSicle Lossy</a>. These combined seem to do a good job of maintaining image quality while drastically reducing the size of the file.</p>

<h3 id="level-3-convert-to-the-correct-image-type">Level 3: Convert to the correct image type</h3>

<p><img src="/assets/images/jpegxr-logo.svg" alt="JPEG XR logo" width="100" />
<img src="/assets/images/jpeg2000-logo.svg" alt="JPEG 2000 logo" width="100" />
<img src="/assets/images/webplogo.png" alt="WebP logo" width="100" /></p>

<p>At this point, you’ve already exhausted even the most advanced compression optimization techniques. And while these programs do all of the work for you in a matter of seconds, the truth is you may need to scrap your images altogether if they are in the wrong image format.</p>

<p>How do you know if they’re in the right format? Follow this simple questionnaire and then save your image into the new file format (and then repeat levels 1 and 2 to compress your new images!).</p>

<h4 id="does-it-look-like-a-photograph-use-a-jpg">Does it look like a photograph? Use a JPG</h4>

<p>JPGs were meant to be used with photographs. If you have avatars of your team, photographs of your office, or other real-life images, make sure they are in the JPG format.</p>

<h4 id="does-it-look-like-a-computer-generated-graphic-or-drawing-use-a-png">Does it look like a computer-generated graphic or drawing? Use a PNG</h4>

<p>Everything else can be a PNG. Other formats may provide better quality, but if you’re serving a web or image application, a PNG will do and is universally read by every kind of application. The one exception to this would be icons…</p>

<h4 id="does-it-look-like-an-icon-use-an-svg">Does it look like an icon? Use an SVG</h4>

<p>Icons are also computer-generated, but the difference is that icons are generally used alongside typography. If you think the image will be used in a menu, on a button, or is meant to symbolize something, it’s probably an icon, and icons will benefit from being SVGs because they can be scaled along with your type and lose 0 fidelity. They will look just as crisp as your fonts and will be much smaller as SVGs as well.</p>

<h4 id="are-you-supporting-the-latest-browsers-and-dont-care-about-firefox-use-webp-jpeg-2000-and-jpeg-xr">Are you supporting the latest browsers and don’t care about Firefox? Use WebP, JPEG 2000, and JPEG XR</h4>

<p>Finally, there is a push for next-generation image formats. JPG and PNG have been around for more than two decades, and it’s about time we have some new formats that innovate to maintain image quality without bloating our applications. Even better, they don’t require you decide between image type. For example, WebP works great for both photographs and computer-generated images.</p>

<p>The downside is that support is fragmented across devices and browsers. <a href="https://developers.google.com/speed/webp/">WebP</a> was made by Google, so it’s naturally designed only for Chrome and Chrome mobile. JPG also has evolved formats, but <a href="https://jpeg.org/jpeg2000/index.html">JPEG2000</a> is only supported by Apple (Safari and Safari Mobile), while <a href="https://jpeg.org/jpegxr/index.html">JPEG XR</a> is only supported by Microsoft (IE and IE Edge).</p>

<p>What about Firefox? There is no next-gen format for this browser, but they do have a <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1294490">2-year-old bug ticket</a> to implement WebP and it is assigned, but who knows when this will land.</p>

<h3 id="level-4-source-sets--fallbacks">Level 4: Source sets &amp; fallbacks</h3>

<p>If you’ve chosen the correct image and you’ve compressed the heck out of it, you’ll want to make sure it’s being served on the right device in the right aspect ratio. I alluded to this back in Level 2, but if you have concerns about Retina displays like MacBook Pros, or even x3 quality for the newest iPhones, you’ll want to ake multiple copies of your image in all of these formats.</p>

<p>Going back to the previous example, if you serve avatars in a 64x64 JPG format, you’ll also want to make copies of dimension 128x128 for Retina <em>and</em> 192x192 for Retina x3. The best way to do this is to <strong>start with a larger image and scale down, rather than scale up</strong>. You know those crime dramas where they ask the hacker to “<em>Zoom! Enhance!</em>”? We all know that doesn’t work in real life, and that same thing holds true for your images - you can’t add clarity where there was none in the first place.</p>

<p>Instead, start with the original source image (say, 512x512) and scale down to 192, save a copy, then 128, save another copy, then 64, and save that last copy. This will result in a less blurry, albeit still lossy (because you are removing information in the form of pixel fidelity) set of images.</p>

<p>With all of these duplicate, scaled images, how do we tie this all together? The image attribute known as <code class="highlighter-rouge">srcset</code> comes to the rescue:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>&lt;img src="avatar.jpg" srcset="avatar-sm.jpg 1x, avatar-md.jpg 2x, avatar-lg.jpg 3x" alt="Your responsive photo avatar"&gt;
</code></pre></div></div>

<p><code class="highlighter-rouge">Srcset</code> is pretty amazing. It will always default to the original <code class="highlighter-rouge">1x</code> magnifier like a normal image tag would. However, if it does find the other images, it will apply them to the given aspect ratios for your device. In other words, if you are viewing the photo on a 2015 MacBook Pro, the browser will select <code class="highlighter-rouge">avatar-md.jpg</code>, but if you are on an iPhone X, it will select <code class="highlighter-rouge">avatar-lg.jpg</code>. And if you’re in a military bunker using IE8, it will fall back to <code class="highlighter-rouge">avatar-sm.jpg</code>.</p>

<p><code class="highlighter-rouge">Sizes</code> is another property for responsive images but it relies on the width of the device rather than the pixel density. The format is the same:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>&lt;img src="bg-slim.png"  srcset="bg-slim.png 320w, bg-wide.png 900w" sizes="(max-width: 600px) 320px, 50vw" alt="Your responsive background"&gt;
</code></pre></div></div>

<p>You specify an image, space, the size the image should be displayed, with a <code class="highlighter-rouge">w</code> descriptor for the <code class="highlighter-rouge">srcset</code>, and then, using the <code class="highlighter-rouge">sizes</code> attribute, specify the media queries at which point the various sources should be used.</p>

<p>The only downside? <strong><code class="highlighter-rouge">Srcset</code> is not supported in IE</strong>. It <em>is</em> supported in IE Edge, and the <code class="highlighter-rouge">sizes</code> attribute is supported everywhere. In my honest opinion, this isn’t something to worry about because all of the Retina devices using IE are already new enough to support IE Edge. Anything that still requires IE11 and down likely doesn’t have a Retina display anyway (unless it is connected to an hi-density external monitor) so you likely won’t run into this problem being a real blocker for you.</p>

<h2 id="something-is-better-than-nothing">Something is better than nothing</h2>

<p>This is not an exhaustive list of image optimization techniques nor is it meant to be a prescriptive formula. <strong>Even if you only run ImageOptim on all of your images in your application, you could be saving upwards of 60-80% of what your users have to download.</strong></p>

<p>Images, audio, and video, <em>not</em> your source code, will comprise the largest space for your application by far. Effectively choosing, compressing, and displaying your images will have a marked impact on both application size and performance for your end user experience. The best part is it only takes a few downloads and a few more seconds to run your images through a very easy set of tools that will provide an instant improvement without sacrificing quality.</p>

	  ]]></description>
	</item>


</channel>
</rss>
