<?xml version="1.0" encoding="UTF-8" ?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
   
      <title>https://adamconrad.dev</title>
   
   <link>https://adamconrad.dev</link>
   <description>The website and home of engineering leader Adam C. Conrad.</description>
   <language>en-us</language>
   <managingEditor> Adam C. Conrad</managingEditor>
   <atom:link href="rss" rel="self" type="application/rss+xml" />
   
	<item>
	  <title>Selling ideas</title>
	  <link>//blog/selling-ideas/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2021-04-28T11:04:00-04:00</pubDate>
	  <guid>//blog/selling-ideas/</guid>
	  <description><![CDATA[
	     <p>I did an AMA on Clubhouse a little while ago and there were a lot of awesome, hard-hitting questions on the panel. So for the next several weeks, I’m going to write out my thoughts to each of the questions and how I answered them.</p>

<blockquote>
  <p>When you introduce a new project or idea to a team of developers, how do you sell that to the team to get them excited about it?</p>
</blockquote>

<p>Believe it or not I used to work in sales. For a little while I ran my own <a href="https://anonconsulting.com">consulting firm</a> full-time (I still accept clients but for small, one-off consulting reports and advice). Running your own services business is arguably more sales than is it actual service development.</p>

<p>So when folks ask me about sales I point them to the classic book <a href="https://www.amazon.com/SPIN-Selling-Neil-Rackham/dp/0070511136">SPIN Selling</a>. It’s for sales people by sales people.</p>

<p>But Adam, I’m an engineering manager. Why would I need to read a book about traditional sales to convince people to buy things?</p>

<p><strong>Because everything is sales</strong>.</p>

<p>You sell yourself when you interview for a job or go on a date. You sell your products or <a href="https://anonconsulting.com">services</a> if you’ve ever been an entrepreneur of a business. And most commonly, you sell <em>ideas</em> to your teammates and stakeholders to convince and influence people to believe in the things <a href="/blog/dont-call-it-tech-debt">you believe in</a>.</p>

<p>So who better to learn sales from than actual sales people? If you ever have the chance to shadow sales people at your current company, <strong>do it</strong>. You will not only build rapport with folks in other departments that you normally wouldn’t, you’ll also learn a ton about how to influence and persuade people.</p>

<p><strong>Selling ideas is all about persuasion and influence.</strong> There are books for those as well but I’ve got a <a href="/blog/teach-yourself-engineering-management/">whole curriculum for that</a>. To get you started, you really only need to remember what the SPIN acronym actually stands for:</p>

<ul>
  <li><strong>Situation</strong>: where are you today? What’s the current “state of the union”?</li>
  <li><strong>Problem</strong>: what are the current pain points and frustrations that your new project is aiming to solve?</li>
  <li><strong>Implication</strong>: what is the value of your solution? What would it look like if these frustrations and problems went away as a result of your solution being implemented?</li>
  <li><strong>Need-Payoff</strong>: how important or urgent is it for you to solve this problem? What are the benefits of building this solution for customers?</li>
</ul>

<p>If you can <strong>construct a narrative</strong> that incorporates all 4 of those kinds of questions into the new project or idea you’re bringing to your teams then you are much more likely to get them energized and excited to make a real difference for your customers.</p>

	  ]]></description>
	</item>

	<item>
	  <title>Start today</title>
	  <link>//blog/start-today/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2020-12-31T21:30:00-05:00</pubDate>
	  <guid>//blog/start-today/</guid>
	  <description><![CDATA[
	     <p>I don’t like when people ask me what my New Year’s Resolutions are.</p>

<p>I don’t have any and I never will.</p>

<p>If I want to resolve to do something - I just do it. Right then and there. I’m writing this post on the night of the last night of the year not because it was meant to close out one of the worst years in the history of my life but because there is no reason to wait.</p>

<p>Let me repeat that: <strong>there is no reason to wait</strong>.</p>

<p>January 1st doesn’t usher in some reset button like how turning 26 magically drops your car insurance rates. January 1st doesn’t absolve you of the things you missed yesterday that you know you could have physically, emotionally, or spiritually started yesterday. January 1st doesn’t care.</p>

<p><strong>Resolve to never make arbitrary resolutions again.</strong></p>

<p>Resolve to take charge of your life and the things you want to accomplish <em>the moment you want to accomplish them</em>. Inertia is a powerful thing and that first push is the easiest when you are most motivated to start: <strong>the moment you consider it a possibility</strong>. As soon as you know you can (or want) to achieve something is the time when you have the motivation and inner encouragement to make that a reality.</p>

<p>2021 will be better. But you don’t have to wait for tomorrow to start living better today.</p>

	  ]]></description>
	</item>


</channel>
</rss>
