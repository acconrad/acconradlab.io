<?xml version="1.0" encoding="UTF-8" ?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
   
      <title>https://adamconrad.dev</title>
   
   <link>https://adamconrad.dev</link>
   <description>The website and home of engineering leader Adam C. Conrad.</description>
   <language>en-us</language>
   <managingEditor> Adam C. Conrad</managingEditor>
   <atom:link href="rss" rel="self" type="application/rss+xml" />
   
	<item>
	  <title>How to design an algorithm</title>
	  <link>//blog/how-to-design-an-algorithm/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2018-08-28T10:28:00-04:00</pubDate>
	  <guid>//blog/how-to-design-an-algorithm/</guid>
	  <description><![CDATA[
	     <p>If you missed my <a href="/blog/why-hiring-is-broken-and-how-im-dealing-with-it">previous article</a>, I’m going to spend a series of articles providing notes as I audit <a href="http://www3.cs.stonybrook.edu/~skiena/373/">Steven Skiena’s CSE 373 Analysis of Algorithms class</a>.</p>

<p>In the first lecture, Skiena mentions you should take a data structures course and a linear algebra course before studying this material.</p>

<p>For professional (read: practical) purposes, we’re obviously not starting from scratch, but peaking at the syllabus I do believe we can incorporate data structures by implementing them in JavaScript (this is, after all, a front-end blog) along the way.</p>

<p>And as much as foundational linear algebra will help, there simply won’t be anything in a technical interview that would warrant deep study anyway, so we can safely skip this prerequisite.</p>

<h2 id="what-is-an-algorithm">What is an algorithm?</h2>

<p>An algorithm is <strong>an instruction set.</strong> Kind of like a recipe for a food dish. And just like that recipe for cauliflower rice you found on some random blog, you can translate that recipe into any language you want.</p>

<p>Algorithms are the same way: they are language-agnostic and can be expressed in a human-readable form or machine-readable. The simpler the idea, the easier it’s going to be to express in English. The more complex or nuanced the algorithm is, the more likely you’ll want to lean on a machine language like Python or JavaScript.</p>

<p>For practical purposes, think of the high-level overview of your algorithm as something you’ll explain to an interviewer in English before you dive into the code, but understand that in order to prove your chops as a programmer the code will have to be the primary source for explaining and validating the algorithms you design.</p>

<p>The two defining characteristics of an algorithm that separate an algorithm from other instructions are:</p>

<ol>
  <li><strong>It’s correct.</strong> Has anyone ever received credit for implementing an algorithm in a tech interview that didn’t produce the correct result every single time?</li>
  <li><strong>It’s efficient.</strong> We’d like our algorithms to run sometime before we get old.</li>
</ol>

<p>Now, <em>technically</em>, programs don’t have to run correctly to be acceptable. Programs that run instructions that are <em>mostly</em> correct are known as <em>heuristics</em>. These will become important when studying approximation algorithms later, but for now, assume that correctness is a requirement.</p>

<h3 id="how-do-you-prove-an-algorithm-is-correct">How do you prove an algorithm is correct?</h3>

<p>Proofs were not my strong suit in high school or college. For some reason, it never really clicked for me because the steps in a proof never seemed to line up with the logic my brain used to jump from one step to the next.</p>

<p>Luckily, the easiest way to prove correctness is to prove something <strong>isn’t correct.</strong></p>

<p>Wait, what? How does proving the opposite help us here?</p>

<p>Well, when we’re trying to figure out a correct and efficient algorithm to solve a problem, we can narrow the scope of possible choices by eliminating the ones that are demonstratively incorrect. Proving by counterexample can be far easier than other methods.</p>

<p>As a trivial example, suppose we have a total <em>T = 6</em> we want to strive for by adding up numbers in a valid set like <em>S = [1,2,3]</em>. It might seem like a very simple algorithm that will solve this problem is to pick numbers from left to right until we reach the total <em>T</em>. Even if you add in a big number at the beginning like <em>S = [5,1,2,3]</em> this works since we can just scrap the last two numbers.</p>

<p>What’s a counterexample that wouldn’t work?</p>

<p>How about <em>S = [5,2,4]</em>.</p>

<p>First, we pick 5, which is less than 6. There are no other numbers in our set that could add up to equal 6 after already picking 5, but there <em>is</em> a valid configuration that would still satisfy <em>T</em> (2 and 4). That’s proof by counterexample that our algorithm was not correct. It’s also a pretty simple counterexample. <strong>Counter-examples can be useful in the real-world to quickly help you assess if the path you’re going down is a good one or not.</strong></p>

<p>If you can come up with a relatively simple counterexample (simple meaning it should only require a handful of variables or items) you know that your algorithm is dead on arrival and you’ll need to try something else. If you can’t, you’re probably on the right track, but that doesn’t mean your algorithm is definitively correct.</p>

<h3 id="what-techniques-are-used-to-prove-correctness">What techniques are used to prove correctness?</h3>

<p>One way to prove correctness is induction. <strong>Proof by induction indicates that if we can solve for a base case <em>and</em> the general case for <code class="highlighter-rouge">n+1</code>, we know we’ve provided a correct answer for all possible inputs.</strong></p>

<p>There are two important connections to make here about induction which are useful in a professional setting:</p>

<ol>
  <li><strong>Proof by induction is a mathematical form of recursion.</strong> Recursion is a fundamental concept in programming which allows a function to call itself. It allows us to split up large problems into smaller ones.</li>
</ol>

<p>The classic example here is the Fibonacci sequence (a sequence of integers where the current number is the sum of the previous two numbers). To calculate Fibonacci for a value <em>n</em> in the sequence, you <em>could</em> count up all of the previous numbers manually for each input of <em>n</em>, but that would not only be slow and laborious, it would also be difficult to express as a program.</p>

<p>Another way would be to count a few base cases (n = 0 and n = 1) to get the counting started, and then continuously call a <code class="highlighter-rouge">Fibonacci</code> function with the summed values from the previous step. Recursion is what allows us to accomplish this in code. It is the programming strategy for tackling induction, which is the mathematical strategy for proving statements for algorithms which operate on our sets of data.</p>

<ol>
  <li><strong>Proof by induction is useful for summation.</strong> If you’re adding up a lot of inputs together, and you need to prove it will work for all cases, even ones larger than the set you have defined, it can be proven with induction.</li>
</ol>

<p>But are you ever going to need to formally prove something at work or in an interview? Absolutely not. <strong>But you will need to test your code, and tests are a form of proof.</strong></p>

<p>So while a formal mathematical proof of induction is likely way more rigorous than you will ever need to showcase in a professional setting, it does set the tone that you can’t simply write code and have people assume what you wrote is correct. It needs to be tested somehow, so if you have the mindset that your algorithm needs to be proven correct in some form, you’re on the right track to writing quality code.</p>

<h3 id="and-how-do-you-prove-something-is-efficient">And how do you prove something is efficient?</h3>

<p>If we’ll primarily be using code to express our algorithms, and tests to prove their correctness, Big O notation will be used to prove our algorithms are efficient.</p>

<p>We’ll cover Big O in a later post in this series, but the important thing to remember now is that in general, you’re going to want to strive for things that take a reasonable amount of time on large data.</p>

<p>For example, if something you design takes an <code class="highlighter-rouge">n!</code> factorial amount of time, anything over a measly 30 items and you’re dealing with numbers larger than the number of stars in the known universe. You <em>probably</em> want something that runs a bit faster than that.</p>

<h3 id="the-big-picture-on-the-properties-of-algorithms">The big picture on the properties of algorithms</h3>

<p>Most CS courses (and most schools) only ever care about these two things. If your teachers and TAs can successfully run your program within a reasonable amount of time, you get an A. Real life doesn’t give you an A for these two things because <em>you don’t work in a vacuum</em> as you do on a problem set or exam. So what things are missing from the real world picture?</p>

<ul>
  <li><strong>Orthogonality:</strong> Is your code dependent on other stuff? Are you writing stateful or functional code? Since most coding whiteboard problems are isolated and self-contained, you usually can’t test for this, so make sure you present a portfolio of real-world projects and open source code to demonstrate this</li>
  <li><strong>Readability:</strong> You can write the hackiest crap to get an algorithm to work, but in the real world other people can’t read or use that code, and that’s a fail. Make sure if you have time and your code is correct and efficient, to <em>refactor</em> to demonstrate you can write readable, reusable code that is DRY (don’t repeat yourself) and orthogonal (or at least that it can be written as part of an orthogonal system)</li>
</ul>

<h3 id="the-first-step-in-designing-an-algorithm">The first step in designing an algorithm</h3>

<p>So now that we know what defines an algorithm and what is required to prove it’s worth using to solve our problems, the next step is to decide how we will design our algorithms. Modeling a problem means knowing the objects you’re dealing with, and there are two classes of objects we will cover:</p>

<h4 id="combinatorial-objects">Combinatorial objects</h4>

<p>A <em>combinatorial object</em> is just a fancy way of saying “what kinds of things can I use to count with?” Since machines are just big 0 and 1 factories, combinatorial objects are the way for us to collect and organize all of the 0 and 1 math our machines are performing thousands upon millions of instructions per second. What kinds of things are we talking about?</p>

<ul>
  <li><strong>Permutations:</strong> reorderings of a set. Colloquial terms for this include words like <em>arrangement</em>, <em>tour</em>, <em>ordering</em>, and/or <em>sequence</em>.</li>
  <li><strong>String:</strong> sequence of characters or patterns. Think of strings like permutations but with letters instead of numbers. Words like <em>text</em>, <em>character</em>, <em>pattern</em>, <em>label</em>, <em>sentence</em> are key insights that you’re dealing with string data.</li>
  <li><strong>Subsets:</strong> portions of a set. If you see words like <em>cluster</em>, <em>collection</em>, <em>committee</em>, <em>group</em>, <em>packaging</em>, or <em>selection</em>, you’ve probably got a subset.</li>
  <li><strong>Points:</strong> locations in space. Words like <em>node</em>, <em>site</em>, <em>position</em>, <em>record</em>, or <em>location</em> are all references to <em>points</em>.</li>
  <li><strong>Graphs:</strong> nodes with vertices to connect them and give them direction. We mentioned this much earlier in this article. Words like <em>network</em>, <em>circuit</em>, <em>web</em>, and <em>relationship</em> all describe graphs.</li>
  <li><strong>Trees:</strong> graphs that flow in one direction and don’t end up where they started (acyclic). When they’re perfectly balanced, they literally look like a Christmas tree. Words like <em>hierarchy</em>, <em>dominance relationship</em>, <em>ancestor/descendent relationship</em>, <em>taxonomy</em> are all indicators you’re dealing with trees in your problem.</li>
  <li><strong>Polygon:</strong> graphs that do start and end at the same place (cyclic) which represent a shape or region in a physical space. Other words like <em>configuration</em> and <em>boundary</em> all indicate that your problem statement might be dealing with polygons.</li>
</ul>

<h4 id="recursive-objects">Recursive objects</h4>

<p>A <em>recursive object</em>, unlike a combinatorial object like from above, just takes the countable things and asks “what happens when you remove one of those things?”</p>

<p>In other words, if you <strong>remove 1 item from a set it results in a smaller set of permutations.</strong></p>

<p>Same thing with a string: just delete one letter and you have a smaller string.</p>

<p>Subsets are inherently recursive because <strong>by removing one object, that in itself is a subset</strong>.</p>

<p>With a set of points, split the points down the middle with a line and now you have two smaller sets of points. (Same applies to a polygon).</p>

<p>Deleting one node from a graph is just a smaller graph.</p>

<p>Deleting one node from a tree is just a smaller tree. You get the idea…</p>

<p>With recursive objects, your base case is always the empty or solo set (or in the case of a polygon, a triangle).</p>

<p>With the above objects available at our disposal, we have all of the tools necessary to begin mapping our problem out into real objects which we can then manipulate with our algorithms.</p>

<h2 id="the-daily-problem">The Daily Problem</h2>

<p>Skiena offers a <a href="http://www3.cs.stonybrook.edu/~skiena/373/hw/daily.pdf">problem to think over for each following lecture</a>. This is sort of like a quick practice problem to test if you’ve grasped the previous lecture’s core ideas. At the start of the next lecture, he goes over the problem (and solution) in detail. So for the August 30th lecture 2 days from now, he’s going to <strong>solve Problem 1-5 in <em>The Algorithm Design Manual</em></strong>. Here’s what it looks like:</p>

<blockquote>
  <p>The <em>knapsack problem</em> is as follows: given a set of integers <code class="highlighter-rouge">S = {s1, s2, . . . , sn}</code>, and a target number <code class="highlighter-rouge">T</code>, find a subset of <code class="highlighter-rouge">S</code> which adds up exactly to <code class="highlighter-rouge">T</code>. For example, there exists a subset within <code class="highlighter-rouge">S = {1,2,5,9,10}</code> that adds up to <code class="highlighter-rouge">T = 22</code> but not <code class="highlighter-rouge">T = 23</code>.</p>

  <p>Find counterexamples to each of the following algorithms for the knapsack problem.</p>

  <p>That is, give an <code class="highlighter-rouge">S</code> and <code class="highlighter-rouge">T</code> where the algorithm does not find a solution which leaves the knapsack completely full, even though a full-knapsack solution exists.</p>

  <p>(a) Put the elements of <code class="highlighter-rouge">S</code> in the knapsack in left to right order if they fit, i.e. the first-fit algorithm.</p>

  <p>(b) Put the elements of <code class="highlighter-rouge">S</code> in the knapsack from smallest to largest, i.e. the best-fit algorithm.</p>

  <p>(c) Put the elements of <code class="highlighter-rouge">S</code> in the knapsack from largest to smallest.</p>
</blockquote>

<p>My plan is to solve the problem before the lecture, throw it up in the next article in the series to start off the article, see if it lines up with the answer given in the lecture, and then compare and contrast the different approaches as well as discuss any flaws or issues that arose along the way.</p>

<h2 id="see-you-next-time">See you next time!</h2>

<p>Wow, this is probably one of the longest posts I’ve ever written. I hope this post has convinced you to finally learn data structures and algorithms once and for all. Even if you aren’t dedicated to taking the entire course, I hope this stands to be a reference guide if there are ever parts or pieces of this that you might want to reference or check when working with these things in JavaScript.</p>

	  ]]></description>
	</item>


</channel>
</rss>
