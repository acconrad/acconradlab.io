<?xml version="1.0" encoding="UTF-8" ?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
   
      <title>https://adamconrad.dev</title>
   
   <link>https://adamconrad.dev</link>
   <description>The website and home of engineering leader Adam C. Conrad.</description>
   <language>en-us</language>
   <managingEditor> Adam C. Conrad</managingEditor>
   <atom:link href="rss" rel="self" type="application/rss+xml" />
   
	<item>
	  <title>A quick and dirty guide to Kanban</title>
	  <link>//blog/quick-and-dirty-guide-to-kanban/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2020-11-30T16:18:00-05:00</pubDate>
	  <guid>//blog/quick-and-dirty-guide-to-kanban/</guid>
	  <description><![CDATA[
	     <p>I recently saw a question about how to implement Kanban and I realized I could very succinctly describe what is involved in the practice. Lots of people write whole books and courses on Kanban but I think that is completely unnecessary. I propose that you can learn (and implement Kanban) in about 5 minutes with a quick, one-page read.</p>

<p>This guide is purposefully short because I want you to know that <strong>there really isn’t all that much to this practice</strong> so I’m going to save you all of the fluff and background that usually accompanies one of my posts.</p>

<p><strong>Kanban is just a way of organizing your teams’ work.</strong> It’s all about <em>continuous improvement</em> and the <em>acknowledgement that priorities change constantly</em>.</p>

<p>I think <strong>most software teams that don’t have hard deadlines would benefit more from Kanban than Scrum</strong>. The reason is that software is inherently complex and often has moving targets. So gaming companies, which do have hard deadlines for when their games are released, probably won’t benefit from Kanban and should stick to the Scrum (i.e. waterfall) approach. If you’re a SaaS software product building things on-the-fly, consider Kanban. So what is involved?</p>

<h2 id="1-put-your-tasks-on-a-visual-board">1. Put your tasks on a visual board</h2>

<p>So many tools exist that already do this: Trello, Asana, and Jira are three I can think of that already do this aspect of Kanban: <strong>become aware of what you are <em>actually</em> working on</strong>. Just showing everything to your teams helps you understand where the blockers, bottlenecks, and underutilized capacity lies.</p>

<h2 id="2-create-a-simple-workflow-to-start">2. Create a simple workflow to start</h2>

<p>I would recommend a 5-step workflow that has scaled to multiple teams on very large products:</p>

<ol>
  <li><strong>Inbox:</strong> The stuff that just came into your backlog that is yet to be prioritized</li>
  <li><strong>In scope:</strong> The stuff that you’ve prioritized and could actively pick up to work on</li>
  <li><strong>In progress:</strong> The stuff someone is building with code and tests</li>
  <li><strong>In review:</strong> The stuff that has a Pull Request or some other kind of gate to review quality of the work to be delivered</li>
  <li><strong>Done:</strong> The stuff that is out on your production servers and released to customers</li>
</ol>

<h2 id="3-use-the-workflow-and-check-for-blockers">3. Use the workflow and check for blockers</h2>

<p>With this workflow you’ll see tickets moving from one step to the next. The number of tickets in each column tells you something. When tickets pile up in a given column here is how I investigate them:</p>

<ol>
  <li><strong>Lots in inbox:</strong> Check with product management to find time to prioritize - either lots of new ideas are being sourced (good) or lots of bugs, issues, or debt are accumulating (bad).</li>
  <li><strong>Lots in scope:</strong> The sprint just started (not an issue). If the sprint has been going on for a while, either the team committed to too much, too many bugs came into the sprint, or there is a bottleneck further up the workflow.</li>
  <li><strong>Lots in progress:</strong> <em>You should only ever have as many tickets in progress as there are people</em>. If there are fewer then not everyone is working on something and if there are more then someone is juggling more than one task at a time. <strong>The whole thing with Kanban is that a great workflow means doing one thing to completion.</strong> So don’t let anyone work on more than one thing at once (in terms of active development - you could have a ticket in review waiting for PR feedback while you are starting on another ticket)</li>
  <li><strong>Lots in review:</strong> Developers are not paying attention to Pull Requests and product/design are not checking the acceptance criteria are being fulfilled. My teams provide a guarantee of 4 hours to review their teammates’ code. Make sure you have <a href="https://help.github.com/en/github/creating-cloning-and-archiving-repositories/about-code-owners">code owners set up</a> and ensure all of the right parties are seeing notifications that they need to review the code.</li>
  <li><strong>Lots in done:</strong> Nothing wrong with that.</li>
</ol>

<h2 id="4-refine-your-workflow">4. Refine your workflow</h2>

<p>Start with this template and see what works for your teams. Maybe you have a staging server that needs to be pushed for review before you can push to production so insert a column between steps 4 and 5. Maybe you have separate, distinct steps for Code Review, Design Review, and Product Review - add them in. <strong>What matters is that you take the basics of a Kanban flow and iterate on your workflow to suit the productivity of your teams.</strong> And further, each team may have a different workflow that works for them so <strong>be open to adopting not just one workflow but many</strong>.</p>

<h2 id="5-measure-and-learn">5. Measure and learn</h2>

<p>Okay so you did all of this stuff and now you’re running Kanban (all of this can be turned on with a switch <a href="https://trello.com/b/C4Awm5lK/kanban-board">in Trello</a> or <a href="https://www.atlassian.com/agile/tutorials/how-to-do-kanban-with-jira-software">Jira</a> or <a href="https://asana.com/uses/kanban-boards">Asana</a>). How do you know this was the right move? Kanban is great for measuring a few things which correlate really well with <a href="https://www.amazon.com/Accelerate-Software-Performing-Technology-Organizations/dp/1942788339?SubscriptionId=AKIAILSHYYTFIVPWUY6Q&amp;tag=duckduckgo-ffab-20&amp;linkCode=xm2&amp;camp=2025&amp;creative=165953&amp;creativeASIN=1942788339">high-velocity teams</a>:</p>

<ol>
  <li><strong>Cycle time</strong>: How long does it take for completed dev work to actually get into the hands of customers? High-performing teams have a very short feedback cycle for review and release.</li>
  <li><strong>Lead time</strong>: How long does it take for a ticket to arrive on the door of the backlog and get it all the way to the done step (i.e. how long does it take for someone to ask a developer to do something and then have it delievered to customers). Cycle time is a subset of lead time but is important to distinguish to help bring to light the things that are in the control of the developer (planning, prioritizing, and working on the product) and out of the control of the developer (the review and release process). Just like cycle time, lead time should be fast and fluid.</li>
  <li><strong>Throughput</strong>: How much stuff gets put into <code class="highlighter-rouge">DONE</code> in a given sprint? Is it trending up or down or stagnating? High-performing teams can get through a lot of stuff quickly (duh).</li>
  <li><strong>WIP</strong> (work in progress): Are people actually focusing or trying to juggle a lot? High-performing teams have a low WIP (ideally 1 per developer).</li>
</ol>

<p>If you can measure these 4 things and trend them in the right direction, this should indicate that it was a good decision to adopt Kanban.</p>

	  ]]></description>
	</item>

	<item>
	  <title>When to use Scrum or Kanban</title>
	  <link>//blog/when-to-use-scrum-or-kanban/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2019-03-06T12:05:49-05:00</pubDate>
	  <guid>//blog/when-to-use-scrum-or-kanban/</guid>
	  <description><![CDATA[
	     <p>I used to believe Kanban was the solution to all project management problems. Kanban, if you don’t know, is a Japanese word that means “just-in-time” that was developed by Toyota for lean manufacturing. For software development it just means that you continuously groom tickets for priority and don’t create rigid structures around sprints (like Scrum).</p>

<p>This made a lot of sense to me because I work in web development where the web has no gates on pushing changes and companies continue to innovate faster and faster. How could any other methodology be any better? It turns out I was wrong and you may be wrong too if you think that Scrum has to be inferior in all scenarios. I now use both methodologies for different purposes.</p>

<h2 id="when-to-use-kanban">When to use Kanban</h2>

<p>Kanban is still great. The central tenants of Kanban are:</p>

<ul>
  <li>Visualizing your work with a progress board</li>
  <li>Limiting work in progress to reduce time to completion</li>
  <li>Maximizing developer flow</li>
  <li>Continuous delivery and therefore continuous change</li>
</ul>

<p>These objectives work for certain products and teams. I will use Kanban when:</p>

<ul>
  <li><strong>We’re building a web product.</strong> Nothing is federated and you can push to the web at any time.</li>
  <li><strong>The first version of the product is already live.</strong> It’s easier to think in terms of continuous change when you are going from 1 to <em>n</em> instead of 0 to 1.</li>
  <li><strong>We don’t have rigid deadlines.</strong> If everything is continuous then each push to production is in essence a new version of the product for your customers to gain value from. If you don’t need to “release” a collection of features then each push adds value.</li>
</ul>

<p>If you work at a modern web company with a SaaS product you’d probably stand to benefit from the Kanban framework. Living in this bubble I forgot that many software companies don’t fit this mould. Lots of teams operate in a different way.</p>

<h2 id="when-to-use-scrum">When to use Scrum</h2>

<p>In comparison, Scrum’s primary objectives include:</p>

<ul>
  <li>Commitment to deliver your work through sprint intervals</li>
  <li>Continuous learning and refinement with each sprint</li>
  <li>Ceremony and roles to increase teamwork</li>
  <li>Velocity defines refinement strategies</li>
  <li>Change comes between sprints</li>
</ul>

<p>Here are a few scenarios where Scrum will be superior to Kanban:</p>

<ul>
  <li><strong>We’re building a mobile product.</strong> Mobile is federated by app store releases. As wild as this sounds, it can actually be detrimental to release mobile software too frequently. The app stores think your software is to buggy and your customers hate to keep downloading your new releases.</li>
  <li><strong>We need to build a concrete MVP.</strong> When you have a clear 0 to 1 moment (i.e. going from nothing to something) it’s helpful to have a north star separated by a series of sprints. There is no continuous delivery element here because no delivery prior to the MVP adds value to your customers.</li>
  <li><strong>We have a deadline we have to work back from.</strong> This is particularly relevant for developers in the gaming industry. You need to increase velocity and you have to reach a deadline with enough buffer to make sure you don’t miss.</li>
</ul>

<p>At Indigo we use Scrum for our mobile teams because we know we don’t want to push to the app store more frequently than once every two weeks. This aligns nicely with the common two week sprint cadence for the Scrum framework.</p>

<h2 id="when-to-blend-both">When to blend both</h2>

<p>You can take elements of both and create a blend of the two frameworks. You can organize projects into sprints but make sure you limit work in progress. You can also use Kanban boards with set releases with changes mid sprint. Ultimately, you can do whatever you want as long as <strong>it works for your team</strong>. I don’t know of other modern frameworks for project management so if I’m missing one that has you captivated please <a href="https://twitter.com/theadamconrad">let me know</a>.</p>

	  ]]></description>
	</item>


</channel>
</rss>
