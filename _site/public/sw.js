var CACHENAME = 'userinterfacing_v7';

self.addEventListener('install', e => {
  e.waitUntil(
    caches.open(CACHENAME).then(cache => {
      return cache.addAll([
        '/about/',
        '/about/?homescreen=1',
        '/rss.xml',
        '/rss.xml?homescreen=1',
        '/newsletter/',
        '/newsletter/?homescreen=1',
        '/aframe-demo.html',
        '/aframe-demo.html?homescreen=1',
        '/assets/css/main.css',
        '/assets/css/main.css?homescreen=1',
        '/assets/js/jquery.fitvids.js',
        '/assets/js/jquery.fitvids.js?homescreen=1'
      ])
      .then(() => self.skipWaiting());
    })
  )
});

self.addEventListener('activate',  event => {
  event.waitUntil(self.clients.claim());
});

self.addEventListener('fetch', event => {
  event.respondWith(
    caches.match(event.request, {ignoreSearch:true}).then(response => {
      return response || fetch(event.request);
    })
  );
});

self.addEventListener('message', event => {
  if (event.data.action === 'skipWaiting') {
    self.skipWaiting();
  }
});
