---
title: Updates right to your inbox
layout: page
class: 'post'
navigation: 'True'
current: newsletter
---

I provide a weekly newsletter for both email and RSS featuring a highly-curated list of articles in the field of user interface development and engineering management. These are hand-crafted each and every week by me to ensure the highest level of relevance and quality.
## Why sign up for the list
The two biggest reasons to sign up for the newsletter are **content quality** and **exclusive articles** you won’t find anywhere else. I try to offer a few advantages in our newsletter that we haven’t seen anywhere else:
### Brief, but up-to-date
I’m not sure why, but we always thought the whole point of newsletters was so that you didn’t have to sift through a ton of stuff. And yet, most email newsletters are still super dense. **I try to keep the number of articles to less than 10 so you only see the best content**. I also make sure they are relevant to what you’re interested in now, not last year. You won’t likely ever find a throwback article, particularly in a field that is changing so rapidly.
### Organized by experience level
It’s really hard to judge from a title (outside of the few extremely obvious ones) what articles are aimed at beginners versus seasoned veterans. **Wouldn’t it be great if you could skip all of the articles that are too easy (or too advanced)?** I thought you’d say yes. I also make sure we provide a few extras, as well as articles on the bleeding-edge, so there’s always something new to discover, regardless of your experience with UI.
### You’ll learn something new
The software disciplines attract people who naturally crave knowledge. **You deserve a newsletter that actually helps you move forward in your career.** I tend to skip purely opinion pieces and fluff articles, in favor of articles that teach and instruct. That doesn’t mean they’re necessarily technical; lots of great articles can teach about soft skills and aspects outside of design and programming, and we won’t shy away from those. I will also make sure that if it is an opinion piece, it provides at least one takeaway.

Not sure about an article? **I annotate every link with a summary.** So even if you can only read the headline, we’ll follow up with either why you should read it, or what you should take away from the article.

## What are you waiting for?

Sign up now and start getting all of our content (and _way_ more) delivered right to your inbox!

<iframe src="https://userinterfacing.substack.com/embed" width="100%" height="320" style="background:#f7f7f7;" frameborder="0" scrolling="no"></iframe>

Prefer RSS? [Subscribe here](/rss.xml).
