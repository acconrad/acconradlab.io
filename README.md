# Hello World

**Hey there, you found the source code to my personal branding homepage. That's cool.**

## Hire me

See something you like? Want to know more? I run a [software consulting firm](https://www.anonconsulting.com) specializing UI development and UX consulting. [Follow me on GitHub](https://github.com/acconrad) or send me an email at **me [at] adamconrad [dot] dev**, I'd be happy to chat with you!

## Software Development Achievements

Forked from [here](https://gist.github.com/jasonrudolph/1133830#file_programming_achievements.md), a tracking my own development journey:

**Learn a variety of [programming paradigms](http://en.wikipedia.org/wiki/Programming_paradigm "Programming paradigm - Wikipedia"):**

- [x] Write an application in assembly language **Part of coursework in [CS31](http://cs.brown.edu/courses/csci0310/) writing MIPS assembly**
- [x] Write an application in an imperative language **Part of coursework in [CS123](http://cs.brown.edu/courses/cs123/) writing C for computer graphics**
- [x] Write an application in a functional language **https://github.com/acconrad/peergym written in Elixir**
- [x] Write an application in an object-oriented language **https://www.gocatalant.com written in Ruby**
- [x] Write an application in a prototype-based language **https://www.indigoag.com written in JavaScript**
- [ ] Write an application in a logic programming language
- [x] Write an application using the Actor model **https://www.coachup.com using Ruby on Rails ActiveRecord model**
- [ ] Write an application in a concatenative language
- [x] Write an application in a array-oriented language **Part of coursework in [EN4](https://www.brown.edu/Departments/Engineering/Courses/En4/) writing MATLAB**
- [x] Write a data definition and data access layer for a complex domain **https://www.indigoag.com written DALs for Node backend**

**Experience the ins and outs of programming for different platforms:**

- [x] Write a nontrivial web app **https://github.com/acconrad/peergym**
- [x] Write a nontrivial desktop app **https://www.gettoknowapp.com Slack bot for Desktop**
- [ ] Write a nontrivial mobile app
- [ ] Write a nontrivial game
- [ ] Write an embedded app
- [x] Write a realtime system **https://www.gettoknowapp.com Slack bot with real-time message passing to and from Slack**

**Enhance your understanding of the building blocks that we use as developers:**

- [ ] Write a networking client (e.g., HTTP, FTP)
- [ ] Write a device driver
- [x] Write a B-tree database **Part of coursework in [CS127](http://cs.brown.edu/courses/cs127/) writing a database from scratch**
- [ ] Wrap an existing library to provide a better (more pleasant) user experience
- [ ] Write an application or framework that provides a plugin model
- [ ] Write a testing framework
- [ ] Write a programming language
- [ ] Write an interpreted programming language
- [ ] Write a compiled programming language
- [ ] Write a logic-based programming language
- [ ] Write an operating system kernel
- [ ] Write a text editor
- [ ] Write a game engine

**Enlighten yourself with koans, katas, and the wisdom of ages:**

- [x] Complete five [code katas](http://en.wikipedia.org/wiki/Kata_\(programming\) "Kata (programming) - Wikipedia")
- [x] Complete the [programming koans](http://sett.ociweb.com/sett/settJan2011.html "Learning Programming Languages with Koans - Object Computing, Inc.") for a language that you want to learn
- [x] Attend a [code retreat](http://coderetreat.com/ "Code Retreat with Corey Haines")
- [ ] Read [SICP](http://mitpress.mit.edu/sicp/ "SICP web site") and complete all the exercises
- [x] Read a Standard **Read the [DOM Standard](https://dom.spec.whatwg.org/) as part of [my work with Deno](https://github.com/denoland/deno/pulls?utf8=%E2%9C%93&q=is%3Apr+sort%3Aupdated-desc+is%3Aclosed+author%3Aacconrad)**

**Program in the open:**

- [x] Contribute to an open source project **https://github.com/elixir-lang/elixir/pull/7073**
- [x] Have a patch accepted **https://github.com/elixir-lang/elixir/pull/7073**
- [x] Earn commit rights on a significant open source project **[My contributions to Deno](https://github.com/denoland/deno/pulls?utf8=%E2%9C%93&q=is%3Apr+sort%3Aupdated-desc+is%3Aclosed+author%3Aacconrad)**
- [x] Publish an open source project **https://github.com/acconrad/peergym**
- [ ] Perform a [Refactotum](http://thinkrelevance.com/blog/2007/04/03/twir.html "Refactotum") of an open source project
- [ ] Contribute to a language or Standard

**Learn by teaching others:**

- [x] Present a lightning talk **[VR on the web](https://www.meetup.com/boston_JS/events/252406830/)**
- [x] Present at a local user group **[Organizing React applications](https://www.meetup.com/ReactJS-Boston/events/261436318/)**
- [ ] Present at a conference
- [ ] Deliver a training course
- [x] Publish a tutorial **[How JavaScript browser engines work](https://softwareengineeringdaily.com/2018/10/03/javascript-and-the-inner-workings-of-your-browser/)**
- [ ] Publish a constructive code review of an open source project
- [ ] Write a programming book
