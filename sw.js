var CACHENAME = 'personal-site-v3';

self.addEventListener('install', e => {
  e.waitUntil(
    caches.open(CACHENAME).then(cache => {
      return cache.addAll([
        '/rss.xml',
        '/rss.xml?homescreen=1',
        '/newsletter/',
        '/newsletter/?homescreen=1',
        '/aframe-demo.html',
        '/aframe-demo.html?homescreen=1',
        '/assets/css/main.css',
        '/assets/css/main.css?homescreen=1',
        '/assets/css/ghost.min.css',
        '/assets/css/ghost.min.css?homescreen=1',
        '/assets/fonts/fontello.eot',
        '/assets/fonts/fontello.eot?homescreen=1',
        '/assets/fonts/fontello.svg',
        '/assets/fonts/fontello.svg?homescreen=1',
        '/assets/fonts/fontello.ttf',
        '/assets/fonts/fontello.ttf?homescreen=1',
        '/assets/fonts/fontello.woff',
        '/assets/fonts/fontello.woff?homescreen=1',
        '/assets/fonts/fontello.woff2',
        '/assets/fonts/fontello.woff2?homescreen=1',
        '/assets/images/avatar.jpg',
        '/assets/images/avatar.jpg?homescreen=1',
        '/assets/images/favicon/apple-touch-icon.png',
        '/assets/images/favicon/apple-touch-icon.png?homescreen=1',
        '/assets/images/favicon/favicon.ico',
        '/assets/images/favicon/favicon.ico?homescreen=1',
        '/assets/images/favicon/favicon-16x16.png',
        '/assets/images/favicon/favicon-16x16.png?homescreen=1',
        '/assets/images/favicon/favicon-32x32.png',
        '/assets/images/favicon/favicon-32x32.png?homescreen=1',
        '/assets/images/favicon/safari-pinned-tab.svg',
        '/assets/images/favicon/safari-pinned-tab.svg?homescreen=1',
        '/assets/images/favicon/site.webmanifest',
        '/assets/images/favicon/site.webmanifest?homescreen=1',
        '/assets/js/index.js',
        '/assets/js/index.js?homescreen=1',
        '/assets/js/jquery.fitvids.js',
        '/assets/js/jquery.fitvids.js?homescreen=1',
        '/',
        '/?homescreen=1',
      ])
      .then(() => self.skipWaiting());
    })
  )
});

self.addEventListener('activate',  event => {
  event.waitUntil(self.clients.claim());
});

self.addEventListener('fetch', event => {
  event.respondWith(
    caches.match(event.request, {ignoreSearch:true}).then(response => {
      return response || fetch(event.request);
    })
  );
});

self.addEventListener('message', event => {
  if (event.data.action === 'skipWaiting') {
    self.skipWaiting();
  }
});
