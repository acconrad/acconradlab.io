---
title: Selling ideas
description: When you introduce a new project or idea to a team of developers, how do you sell that to the team to get them excited about it?
layout: post
date: '2021-04-28 11:04:00'
tags: sales motivation consulting
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

I did an AMA on Clubhouse a little while ago and there were a lot of awesome, hard-hitting questions on the panel. So for the next several weeks, I'm going to write out my thoughts to each of the questions and how I answered them.

> When you introduce a new project or idea to a team of developers, how do you sell that to the team to get them excited about it?

Believe it or not I used to work in sales. For a little while I ran my own [consulting firm](https://anonconsulting.com) full-time (I still accept clients but for small, one-off consulting reports and advice). Running your own services business is arguably more sales than is it actual service development.

So when folks ask me about sales I point them to the classic book [SPIN Selling](https://www.amazon.com/SPIN-Selling-Neil-Rackham/dp/0070511136). It's for sales people by sales people.

But Adam, I'm an engineering manager. Why would I need to read a book about traditional sales to convince people to buy things?

**Because everything is sales**.

You sell yourself when you interview for a job or go on a date. You sell your products or [services](https://anonconsulting.com) if you've ever been an entrepreneur of a business. And most commonly, you sell _ideas_ to your teammates and stakeholders to convince and influence people to believe in the things [you believe in](/blog/dont-call-it-tech-debt).

So who better to learn sales from than actual sales people? If you ever have the chance to shadow sales people at your current company, **do it**. You will not only build rapport with folks in other departments that you normally wouldn't, you'll also learn a ton about how to influence and persuade people.

**Selling ideas is all about persuasion and influence.** There are books for those as well but I've got a [whole curriculum for that](/blog/teach-yourself-engineering-management/). To get you started, you really only need to remember what the SPIN acronym actually stands for:

* **Situation**: where are you today? What's the current "state of the union"?
* **Problem**: what are the current pain points and frustrations that your new project is aiming to solve?
* **Implication**: what is the value of your solution? What would it look like if these frustrations and problems went away as a result of your solution being implemented?
* **Need-Payoff**: how important or urgent is it for you to solve this problem? What are the benefits of building this solution for customers?

If you can **construct a narrative** that incorporates all 4 of those kinds of questions into the new project or idea you're bringing to your teams then you are much more likely to get them energized and excited to make a real difference for your customers.
