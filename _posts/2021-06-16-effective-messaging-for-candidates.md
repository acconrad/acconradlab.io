---
title: Effective messaging for candidates
description: Looking to reach out to candidates for your company? Try these techniques.
layout: post
date: '2021-06-16 20:28:00'
tags: hiring communication interviews
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

Recruiters have told me that they achieve a 7% (at best) success rate for replies to messages from candidates. Meanwhile, I'm able to secure a **60% success rate** when I reach out to prospective engineering candidates. The main reason for this is that **engineers prefer to hear from other engineers**. The weight of a message from a VP or Director is much greater than that of a recruiter or someone outside of the engineering org. That's why it is so important to [message the candidates yourself](/blog/its-not-hard-to-care/).

## How to set up your messages

I reach out to candidates on LinkedIn. It's hard to find candidate emails. Honestly, I'm getting enough of a response that I don't need to stalk them anyway. This saves me a lot of time so I can just focus on the message.

### Start your message off with a real greeting

Don't rely on LinkedIn to create a salutation. Sure, they guarantee you won't screw up the spelling (or worst, copy-paste the wrong name). But they look fake as hell. _Don't be fake_.

### Next, I'll admit they're probably happy where they are

Most candidates are off the market so I'm not going to pretend they're actively looking and excited to see what we have to offer.

_It helps if they have been somewhere for a few years_. I'll call that out too because then they understand that I'm reaching out because it's reasonable to assume they might be starting to look elsewhere. On the flip side, I won't reach out to someone who started a new job 3 months ago. That would just prove I'm not paying attention.

### Why they would be the right fit for the role I have open

After we get the reason for connecting out of the way I connect **what they have on their resume to what I'm looking for**. I'll get specific - their Python experience, a previous employer in the logistics industry, or an interest in microservices and building stream processing tools. Just something to show that I'm reading the fine print and not just the absolute basics like their name.

### Hone the message of why we're a big deal

_We're making software to help farmers raise, sell, and distribute their crops sustainably_. That's it. You need to get your message down to something succinct. It helps that we have a strong mission for folks to connect to (and [I'm hiring](https://indigoag.com/join-us#openings)).

### Close the sale

I always end by saying something to the effect of "if that sounds interesting can we set up a time to chat next week?" This is a lesson I've learned from my days in consulting sales. People are more likely to respond if they **have a specific request with a specific timeframe to respond**. The fewer reasons for a candidate to say no the better.

## Messaging logistics that might be helpful

Now that you're ready to hit send, here are the last few things to keep in mind to further optimize your open and response rate:

* The _best day_ to send a message is **Thursdays**. Or at the least - not weekends and not the start/end of the week.
* The _best time_ to send a message is **noon EST**. It's lunch on the east coast and breakfast on the west coast. People check their emails and messages at this time. If they're already on the platform they have more inertia to read and answer.
* The _best length message_ is **under 500 characters**. I break that rule often. After all, it's hard to capture everything above in under 500 characters. I never go above 600-650 characters. If you can tailor an authentic message in under 500 characters I'd love to see it because I've had a real hard time making that happen.
* **Respond within 24 hours.** I'd advise you to check LinkedIn daily. It helps keep your message congruent in being interested in the candidate by truly wanting to hear from them with a sense of urgency.

With all of these strategies implemented I routinely receive a **60% open rate and a 30% reply rate**. Even if a candidate says no - you can **still win a fan**. Be sure to thank the candidate for opening and responding and be sure to **ask them if they know anyone who would be a better candidate we should talk to**. Most people are willing to offer up a name (or 3). All of these ideas will help further expand your network and get you closer to hiring your next engineer.
