---
title: Behavioral interviews
description: There is only one thing to keep in mind for behavioral interviews.
layout: post
date: '2022-03-29 14:01:00'
tags: jobs hiring interview-prep
subclass: post tag-test tag-content
categories: interviewing
navigation: 'True'
article: 'True'
---

Earlier this month, I [mentioned](/blog/changing-jobs-during-the-great-resignation/) the importance of behavioral interviews for engineering managers. This next series of blog posts is going to tackle behavioral interview questions.

## Getting started

Because behavioral interviews tackle human interactions, and humans are complex, you can expect a lot of variety in this bucket of questions. Before we explain the aspects of each behavioral question, it's wise to start with foundational preparation for all behavioral questions.

### The STAR method

**The most important thing to remember about behavioral interviews is the STAR method.** STAR stands for **S**ituation, **T**ask, **A**ction, **R**esult.

The _situation_ is the background for how your story is set. Who is involved? How are they connected? Why are you involved?

The _task_ is what you were asked (or decided) to do in light of this situation. What did you have to get done? What did others have to get done? How did you influence others to get things done?

The _action_ is the application of that task. What did you do against what was expected of you? What did others do as a result of your actions? What influence did you impart on those around you?

The _result_ is the outcome of your actions. How did you perform your tasks? Did it resolve the situation? Were there any unintended consequences, good or bad, from what you did in this situation?

This format is the gold standard for answering behavioral questions. While you won't need this for every behavioral question, you can't go wrong if you use it as a fallback.

### Short and sweet

**The second most important thing to remember about behavioral interviews is brevity.** Behavioral interviews do not linger on one question. Many questions are asked in a short amount of time, often with lots of follow-up questions. To ensure interviewers get all of the signals they need, you must answer questions succinctly. The best way to practice this is to keep your answers short and without filler.

Applying both lessons, you should aim to **practice behavioral interview questions using the STAR method, capping your answers at under 2 minutes per question.** It is strongly recommended that you use a timer to ensure that you are forcing yourself to keep time in mind when answering questions. Doing this will ensure you keep your answers short with the ideal format.

### Create an autobiography

Your resume summarizes the highlights of your career. As an exercise, **re-tell your resume in an expanded autobiography**. With each project, answer the following:

* Why were they tackled?
* What were their objectives?
* How were you involved? Who did you work with?
* What were the outcomes of those projects?
  * What mistakes and failures did you make?
  * What successes did you enjoy?
* What roadblocks and conflicts did you hit along the way? How did you overcome them?
* What tradeoffs did you have to make? How did you choose between all of the options?
* What did you learn? What would you do differently today?

Cataloging all of these points across all of your major technical projects throughout your career will provide the necessary information to tackle any question you're asked. With this information captured, it will be **less about preparing for questions and more about aligning the correct situations with the right questions**.

## It's all about me

As a last reminder, don't be ashamed to highlight your specific achievements. It is expected that you will showcase teamwork and interpersonal skills. For these questions, **interviewers want to know what _you_ did**. If someone were to ask you, "what was a time you led a team to a huge success?" this would be a _weak_ answer:

> We had to fix a showstopper bug on our account settings billing page. We had to triage the bug and coordinate with the developers to find the root cause the problem and fix it. Our on-call developer quickly resolved the bug and restored access to this crucial billing page. We estimate it saved the company $100k in lost revenue by implementing these reactive practices.

Why is this a poor answer? It follows the STAR method, it's short, and recounts an actual success you experienced in a previous role.

**The problem is it says nothing about what _you specifically did_ in this situation.** Were you the team lead? The engineering manager? What part of the triage process did you participate in? Did you  perform the triage? Did you implement the triage process documentation for the team? Did you hold a post mortem or root cause analysis to learn from this mistake? Did you run the report to quantify the impact?

These questions will swirl in the interviewer's mind. They will have to spend several minutes picking apart your answer to discover your contribution. This will take away from the other questions they wanted to ask without capturing the signal they were looking for.

**Behavioral interviews are where it is okay to talk about yourself and how you succeeded in the past.** Do not be afraid to use the words "I," "me," or "mine." In the end, only _you_ are changing jobs, not your whole team.
