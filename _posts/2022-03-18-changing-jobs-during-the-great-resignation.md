---
title: Changing jobs during the Great Resignation
description: Why I left my director role for an EM job at a FAANG company.
layout: post
date: '2022-03-18 20:53:00'
tags: jobs hiring book-club interview-prep
subclass: post tag-test tag-content
categories: interviewing
navigation: 'True'
article: 'True'
---

The last time I contributed to this blog was exactly 7 months ago. There is a reason it has been so long: I changed jobs.

## I got stuck

No good can come from putting your old company on blast. Suffice it to say I was not growing anymore and I needed a change.

Once I knew it was time to leave, I needed a plan to get to my next destination.

## Prep before the prep

This was the longest job I had ever held. I was extremely out of practice interviewing as the interviewee. As an engineering manager, you have a very unique perspective. You understand how hiring decisions get made. So I knew what my new hiring managers know in terms of what to look for. Since I knew what to look for in someone like myself, my first task was to **create a list of companies I wanted to work at** and then **figure out the hiring process for each of them**.

### What I looked for

I started by looking at Levels.fyi. On Levels, I wanted to see what similar profiles were getting offered.

I looked at all of their salary data for engineering managers. I filtered on 11-12 years of experience and only selected new hire offers. Finally, I sorted by highest salary. I then applied to the top 10 companies on that list who either hired remotely or hired in my area (Boston).

All in all, including a few companies that reached out to me, my final list looked like this:

* Amazon
* Brex
* Coinbase
* Cruise
* Dropbox
* Facebook
* Google
* Instacart
* LinkedIn
* Netflix
* Physna
* Plaid
* Salesforce
* Stripe
* Twitter
* YUM Brands

### Doubling down on prep

Once I had my list of companies, it was time to start learning about their hiring processes. This is where Blind came in. Lots of people reveal what is asked at their companies. **They also offer to refer you to their company**. If I could do it again, I would have asked for referrals from all of those companies above.

I cannot stress this enough: **referrals are the most important part of getting your foot in the door**. So much so that the only 7 companies who offered an on-site were companies where I had a referral. Even still, I had referrals at both Google and Salesforce, both of which went nowhere after the initial submission phase.

Based on scouring Blind's forums, I knew I had to focus on two general areas: [systems design](/tag/systems-design/) and behavioral questions. If I wanted a job at Google or Facebook, I'd have to focus on [coding](/tag/algorithms) as well.

## My study plan

Now that I had a plan of attack, I knew I needed time. After spending way too much time watching YouTube videos on [how to prepare](https://www.youtube.com/watch?v=7UlslIXHNsw), I realized I needed at least 3 months to prepare. Because of this, **I didn't apply to these companies right away**. Instead, I knew that the actual interview process, even if it was slow, would still be faster than how long it would take for me to prepare for their interviews.

A major issue in getting started was **the lack of information on preparing for engineering manager interviews**. **The individual interviews were the same for ICs and managers.** **The differences were in the quantity of each segment**. Based on this, **I split my time equally between coding, systems design, and behavioral prep**. Even though only two of my companies required coding interviews for managers, I knew I needed the most help here.

### Assemble the questions

Blind and Reddit offered me a wealth of information on the questions I should study. It all came down to 3 sets of questions:

1. The [Blind 75 coding questions](https://leetcode.com/list/xi4ci4ig/)
2. The 25-35 system designs by [Narendra](https://www.youtube.com/watch?v=umWABit-wbk), [Gaurav](https://www.youtube.com/watch?v=OcUKFIjhKu0), and [Mikhail](https://www.youtube.com/watch?v=bUHFg8CZFws)
3. The behavioral questions on [Exponent](https://www.tryexponent.com/guides/amazon/em-interview/questions) and across the web - about 75 of these as well

### Spread the questions out over time

Since every company has agreed to a 45-minute interview, I knew I could not spend more than that time on any single question.

I also knew that coding exercises were stacked two at a time per interview. That means I would have to cram two questions in a 45-minute coding session and use the remaining time to study any answers that I didn't arrive at myself.

With roughly 175 questions in my study bucket at 45 minutes each, that's 7,875 minutes of study. With a one-year-old and a full-time job on my plate, I knew I couldn't stomach an intense study regimen for more than 3 months. Overall, that's **90 minutes of study per day, every day, for 3 months straight**.

### Finding the time

As a powerlifter, I workout 4-6 days per week, and my workouts often take 60-90 minutes or more. I knew I could at least maintain my progress by cutting down the number of lifting days to three. Stacking exercises back-to-back could create an intense but effective workout in about 45 minutes instead of 90. That reallocated more than two-thirds of my time for studying without negatively affecting anything else, including work, sleep, and family time. I had to find an additional 30 minutes a day.

Once I showed the potential salaries to my wife, we both agreed it would be perfectly reasonable for me to ask for 30 minutes of additional "me time" per day to prepare for my next job,

## Doing the prep

As you can see, even for a dad with a full-time job, I was able to find consistent time to study each day. With my questions, my timeline, and my [study music](https://www.youtube.com/watch?v=1uWj-Up7qT0), it simply came down to picking enough questions a day, every day, for the next 3 months.

I live and die by Asana. I created a project for interview prep and added all 175 questions I wanted to study to one to-do list and scrambled the questions. **Each day my goal was to pick two new questions I hadn't seen before and one old question I hadn't answered perfectly.** All questions that were answered perfectly were completed and never seen again. Any question I messed up was thrown back into the bucket and resurfaced via an Anki deck that timed my spaced repetition.

### How it really went

In truth, this is how I would have liked to implement my study plan from the beginning. In reality, I spent a bunch of time, in the beginning, learning how to prepare. This ate into my study time significantly so I had to play catch up later on.

Further, the behavioral questions were much easier after I wrote down the important milestones of my career. Once I had **two examples for each behavioral question in the [STAR method](https://www.thebalancecareers.com/what-is-the-star-interview-response-technique-2061629)**, the bulk of the behavioral prep was done. Recall often took only a few seconds, so I could easily study 10-20 questions a night.

This allowed me to spend the majority of the second half of the three months on just systems design and coding questions. Spaced repetition allowed me sufficient space to burn these examples into my brain in long-term memory so I wouldn't forget any details going into my interviews.

One thing that really helped minimize the amount of study time on systems design was to listen to the YouTube playlists while I was working out and sleeping. Yes, I would often start a YouTube playlist before bed and fall asleep listening to it. I must have repeated the same 40 videos half a dozen times. I saturated my brain with these concepts so that I could passively retain the knowledge.

## If I could do it again

Someone recently asked me how long I took to prepare and if I felt like it was enough. I think I was slightly under-prepared and would have liked an additional two weeks to study. All things considered, that's a pretty spot-on study plan. And if I hadn't spent so much time trying to figure out how to study in the first place, I easily would have found those extra two weeks to draw from at the beginning of my prep.

Now that I have my Anki decks saved, my playlists on YouTube, and my lists in Leetcode, I could cut my prep in half. I also know that senior managers are not asked coding questions at Google or Facebook. If I were to look elsewhere with a promotion in mind, I would apply a month out. That would provide sufficient time to prepare my behavioral and system design questions with these Anki decks.

## On timing

**Timing is everything in interviewing**. If you're studying for three months, you want to start applying about a month before your prep is over. Companies drag their feet (unless you have other offers, more on that later). **You want to make sure you can stack as many interviews on top of each other**.

For reference, the seven companies I took interviews with all occurred, start to finish, within two weeks. One week for submissions and logistics, two weeks for interview sessions across several rounds, and one week for offers &amp; negotiations.

Doing it this way will not only **save you time** but will **increase your offers** via competing bids. Companies move at a glacial pace when you have no offers. Companies will escalate the interview process, in under 48 hours if you have compelling counteroffers.

### On negotiating

There's not much to say here that hasn't already been said [on this blog](https://haseebq.com/my-ten-rules-for-negotiating-a-job-offer/). Just go read it. Anything I say here would just regurgitate what is written there. It's perfect and describes how I approached my offers.

## So how did I do?

My interview funnel looked like this:

* 16 companies reached out/I applied to
* 7 companies (Amazon, Brex, Facebook, Netflix, Physna, Stripe, YUM) replied and invited me to final rounds
* 2 of 7 companies rejected me
* 1 company I declined before the final round due to logistics. I knew I wasn't excited enough about the role to rush with other offers on the table.
* 1 told me I passed the interviews but never gave me an offer. I suspect it was because the other bids were too high.
* 3 companies gave me offers
* **I ended up at Facebook.**

In terms of negotiating, let's just say for my years of experience, **I would have the third-highest new offer on Levels out of over 200 offers on their site**. And that includes the fact I don't live in SF, Seattle, or NYC where offers are 5% higher than mine with a much higher cost of living.

In the end, **I passed all of my FAANG interviews and landed in the top 2% of offers for someone with my experience**. Because of this, I feel like my interview prep was worth my time and effort.

In the future, I will **ask for referrals for all of my applications** and **not worry about prep**.

Get a referral for all of the jobs you apply for, study a standard list of questions, stack your interviews, and negotiate like a champ. Success is all but guaranteed.
