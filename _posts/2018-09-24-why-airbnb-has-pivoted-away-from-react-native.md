---
title: Why Airbnb has pivoted away from React Native
description: Airbnb has switched from React Native to raw native iOS and Android applications. Here's why.
layout: post
date: '2018-09-20 10:28:00'
tags: javascript iOS Android react-native
subclass: post tag-test tag-content
categories: mobile
navigation: 'True'
image: 'https://cdn-images-1.medium.com/max/2000/0*QTNKnQLRTSBoaOv6'
article: 'True'
---

For the last two years, Airbnb has been using React Native to accelerate development of their mobile platforms. React Native is an open source framework developed by Facebook that allows JavaScript developers to create familiar React components that work across web, Android, and iOS development environments.

Recently, Gabriel Peal [wrote an article](https://medium.com/airbnb-engineering/react-native-at-airbnb-f95aa460be1c) outlining Airbnb’s decision to move off of React Native by 2019 in favor of native mobile development tools. We sat down with Gabriel to discuss the history of mobile development at Airbnb, the introduction of React Native, its benefits and drawbacks, and ultimately why Airbnb decided to move on from React Native.

## A timeline of mobile development at Airbnb

![](https://cdn-images-1.medium.com/max/2000/0*QTNKnQLRTSBoaOv6)

Originally Airbnb was just a website with increasing traffic on mobile platforms. By 2012, the engineering team recognized that in order to support the demands of its mobile user base, they would need to invest additional resources into mobile platforms. For four years the team built out iOS and Android applications with dedicated mobile developer resources and infrastructure.

However, by 2016, the landscape had shifted so far toward mobile that the development team could not keep up with demand. Without the necessary talent for mobile developers, and a vast majority of developers trained on the web, a few teams began to look at cross-platform solutions.

Airbnb’s web front-end is built with React, and the vast majority of developers at Airbnb are comfortable with this popular web framework. In 2016 Facebook launched React Native after using it internally with their iOS and Android apps. A few developers on the team saw this as a great way to leverage their existing web development team to assist in development of their mobile applications. A few months later, with a test pilot app ready, Airbnb launched their first set of components using React Native to support their iOS and Android native applications.

React Native rapidly increased the development cycle of their mobile applications, and further investments were made into developing, supporting, and providing infrastructure to its 800,000-line mobile codebase.

## Benefits and drawbacks of React Native

![](https://cdn-images-1.medium.com/max/2000/0*pjcJTIj2UKtKvZmz)

Clearly, the biggest benefits to React Native for Airbnb were the [rapid development and platform agnosticism](https://softwareengineeringdaily.com/2017/04/11/the-future-of-react-native-with-brent-vatne-and-adam-perry/). With React Native, Airbnb could leverage its entire development team of engineers to help build and support their mobile applications. This also meant that future hires could also be trained regardless of their background in mobile or web. It also meant a leaner codebase, because one repository could be deployed across three platforms. But with these benefits come challenges as well. Without being prescriptive in his advice to use (or not use) React Native, Peal offers these considerations when deciding to go with React Native:

## 0 to 1 is a major investment

A common misconception is that when Airbnb decided to use React Native, that they made a complete switch. This is not only not true, but to this day the React Native code represents only 15–20% of the codebase and developer resources. It was never a majority platform for the company, and part of that was the immense upfront cost to enable even the first set of components for the mobile platforms.

Even though React Native is a framework and vastly simplifies mobile development, it is not trivial to get up and running, particularly if you have an existing codebase. **Expect to invest a substantial amount of time and energy to integrating React Native into your existing mobile codebase.**

## Developers will still need to write native code

React Native is a great choice for simple applications where the APIs have a clear bridge between two platforms. In the earlier days of the Airbnb mobile apps, the platform was a great accelerator for the simpler tools within the mobile application. Eventually, the APIs will not operate exactly the way you want and you will have to dive into the native libraries to make the necessary adjustments.

Some examples where native bridges will need to be written include the networking stack, experimentation, internationalization, deep links, navigation, and advance map features.

If you hire a JavaScript developer to work on your React Native project, **expect that they will have to write native code to bridge any gaps in functionality.**

## Beware of landmines

Peal warns of “landmines, or interactions in the code that can be extremely difficult to track down and reproduce. One example involved React Native components rendering all white on certain phones. This bug was not only unreliable but difficult to track down.

The solution was to turn off initializing [Fresco](http://frescolib.org/), the standard React Native image rendering library for Android. To this day, the engineers still do not understand why this fixed the bug. **If Airbnb can run into landmines that turn seemingly simple tasks into weeks-long bug hunts, expect you will, too.**

## Investment is never-ending and slow

Many teams will prototype a small mobile application with React Native and immediately believe that it will solve all of their problems. But like all open source software, the platform is ever-changing with new bugs and issues discovered daily. **Expect to make continuous investments to support the volatile landscape of the React Native ecosystem.**

One example included a patch that Airbnb engineers wanted to get into the React Native framework as soon as possible to align with their product goals. Unfortunately, React Native’s development lifecycle only pushes out a release once every four weeks. If they timed their fix wrong, they would have to wait almost a month before their changes could be adopted by the React Native framework. With the speed of development at Airbnb, they simply could not afford to take that risk.

## Alternatives to React Native

![](https://cdn-images-1.medium.com/max/2000/0*4fuJ9LhmxaGslsK6)

Ultimately the drawbacks outweighed the benefits, which prompted Airbnb to reconsider, and ultimately withdraw from React Native development. While support will continue through the end of the year, Airbnb plans to discontinue active development of React Native projects by early 2019.

Even if you still are not sold on the value of native-first development for mobile applications, there are other cross-platform alternatives besides React Native. [Flutter](https://flutter.io/) is Google’s answer to React Native and has been previously covered on Software Engineering Daily (you can check out more from previous shows [here](https://softwareengineeringdaily.com/2018/07/09/flutter-with-eric-seidel/) and [here](https://softwareengineeringdaily.com/2018/07/11/flutter-in-practice-with-randal-schwartz/)). [Xamarin](https://visualstudio.microsoft.com/xamarin/) is Microsoft’s version of React Native, and additionally provides support for Windows mobile phones as well.

While both frameworks are cross-platform, similar issues can be found like the ones outlined above. Additionally, both frameworks have relatively low adoption within Google and Microsoft, respectively. Ultimately, while cross-platform frameworks solve the majority of simple problems, anything more complicated will ultimately require you to resort back to the native platforms of a given mobile device.

*Originally published at [softwareengineeringdaily.com](https://softwareengineeringdaily.com/2018/09/24/show-summary-react-native-at-airbnb/) on September 24, 2018.*
