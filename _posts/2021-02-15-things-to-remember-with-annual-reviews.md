---
title: Things to remember with annual reviews
description: It's annual performance review season and I've got a few things for you to keep in mind as you go around meeting with your teammates.
layout: post
date: '2021-2-15 21:54:00'
tags: management performance-reviews annual-reviews
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

Annual reviews have closed from the new year so we're now into performance review season. A lot of people I work with seem to have a lot of reservations and anxieties about this time of year. I find that if you keep the following things in mind you tend to have really smooth and easy reviews.

## Weave performance reviews into your 1-1s

The first thing I say to all of my directs every year is "there shouldn't be anything in here that you don't already know and if there is we should talk about it."

We never talk about it.

That's because we **bake feedback into every weekly 1-1**. Anything my folks are doing well (or need work on) are delivered with no more than a one week delay from the moment the event occurs. This allows us to create a true feedback loop where we see performance improvements several times a year rather than once at the annual review.

In fact, I've never had to write an individual development plan for a direct. That's because we don't construct massive waves of change in performance. We tackle issues as they come up rather than in the constraints of an annual review system.

**Tackle one issue at a time and then move on**. If a recurring theme emerges then that is the time to make note of it in the performance review. But that doesn't change the actual game plan. We still tackle feedback and encouragement in real-time and just make sure to highlight major wins/concerns in the annual review based on the previous year's evolution of that feedback.

## Give folks time to read the review

Don't deliver your review at the same time your directs are reading your feedback for the first time. This creates information asymmetry and an awful feeling for your directs. They not only waste time in the meeting reading the review you wrote them but they also can't even begin to process what they have read.

Imagine you gave a particularly negative review. How do you think they are going to feel when reading that information _in front of you_? Probably some combination of mortified and paralyzed because they not only feel bad but they feel like they can't show it so they have no place to go. And if they are non-reactive you might believe that they don't care or aren't willing to take action on this feedback into the next year. Can you see how this is bad for both sides of the review?

Instead, **give directs at least 24 hours to read their reviews before you meet with them**. A week is even better. Since you've already baked feedback into your weekly 1-1s this should already be an easy read but now you save even more time from your meeting by pushing off their reading time out of your scheduled meeting so at a bare minimum you just gained some time back in your calendar.

Now you can use most of the time for your review to **discuss goals for next year and how your feedback can be incorporated into those goals**. This brings me to my last point...

## If you do these right, performance reviews are forward-looking meetings

If both you and your direct are on the same page throughout the year and you've had them read your review a week in advance then why do you even need to meet? **To look ahead.**

Your meeting just became so much more effective because instead of lamenting on the past you can focus on the future. Now your performance review is about doubling-down on the things your directs have done well and weaving improvements into the areas your directs need improvement. The best way to do this is to show them the goals of your team or function and figure out **how your direct plays into achieving those goals**.

Now instead of your performance review being a recollection of the last year's work you can continue to charge ahead with _how last year can indicate what the upcoming year will be like_.

For this review season, for example, I spent about 10 of the 30 minutes pulling up the review, glossing over it with my directs, and leaving room for questions or concerns (of which there were exactly 0). For half of my directs, most of those 10 minutes were actually explaining how the performance review system works because this is their first annual review. The rest of that time was also spent explaining how our performance review system is going to change in this new year.

The remaining 20 minutes were focused on our function's goals for 2021 and how we can weave them into their teams' goals. We talked about the big things we want to accomplish and why they are perfectly positioned to take our business to the next level.

It was aspirational. It was inspirational. It was...exciting. Definitely not what you would expect to hear when you think of performance reviews but if you are doing all of the right things then it doesn't have to be painful; even if you have under-performers. I'd actually say this system is better for under-performers because they aren't caught off guard and they can instead be excited to improve in the future rather than be reminded of the past.
