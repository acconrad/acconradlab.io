---
title: Architecture for managers
description: Systems architecture is a skill I find myself leaning on quite a bit. Here's what you need to know for both front line and senior leaders.
layout: post
date: '2020-08-24 22:22:22'
tags: architecture systems-design
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

In a recent one-on-one with my boss he mentioned that, as a director, one of my core duties involves systems architecture. I was thrown for a loop a bit because while I'm happy to stay technical I didn't think this was what many directors do. Yes, you need enough of an understanding of the systems you build to defend them in presentations with senior leadership or to help guide your teams to victory but I didn't think it would involve things like architecture review and the kinds of systems design thinking you see [in interviews](https://github.com/madd86/awesome-system-design). Though in just typing that I'm really glad I now have this muscle to flex daily because I know big companies put a focus on systems design for their management interviews.

So what does systems architecture look for an engineering manager or director?

## Systems design

Of course I had to bring this up. I already linked to a big list of curated questions to think about in practicing systems design but I'd also point you to [this article](https://robertheaton.com/2020/04/06/systems-design-for-advanced-beginners/) from a few months ago which sort of builds on the basics. I know I need to study these more but I do have a few architecture examples that seem particularly relevant to the domain I work with in my day job.

## Understand your domain

Speaking of understanding the domain you need to ask yourself "how can my business domain operate within the context of the technical solution we've built?" In my case we work in logistics. So how can engineers, designers, product managers, and logistics coordinators all operate within the context of the supply chain software we've built?

That means I need to understand the business domain from all angles and all consumers of this data: from the employees running the dashboards and admin software to our drivers who power the trucks that move our goods across the country. The domain we operate in must be understood to provide an adequate technical solution.

## Understand your business

I already spoke about business domain but let's zoom out to the entire business: even if you have a solution for your customers and the employees who work on that software, how do you fit this solution into the greater context of the company you work for? How does the culture of software in your business adopt a solution like the one your teams are building? If you're working in B2B and there's a big sales culture with heavy investments in Salesforce you're going to need to find a way to integrate into Salesforce because building your own soltuion (or ignoring it) just won't cut it. As much as we pride ourselves in tech that we can build anything, we can't forget that our **software does not exist in a vacuum** so the systems we integrate must work within the context of the business we are a part of.

## Be mindful of velocity

Now that you know who you're building for and the context you're building solutions in, you need to build something that will scale _quickly_. I can't think of a single company that would like you to take your time building out the perfect solution at the expense of capturing customer revenue. If you're going to build something you can't just be thinking about technical scale like horizontal or vertical scaling. You also have to think about developer scaling: will this design decision increase or decrease the velocity of my teams? Can we build our solutions in such a way that **anticipate future needs** and **allow everyone to do their jobs more efficiently?**

One way to make your teams faster is to build a UI pattern library so UI developers can re-use components without having to build them from scratch. Another way to accelerate teams is to build an admin dashboard for your sales reps to complete transactions on behalf of customers who get the [white glove experience](https://snapshotinteractive.com/deliver-white-glove-treatment-client-part-1/), complete with validations and error checking so they get the transaction right the first time with a better chance of accepting payment.

While the second feels like regular old product work, it is still an improvement in velocity. You know that sales reps are going to push hard to complete sales so anything you can do to get to the sale faster with less errors is going to accelerate your whole company (even if they aren't developers).

## Start with the ideal state and work backwards

With all of this in mind you want to start by building a picture of your ideal state of your applications. What does the future look like if you could snap your fingers and build your ideal solution today? How does it handle millions of users executing billions of transactions flawlessly? What systems and integrations are required to achieve this beautiful feat?

Now take that ideal world and work your way back to today. What is required to get from Point A to Point B? More importantly, _when_ is it required? We don't want to pre-optimize or over-engineer solutions that we don't need. It is important to identify landmarks of scale so we know when to switch from Phase 1 to Phase _n_ throughout our architectural journey.

Once you have that road map, you have begun to build a complete picture of the systems architecture for you software from today to the next several years.

## Building the solution that meets everyone's needs

It's no small feat to create something scalable, fast, and works for both employees and customers. But then again, the job you're in isn't meant for everyone either! It took you years of practice, failure, and learning to get to a level of management that we're at today. To continue to push and improve, we have to be thinking even bigger than we thought yesterday. I know I have a lot of homework to do over the next several weeks.
