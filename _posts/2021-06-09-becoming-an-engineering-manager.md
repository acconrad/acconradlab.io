---
title: Becoming an engineering manager
description: So you're thinking of becoming an engineering manager...
layout: post
date: '2021-06-09 21:00:00'
tags: beginner manager training onboarding
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

The following is a README I give to all of my new managers. Think of it like a step-by-step guide to the broader [90-day plan](/blog/a-90-day-plan-for-managers/) you would offer your manager when you start in a new role.

## Welcome!

Your first few months as a manager here at Indigo are crucial to your success, and we’re here to support you!

This new manager issue is a launchpad, and it connects you with crucial information about being a manager at Indigo. It has been designed to ensure you start your journey as a manager with all the resources and training available.

Items on this issue are identified as either **required** (in bold) or _optional_ (in italics). Optional items may not be applicable to your style or team dynamic. Experiment with them for a few days or weeks. If you discover a useful practice or resources outside of this list, please feel free to leave a comment or edit this file.

### Your First Week

* <input type="checkbox" /> **Schedule a handover meeting with the current team manager**. Items covered should include recent feedback, projects, individual goals, and any shared documents or performance reviews most recently given.
* <input type="checkbox" /> **Schedule 1:1 meetings with team members ([Template](https://getlighthouse.com/blog/one-on-one-meetings-template-great-leaders/)) ([First 1-1 Questions](https://larahogan.me/blog/first-one-on-one-questions/))**
* <input type="checkbox" /> **Update your team and organization wiki pages to reflect the new reporting structure**
* <input type="checkbox" /> **Request access to any team chat channels you should be in**
* <input type="checkbox" /> _Create a README of how you work_
* <input type="checkbox" /> _Read [Transitioning to management as a Technical Lead Manager](https://adamconrad.dev/blog/technical-lead-management/)_

### Your First Month

* <input type="checkbox" /> **Focus on 1-1s: ask questions, build rapport, and discuss career development plans with each direct report ([Questions Reference](https://getlighthouse.com/blog/one-on-one-meeting-questions-great-managers-ask/))**
* <input type="checkbox" /> **Review your hiring process**
* <input type="checkbox" /> _Define or review measurable goals for your management role with your manager in a 1-1_
* <input type="checkbox" /> _Modify this document to incorporate learnings you’ve had as a new engineering manager_

#### Recommended Reading List

- _The Effective Manager_ by Mark Horstman (and the accompanying _Manager Tools_ podcast)
- _Resilient Management_ by Lara Hogan
- _High Output Management_ by Andy Grove
- _The 27 Challenges Managers Face_ by Bruce Tulgan (particularly for conflict resolution)
- _Accelerate_ by Nicole Forsgren, Jez Humble, and Gene Kim
- _An Elegant Puzzle_ by Will Larson
- _Extreme Ownership_ by Jocko Willink (and his Jocko Podcast)
- _The Making of a Manager_ by Julie Zhuo
- _The Manager’s Path_ by Camille Fournier
- _The Talent Code_ by Daniel Coyle

### Days 31-60

* <input type="checkbox" /> _Schedule a coffee chat with your recruiting team_
* <input type="checkbox" /> _Begin incorporating feedback into your 1-1s ([podcast on 1-1s & feedback](https://www.manager-tools.com/2009/10/management-trinity-part-2))_

### Days 61-90

* <input type="checkbox" /> _Begin incorporating coaching & delegation into your 1-1s ([podcast on coaching & delegation](https://www.manager-tools.com/2009/10/management-trinity-part-3))_
* <input type="checkbox" /> _Work with your manager on decision-making techniques to effectively delegate work and get more out of your direct reports ([Template](https://www.groupmap.com/map-templates/urgent-important-matrix/))_

### 3 Months and Beyond

* <input type="checkbox" /> _For Technical Lead or Interim Managers: Decide with your manager if you want to continue down technical leadership (e.g. Staff engineer, Principal engineer) or transition to professional leadership (e.g. Engineering Manager)_
* <input type="checkbox" /> _Walk through your first quarterly goal setting for your directs with your manager ([Template](https://docs.google.com/document/d/1p7-Jo45VAw-RTUK97p6r9G0y0RtbmRxIlRntrmcfwHM/edit#))_
* <input type="checkbox" /> _Learn about the performance review process at your company in your manager 1-1_
