---
title: How to demonstrate impact
description: How do you actually know you have done something useful? Does everything need a dollar value attached to it?
layout: post
date: '2019-02-10 23:54:49'
tags: impact revenue-generation cost-cutting
subclass: post tag-test tag-content
categories: engineering
navigation: 'True'
article: 'True'
---

There are two fundamental ways to demonstrate impact for your company: **generate revenue or cut costs**. How do you measure that impact as a software engineer?

## Ways you can generate revenue

The advice I've always heard is to be part of a revenue-generating division in a company. While engineering on the whole is a cost center, the easy way to tell if you are on the revenue side is asking yourself **"does my product make money?"** If the answer is an emphatic _yes_ then you're good. Usually product teams are revenue generators. As an individual, here are the ways you add revenue to your bottom line:

* Measure the value of the work you do. Does your new product offering increase conversions? By how much? How much is a conversion worth?
* Up-sell, cross-sell, and ensure that all of your products are easy to find
* Make your application [fast](https://www.gigaspaces.com/blog/amazon-found-every-100ms-of-latency-cost-them-1-in-sales/) and [accessible](https://www.cdc.gov/ncbddd/disabilityandhealth/infographic-disability-impacts-all.html)
* Take a tour of customer service by answering emails/calls - not only will you improve customer relations you will likely source new ideas for products or improving existing ones
* Participate in hackathons - famous examples of product innovations include the [Facebook like button](https://en.wikipedia.org/wiki/Hackathon), PhoneGap and the entire company of GroupMe (which was later bought by Skype and then Microsoft)

## Ways you can cut costs

If you aren't adding revenue then the next best thing is to cut costs. If you work on R&D, infrastructure, DevOps, or IT, then you are part of a cost center and your focus should be on cutting costs. Here are ways you can cut costs as an individual engineer:

* Find a cheaper alternative for a tool you already use
* Build an in-house replacement for something you use as a tool
* Take over something that is being maintained by consultants (i.e. consultants are _expensive_)
* Say "no" to meetings without agendas, goals, and action items
* Use automation tools that save you significant amounts of time
