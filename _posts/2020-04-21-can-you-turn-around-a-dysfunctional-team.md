---
title: Can you turn around a dysfunctional team?
description: You're a part of a team that isn't working well. Is there no hope? Or can you turn the ship around?
layout: post
date: '2020-04-21 14:09:19'
tags: organizations teamwork ownership
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

I recently read a post on the [habits of high-functioning teams](https://deniseyu.io/2020/05/23/habits-of-high-performing-teams.html) with a great discussion over on [HackerNews](https://news.ycombinator.com/item?id=23290943). One person, in particular, wondered if you could apply these concepts on to dysfunctional teams and turn them around. Since I've have the opportunity to turn around teams before I thought I would weigh in on this question with each habit.

Short answer: **yes, dysfunctional teams can turn around**. I can't say all of them can because sometimes teams are just set up to fail in certain organizations (I hope you are never on one of them). With that out of the way, let's dive into team one-by-one.

## Psychological safety and trust work on anyone

This one to me is just so obvious. **If you learn to trust people, they will give back by being their best selves.** No one wants to be infantilized (except for my infant, he doesn't have a choice). My stance is *if you made it through the barrage of interview questions, then we trust you innately to some extend to get the job done*. So then you should follow through on that and trust your team! And it can start today.

The first step is to listen. Start having regular 1-1s with everyone on your team, even the people that don't report directly to you. [Get to know](https://gettoknowapp.com) them. Learn about who they are outside of work. We're all humans with emotions, thoughts, feelings, hobbies, and interests; we're not just your employees. So learn about what makes us tick and you'll find the underlying motivators for high productivity.

Second, continue to listen, but publicly. Now that you have private 1-1s, you also need public retrospectives for growth and learning. This was suggested in the original article and I would say it's a good 1/5th of the responsibilities I have as a director is to synthesize the learnings from our retrospectives. **Grwoth and learning come from failure, so don't chastize it; celebrate it!**

Finally, keep the two things separate. Don't mix public and private conversations because people hate to be chided in public and sometimes would like more than just praise in private. If you start to blur the lines it makes people feel like you don't have their best interest at heart.

And that includes stuff way beyond feedback. Are you seeing someone get burnt out? Don't just sit there, give them a day off! Ask them to take a mental health day to recharge. The worst decision here is no decision. Each of these olive branches builds trust, which is arguably the greatest currency you have as a manager.

## Great hygiene starts with checklists

So we know that dysfunctional teams can start to trust each other if you begin by extending olive branches. Once you have trust, can you whip the team into shape? You can if you make it so easy they can't mess it up. That's where checklists come into play. In fact, there is an [entire book on this concept](http://atulgawande.com/book/the-checklist-manifesto/).

Checklists are great because if you just follow them, you get results. What kinds of checklists are useful to turn around teams?

* **Pre-commit hooks.** Forgot to run the linter? Didn't add sufficient test coverage? Catch them before they hit code review with a git hook tool like [husky](https://github.com/typicode/husky).
* **CI/CD gates.** Tools like Jenkins and CircleCI can help you bake in best practices like checking for sufficient test coverage, that all tests pass, and that you've audited your libraries to ensure no security vulnerabilities.
* **Commit templates.** Ideally you can add them to your [commit template](https://gist.github.com/lisawolderiksen/a7b99d94c92c6671181611be1641c733) so whenever someone spins up a new pull request they have to check all of the boxes to verify they have consciously agreed to adhered to your standards and best practices.
* **Post-mortems.** Break the site? Let's chat about it. Found a security bug? Let's take 20 and recap. Post-mortems, if we have psychological safety, are not about the blame game, but about the [5 Whys](https://en.wikipedia.org/wiki/5_Whys) and learning from our failures. See a recurring theme here?
* **Meeting notes.** Even meeting notes have a [template](https://www.atlassian.com/software/confluence/templates/meeting-notes). The more important thing is that you get into the habit of doing them so people don't waste time in meetings and have stuff to take away for later.
* **Retrospectives.**  Yes, retros also have [easy-to-follow](https://www.atlassian.com/software/confluence/templates/retrospective) templates for checking off all of the things you need to cover to start growing and learning on a weekly basis.

Hygiene doesn't have to be difficult. Just set some guardrails in place for your teams and soon enough they will make these habits automatic because they are drilled into their brains with these checklists.

## Get everyone to participate in the game

The next habit is around giving everyone an opportunity to experience all facets of your team running production software. The article refers to this as something akin to RPG experience points for leveling up your characters. Put it another way, it's about **succession planning**.

If your top engineer were to disappear in the Bermuda Triangle tomorrow, who would run your on-call systems? Who would know the innermost workings of the system architecture? And how exactly _does_ Kafka work, anyway?

Don't get caught without answers to any of those kinds of questions. Team rotations, pair programming, and [ownership registries](https://lethain.com/productivity-in-the-age-of-hypergrowth/) are crucial to ensuring you spread the load across the entire team and don't have a single point of failure. You wouldn't scale your systems with only one copy of your database, so why are you putting all of your knowledge into one senior developer?

The best way to implement this on a dysfunctional team is to just have an extended 1-1 with your top performers and get them to do a brain dump of everything they know. Then cross reference that with the rest of your team and see if there is overlap. For the places where you have no overlap, start looking for volunteers from the junior ranks. Good junior and mid-level engineers will chomp at the bit to gain more responsibility and up their skills.

## ABC - Always be communicating

The last point of high-functioning teams is they communicate with generosity and good intentions. This one is probably the hardest nut to crack with regards to dysfunctional teams. Again, if you start with trust, you're more likely to get open communication.

But dysfunctional teams that have trust still may be bad at communicating. This seems to take two forms:

1. They don't write anything down and forget what they've learned
2. They don't know how to chat with each other

The first one can be solved with the earlier point about checklists. If you get people into the habit of writing stuff down all of the time, the wiki no longer feels like a daunting place where text goes to die. Instead, it becomes a thriving, living, breathing document that everyone contributes to. But it needs to become habit before it can really add value.

The second point is really tough. If you've got thorny personalities or people who just don't know how to "place nice" over Slack can come off as curt and abrasive even if they don't mean to. I know plenty of folks in software who want to use as few words (and punctuation) as possible to get their point across. I get it - time is of the essence for a software engineer.

The problem is that when you make the answer to everything

> k

you make people really uncomfortable. Like it or not, people assume the worst and will analyze the crap out of a vague message. Was it dismissive? Does "k" actually mean acknowledged or go away?

To fix this, you probably need a page in your wiki on something to the effect of [Slack etiquette](https://slackhq.com/etiquette-tips-in-slack). For some, this may seem ridiculous that in this day in age we still need to train adults on how to play nicely with each other.

But we have to eat our own dog food here; you have to assume positive intent yourself that maybe some people just don't realize they look mean and nasty even if they don't intend to and they genuinely have never had issues before communicating in such a Spartan fashion. If that's the case, your etiquette guide serves as a friendly primer.

## All is not lost

If we look at all of these habits above, they apply to high-functioning team and can be used to transform dysfunctional teams. **It just takes work.** You need to put the effort in to transform an under-performing team. You also have to have the patience to see it through. Not everyone has the stomach to [turn the ship around](https://www.youtube.com/watch?v=ivwKQqf4ixA) but if you're in this position there is hope for you yet!
