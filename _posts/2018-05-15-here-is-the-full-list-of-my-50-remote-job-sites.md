---
title: Here is the Full List of My 50+ Remote Job Sites
description: A list of over 50 online job sites that are remote-friendly and support working from anywhere without having to show up to an office everyday.
layout: post
date: '2018-05-15 23:25:00'
tags: employment remote-work jobs
subclass: post tag-test tag-content
categories: employment
navigation: 'True'
image: 'remote-jobs.png'
article: 'True'
---

Are you looking for a job as a UI developer or UX designer?

Do you hate leaving the house?

Do you love leaving the house for places that have sand, surf, or other beautiful features of nature?

Then you should think about getting a remote job.

Remote job positions have become far more common than they once were. It used to be the case that you had to pull some serious strings (or be a complete superstar) in order to swing working from home even part-time. Now there are lots of businesses, large and small, that are embracing the remote workforce.

After starting [my own consulting firm](https://www.anonconsulting.com) I knew I had to expand my reach. After doing an exhaustive search, I found quite a few places to find gigs that are okay with remote workers. In fact, I found over 50 remote job sites that are looking for full-time, part-time, and contract talent.

## Common myths about job sites

Job sites conjure up a lot of mixed emotions. Here's a couple of common myths that people think of when they think job site:

* **Bottom of the barrel jobs** - only the worst jobs get posted on these sites. _Actually: lots of great roles with great salaries are advertised online._
* **So many jobs get lost in the fray** - recruiters are on full blast on job sites. _Actually: yes, but lots of respectable companies post directly on here as well._
* **They are only in major cities** - I live in Oklahoma and everything is in NYC. _Actually: did you read the title? You can work anywhere now!_

Here's an example job I pulled today. Does this look like a crappy job?

![Remote jobs can be awesome](/assets/images/remote-jobs.png)

No matter where you live or what you do, making $200k+ working from home (or the beach) sounds like a dream come true. And I literally spent 30 seconds checking one of the sites I'm going to list below.

Whether you're a UI developer or a data scientist, the future of work is remote and agile, so here is my master list of sites I use to land new contracts:

## The complete list of 50+ remote job sites

* [10x](https://10xmanagement.com/)
* [AngelList](https://angel.co/jobs)
* [Authentic Jobs]( http://www.authenticjobs.com/)
* [Clevertech](http://www.clevertech.biz/)
* [Crew](https://crew.co/)
* [Crossover](https://www.crossover.com/)
* [CXC Global](https://cxcglobal.com/)
* [Dribbble Remote](https://dribbble.com/jobs?location=Anywhere)
* [FlexJobs](https://www.flexjobs.com/jobs/telecommuting-programmer-jobs/)
* [FreelanceDevLeads](https://freelancedevleads.com/)
* [Freelancer](https://www.freelancer.com/)
* [Get Better Luck](https://getbetterluck.com/)
* [Gigster](https://gigster.com/)
* [Github Jobs](https://jobs.github.com/)
* [HappyFunCorp](http://happyfuncorp.com)
* [Hired](https://hired.com/contract-jobs)
* [HackerNews Who is Hiring](http://hnhiring.me/)
* [Indeed](https://www.indeed.com/q-Contract-jobs.html)
* [Interviewing.io (as an interviewer)](https://interviewing.io/faq)
* [Jobscribe](http://jobscribe.com/)
* [Karat](https://jobs.lever.co/karat/d44ab283-c7c0-4bbd-b8c3-4dc0ced64c86)
* [Landing Jobs](https://landing.jobs/)
* [Metova](http://metova.com/)
* [Mirror](http://mirrorplacement.com/)
* [Mokriya](http://mokriya.com/)
* [Moonlight](https://www.moonlightwork.com/)
* [NewtonX](https://www.newtonx.com/)
* [NoDesk](https://nodesk.co)
* [Outsourcely](https://www.outsourcely.com/remote-business-services-jobs)
* [PullRequest](https://www.pullrequest.com/reviewers)
* [Reddit For Hire](https://www.reddit.com/r/forhire)
* [Reddit Jobbit](https://www.reddit.com/r/jobbit)
* [Reddit Web Work](https://www.reddit.com/r/web_work)
* [Remote](https://remote.com/)
* [Remote Base](https://remotebase.io/)
* [Remote OK](https://remoteok.io/)
* [Remote Work Hub](https://remoteworkhub.com/remote-jobs/)
* [Remote.co](https://remote.co/remote-jobs/)
* [Remotely Awesome Jobs](https://www.remotelyawesomejobs.com/)
* [Remotive](https://remotive.io/find-a-job/)
* [Remotus](http://remotus.com/)
* [SkipTheDrive](https://www.skipthedrive.com/)
* [SkillHire](https://www.skillhire.com/)
* [Stack Overflow](https://careers.stackoverflow.com/jobs?allowsremote=True)
* [Toptal](https://www.toptal.com)
* [Upwork (formerly Elance / oDesk)](https://www.upwork.com)
* [We Work Remotely](https://weworkremotely.com/)
* [WFH (Work From Home)](https://www.wfh.io/)
* [WhoIsHiring](https://whoishiring.me/)
* [Work Remotely](https://workremotely.io/)
* [Working Not Working](http://workingnotworking.com)
* [Workstate](http://www.workstate.com/)
* [Working Nomads](https://www.workingnomads.co/jobs)
* [X-Team](https://x-team.com/)
* [YunoJuno](https://www.yunojuno.com/)

That enough places for you? There is so much here that you could apply to 5 jobs on 2 sites every day for a month and _still_ have more work to find!

## Bonus: How to make the most of remote job sites

So now that you have more remote work than you could possibly handle, how do you go about effectively navigating all of these roles and positions?

### Polish up your résumé and have it ready

Having your résumé up-to-date may seem like obvious advice, but there are a few ways to do that update that will help you stand apart from the crowd:

* **Write it in LaTeX** - this is my personal opinion, but I think there's a subtle respect in the software community for documents written in LaTeX. Maybe it's because [Donald Knuth wrote it](https://www-cs-faculty.stanford.edu/~knuth/), or maybe it's because it just looks so slick in PDF form.
* **Get a good, clean theme** - something that is easy to read, clean, and that focuses on just the facts. You can take care of both points by writing a LaTeX resume on [ShareLaTeX](https://www.sharelatex.com/templates/cv-or-resume).
* **Keep it to 1 page, regardless of your experience** - even if you have 25+ years of experience, you can trim it down. No one cares about your associate designer role straight out of college in the 80s.

### Get a professional headshot (for FREE)

Take a look at the bottom of the page. My picture was taken in a professional photo shoot. Did you know you can get a photo just like that for free? I am not kidding you. **Search Craigslist for college photographers looking to build their portfolio**.

![Find headshots on Craigslist for cheap](/assets/images/headshots.png)

Lots of art and photography students will post ads like the one above for dirt cheap (or even free) because it's more important to them to build their portfolio than to charge a premium rate for all of their expensive gear and techniques.

Would a true professional be better? Sure. But not for the price. Remember, this is a photo for your online social networks and job sites (Twitter, LinkedIn, your online portfolio for the sites above), not for your wedding. These sites heavily [compress and shrink your photos](/blog/the-fastest-way-to-increase-your-sites-performance-now/) anyway. All you're looking for is something that will stand out and says you mean business.

### Write a killer proposal/cover letter

Now that you look sharp, you need to make an even sharper introduction. This seems to be the one place where people just completely blow by and ignore altogether. They assume that once their résumé is sent, they can just sit back and forget about the company they applied to.

Instead, you should double down and tell them exactly why you are the perfect fit for the job. What work have you done that required similar skills? Have you worked in a similar industry vertical? Unless this is your first job, you likely have some experience you can draw from to highlight in your cover letter. **Showcase whatever you have that will let the recruiter know that you are the perfect person to fill this role they have.**

### Ace the interview

I'll probably write a follow-up article in the future about the art of the interview. In the meantime, know that interviews are just games; and games can be studied and won. To win, you have to put in the time.

![Use interview books to ace your interview](/assets/images/epi-python.png)

A friend of mine recently got his job at Google by studying one of those interview prep books. It took him about 90 hours of concentrated effort to feel comfortable answering the kinds of problems you see in the toughest interviews. If you put in a month of late night studying and practice problems, you'll be good enough to tackle Google, which means you'll be good enough for any remote job you're looking to land.

## Still no luck? Have them come to you!

If you _really_ want the most woke advice possible, then don't even bother getting in the rat race in the first place.

Wait...what?

Did I just tell you to ignore that massive list of websites I just posted? Well, kind of. At the end of the day, it all comes down to two parties trying to connect to fill a position. You can either go out and find the role, or the role can find you.

How can you possibly get companies to find you?

In addition to a nice résumé and great headshots, you should have a website, complete with a blog and portfolio to showcase to the world. Maybe you've spoken at a few local meetups - are those videos online? Do you have a podcast? This is a topic worth an entire series of articles, but the basic idea is that you want to be a known quantity online. **If you put in a lot of effort to get your name out there, eventually people _will_ find you.**

In the meantime, start applying today with the sites above! Did I miss any? Which ones are your favorite?
