---
title: Teamwork makes the dream work
description: A team-first mentality always wins with startups.
layout: post
date: '2019-01-09 22:44:58'
tags: teams leadership
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

This is a phrase my best friend tells me all of the time.

The first time I heard it I thought it was cute but over time it has become downright pithy and succinctly describes how I think about building teams.

For my teams, one of our top values is teamwork. It means putting the team before yourself. It also means giving yourself permission to rely on your team to get things done. Heroes are _not_ welcome.

Heroes, rock star programmers, ninjas...whatever you call them, these are the kinds of people who pull all-nighters and get lots of things done. They "save" projects. But at what cost?

The cost is alienation. They tend to think that since they saved the project by hitting the deadline that they are owed something by the rest of the team. They also tend to bring a "holier than thou" attitude going forward.

And while people may applaud the rock star for their performance it does not encourage them to work well with the rest of the team; if anything, it encourages rock stars to continue to fly solo because the rest of the team just gets in their way.

Have you ever wondered why sports teams only have one or two all-stars? Why doesn't LeBron James team up with Kevin Durant and Carmelo Anthony? Why not have Tom Brady gang up with OBJ and every awesome player?

They actually do this once a year and it's called the All-Star Game. Every sport has it and guess what: virtually no one watches it. Why is that? Beyond the fact that it's an exhibition game with no real consequence, I think another underlying issue is that **a bunch of divas don't play nice with each other**. If you have a whole team of all-stars then no one is a stand out and they _hate_ not being the stand out. The irony of having great players on a team is that if they can't actually be great they don't really want to play for a team.

**The best players are still at the mercy of the teams they play for or else they don't have a platform to stand out.** This is a powerful realization if you have a rock star programmer on your team. As much as they may whine and moan at the incompetence of their teams from time to time, it is the existence of that team that allows them to stand out. If they don't wise up to meeting the needs of the team then _they_ fail to actualize their own true potential.

This is why the **team always wins over the individual**. Because even if your interests are purely selfish and all you want to do is write great code with no one in your way, the only way you can make that happen in a successful company is to **build up and support a successful organization around you**.
