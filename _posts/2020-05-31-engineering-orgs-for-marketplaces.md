---
title: Organizational design for marketplace companies
description: Want to learn how to design your engineering organization with a two-sided marketplace? I've worked at four of them and can help.
layout: post
date: '2020-04-21 14:09:19'
tags: organizations organizational-design engineering-leadership
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

I've worked at four marketplace startups in my relatively short career. I do at least have enough experience on how I've seen successful (and unsuccessful) companies scale their engineering organizations for marketplaces. There is a unique problem of scale that only happens with marketplace startups and it is worth addressing as a post. If you are an engineering leader for a growing marketplace startup, here are a few tips to handle team growth in many stages.

## Why are marketplace startups special snowflakes?

If you follow [Andrew Chen's](http://andrewchen.co/) musings on marketplaces, you'll see there are some unique dynamics at play. The most obvious difference with most companies is that there are **two primary personas to concurrently manage**: supply and demand users. Both are needed to keep a marketplace going so both require the utmost attention and care. A company like Facebook only needs to keep the general user happy. But Airbnb needs to make sure that home owners have a great experience listing their homes to keep supply up _while also_ ensuring that demand is there to keep houses filled and that these customers pay in a timely manner while feeling safe being in someone else's home.

This need to please two personas instead of one presents unique challenges because right out of the gate you're going to need to have two products under one umbrella application: one for buyers and one for sellers. So do you fracture your team into two from day one? Let's dive in.

## At first, focus on supply

When your engineering team is small enough for just you to manage (anywhere from 8 to 12 depending on how masochistic you are), you should focus on supply. Why supply? Because no demand customer wants to enter a marketplace with nothing to purchase. It is far worse to flood a site with demand customers and have nothing to buy than have supply customers with nothing to sell.

If your marketplace is novel, then your supply customers are already unable to sell the thing they are now trying to sell on your marketplace. So if they create a profile and no one buys they are no worse off than they currently are, which means they can tolerate having a dormant profile for a while. **Your marketplace provides pure upside to their life which is increasing their revenue, so they will tolerate a subpar or empty experience to start.**

Demand customers, however, do not have that same tolerance for a subpar experience. If they go to your site to find 0 vendors able to fulfill their needs, they will leave and possibly never come back. You cannot mess up this relationship, so it is vital that you give them what they want from day one, which means flooding your site with ample supply. So cater to your supply-side of the marketplace first and you will have plenty of room to scale for marketing and sales when those channels can finally start acquiring demand.

## Once supply hits MVP status, use that same team for demand (or hire for it)

As soon as you have something [minimal but loveable](https://firstround.com/review/dont-serve-burnt-pizza-and-other-lessons-in-building-minimum-lovable-products/), you can start to focus on the other side of the marketplace. It's very difficult to raise money on a concept, so my guess is once you have your MVP for the supply-side you're still small enough (i.e. a handful of engineers in your flat org) that you haven't hired enough people to warrant to dedicated product teams. So instead, **put your supply-side app on maintenance mode and start all over again with your demand-side of the marketplace.**

To reiterate, your supply-side users see your app as pure upside, so they are willing to tolerate a lot if you provide them value. So if you give them a great starter experience, you don't need to continuously reinvest in their success (yet). For now, squash bugs, make the experience liveable, and then move on to completing the marketplace with the same team. This initial team will be the core owners of your product, and now is the time you should start looking towards succession planning to find these key early leaders who can start to run the two teams once your org is big enough to support dedicated teams to each side of the marketplace.

### Your internal teams are going to want to play around as well

As a corollary to this shift for your engineering team, you're going to need to also think about the internal users (e.g. customer experience, account managers, PM, and design) who will want to play with the app as well. You'll want super user accounts, an admin panel, and lots of other back office tools to support the marketplace under the hood.

While I highly encourage teams to [dogfood their own apps](https://newrepublic.com/article/115349/dogfooding-tech-slang-working-out-glitches) instead of prioritizing internal admin dashboards, you're going to need to account for both kinds of interactions. My advice here is to **build these functionalities only when absolutely necessary**. When someone at the company cannot get their job done because there isn't a tool for it, then build it. Until then, focus on your supply and demand customers.

## Next fund raise event, add that second team

Now you have a true MVP with a dozen engineers or so. What's next? If you could snap your fingers and magically hire a dozen more engineers, you'd have two teams dedicated to each side of the marketplace. But with limited resources and the cost to train and hire folks, this isn't going to happen anytime soon. So instead accept that **the needs of your marketplace customers will constantly oscillate in priority between supply and demand**.

I've seen it happen in every single marketplace company I've worked at: you build something for 6 months for a supply-side customer and next thing you know the next two quarters are completely devoted to the demand-side customer with senior leadership wondering how we could have been so negligent to this cohort for so long. Fast forward another six months and you'll hear the exact same message about the supply-side all over again. You're never completely ignoring either side, you just end up dramatically shifting the priority structure based on your team resources and the marketplace economics that are present with your product.

I haven't said much about the actual engineering organizational design at this point because there really is none. It basically looks like this:

```
You
+-- The 8-12 engineers that work for you
```

At some point you'll grow large enough that you simply cannot handle another direct report. If you followed my advice from above, you should have found your star performer who hopefully has a knack (and desire) for management and is willing to run another team. At that point your _n_ th engineering hire (the one where you decide you can't manage their career anymore) is the catalyst to divide the team into two:

```
You
+-- 6 of the engineers to stay on Team Alpha
+-- Star engineer/manager of Team Bravo
|   +-- 6 of the engineers to join Team Bravo
```

I've left the division vague on purpose because every engineering organization is different in how they want to segment teams. Ideally you have 1 team for supply and 1 team for demand and it is up to you to choose which team you want to run versus your newly-minted engineering manager/tech lead.

What this does is allow for growth of both teams without neglecting either one. **With the understanding that you will need to accept some tolerance in which team gets more resources**.

Per my earlier point about the oscillating needs of your marketplace personas, sometimes you will need to shift teammates from one team to another because the needs of a particular side of the marketplace are so large that you need to swarm on important initiatives and move towards maintenance mode for the other side. In that case, you still have a team at the ready, but the larger team can be bolstered and scaled as desired by product or senior leadership.

At some point, the group will be large enough that your organization should put both you and your EM at the limit:

```
You
+-- 8-12 engineers on Team Alpha
+-- Star engineer/manager of Team Bravo
|   +-- 8-12 engineers on join Team Bravo
```

With an engineering organization hovering somewhere between 18-26 people (including yourself), things will start to get interesting. Depending on the company, the size of the whole outfit has grown anywhere from 50 to 100 people and you will have hopefully seen a few rounds of fundraising and numerous shifts in company culture. This is the pivot point where things have to be designed for scale.

## Finally, build a process for scale

If you've made it this far, your company is ready to prove product-market fit and you're going to have to think more about how you can scale your organization to meet the demands of your ever-growing customer base. I could give more advice on this topic but once you have enough this many engineers the nuances of marketplaces no longer matter since you can support both sides of the marketplace.

There are plenty of books and articles on how to design an organization for scale, so I'll offer some quick tips from how to get from two dozen to four dozen engineers to really fill out your marketplace engineering teams:

* **Platform engineering** (+8 engineers). These engineers will chiefly support your internal customers, who now include developers. They need help with a platform that can scale to meet all of these customer needs.
* **Infrastructure/DevOps/SRE** (+4-8 engineers). For smaller teams you can probably get away with Heroku or some DevOps-As-A-Service. At some point your operation gets large enough that you'll want dedicated folks who can support all of that infrastructure to scale on demand because the cost of hiring someone is cheaper than the cost to keep Heroku churning at a fast pace.
* **Mobile/Desktop engineering** (+8-16 engineers). If your MVP was on web, you probably need an equivalent mobile offering (or vice versa). Additionally, you may need to think in doubles since you'll have to support iOS first and then Android. The cycle for supply and demand MVPs for these device-specific products starts all over again.

As you can see, we've quickly gone from 26 to as many as 58 in your engineering organization. You'll now have _several engineering managers reporting to you_.

If you scale to all of these teams, you may have enough engineering managers that it is again too difficult to manage all of these relationships and you'll have to think about adding a Director of Engineering (or two) to split the managers with you. Again, I don't want to prescribe an exact rubric at this point because this topic has been covered numerous times, but if I were to build a marketplace engineering organization from scratch, this is the methodology I would follow.

Hopefully this was a useful exercise if you're a brand new CTO or VP of engineering that is working at a marketplace startup. If you found this useful but I missed something or you have questions on this approach, I'm always happy to chat on [Twitter](https://twitter.com/theadamconrad) or [HackerNews](https://news.ycombinator.com/user?id=acconrad) where I'm most likely to be talking about this stuff. Happy scaling!
