---
title: Anticipating needs is a great way to manage up
description: Getting ahead of the curve by experimenting with initiatives and anticipating your manager's needs helps everyone.
layout: post
date: '2019-08-19 12:19:06'
tags: managing-up meetings
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

You need to anticipate your manager's needs. This is crucial for success in any job because making your boss's life easier is one of the best ways of getting their attention about the value you bring to the company.

## Visibility

The first thing you might be thinking is "what's in it for me to try and do my manager's job?" Start by flipping the script: if you have direct reports (or imagine that you do), how would you feel if one of them approached you to lighten the load or make you look better? It's happened to me before and I can tell you it's both relieving and flattering.

When you take [extreme ownership](https://www.amazon.com/Extreme-Ownership-U-S-Navy-SEALs/dp/1250067057) over your career it can be surprising and liberating to have someone else want to own things for you. It's also proof that your system of delegation and ownership is working when others look to your example and want to emulate and uplift the whole organization. So if we go back to your situation and you help out your boss you should expect welcome relief and joy that you are checking in on them.

## Extreme ownership

Now you understand the value of helping out your manager but you still haven't mastered extreme ownership. What is it and why can it help you?

As I linked above, you can read the book for a series of anecdotes or I can summarize it as this:

> Extreme ownership is owning up to everything within your control; the good _and_ the bad.

Your boss is concerned your team is going to miss the deadline. You believe it is because there is a developer who isn't pulling their weight.

Wrong.

When you do a root cause analysis of why the developer isn't pulling their weight, you can always trace it back to _you_. You didn't set expectations for delivery on the deadline. You didn't check in every week to see if all team members were up to speed and unblocked. You didn't provide training or education for those falling behind who needed more assistance. If something goes wrong within your org then there is always more you can do to improve the outcome.

I have heard this advice paraphrased many times since becoming a manager and it is that your team gets credit for all of their success and you credit credit for all of their failures. When your team launches a big app you congratulate your team's tireless efforts in front of the whole company. When a security vulnerability loses you a customer then you take one on the chin in front of your senior vice president. This is the price you pay for moving up the management ladder.

## The key to moving up

Beyond staying humble the real reason is around your position. If you run a team by definition you're already celebrated for being a leader with your title and salary. What more do you need? **You need stellar employees who like working for you.** In order to maintain that title, salary, and status, you need to be seen as a leader worthy of more directs and more responsibility. The only way to do that is to enable successful teams who like working for you.

No one wants to promote a leader to a senior leader who can't retain their employees or get the job done. And the only way to do that is to make your teams awesome. One _really_ easy way to help do that is to take flak when things go wrong and prop them up when they succeed.

## An example: organizational design

One night I was curious about how my teams would [scale](https://lethain.com/sizing-engineering-teams/) over the next year or two so I played around with Excel for maybe 20-30 minutes. It wasn't a lot of time and I had a mental itch I needed to scratch.

The next day in my 1-1 with my manager I decided to show him both my short-term team plan over the next 3 months as well as the 1+ year plan. We had nothing on our agenda about this and we won't even begin to do budgetary talks until the end of the year.

Even with all of those considerations my manager was happy to see it. The real eye opener was that a week later in standup with his direct reports (all directors), he announced all teams would begin to work with him on how their teams would evolve over the next several months/years.

So you tell me: who has the leg up in the conversation? Who spurned this idea? If you're thinking it's me because of my proactive work with the 1-1 I'd say you're right. While I can't confirm for sure, it is awfully circumstantial that a week later my topic of long-term organizational design was to be a central topic for all of my peers to my manager over the next several weeks.

That was the singular moment when I realized the power of anticipating needs and being ahead of the pack is a powerful force in your career.

## Armchair quarterbacking and moving forward

Would I do anything differently? **I would have started sooner** so hopefully I can make that happen for you. Moving forward, this will continuously be a part of my duties particularly when I have more room on my plate for additional capacity. Even if it doesn't revolve around your manager's needs you should try and help others because it's the right thing to do. If you're floating around the pool and someone else is drowning it's only instinct to jump in and help them out; this should be no different while you're working. We should all be doing more to help others when we are in a position to assist so go ahead and do it!
