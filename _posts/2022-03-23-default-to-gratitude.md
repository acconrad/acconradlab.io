---
title: Default to gratitude
description: When in doubt, thank someone.
layout: post
date: '2022-03-23 23:11:00'
tags: gratitude mental-health productivity
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

Last Friday I was completely drained. I didn't have the best day at work, I was sleep deprived, and I wasn't in the best mood.

So I decided to start messaging people and thanking them. I went on a gratitude spree.

**Gratitude has an infinite ROI**. It is [well established](https://theconversation.com/express-gratitude-not-because-you-will-benefit-from-it-but-others-might-134887) that gratitude improves your mental health and wellbeing. If you're sharing your gratitude for others, your genuine feelings will build trust and rapport.

There is literally no downside to honest, heartfelt gratitude. Express it often and with full authenticity. **If your brain has nothing left to give, give gratitude.**
