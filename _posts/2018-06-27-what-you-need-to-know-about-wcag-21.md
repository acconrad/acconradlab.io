---
title: What you need to know about WCAG 2.1
description: Learn how to pass the new Web Content Accessibility Guidelines (WCAG)
  2.1 success requirements to keep your site accessible to the largest possible audience.
layout: post
date: '2018-06-27 14:11:00'
tags: WCAG accessibility
subclass: post tag-test tag-content
categories: accessibility
navigation: 'True'
image: 'blur-codes-coding-577585.jpg'
article: 'True'
---

The W3C just released [an updated version of the Web Content Accessibility Guidelines](https://www.w3.org/TR/WCAG21/) a month ago. It's a pretty massive document with lots of new success criteria and recommendations. Fortunately, you don't need to read the whole thing to understand what changes you need to make to your websites.

Want to jump right to the new success criteria? Here's the full list along with tips on how to pass:

* <a href="#2-1-4">2.1.4 Character Key Shortcuts (A)</a>
* <a href="#2-5-1">2.5.1 Pointer Gestures (A)</a>
* <a href="#2-5-2">2.5.2 Pointer Cancellation (A)</a>
* <a href="#2-5-3">2.5.3 Label in Name (A)</a>
* <a href="#2-5-4">2.5.4 Motion Actuation (A)</a>
* <a href="#1-3-4">1.3.4 Orientation (AA)</a>
* <a href="#1-3-5">1.3.5 Identify Input Purpose (AA)</a>
* <a href="#1-4-10">1.4.10 Reflow (AA)</a>
* <a href="#1-4-11">1.4.11 Non-text Contrast (AA)</a>
* <a href="#1-4-12">1.4.12 Text Spacing (AA)</a>
* <a href="#1-4-13">1.4.13 Content on Hover or Focus (AA)</a>
* <a href="#4-1-3">4.1.3 Status Messages (AA)</a>
* <a href="#1-3-6">1.3.6 Identify Purpose (AAA)</a>
* <a href="#2-2-6">2.2.6 Timeouts (AAA)</a>
* <a href="#2-3-3">2.3.3 Animation from Interactions (AAA)</a>
* <a href="#2-5-5">2.5.5 Target Size (AAA)</a>
* <a href="#2-5-6">2.5.6 Concurrent Input Mechanisms (AAA)</a>

## What do the different conformance levels (A, AA, AAA) mean?
As seen in the WCAG 2.1 document, there are a large number of recommendations for accessibility. It would be unrealistic to expect site owners to adhere to them all, so **each success criterion is given a priority for how important it is to satisfy**.

![Accessibility is important](/assets/images/blur-codes-coding-577585.jpg)

Level A criteria are the most important, followed by AA, and then finally AAA criteria. Each level builds on the other, so it is necessary to satisfy all of the Level A requirements first before trying to satisfy AA or AAA requirements. This is because the W3C [requires full conformance at each level to be recognized as compliant](https://www.w3.org/TR/UNDERSTANDING-WCAG20/conformance.html#uc-conf-req1-head).

> For Level AA conformance, the Web page satisfies **all the Level A and Level AA Success Criteria**, or a Level AA conforming alternate version is provided.

Each section starts with Level A requirements, and builds up to Level AAA, with each requirement adding onto the previous one. For example, Section 3.3 is on input assistance. 3.3.1 and 3.3.2 are Level A requirements. 3.3.3 builds on 3.3.1, which is Level AA. The highest level, 3.3.5 and 3.3.6, base success around much more involved criteria, and thus achieve a rating of Level AAA.

## What conformance levels do I need to pass?
The obvious answer would be to aim for AAA compliance, which is the strictest and most complete standard. The Level AAA standard will create an environment that maximizes your audience reach by providing support for the widest audience of disabled persons.

Practically speaking, **aim for Level AA conformance**. This is the highest realistic conformance level to achieve. The W3C recommends this level as well:

> It is not recommended that Level AAA conformance be required as a general policy for entire sites because it is not possible to satisfy all Level AAA Success Criteria for some content.

One example of Level AAA conformance that cannot be satisfied is [WCAG criterion 2.1.3 for keyboard shortcuts](https://www.w3.org/TR/WCAG21/#keyboard-no-exception). If your application is only available on mobile, such as the Uber app, there is no real keyboard, so it cannot be navigated by keyboard shortcuts, and will not pass this success metric. This, of course, is not a practical requirement because Uber is designed for use on mobile and should not have to try and cater their strategy towards a requirement like this.

![Keyboard events](/assets/images/keyboard-events.jpg)

Now that we have an idea of what level we need to aim for, here are the new success criteria in the updated guidelines and how to pass them.

## New WCAG success criteria

### Level A: The bare minimum

#### <span id="2-1-4">2.1.4 Character Key Shortcuts</span> (<a href="https://www.w3.org/TR/WCAG21/#character-key-shortcuts">source</a>)
##### What it means
Keyboard shortcuts aren't just for power users like vim developers. They are also super useful for the blind, who have no reason for using a monitor or mouse. Therefore, it is a good idea to implement some basic keyboard shortcuts to navigate your site without the need for a mouse.

##### How to pass
* **If you have shortcuts, make sure you can disable them**
* **The shortcut can be mapped to a different key than you provided by default**
* **The shortcut can only be used on a component on in focus.** You can't accidentally trigger the shortcut if it isn't actively on the part of the page that should receive that shortcut.

#### <span id="2-5-1">2.5.1 Pointer Gestures</span> (<a href="https://www.w3.org/TR/WCAG21/#pointer-gestures">source</a>)
##### What it means
Things like touch gestures have many ways of interacting. Single point gestures include tapping or clicking. Multipoint gestures include things like pinch-to-zoom. Path-based gestures include things like swiping or sliding. Your application should account for all of these.

![touch gestures need fallbacks as well](/assets/images/touch-interaction.jpg)

##### How to pass
If you use multipoint or path-based gestures, the **actions that are triggered by those gestures should have a single point fallback**. In other words, if you use the swipe event to mark something as complete, you should also have a touch or click gesture to mark that item complete as well.

#### <span id="2-5-2">2.5.2 Pointer Cancellation</span> (<a href="https://www.w3.org/TR/WCAG21/#pointer-cancellation">source</a>)
##### What it means
Actions can be triggered with, for example, a click. Those actions should be reversible.

##### How to pass
* **Don't use the down-event** (e.g. `keydown` or `mousepress`) unless it is essential
* **Show a popup or alert to undo the previous action**
* **If you use a down-event, an equivalent up-event should be available to undo that action**

#### <span id="2-5-3">2.5.3 Label in Name</span> (<a href="https://www.w3.org/TR/WCAG21/#label-in-name">source</a>)
##### What it means
Images can be used inside of a label to describe content. The component housing those images should have a text fallback of that image content.

##### How to pass
* **If you use an image to label something, the naming attribute should have the text of that image or a description of that image as a fallback.** [Accessible name and description mappings](https://www.w3.org/TR/accname-1.1/#accessible-name-and-description-mapping) include attributes like `alt` for `<img>` tags, [captions, transcriptions, and descriptions](https://webaim.org/techniques/captions/) for `<audio>` & `<video>` content, and `aria-label`, `aria-labelledby`, or `aria-describedby` for all other tags.

#### <span id="2-5-4">2.5.4 Motion Actuation</span> (<a href="https://www.w3.org/TR/WCAG21/#motion-actuation">source</a>)
##### What it means
Actions can be triggered by device motion, such as shaking, tilting, or turning orientation. Motion actions should have the option of being disabled.

##### How to pass
* **Provide a menu item or setting to disable motion events**
* **Don't use motion events unless they are essential to the functionality of the activity**
* **Don't use motion events unless the device is a supported accessibility device with motion in mind** (e.g. haptic response for a Braille reader)

### Level AA: The gold standard

#### <span id="1-3-4">1.3.4 Orientation</span> (<a href="https://www.w3.org/TR/WCAG21/#orientation">source</a>)
##### What it means
Content can be viewed in a variety of orientations, such as landscape or portrait.

##### How to pass
* **Don't limit your content to be viewed in only 1 orientation**

#### <span id="1-3-5">1.3.5 Identify Input Purpose</span> (<a href="https://www.w3.org/TR/WCAG21/#identify-input-purpose">source</a>)
##### What it means
You can programmatically explain the purpose of any input element.

##### How to pass
* **Use available attributes when possible to explain why that input is being used.** For example, use the `autocomplete` attribute on text fields that will take advantage of this functionality.

#### <span id="1-4-10">1.4.10 Reflow</span> (<a href="https://www.w3.org/TR/WCAG21/#reflow">source</a>)
##### What it means
Software like browsers allow you to zoom in and out of the content they display. This causes content to reflow and change how it is laid out on the screen, similar to media queries in responsive design.

##### How to pass
* **Use responsive design meta tags to make the content fit your device.** If content reflows, it shouldn't start requiring you to horizontally scroll.
* **If reflows happen, that should not restrict functionality.** Again, make sure you use the correct meta tags to prevent content from going off of the screen, particularly buttons, links, or other components that provide functionality to the system.

#### <span id="1-4-11">1.4.11 Non-text Contrast</span> (<a href="https://www.w3.org/TR/WCAG21/#non-text-contrast">source</a>)
##### What it means
Contrast exists in all things, not just text. People should be able to interpret and read buttons, select dropdowns, and other components that are not primarily identified as text.

![black and white readers require high contrast](/assets/images/reader.jpg)

##### How to pass
* **Use a [color contrast checker](https://webaim.org/resources/contrastchecker/)** to identify colors that are **greater than a 3:1 ratio of contrast**. For example, use a sufficiently contrasting blue background on a button with white text.

#### <span id="1-4-12">1.4.12 Text Spacing</span> (<a href="https://www.w3.org/TR/WCAG21/#text-spacing">source</a>)
##### What it means
Text needs to breathe. If it's too close together, it's hard to read, especially for those with impaired eyesight.

##### How to pass
* `line-height` > 1.5
* `margin-top`/`margin-bottom` > 2 * font size
* `letter-spacing` > .12 * font size
* `word-spacing` > .16 * font size

#### <span id="1-4-13">1.4.13 Content on Hover or Focus</span> (<a href="https://www.w3.org/TR/WCAG21/#content-on-hover-or-focus">source</a>)
##### What it means
Some consideration is required for content that is only available on hover/focus events, such as popups and tooltips.

##### How to pass
* **You can dismiss this content without having to lose hover or focus.** For example, use the `esc` key to dismiss something.
* **If something is hoverable, you should be able to pass over that content without losing that content.** For example, if you have a tooltip on hover of an acronym like HTML, and you move the mouse onto that tooltip, the tooltip should not disappear because you moved your mouse onto tooltip and off of the acronym.

#### <span id="4-1-3">4.1.3 Status Messages</a> (<a href="https://www.w3.org/TR/WCAG21/#status-messages">source</a>)
##### What it means
A status message is dynamic content like a warning or error. These kinds of messages usually show up at the top of the document and usually break the normal flow of reading the document.

##### How to pass
* **If you issue a status message, an assistive technology like a screen reader should be able to read this message without having to receive focus.** So if you have a blog with a "Read More" button, and the button fails to load the additional content, the status message should be readable by the screen reader without having to use keyboard shortcuts or some maneuver to change where you are in the content flow in order to read what has happened.

### Level AAA: Extra credit

#### <span id="1-3-6">1.3.6 Identify Purpose</span> (<a href="https://www.w3.org/TR/WCAG21/#identify-purpose">source</a>)
##### What it means
Similar to <a href="#1-3-5">1.3.5</a>, you can explain the purpose of all tags in a markup language, not just input elements.

##### How to pass
* Make liberal use of the [ARIA properties](https://www.w3.org/TR/2017/NOTE-wai-aria-practices-1.1-20171214/examples/landmarks/index.html)
* **Provide keyboard shortcuts**
* **Keep pages simple.** Prefer more pages that are simpler in focus than fewer pages with lots of functionality.
* **Use symbols and iconography that users are familiar with** (e.g. right triangle for playing audio or hamburger for menus)

#### <span id="2-2-6">2.2.6 Timeouts</span> (<a href="https://www.w3.org/TR/WCAG21/#timeouts">source</a>)
##### What it means
When a user loses connectivity to a site, such as poor cellular service, popups show things called timeouts which inform the user that service has been interrupted. People like the elderly sometimes cannot complete these kinds of forms in one sitting and may have to revisit this content multiple times.

##### How to pass
* If you use a timeout, **inform the user that there is data loss** and **how long the service will be down for**.
* **Store data for 20 hours or longer.** For example, if you have a checkout form, it should autosave the progress on the form for at least this long.

#### <span id="2-3-3">2.3.3 Animation from Interactions</span> (<a href="https://www.w3.org/TR/WCAG21/#animation-from-interactions">source</a>)
##### What it means
Motion animation can be disabled unless it is essential to the functionality of the component. This is important because high speed or repetitive animations could induce seizures or other physical maladies.

##### How to pass
* Animations that are triggered by some kind of interaction can be disabled unless the animation is required for the component's functionality. **Allow animations to be disabled via a setting or shortcut.**
* **Don't use animations that could cause issues for users.** Police lights, camera flash, repetitive blinking, you get the idea. These kinds of animations are pretty much never needed for an application to work and are just there to add to the design. Avoid them if you can.

#### <span id="2-5-5">2.5.5 Target Size</span> (<a href="https://www.w3.org/TR/WCAG21/#target-size">source</a>)
##### What it means
The target size is the interactable area of an input. It should be sufficiently large enough to make it easy to identify and interact with.

##### How to pass
* **Make the target at least 44x44 pixels.** If the primary target cannot be that large, there should be a fallback component (such as a link) that is that large.
* **Don't worry about links or inline text.** Those are exempt from this rule.
* **Don't worry about elements controlled by the User Agent.** If you can't change the component yourself, this criterion won't hold it against you.
* **Don't worry about elements that are designed to be small on purpose.** For example, an information icon for a tooltip (often portrayed inline as the letter "i" in a circle) is generally presented in a paragraph and would look weird if it were 44x44 pixels in dimension.

#### <span id="2-5-6">2.5.6 Concurrent Input Mechanisms</span> (<a href="https://www.w3.org/TR/WCAG21/#concurrent-input-mechanisms">source</a>)
##### What it means
Content can be interacted with in multiple ways. You can type in a text box, click on the textbox, or touch the textbox with your thumb. Don't assume that users want to interact with those elements in a limited subset of those interactions.

![no input should be turned down](/assets/images/taking-notes.jpg)

##### How to pass
* **Don't restrict any kinds of interactions unless they compromise security or the user specifically disabled them**. For example, if you have a credit card form, just because you've enabled keyboard shortcuts to navigate across input elements doesn't mean you should simultaneously disable touch events because they're a slower interaction.

## You've succeeded!
You should now be well on your way to providing an accessible experience for your users. Obviously, you'll need to provide support for all of the other requirements from the previous versions of the WCAG, but now you are able to update your compliance to the latest standards.

If this all feels like too much for you, [we offer accessibility compliance as a service](https://anonconsulting.com/services/) so you don't have to worry about upgrading to the latest standards. Or [send me a question on Twitter](https://twitter.com/theadamconrad) and I'd be happy to help answer anything that was confusing or difficult to fix.
