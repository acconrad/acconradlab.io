---
title: You only need to be in two kinds of meetings
description: Most meetings are useless but there are a few you should hold on to.
layout: post
date: '2021-03-24 11:04:00'
tags: meetings productivity
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

I've been on a meeting diet lately. I've been rejecting meetings left and right and that's been pissing off some folks. And you know what? Nothing bad has happened and now I have more free time to work. So how do I do it?

## Meeting diet - requirements for a meeting I'll accept

The reason I've rejected so many meetings is because the Outlook invite misses any one of these 4 key pieces of information that I **need** in order to accept an invite:

1. **What is the goal?** It should be clearly stated in a sentence or two (maybe I can skip this if the goal isn't something I can help achieve).
2. **What is the agenda?** I want to know what items will be covered in this meeting (maybe I can attend only a portion of the meeting).
3. **How long is each agenda item?** So many people block off 30 or 60 minute meetings because they are convenient. I want to know _exactly_ how long this meeting needs to be because I'm going to guess it is longer than it needs to be.
4. **Is there any pre-reading I can do in advance?** Is there a wiki document or draft I can comment on? Could I replace the meeting completely with a commenting system or asynchronous communication (most of the time the answer is _yes_).

In addition to this, I'll see **how many people** are on the invite and if any of them are optional. Is everyone _really_ required? Are 40 people in on a meeting that isn't an All-Hands? If so, how can we all possibly contribute to this 30 minute meeting?

Even with everything here I won't double-book commitments and may still need to reject the meeting. But at least with this rubric I can reject with a clear ask (e.g. "can you please attach an agenda so I know what parts of this meeting you need my input on?") that is reasonable and is helpful to everyone involved when the meeting is changed.

## The two kinds of meetings I auto accept

With all of the meetings I reject - there are two kinds I accept without hesitation:

* We need to take collective action
* We're getting a "meeting of the minds" together

Any meeting where you take action is a no-brainer for me: a decision meeting, meetings where we assign tasks after chatting, 1-1s, retros, [planning meetings](/blog/running-effective-planning-meetings). If you ask for a 1-1 with me I'm happy to make time for you.

The other meetings I happily accept are the kind where we have to think out loud together or talk things out. These include all-hands, brainstorming sessions, and 1-1s.

You'll notice 1-1s count in both categories. If you aren't convinced yet that these are your most important meetings you need to [get with the program](https://adamconrad.dev/blog/technical-lead-management/).

Sticking with these two kinds of meetings has cut my meeting time down to only half of my week as a director. At one point I had over 80% of my week in meetings so reclaiming 12 more hours in my week has become a joy.

## Not all conversations are meetings

Last thing I'll say is that I don't consider all interactions with other humans to be considered meetings. As a manager, I think part of my IC work is getting together with potential candidates, doing interviews, and performing hiring outreach.

Yes, I talk to another person and perhaps meet up with them at an event or coffee shop (I miss coffee shops). The key difference is **these meetings don't need agendas, goals, or anything explicit in the calendar**. If we're talking about Indigo to join our company then we both know why we are here and I don't need to subject you to the Meeting Diet regimen. In that case, just enjoy your coffee and talk as humans talked before it all took place through Zoom.

Did I mention I miss coffee shops?
