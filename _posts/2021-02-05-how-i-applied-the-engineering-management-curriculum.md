---
title: How I applied the engineering management curriculum
description: People have asked me how I taught myself engineering management. Here's what I did.
layout: post
date: '2021-2-5 22:28:00'
tags: management learning curriculum
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

Last week I posted a [reading list](/blog/teach-yourself-engineering-management/) of the books I have read (or plan to read this year) on engineering management. My concern is that no one teaches you this stuff in an MBA or at work so you're kind of forced to do this on your own.

Further, it's a challenge because engineering management is like a mid-career switch. In many ways, you're starting over from scratch and many of the techniques you've applied as a software engineer no longer apply as an engineering manager. So where did I begin?

## I first explored some more universal topics

I have to be honest, I don't actually remember when I started reading from this reading list. At the time I was averaging nearly a book a week so it's difficult to pin down in the early days what I read first and in what order. But just reflecting back on the list I'm pretty sure I started with some more universally-applicable books like _How to Win Friends and Influence People_ and _Never Split the Difference_.

I read these two several years ago even before I was an engineering manager. I'm pretty sure _How to Win Friends_ was just a general recommendation on networking and making connections in the industry which has always been a concern of mine. _Never Split the Difference_ became relevant when I started my [consulting business](https://anonconsulting.com) and I wanted to make sure I was coming to the table prepared when my clients wanted to talk pricing.

Throughout the years I've built side projects I've explored lots of books around entrepreneurship so I think that's where _Traction_ and _Venture Deals_ came in but I know for a fact that was also at the time I was plowing through books. I'm pretty sure I read _Venture Deals_ in one day on a bus ride back from NYC and within the next week, I continued that trend with _Zero to One_ and _The Hard Thing About Hard Things_.

None of these are management or leadership books but I think it's important to give you context that **I read books at the moment I was experiencing situations I wasn't prepared for and wanted an expert opinion for how to handle them**.

## Solving problems as they occur

So given that criteria for reading the first engineering management book I read were when I was a tech lead who was offered the chance to have direct reports informally. And I quickly ran into a tricky interpersonal situation.

That's when a coworker recommended me _The 27 Challenges Managers Face_. I had experienced an exact situation that was spelled out in one of the 27 challenges in that book. So my first pass at this book was to solve that exact problem.

My next pass at the book was a quick read of the other 26 issues. I will say I haven't actually read this book thoroughly cover-to-cover because as I mentioned in the last article, I don't think this kind of book benefits from a cover-to-cover read. I think it's great to read quickly to internalize the problems you might face. Later, keep it in your back pocket as a reference guide when you run into tricky situations with your directs.

## My first management job

Once I officially earned my first formal engineering management position, that was when I started diving into my real reading list. It was prompted by a few aptly titled Ask Hacker News posts about how to transition into Engineering Management.

There was one comment that recommended the _Manager Tools_ podcast and so I went right to their website and looked for the basics. That's where the "Management Trinity" was first introduced to me. That weekend I put these three podcast episodes on repeat. Any workout I was listening to them over and over again. I even think I listened to them as I was falling asleep, hoping I would retain this information subliminally.

The podcast is I think over a decade old so once I started listening to the other episodes I heard about the _Effective Manager_ book as a cross-promotion for the podcast so at some point I got the book and read that.

Again, if you've listened to the podcast you've already hit the main talking points. These were the things that taught me the engineering management basics: **1-1s, coaching, feedback, and delegation**. The book and the podcast drill those concepts into your brain with lots of examples.

So for at least the first 6 months of my new management job, all I practiced were those 4 concepts until I felt comfortable in all of them. And then 3 months later I quit.

## Starting my consulting firm

After my management job I read most of the chapters in _Cracking the Coding Interview_ because I figured I'd have to do a bunch of coding interview questions even as an engineering manager. I didn't find any companies I liked that were hiring engineering managers so I started my own consulting firm and became a freelance developer.

At this point, I really kicked up my reading because, in addition to managing teams as an outsider, I had to learn all about marketing, sales, and all of the non-technical things I have never learned before.

This is when I read books like _SPIN Selling_ and _Never Split the Difference_. You won't see most of these books in my recommended reading list because that's for a completely different curriculum on starting your own consulting business which I may someday create a curriculum for as well.

I only ran this company full time for about a year and a half; it would have been longer but my next client turned out to be my next full-time job.

## My current company and current role

I'm currently working at the company I thought was going to be my next consulting gig. I passed all of their hiring tests and everything. As soon as I was ready to sign on the dotted line with my consulting engagement they also offered me a full-time position in case I wanted to take that instead. The offer was better than I expected and it offered health insurance so I took the offer and went back to full-time.

For the first 6 months or so I was in an individual contributor role so my management training took a backseat. Soon enough, a management opportunity opened up, and, leveraging my previous management experience and training, I applied for the role of director (the job I currently have).

Once I landed director I knew I had to level up my learning yet again. I was back into voracious book reading mode.

Since I had nailed the basics with _The Effective Manager_ I didn't feel the need to re-read any older material so I dove right into books that were catered for more senior management. I was reading a bunch of blogs at the time on the topic so I started plucking books from their recommended reading lists. _High Output Management_ came up time and time again so I read it in a weekend.

As I stated in the last article, I didn't get too much out of it that applies to directors directly but I did cross-reference the 200-point checklist that is listed at the end of the book to ensure I covered as many aspects as I could with the team that I had.

I can't name any of the tactics off hand right now but I would just encourage you to read the book and nail that checklist. You'll see that unless you're a VP or CTO you're going to have a hard time being able to act on all of the things Grove recommends.

This is more true today because he ran a hardware company in the 90s so doing things like "walking the floor" on any of the assembly lines simply won't apply to the folks that read this since you're most likely managing software teams.

The other books I read around this time were _The Talent Code_ and _The Little Book of Talent_, both of which were recommended in the Drift company podcast time and time again. These were the books where I applied **identifying top performers** in both retention and hiring. It's also a great way to identify talent habits for yourself in improving your own performance at work.

### Director-level learnings

The first book that really clicked for me as a director was _Extreme Ownership_. Alongside books, I also listen to a lot of podcasts. Jocko Willink's podcast is a favorite of mine and just like the _Manager Tools_ podcast, I heard about the corresponding book enough times that I had to read it.

This was the first new concept I learned in a while: **ownership and accountability**. I knew about these things, sure, but this was the first time I took the ownership part to a new, _extreme_ level. I know, cue eye roll.

But unlike in the past, this was the first time I felt the gravity of my role as a director. I wasn't just making decisions for one team, I was making decisions for lots of teams and lots of people, even some that I didn't directly interface with.

I've heard this saying before: _the further up the career ladder you go, the longer and harder it is to see your successes or failures_. That means the initiatives you drive today might take months or years to see any concrete results or changes in culture.

That's crazy timing because just today I received upward feedback from a skip-level report that I was praised for having been one step ahead of the company on certain initiatives such as building out OKRs that channel down from company or organizational goals.

The truth is I had no plan to be ahead of the curve, I simply saw what other companies were doing, experimented with some tools and tactics, and doubled down on the effective ones. OKRs seemed like a no-brainer to me and I started implementing them over a year ago. More than a year later I am only just receiving feedback from my directs that this was a good tool to use for our teams.

This relates back to the _Extreme Ownership_ because I know everything I teach my teams comes back to me. Every success and every failure I have to own.

It's really easy to say "this person was late and it is their fault for being lazy." It's really hard to see within yourself that they might be late because you never told your teams that arriving promptly for meetings is a cultural habit you want to build.

It's also really easy to say "this person missed the deadline because they didn't work hard enough." It's really hard to see within yourself that they might have missed that deadline because you didn't check in with them when they got stuck and that you didn't reinforce why the deadline was so important to hit in the first place.

At the end of the day, **it _all_ comes back to you**. I'm glad I learned this lesson now.

### A military theme

The next book on my list came from a recommendation from yet another Ask Hacker News thread...because we, as engineers, have never seemed to figure out the transition to engineering management.

I just so happened to have finished a book from a military officer so it was quite impeccable timing that one of the top comments on this thread was about a free leadership book produced by the US Army. I figured if they can train millions of Americans to fight in the most powerful army in the world then they probably have figured out how to train junior officers and generals alike.

_ADP 6-22_, which sounds like the name of a Star Wars robot assassin, was a very short and to-the-point read that I got through in one night. I should probably read this one again. This is like _How to Win Friends and Influence People_ in that it is so fundamental that you learn something new every time you go back and read it. This is more than just a book for leaders like me, this is a book for every kind of leader in every kind of profession.

Because of this broad appeal, I am saddened to say I can't recall any lessons that I haven't already learned elsewhere. I mean we're talking real basic stuff like how to communicate effectively and how to report up the chain of command. These are all such cornerstone topics I couldn't tell you if I learned them because of this book or if I learned them years ago and they were resurfaced through this book.

This post is a reminder to myself that I have to read this book again. It's also a reminder that when I finished reading this book I recommended Jocko Willink cover it on his podcast. He replied back to my tweet but I'm pretty sure he hasn't covered it yet.

In looking for that podcast episode I just discovered the Army [also published an audiobook of this manual](https://rdl.train.army.mil/catalog-ws/view/ADP6-22-Audiobook/index.html) so if you'd rather listen to the book now you have 0 excuses. Both are free to download from the US Army website.

### High octane learning

The last three books I've read on engineering management were probably the most rewarding and the most tactical since I had listened to the "Management Trinity" over three years ago. These were _Accelerate_, _The Manager's Path_, and _An Elegant Puzzle_.

_Accelerate_ teaches you about the [DORA metrics](https://cloud.google.com/blog/products/devops-sre/using-the-four-keys-to-measure-your-devops-performance) which completely revolutionized how I thought about measuring the performance and effectiveness of my teams. It radically simplified my quest for understanding how strong my developers were without having to resort to controversial tools like Code Climate Velocity and Jellyfish. This book on its own can transform how much work your teams get done through their DevOps practices.

_The Manager's Path_ does a great job of spelling out the typical career ladder for a manager. Since I didn't read this book until I was a director, I mostly skipped over the previous roles I had already attained: tech lead and front-line engineering manager.

Director is the next major leap because you go from leading individuals to leading teams via other engineering managers. Now you're not coaching individuals on how to write effective code, you're teaching managers all of the things you were supposed to have learned yourself in the books I had already read.

This book helped me dial in on a few new concepts I hadn't really practiced before: **skip level 1-1s, coaching managers, and peer management**.

As an aside, I can't imagine being a director without having read so many books beforehand. What is it like to teach other managers if we're already really bad at teaching our craft?

I guess this can come through hard-fought experience as well which is why I can't stress enough that **books are one of the cheapest, highest ROI ways to accelerate your career**. There is no way I would be successful as a director today if I didn't make up for the lack of job experience with a massive amount of self-education.

_An Elegant Puzzle_ is the last book I've read on engineering management. It's a pretty large book and I've been slowly making my way through it since the past summer. A big reason why it has taken so long is that I learn something new on nearly every page. I find myself reading and re-reading pages because the nuggets are so golden I have to stop and take a second look.

From sizing engineering teams to delivering presentations to senior leadership, these are the kinds of duties I have to perform as a director. I think one of the reasons I've been able to stay successful in my current role is because of this book. There are literally too many lessons I've learned from this book that it would take me an entire series of blog posts to synthesize them all so I would just encourage you to read the book for yourself.

## What's next

The only two books that were in the curriculum that I haven't mentioned yet are _Nonviolent Communication_ and _The Trillion-Dollar Coach_.

I read _NVC_ a year ago right before we kicked off a big feedback week as a company but I found I had already internalized this framework from the feedback topic on the _Manager Tools_ Management Trinity. The exact, step-by-step framework that Horstman recommends for delivering feedback is almost entirely based on _Nonviolent Communication_. If you've read _The Effective Manager_ you don't need to read this book if you specifically want to deliver feedback as a manager.

I still haven't read _The Trillion-Dollar Coach_ because as I stated before I think the lessons here are likely above my pay grade. That said, I'm still probably going to read it next because I'm really intrigued to read about how this guy has managed to persuade so many top executives to utilize him as a coach. That story alone is astounding enough that I want to read about it and maybe I'll pick up so useful management topics.

Finally, if I were to organize the upcoming reading list from my previous post of 40 books, I'd say the next three after _The Trillion-Dollar Coach_ would be: _Resilient Management_, _Turn the Ship Around!_, and _The Five Dysfunctions of a Team_. I'm incredibly intrigued by _Resilient Management_ because I respect Lara Hogan's work and read her blog. I've heard _Turn the Ship Around!_ more times than I can recall and I am a sucker for any book that has a military theme wrapping management concepts. _The Five Dysfunctions of a Team_ is yet another book that has been recommended so many times in my circles that I have to read it sooner rather than later.

I'm sure there are plenty of books I should have read earlier in my remaining list of 40 books and as I read them I'll be sure to update my curriculum but I'd also love to hear from you what books you've found most valuable in your education as an engineering manager.
