---
title: How to utilize VR on the web with A-Frame
description: A-Frame is a web framework by Mozilla for creating virtual reality environments in your browser.
layout: post
date: '2018-06-29 09:25:00'
tags: vr aframe javascript
subclass: post tag-test tag-content
categories: virtual-reality
navigation: 'True'
image: 'https://cdn-images-1.medium.com/max/2000/0*3jDJjY2obTdS8ahm'
article: 'True'
---

Reality-altering platforms create new challenges for user interface designers. Devices such as the Oculus Rift and Google Daydream represent the next major evolution in interface computing, and you can get started developing it today, even if you don’t have a fancy headset.

## Getting Started

The first thing you need to know about AR/VR development is what interface you want to develop on. There are lots of hardware and software configurations you can develop for, depending on your application and budget.

## Hardware Considerations

![](https://cdn-images-1.medium.com/max/2000/0*3jDJjY2obTdS8ahm)

The first thing you need to consider is what hardware you’re willing to purchase.

At the high end, dedicated AR/VR hardware usually involves a headset with some sort of glove or controller. These are also so resource-intensive that they require connection to a laptop or ideally a tower, which, in 2018, you may not have anymore, so consider that cost as well if you want to experience VR with the best performance and graphics. The top-of-the-line sets have all essentially converged at the $400 price point, which includes the [Oculus Rift](https://www.oculus.com/rift/), [HTC Vive](https://www.vive.com/us/), and the [Google Daydream](https://vr.google.com/daydream/standalonevr/). If you’re feeling super daring, the [Vive Pro](https://www.vive.com/us/product/vive-pro/) is a cool $800.

The value range gives you a headset to provide a high-fidelity view of VR on your phone. Sets like the [Samsung VR](http://www.samsung.com/global/galaxy/gear-vr/) and [Google Daydream View](https://store.google.com/us/product/google_daydream_view?hl=en-US) will get you quality headgear for your smartphone in the $50–100 range, significant savings from the dedicated sets because the headsets are essentially just goggles rather than powered devices.

Finally, we’ve got the budget headsets which provide the barebones equipment to make VR possible. Sets like [Google Cardboard](https://vr.google.com/cardboard/get-cardboard/) cost as little as $7, and provide the same functionality as your mid-range sets, but with some drawbacks.

Phone-based VR platforms aren’t wearable headsets. These devices act more like goggles, which you have to hold up to your head. From personal experience, I can tell you this gets cumbersome and you’ll find yourself switching hands every few minutes.

The other downside is that the fidelity is low. Seeing things in VR for the first time is cool, even on a Cardboard, but that quickly fades when you are looking at something like GoPro VR video on YouTube. Even though the video may be streaming at 1440 resolution or more, the lenses are not a high enough quality to project that video in a manner that makes it feel real.

## Software Considerations

![](https://cdn-images-1.medium.com/max/2000/0*gunusf9_LNexasiB)

Once you have the hardware purchased, this will dictate the level of software you can develop for.

If you have a self-powered VR set like the Oculus Rift, you can utilize any of the heavy-duty 2D/3D rendering engines like [Unreal](https://www.unrealengine.com/what-is-unreal-engine-4), [Unity](https://unity3d.com/), [Cryengine](https://www.cryengine.com/), [Lumberyard](https://aws.amazon.com/lumberyard/), [Rift](https://developer.oculus.com/pc/), and [Sketchfab](https://sketchfab.com/). These involve some serious programming and graphics knowledge in languages like C# or Python. You’ll also probably need a tower to develop on because the horsepower required for testing and debugging is considerably more when the program is uncompressed and compiled for the desktop environment.

Other VR software is aimed at a more general-purpose audience. These platforms, like [Oculus Go](https://developer.oculus.com/mobile/), [Eon](https://www.eonreality.com/), and [WebVR](http://webvr.info/), all support self-powered sets as well as mobile devices. They don’t require as much power to both develop and test, which is great if you are using your phone as your primary method of experiencing VR.

## Our focus

![](https://cdn-images-1.medium.com/max/2000/0*Xa8YaMPhWtsuz2Vb)

So with all of these platforms and devices to choose from, where do we begin? If money is no object, the Vive and the Oculus Rift are the best-in-class option and will maximize the number of software platforms you can develop on. The downside to going all in is how radically different it is to program for dedicated VR devices. If you don’t have a serious graphics or game development background, it can be pretty overwhelming.

If you are a web or mobile developer and want a nice introduction into VR development with platforms you’re already used to, I’d recommend a wearable like the Daydream View. These can be found for under $50 on Amazon but still provide the headset straps so you don’t have to hold it while you are testing and developing on them.

With the release of the web WebVR platform, you can start developing VR apps with the technologies you’re already familiar with, like CSS and JavaScript. Two of the biggest WebVR frameworks are [A-Frame](https://aframe.io/) (Mozilla) and [React 360](https://facebook.github.io/react-360/) (Facebook/Oculus). In this article, we’re going to focus on A-Frame because of its ease of use, extensive [documentation](https://aframe.io/docs/0.8.0/introduction/), and because it is based on the widely popular [three.js](https://threejs.org/) JavaScript 3D library.

## Installing A-Frame

A-Frame is incredibly easy to install, just add a script tag with the src:

    ”https://aframe.io/releases/0.8.0/aframe.min.js”

That’s it! Insert that into the <head> tag of a standard HTML file and you’re ready to get started! The entire framework is contained within this minified file, just like how you would program a web app with jQuery or React.

If you’re wondering why you insert this script in the top of the file, it’s because the VR environment cannot be loaded without the framework. With most JavaScript source files, it’s beneficial to load the DOM first to prevent the scripts from blocking the content you want to show. In this case, the framework creates its own VR DOM and must be loaded to make use of any of the tags you need to create a VR application on the web.

## Hello (Virtual) World

![](https://cdn-images-1.medium.com/max/2000/0*XKMbJv5AZpGWusxi)

So how do we work with A-Frame? It’s nearly as easy to build an A-Frame application as it is to install it:

    <a-scene>
      <a-text value=”Hello, Virtual World!” geometry=”primitive:plane”>
      </a-text>
    </a-scene>

Yup, that’s really all there is to it. Just insert that into the <body> tag of that same HTML file and you have a WebVR application! You can view our extremely simple demo app [here](https://adamconrad.dev/aframe-demo.html) with a mobile phone using Google Chrome. Impressed yet? Here’s how it works:

The <a-scene> is like the <body> tag or <i-frame> for the WebVR world. It is the container that isolates all of the 3D components and acts as a signal to the device that it needs to render this webpage in VR mode. From there, you can insert a whole set of components called primitives.

For this app, we inserted a simple <a-text> [primitive](https://aframe.io/docs/0.8.0/primitives/a-text.html). We add the properties of our primitive just like our standard HTML tags, using inline attributes. As you might guess, the value attribute tells A-Frame what text to render into the primitive.

Since we are operating in a 3D world, we need to project this text onto something. Now that we are no longer dealing with a static surface like the <body> tag, we need to tell A-Frame how we want to interact with this text primitive. The geometry provides a way to interact with text with something like a cursor by stating how we are projecting the text. In this case, we want to project our text onto another primitive component, the <a-plane>, which is simply referred to as a subset of primitives using :plane.

So how do all of these primitives work together? That’s where the Entity-Component-System architecture (ECS) comes in.

## A Brief Intro on the Entity-Component-System Architecture

![](https://cdn-images-1.medium.com/max/2000/0*K7UyFSMafpQI5FFj)

ECS is the architecture of choice for designing games and 3D environments. This architecture is heavily utilized in Unity, the popular 3D engine gaming framework. A-Frame is an ECS framework built on top of three.js, with specific support for VR devices. So let’s break down each of these words to understand the architecture:

**Entity** refers to house component objects in a scene. These are basically <div>s that are named based on the kind of component you are using. For example, <a-text> is the entity that houses our “Hello world!” text. All A-Frame primitives are some form of entities that combine a subset of components.

**Component **is the reusable chunk of data that gets added to entities to bring them to life. So if <a-text> is the HTML entity we add to our site, the value and geometry components are like the CSS to turn that entity into something meaningful.

**System** is the management system that ties components together across all of the entities they belong to within a scene. So if we again refer to the geometry component, the system for that component is what allows geometry to be shared by <a-text>, <a-triangle>, and <a-circle> primitives, controlling the shapes of the objects and how those shapes appear in space in relation to one another.

Understanding ECS is vital to conceptualizing the A-Frame API because A-Frame is essentially a giant collection of starter entities and components organized by various systems. You can then use these systems to build more complex and engaging components or entities to make your own environments richer and more interactive.

## Next Steps

We’re barely scratching the surface on what VR can do today on the web. Even though VR itself is a brand new and growing platform, truly immersive web experiences are already possible with browsers like [Supermedium](https://supermedium.com/). These VR-first browsers provide a platform to showcase the power of frameworks like A-Frame.

If this article piqued your interest in really diving into WebVR and A-Frame, A-Frame provides a guided tutorial series called [A-Frame School](https://aframe.io/aframe-school/#/1) to walk you through building your first WebVR application.

Once you think you’ve got something unique to share, there are a few places to promote your work. Supermedium offers a [directory](https://webvr.directory/) for compelling WebVR projects, as well as sites such as [WITHIN](https://vr.with.in/).

VR right now is very similar to the early days of the web: fun, expressive, and weird. There’s no need to try to aim for professional or production quality. Rather, we’re in the nascent stage where expression and creativity thrive, pushing people to test the limits of what is capable while the technology is still being fleshed out. There’s no better time to get ahead of the curve with the most bleeding-edge user interface than in AR/VR on the web with A-Frame.

*Originally published at [softwareengineeringdaily.com](https://softwareengineeringdaily.com/2018/06/29/how-to-get-started-in-vr-with-a-frame/) on June 29, 2018.*
