---
title: How to practice frontend engineering
description: What do frontend developers actually do in their day jobs? How do you get good at that?
layout: post
date: '2020-06-21 11:38:34'
tags: software-engineering frontend interviewing
subclass: post tag-test tag-content
categories: engineering
navigation: 'True'
article: 'True'
---

I'm a big fan of Will Larson's blog so his recent post on [practicing backend engineering](https://lethain.com/how-to-practice-backend-engineering/) inspired me to write an equivalent article for frontend given my previous experience in that space before moving into management.

I will say one thing that makes it easier to practice frontend engineering is that everything can happen within the browser. You don't even need a site with real data to practice! So what does frontend practice look like?

* **Writing HTML templates.** You need some sort of document to express your design's content.
* **Designing with CSS.** The look, feel, color, and experience is most uniquely expressed via the design controls within your style sheets.
* **Making pages interactive with JavaScript.** As clients have become more interactive there has been more and more reliance on frontend JavaScript frameworks to build these client-side application as quickly as possible.
* **Building infrastructure to support the above three.** Things like linting, schema generation, UI library building, lossless image compression, and asset packaging are part of the piping required to make the previous three run smoothly.

## Rebuild the design of your favorite site

The absolute easiest, no-brainer way to practice frontend is to just pick your favorite big site like Facebook, Twitter, or Stripe and re-implement the pages you see. See how close you can get to pixel-perfect renditions of your favorite sites. Nail the CSS, the JavaScript interactivity, and the semantic HTML. See what libraries and plugins they use. And the best part, **you can just view source to check all of your work**. Unlike backend engineering, the answers are right in front of you. If you're ever stuck or wondering how they did something, all of the code is available right in your browser client.

## Create a blog or write your own static site generator

So many code schools or pet projects have you building a Twitter clone or some hobby toy project you will never use again. If you build your own blog using your own static site generator, **there's you're homepage**! What a better way to practice frontend than to actually build something you will really use if for not other reason than to showcase as your homepage your frontend skills!

Make your design as fun and whacky as possible. Use fringe, bleeding-edge CSS tools. Write about your experience building the blog _on your blog_.

## Whatever project you choose, make sure you test yourself on these things

It's not enough to simply copy a design or write whatever you want. There are a few ways to separate your code from that of a professional.

* **Have it pass a bunch of audit tools.** [Google Lighthouse](https://developers.google.com/web/tools/lighthouse/#psi), [YSlow](http://yslow.org/), [aXe](https://www.deque.com/axe/), and [React Developer Tools](https://reactjs.org/blog/2018/09/10/introducing-the-react-profiler.html) are all examples of ways to ensure your code is fast, accessible, and easy for crawlers to read.
* **Make it responsive for every size.** Use the principles of [mobile-first responsive design](https://www.uxpin.com/studio/blog/a-hands-on-guide-to-mobile-first-design/) to create a site that works on any screen size. You should be able to grow and shrink your screen on your desktop (or phone) and have the site look beautiful no matter what random width you have set.
* **Run your code against linters and formatters.** Make liberal use of [Prettier](https://prettier.io/) and [ESLint](https://eslint.org/) to write well-formatted, easy-to-read code. Understand the rules and why you would use them.
* **Use a bundler and packaging tool.** Most sites use something like [Grunt](https://gruntjs.com/), [Gulp](https://gulpjs.com/), or [Webpack](https://webpack.js.org/) to efficiently package their assets for browsers to consume. Even if it might be overkill for your blog, it is still useful to get practice using it and optimizing your bulids for extreme performance.
* **Really understand every tab of Chrome/Firefox DevTools.** What would a memory leak look like? How could you tell if there's a performance problem on the client or the server? Can you spot [runtime issues](https://developers.google.com/web/tools/chrome-devtools/rendering-tools/) in painting, rendering, composing, and scripting? DevTools can give you all of these answers if you know where to look.

If you can nail these points, you will be thinking not just like a hobbyist, but a professional frontend developer.

### Throw it all away and do more with less

And when you managed to get everything passing perfectly with all of the practice above, start over with as little as possible. There's a reason I love [this website](http://motherfuckingwebsite.com/). It does _exactly_ what it says it will do and nothing else. Sometimes I think we've forgotten what the web was really intended for. It is first and foremost a document store and linking tool. We use the web to share information. The hyperlink is the glue that connects these documents to create a literal web of networked pages.

The humble `<form>` is the purest representation of interactivity between your browser and the rest of the internet. Between documents and forms, that's really all we need. Everything else is just a pretty distraction designed to keep you clicking and engaged. If we remember the roots of what we are building we tend to forget the noise of the latest SPA framework or hip new bundler and instead just focus on information architecture and user experience. Don't lose sight of that.

## Additional resources

Beyond hobby projects, I tell most people they should just build a real SaaS app and scratch their own itch. Push something real to production that you can charge customers for. This will have you not only practicing frontend, but backend engineering as well. More than you bargained for but there is truly no better way to practice your craft.

To create front ends that scale, I'd recommend a few more resources that I've found incredibly valuable over the years that stand the test of time:

* _[Front End Architecture for Design Systems](https://www.amazon.com/Frontend-Architecture-Design-Systems-Sustainable/dp/1491926783)_ - This book is the reference I use to cover architecture for even the biggest production apps I work on. Though some of the tools and technologies are dated, the tenants of architecture remain indelible.
* [MDN Web Docs](https://developer.mozilla.org/en-US/docs/Web) - The gold standard for understanding everything web technologies. This is easily the most comprehensive guide and reference for all things HTML, CSS, and JavaScript. They have tutorials, reference material for every property and function in all three specs, as well as a host of great examples and polyfills. I probably reference this at least once a week.
* [Google's Web Fundamentals](https://developers.google.com/web/fundamentals) - These may be fundamental but they are by no means easy or rudimentary. I still find plenty to learn even with more than a decade of experience. These documents always update to include the latest and greatest web technologies as well.
* [Nielsen Norman Group](https://www.nngroup.com/articles/) - There is no better team to learn user experience from. This UX firm has authored some of the best selling books on the topic are considered the foremost experts in their field.
* [Refactoring UI](https://refactoringui.com/) - This book, and their [free collection of tweets](https://refactoringui.com/) has single-handedly made me a better designer more than any other resource. If you want to create beautifully-designed sites with the tools you already use, you _have_ to check out this book and site.
