---
title: A 90 day plan for managers
description: Ninety day plans are smart and you should have one if you want to hit the ground running at a new management job.
layout: post
date: '2019-07-24 12:19:06'
tags: managing-up planning
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

Whenever I've been promoted or moved into a management position, I reach for [this book](https://www.amazon.com/First-90-Days-Strategies-Expanded/dp/1422188612) which I learned about after reading [this post](https://hbr.org/2009/03/why-the-first-100-days-matters) on how your first three months are so important for setting the right tone with your manager and the role you are signing up for.

I'm going to walk you through the 90 day plan I used for my current jump into director that is based off of this research so you can implement something like that for yourself if you are lucky to be gifted a newly-minted management position.

## Days 1-30: Evaluation

The first 90 days are important. By extension, the first 30 of those 90 days are the most important.

The reason is because first impressions are everything. When you land a new role you may not be new to your company (or maybe you are) but you are new to your team _in this role_. It is therefore vital to your success to spend these days getting to know your team(s).

**Start by setting up 1-1s up and down.** These should be with every direct, every week, for at least 30 minutes per session. No exceptions. You'll also want to make sure your 1-1 with your manager (and a skip-level with your manager's manager) is set up as well. These are necessary not only for building relationships and rapport but also for information gathering and learning the lay of the land.

**Set and get expectations.** Your first 1-1 with your manager should tell you why you're here and what is expected of you. Once you know that, you should be able to pass down and delegate those expectations to your directs. Knowing where you stand and what you need to do ensures what you believe your role is about matches what your manager (and your company) believes what your role is about.

**Assess your product.** While you assess your people you also need to assess their output: the product you are developing. Given my audience I'm going to assume you're developing some sort of digital distributed product like a SaaS web app. This means quite a few things:

1. Can you pull down, build, and run your code? Can you run the tests that come with it?
2. Once it is running, can you log in, sign up, and do the things your customers do?
3. How long are things taking? Can you navigate your app quickly?
4. Do you run into bugs? How frequently? Do you have a lot of test coverage to catch errors?
5. If you change languages, time zones, or use a screen reader, can you still use your application? What if you turn off JavaScript?
6. Are there any ways to perform unsafe activities? What if you pasted code into a feedback form or tried to make unauthorized server calls from a curl command?

These are just a few ideas I came up with off of the top of my head. The important thing is you do the digging yourself and assess what you think of the thing you are responsible for. If there's a problem, is it with the team or the product itself? Is it fixable? We'll dive into this piece after the first month.

## Days 31-60: Recommendation

Now that you know what you are dealing with your next step is to make recommendations to your manager as to what to do next. If you're in a highly autonomous organization you may only need to recommend for the sake of posterity but it is still a useful exercise to make sure your consultation is thorough.

I split my recommendations into three major buckets.

### Team recommendations

This bucket is about stack ranking your team. Some people might be put off by this but the truth is you will naturally discover who are your star players and who you could do without. Whether you want to bucket them consciously or subconsciously you're naturally going to do this anyway so it is better to accept that and get ahead of it.

Once you know that you can look at their roadmaps and how well their are at estimating their efforts. For that we move into process.

### Process recommendations

What is their process for reaching those estimates? Do they use story estimates based on the Fibonacci sequence? Do they t-shirt size bigger chunks of work? Are those terms completely foreign to your teams and just wing it? You want to know what their process is (or lack thereof) and if there are any inefficiencies with their approach.

Given you are a new manager for this group there almost certainly are some process deficiencies otherwise they wouldn't hire you to take over this team. **This may be one of the most key levers you can pull to make a great first impression.** You may not be able to hire or fire anyone right away but you can change how this current team works.

Some ideas I would introduce at this point are:

* Feedback in your 1-1s
* Adopting some kind of software development lifecycle (such as the Google Design Sprint)
* Standardizing individual and team meetings
* Setting your teams' mission, purpose, values, and goals
* Building transparency through heavy use of documentation and over-communication

Many companies lack these basics so any opportunity you tackle low hanging fruit will be easy for you to implement and also make you look really good and productive in your first few weeks. From here you might even be able to extract some practices that might be useful to groups outside of your organization which is where the last part comes in.

### Engineering recommendations

I would hazard a guess that one or more of your recommendations are not only new to your teams but new to your organization if not the entire company. These are your highest-lever recommendations that will have the biggest impact on the business. **Promote the heck out of these.**

Start by creating a proof of concept within your teams. Measure them over the next several weeks. Any sort of sizeable improvement should be lauded and promoted to other groups to try out. Continue to measure and repeat to see if your success is magnified. If so, you're onto something really great.

Tools and coding standards are great opportunities I've found in my first 60 days. I noticed our code had one tool to handle code coverage and another tool to handle code quality metrics like [cyclomatic complexity](https://en.wikipedia.org/wiki/Cyclomatic_complexity). I showed our company a tool that did both and cost less. How could you say no to that? Which leads me to the last 30 of these 90 days: actually getting things done.

## Days 61-90: Implementation

You've assessed your group. You've recommended how you need to steer the ship. Now you've actually got to grab the wheel and take charge! The first 90 days only matters if you turn ideas into reality.

I've seen people make the mistake of dragging out the assessment and recommendations (or even just the assessment) across the whole 90 days. **Without anything to actually show for your business in terms of value-add to customers is like hitting a triple in baseball only to have the next three batters strike out.** A triple is great, don't get me wrong, but you can hit all the triples you want and still never win a single game if you don't get to home plate and score some points.

In other words, **you need to take all of the word and advice you've collected and show some results**. People expect a ramp up period so no one is expecting high-output from day one. But if you really want to cement you first 90 days as something to celebrate then you need to put the pedal to the metal. As a director, I see my primary tasks manifest in 6 ways:

1. Hiring
2. Career development
3. Project management
4. Running retrospectives
5. Analyzing engineering metrics
6. Architecture/Systems design

Your recommendations should influence one of your primary tasks. If they don't then you aren't acting on them and they aren't worth executing. There is no better way to cap off the first three months of your new role with a series of wins under your belt.

Discover what your primary tasks are (which you should have discovered when you first had expectations set with your manager) and **implement your recommendations into duties of your job daily work**. Once you have your wins it get the ball rolling for the tasks you need to perform in the next 90 days and the 90 days after that. Soon enough you'll be gunning for the next promotion and you'll start the process all over again and I look forward to the next time you read this article.
