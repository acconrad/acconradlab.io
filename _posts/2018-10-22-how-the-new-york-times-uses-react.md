---
title: How the New York Times uses React
description: The New York Times is heavily invested in React for their online publication. Here's how they use it.
layout: post
date: '2018-10-22 10:28:00'
tags: javascript react data-visualization
subclass: post tag-test tag-content
categories: industry
navigation: 'True'
image: 'https://cdn-images-1.medium.com/max/2000/0*aWjIJv7-NkS_2Tcl'
article: 'True'
---

The New York Times is one of the largest and most respected media publications today. The Times must continue to innovate with the articles and content they write and also the digital experiences used to deliver award-winning content to serve their millions of subscribers. Over the past few years, the New York Times has invested heavily in both React and GraphQL to deliver a high-quality experience to their subscribers. This show dives into the scope and extent with which the New York Times refactored their website to incorporate React and GraphQL.

## The Old Times

![](https://cdn-images-1.medium.com/max/2000/0*aWjIJv7-NkS_2Tcl)

Since the 1990s, the *Times* has been on a mission to digitize their archive while simultaneously expanding their audience reach by augmenting the newspaper with a digital platform. Originally the *Times* was entirely server-side rendered with PHP. Most of the pages you’re familiar with, such as the front page, individual articles, and collections are all rendered with PHP. Developers at the *Times* began to introduce React and GraphQL (first as experiments for immersive articles) along with with other open source technologies like Node and Relay in order to adapt to developer trends.

The *Times*, unlike other businesses, had immediate concerns utilizing these new open source technologies. For starters, performance is paramount. They couldn’t afford to lose readers to other major media outlets from a site that was too slow. In addition, accessibility is a major concern. A significant portion of readers listen to the *Times* with a screen reader. It is important that React be able to not only create fast pages, but also render HTML properly to support the appropriate attributes to enable accessible viewing.

## The New Times

![](https://cdn-images-1.medium.com/max/2000/0*EPygjFcOqTdwtGN8)

Regardless of the challenges, React has already been paying dividends at the *Times*. Development teams have been able to collaborate faster and with less duplications in code via reusable components on the front end. Being able to plan around components has helped many teams centralize around one central language, rather than the collection of disparate languages and repositories in the past.

The other benefit of these components is in reusability across media platforms as well. While most businesses design for desktop and maybe web, the *Times* must also account for tablets, screen readers, Apple TV, and virtually every possible device where one could read the news (and that includes legacy devices like Blackberries and Palm Pilots). Having components that can easily be transferred across devices drastically accelerates development.

React isn’t the only catalyst for development at the *Times*. With so many data sources to pull from, it can be quite cumbersome to hook all of them up in order to deliver a seamless experience for readers. GraphQL has also been a benefit because it allows one central API to read data from on the front-end, while on the back-end lots of different services and databases can connect to GraphQL to deliver that data to end-user devices. The result is clean data retrieval in React components without having to worry about how to hook up and connect to the *Times’* collection of data sources.

## Refactoring the Times with React, GraphQL, and Relay

Originally the refactor started with GraphQL. Handling the data layer was the primary consideration with so many data sources and devices consuming them. *Times* developers considered GraphQL as well as Falcor, the API server from Netflix. In evaluating these servers, the primary concern was in all of these disparate data sources: some in BigTable, some in S3, and others in APIs like Stripe. Can you grab everything in a single query? GraphQL provided the answer.

## How requests work in GraphQL

![](https://cdn-images-1.medium.com/max/2000/0*yFuUjwD2WTxTJtEH)

GraphQL works as a wrapper for requests to a server. Developers will issue JavaScript code on the client in the form of GraphQL queries. Those queries will make a request to the “server” via a normal AJAX request. That server may actually manifest as several requests. The benefit of GraphQL, Falcor, and all of these other similar services abstract away the strategies behind the hood.

For example, each of those requests may require a different strategy to handle object-relational mapping or caching of frequently requested data (whether from a CDN, in-memory stores, or relational databases).

## Integrating React with GraphQL

React can be sprinkled into your front end to handle this data retrieval via GraphQL once the queries and fragments (which are fractions of a query) are in place. A common strategy for introducing a new front-end framework is to server-side render with your existing application, whether that’s Ruby on Rails or PHP’s Laravel, and then introduce React to manage and organize your JavaScript code. With GraphQL, you can co-locate your data with your components, allowing you to easily manage the client-server relationship, making it easier to wean off of your server-side requests.

From there, you can begin to introduce GraphQL clients like Relay and Apollo. Relay works by decorating your React components to seamlessly integrate your GraphQL queries with your rendered JSX. Co-location becomes a design pattern when working with Relay.

Apollo is another GraphQL client that is framework agnostic. While both work with React, only Apollo can work with other JavaScript frameworks. Relay is also much more opinionated in terms of how you structure your application, while Apollo has a variety of options that range from minimalist integration to a complete client build.

The real benefit of a GraphQL client lies in binding the data to to your React components. While GraphQL does a great job of abstracting away all of the different data sources you want to fetch from, it doesn’t do the best job when it returns all of that varied data. That’s where these clients come in. Tools like Relay and Apollo will take in all of that data and make it easy to consume for your React components. They are meant to be the front-end frameworks for hooking up your UI to your data.

## Why GraphQL won over Falcor

![](https://cdn-images-1.medium.com/max/2048/0*R3RyK0Db8uS39Fm_)

With all of this complexity around these data aggregators, how did the *Times* ultimately decide to use GraphQL? “I think the big appealing thing about GraphQL is that it was a spec,” said James Lawrie, one of the software engineers working at the *Times*. By having a reference implementation, it meant that GraphQL was easy to integrate with all of the various languages and services that the *New York Times* utilizes.

## Why Apollo won over Relay

Relay was the early choice for the *Times* to bring data back from GraphQL to their 400+ React components. Unfortunately, limitations in flexibility and integration with React Router made it difficult for the *Times* to move forward with future versions of Relay. Because Apollo offered many of the same features as Relay, but with the additional flexibility of implementation, it made it easy for the *Times* to ultimately switch and settle on Apollo over Relay for their GraphQL client.

## Wrapping up and the future of media

![](https://cdn-images-1.medium.com/max/2048/0*zfRkIBZD1XkDISS3)

Now that the *New York Times* is fully committed to the React stack, the future is bright for future media. With React, the *Times* has additional flexibility to venture into other devices quite easily. With React Native, the *Times* can now deliver a native mobile experiences utilizing many of the same components they use on the web. The *Times* also has a 360 video and VR department, which can now take advantage of React 360, the AR/VR platform by Facebook, and the newest extension of the React ecosystem. With so many avenues to choose from, React makes it possible for the *New York Times* to stay competitive against the likes of the *Washington Post *and the *LA Times*. While the *Times* started as a print newspaper, it is now ushering in a new era of being a rich, fully-immersive digital media company, much of that thanks to React and GraphQL.

*Originally published at [softwareengineeringdaily.com](https://softwareengineeringdaily.com/2018/10/22/react-and-graphql-at-the-nytimes/) on October 22, 2018.*
