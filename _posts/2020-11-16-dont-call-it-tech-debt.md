---
title: Don't call it tech debt
description: Tech debt as a term is harmful. In fact, it should have never been invented.
layout: post
date: '2020-11-16 23:31:00'
tags: tech-debt productivity
subclass: post tag-test tag-content
categories: programming
navigation: 'True'
article: 'True'
---

I think we, as a profession, shot ourselves in the foot when we coined the term _tech debt_. This [recent post](https://daverupert.com/2020/11/technical-debt-as-a-lack-of-understanding/) (though my browser's search bar found no less than 3 recent articles I read on the subject in Hacker News) got me thinking that the term tech debt is an exercise in bad marketing.

Debt is evil. It's the stuff that can [live on](https://money.com/student-loan-forgiveness-death-discharge/) longer than you do. There's never been an instance where calling something "debt" elicited a positive response. So why do we tell our Product Managers we have to _tackle_ tech debt?

Is it to create urgency? I hope not. Everyone believes their priorities are the most important priorities. So an engineer's tech debt will never seem more important than a PM's product backlog.

Is it to create gravity around the seriousness of the work involved? Then why is it that PMs always bucket "20% time" for making room for tech debt that is never fully paid off?

I loved this analogy I saw on Hacker News about this concept:

> If you run a commercial kitchen and you only ever cook food, because selling cooked food is your business -- if you never clean the dishes, never scrape the grill, never organize the freezer -- the health inspector will shut your shit down pretty quickly.
>
> Software, on the other hand, doesn't have health inspectors. It has kitchen staff who become more alarmed over time at the state of the kitchen they're working in every day, and if nothing is done about it, there will come a point where the kitchen starts failing to produce edible meals.
>
> Generally, you can either convince decision makers that cleaning the kitchen is more profitable in the long run or you can dust off your resume and get out before it burns down.

On the flip side, if you ever have the luxury of getting ahead of your technical debt then you can contribute things called [technical credits](https://www.stillbreathing.co.uk/2016/10/13/technical-credit). As you can imagine, technical credits are the opposite of tech debt: things that _pay dividends_ to your code rather than incur a cost and drag it down. [Progressive enhancement](https://gomakethings.com/progressive-enhancement-graceful-degradation-and-asynchronously-loading-css/) is touted as an example of technical credit.

Marrying these two concepts, I actually think a way around the horrible marketing of tech debt can be resolved with Agile terminology that is already familiar to our product-positive counterparts: **engineering-driving work.**

## Engineering-driven work over tech debt

At the end of the day, whether they be debt or credit, the work engineers want to accomplish to make their code better is something personal to them and them alone. Similarly, your product team has their own vision of what your code can become for your customers. So if we segment on the _owner_ instead of the _outcome_ we remove the negative connotations of the work involved.

On my teams, we make this distinction painfully simple in Jira:

1. **Was the work engineering-driven?** Create a task.
2. **Was the work product-driven?** Create a story; use sub-tasks to create the work the engineers will do to complete that story.

Everything lives on the same playing field and is up for grabs to assign priority. So rather than bucket 20% time for debt, if all engineering-driven tasks (whether they be credits, debts, or something else) are the most important work to be done this week, they take up 100% of the sprint. Similarly, if product's work is the most important, it can dominate or completely own a sprint.

The important thing is that **the owner of the ticket sells why their work should be as important as it is**. If you can't sell why a critical security upgrade is vital to the health of your application then you need to learn how to sell and influence others. But then again, you might think that switching from Formik to React Hook Forms would be awesome but if it's based on personal bias and no tangible value creation for developer productivity you're dead on arrival.

So I encourage all of my engineers to read sales books. Learn how to sell and influence others. Convincing someone that what you want to do matters is a much better message than starting things off on the wrong foot by calling your work debt that needs to be repaid. Debt makes people wince. Creating value makes people smile. Paying down tech debt adds value so lead with that and focus on why your work is important instead of giving it the Scarlet Letter from the onset.
