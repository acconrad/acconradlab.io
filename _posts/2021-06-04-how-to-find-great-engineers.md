---
title: How to find great engineers
description: A playbook for how to build an engineering team.
layout: post
date: '2021-06-04 23:29:00'
tags: team-building hiring playbooks
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

One of the hardest jobs as an engineering manager is to hire and [build](/blog/building-teams) a great team. The other is to retain and engage them (but that’s for another day). **The purpose of this article is to walk you through step-by-step how to find and hire great engineering talent**.

## Top of the funnel

* **Create a Monthly Engagement Calendar**. Hiring cycles often happen at the start of the month (and year) so __do most of your sourcing at the beginning of the month__.
* **Reach out to DEVELOPER social networks**. No - not LinkedIn. Not Facebook. Not Instagram. Here’s some that have worked well:
  * _Hacker News “Who is Hiring” thread_. Post every month on the first business day. The challenge is you can only do 1 posting per company. So only have 1 engineering manager post for your company.
  * _Reddit subs for your programming language/framework_. r/technology or r/programming will get you nowhere. But if you’re hiring a Scala developer, r/scala allows you to post job threads.
  * _Slack communities_. Hiring JavaScript developers? There’s a [Slack community for that](https://babeljs.slack.com/).
* **Find language/framework-specific channels**. Still not getting any interest from the first two steps? Here’s an example of where you can source JavaScript developers from:
  * For Node:
    * https://discordapp.com/invite/vUsrbjd
    * https://dev.to/t/node
    * https://hashnode.com/n/nodejs
    * https://spectrum.chat/node?tab=posts
  * For React:
    * https://discordapp.com/invite/0ZcbPKXt5bZjGY5n
    * https://dev.to/t/react
    * https://hashnode.com/n/reactjs
    * https://spectrum.chat/react?tab=posts
* **Do the work yourself**. Your recruiting team, if you're lucky enough to have one, is probably great. And they need your help! Their job is to supplement your hiring, _not do it all for you_.

> The best way to convince developers to join is to be genuinely interested in them and convince them that the role you are hiring for is __perfect for them__.

## First contact

* **Set up a 30-minute phone call ASAP**. Since you’ve posted to so many communities, it can be difficult to respond and check in on them. _Encourage folks to email you to schedule a time to chat on the phone and engage with them personally_.
* **Keep the phone call informal**. You really only need to focus on 4 things:
  * What your company is about
  * What _your team_ is about and how it fits into your company's grand vision.
  * Learn more about the candidate, what they’re looking for, and why they’re interested in you
  * A brief overview of the hiring process (this leads them into the next step by encouraging them to start your interview process).

## Interview unconventionally

You’ve invested a lot of effort into hiring. You may not have implemented all of those strategies but **the most important thing about your interview process should be to stand out from the crowd and wow your candidates with a pleasant & fair interview experience**.

**People _loathe_ contrived interview problems** (e.g. reverse a linked list in memory, traverse a 2D array in a spiral). They especially hate when those problems have nothing to do with the actual work they will be doing. So don’t ask them!

**People _love_ practical questions that test the skills they use**. Interviewing someone on the UI layer? Ask them to implement a mockup. Need an architect? Have them design Twitter. If you engage candidates with problems they will really encounter, you’ll learn their true capabilities and they, in return, will feel properly evaluated (read: happy with the interview experience).

**People are _impressed_ by non-traditional approaches**. I've used tiny take-home pull request exams to engage an often overlooked part of every developer’s core job: reading code. Most interviews require developers to write code, but few ask them to read code.

## Related articles

* https://slack.engineering/refactoring-backend-engineering-hiring-at-slack-b53b1e0e7a3c
* https://angel.co/blog/how-to-hire-world-class-engineers
* https://www.freecodecamp.org/news/hiring-right-an-engineers-perspective-on-tech-recruiting-7ee187ded22d/
* https://triplebyte.com/blog/companies/outcompete-faang-hiring-engineers
* https://www.intercom.com/blog/how-we-hire-engineers-part-1/
* https://www.intercom.com/blog/how-we-hire-engineers-part-2-culture-contribution/
* https://www.intercom.com/blog/inbound-recruiting-engineers/
