---
title: My most referenced articles
description: These are the online articles I refer back to the most.
layout: post
date: '2019-10-08 22:51:57'
tags: reference programming
subclass: post tag-test tag-content
categories: programming
navigation: 'True'
article: 'True'
---

I've noticed I keep referencing back to certain articles over and over again. Here's a list of articles or videos I find myself constantly referring to even way after it was first published:

* [Back to basics - SOLID](https://thoughtbot.com/blog/back-to-basics-solid) - This article is just so good at explaining some of the more useful design patterns you can use in web development. I probably refer to this article _at least_ twice a month.
* [Magic numbers](https://en.wikipedia.org/wiki/Magic_number_(programming)) - I feel like when I ask "is that a magic number?" people look at me like I have 3 heads. So I keep having to pull this one to remind people not to use magic numbers and yes they are a real thing.
* [Questions for our first 1:1](https://larahogan.me/blog/first-one-on-one-questions/) - For new engineering managers under my wing or whenever I run out of things to ask I keep coming back to this article because it's great advice (and it has a Google Doc template which I appreciate).
* [How to start a startup](https://www.youtube.com/channel/UCxIJaCMEptJjxmmQgGFsnCg) - I think I've watched this, no joke, 5 times for all of the lectures. I want this drilled into my brain forever.
* [The Future of Programming](https://www.youtube.com/watch?v=ecIWPzGEbFc) - Another talk I've watched so many times I hope to someday memorize it.
* [Systems Design Interview series](https://www.youtube.com/watch?v=quLrc3PbuIw&list=PLMCXHnjXnTnvo6alSjVkgxV-VH6EPyvoX) - I keep watching these because I firmly believe as an engineering manager this is one of those things that is still technical that I need to master.
* [Sizing engineering teams](https://lethain.com/sizing-engineering-teams/) - If I ever need to think about how I'm going to restructure my teams, this is the article I reference. Every. Single. Time.
* [The leadership library for engineers](https://leadership-library.dev/The-Leadership-Library-for-Engineers-c3a6bf9482a74fffa5b8c0e85ea5014a) - I'm an engineering leader. And I love to learn. Here's enough learning to melt your face.
* [Awesome Elixir GitHub project](https://github.com/h4cc/awesome-elixir) - I write a lot of Elixir for fun. All of my side projects use it. If I need a plugin or a resource, I start here first.
* [Motherfucking Website](http://motherfuckingwebsite.com/) - When people want an example of what perfect looks like, I show them this. Simple, responsive, accessible, and fast. What more do you _actually_ want? Sometimes I think I should just make my website look like this because it's what I want to do even though I know I sort of need to do my own blog design otherwise people wouldn't think I have a front end background. But I can dream I suppose...
