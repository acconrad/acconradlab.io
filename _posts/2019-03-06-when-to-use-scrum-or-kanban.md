---
title: When to use Scrum or Kanban
description: These two Agile methodologies are highly contested for project management. Which do you use and why?
layout: post
date: '2019-03-06 12:05:49'
tags: project-management agile
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

I used to believe Kanban was the solution to all project management problems. Kanban, if you don't know, is a Japanese word that means "just-in-time" that was developed by Toyota for lean manufacturing. For software development it just means that you continuously groom tickets for priority and don't create rigid structures around sprints (like Scrum).

This made a lot of sense to me because I work in web development where the web has no gates on pushing changes and companies continue to innovate faster and faster. How could any other methodology be any better? It turns out I was wrong and you may be wrong too if you think that Scrum has to be inferior in all scenarios. I now use both methodologies for different purposes.

## When to use Kanban

Kanban is still great. The central tenants of Kanban are:

* Visualizing your work with a progress board
* Limiting work in progress to reduce time to completion
* Maximizing developer flow
* Continuous delivery and therefore continuous change

These objectives work for certain products and teams. I will use Kanban when:

* **We're building a web product.** Nothing is federated and you can push to the web at any time.
* **The first version of the product is already live.** It's easier to think in terms of continuous change when you are going from 1 to _n_ instead of 0 to 1.
* **We don't have rigid deadlines.** If everything is continuous then each push to production is in essence a new version of the product for your customers to gain value from. If you don't need to "release" a collection of features then each push adds value.

If you work at a modern web company with a SaaS product you'd probably stand to benefit from the Kanban framework. Living in this bubble I forgot that many software companies don't fit this mould. Lots of teams operate in a different way.

## When to use Scrum

In comparison, Scrum's primary objectives include:

* Commitment to deliver your work through sprint intervals
* Continuous learning and refinement with each sprint
* Ceremony and roles to increase teamwork
* Velocity defines refinement strategies
* Change comes between sprints

Here are a few scenarios where Scrum will be superior to Kanban:

* **We're building a mobile product.** Mobile is federated by app store releases. As wild as this sounds, it can actually be detrimental to release mobile software too frequently. The app stores think your software is to buggy and your customers hate to keep downloading your new releases.
* **We need to build a concrete MVP.** When you have a clear 0 to 1 moment (i.e. going from nothing to something) it's helpful to have a north star separated by a series of sprints. There is no continuous delivery element here because no delivery prior to the MVP adds value to your customers.
* **We have a deadline we have to work back from.** This is particularly relevant for developers in the gaming industry. You need to increase velocity and you have to reach a deadline with enough buffer to make sure you don't miss.

At Indigo we use Scrum for our mobile teams because we know we don't want to push to the app store more frequently than once every two weeks. This aligns nicely with the common two week sprint cadence for the Scrum framework.

## When to blend both

You can take elements of both and create a blend of the two frameworks. You can organize projects into sprints but make sure you limit work in progress. You can also use Kanban boards with set releases with changes mid sprint. Ultimately, you can do whatever you want as long as **it works for your team**. I don't know of other modern frameworks for project management so if I'm missing one that has you captivated please [let me know](https://twitter.com/theadamconrad).
