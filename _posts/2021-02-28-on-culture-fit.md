---
title: On culture fit
description: Culture fit is a hiring practice that doesn't scale with team size. Some thoughts on how you can find great people in an inclusive way.
layout: post
date: '2021-02-28 11:04:00'
tags: management hiring anti-patterns
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

I was asked during a recent [panel on Clubhouse](https://www.linkedin.com/events/engineeringmanagementamawithsen6770107754863570944/) about assessing culture fit. What was once an innocuous term for "people who aren't terrible human beings" has quickly become a trigger for exclusivity, gate-keeping, and homogeneity. Here are my thoughts on how things got out of hand with this hiring practice.

## The benefits of finding a cultural fit

I can understand where culture fit is seen as a necessary tactic at some places. At small startups, companies are looking for chemistry between the founders and these early-stage employees. I get it; all of you are going to spend a lot of time together taking a big bet that the thing you are building is going to change the world. I was the first employee at a startup and I spent many nights working with the founders until 4am and it all started with a poker game.

For these early companies, you're looking for employees to **define your culture** which is largely based on the principles that the founders already hold dear. So if you, as founders, have a semblance of culture, you're looking to ensure the keys to your initial success stay intact when expanding to hired employees.

On the surface, this makes sense. Many years later I'm looking back on some of these interviews with these founders and I'm left wondering if the toxic elements germinate even at the smallest of startups.

### A culture of homogeneity

I remember a very peculiar interview I had with an early-stage company. We did quite a few technical rounds followed by a round of beers at a local bar. I knew it was about getting to know the founders but in retrospect, it was a sign of being part of the group.

Was I a techie? Did I wear the right hoodies and jeans? Did I know how to hold down a beer (or five) and still gun sling code like a pro? Was I willing to do whatever it took to get the job done in a work-hard-play-hard atmosphere?

This is where the original idea of finding chemistry ballooned into a flawed practice I see at many companies now, both large and small.

## How to fix the culture fit interview

My answer, by the way, on the panel was to completely do away with traditional culture fit interviews. I think they are a stain on the tech industry that we will look back on in 5-10 years as a "what were we thinking?" kind of clarity. **Culture fit interviews as you see them today promote the opposite of Diversity, Equity, and Inclusion (DEI) - Homogeneity, Inequity, and Exclusion (HIE)**.

### Seek additive perspectives rather than reductive ones

Culture fit can be a tactic to find a *homogenous* group of people. It's a test to determine if people are like you. A common refrain here is "can I see myself having a beer with this person after work?" So ask yourself:

* Why do we need to have beers? Why not tea?
* Why do we need to hang out after work? Isn't a coworker relationship during normal hours enough?
* Why do we need to be out somewhere together? Does that diminish the contributions of my remote colleagues?

Diversity is about different perspectives from different backgrounds. And it's not just about demographics like race and socioeconomic background. The kinds of companies you've worked for, whether big or small, all bring new perspectives about how to run and scale an organization.

The kinds of jobs your team has had in the past contribute as well: "purebred" software engineers from CS schools offer a completely different window into building software than folks who never went to college and spent the last 5 years working in retail who just recently transitioned into a software job.

A good interview asks "**how can this person's perspective _add_ to our existing culture**"? There's no specific question that will give you this answer. You just have to remove the bias out of your assessment which takes a lot of hard work and conscious effort.

### Do the work of getting more people in the door

Traditional culture fit assessments are *inequitable* because it is a subjective evaluation with an unspoken set of rules. There is no way to get a correct answer to a culture-fit interview. That is because every company has a different way to "grade" that question which makes it impossible to provide a way to improve or level the playing field for those that need a boost to balance their technical capabilities and job qualifications.

Meanwhile, equity is directly aligned with providing a boost for those who have been systematically left out of the conversation. If you're used to talking to white MIT grads from Boston or Silicon Valley then **you need to explicitly devote more time and attention to the overlooked BIPOC candidates without the same signaling credentials**.

You're trying to explicitly create room and opportunity for those that are often overlooked. If they already aren't part of the in-group, how do you expect to help them with an in-group culture fit question about grinding exams back at Stanford before a kegger?

### And then do the work of getting them in the club

Finally, culture fit interviews can be *exclusive* because those unwritten rules I spoke of earlier are designed to weed out people who don't fit the mold of the [prototypical](https://www.imdb.com/title/tt0151804/) [engineer](https://www.hbo.com/silicon-valley) or any other kind of employee you're used to seeing. The question is all about showing signals that you're part of the group.

Even without a culture fit question, people will assess culture on first impressions. Have you ever grimaced at an engineer coming into an interview in a suit? Made a snap judgment that the person might be too uptight or corporate? We've all subjected others to unfair biases and it doesn't make things better to put someone through an entire interview centered around this concept.

**Inclusive interviews are about bringing everyone into the conversation without adding more reasons to deny someone a seat at the table**. It's about the opportunity for people in a field that is dominated by white males and has been this way for decades. It is our responsibility and duty to represent and include all kinds of people in a way that better reflects the diversity of the people in our nation and in our world.

So what does a _good_ culture fit interview look like when these principles are introduced?

* **It is open to a wider candidacy pool in the first place.** Step one is making sure this interview is asked to more than just white males. Ensuring you remove bias in your job descriptions and in your phone screen assessments ensures that more candidates from more diverse backgrounds land the on-site interview in the first place so they have the opportunity to answer the culture fit interview at all.
* **It asks the candidate to share how their experience is relevant in solving the problems you are facing.** Could your startup benefit from someone at a big company who can deal with your fast growth and scalability issues even if they've never worked for a small team before?
* **It values the universal qualities of exceptional coworkers.** I'd be hard-pressed to find a hiring manager who wouldn't value adding an employee who works hard, strives to do the right thing for customers, and brings positive, uplifting energy to their coworkers. Can you look for those traits in a wider demographic of candidates?
* **It is evaluated on no right answers.** This is the biggest trap of the culture fit interview today is that there is one right answer that signals for culture fit. Instead, look to see if an unconventional answer complements your culture and adds to it rather than fits in an "acceptable" list of responses.
