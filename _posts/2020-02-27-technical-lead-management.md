---
title: Technical Lead Management 101 or How to Try Out Management
description: Technical engineering managers combine the technical prowess of tech leads with a comfortable, limited scope of engineering management.
layout: post
date: '2020-02-27 23:00:19'
tags: engineering-management tech-lead
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

> I wrote this for my tech leads who needed a crash course in engineering management. The advice you see in this article is the advice I used to help level up my team leads.

At some point in your technical career you'll have to make a choice: stay on the individual contributor (IC) track or move into management. Many seem to believe that the latter is the only way to move up in your career.

It's not!

Plenty of people want to (and deserve) to grow their careers as software developers without having to manage others. **Regardless, at some point you'll have to test the waters to see which is right for you.** To ease this transition and minimize the risk of inundating developers with too much change, we've created a hybrid role called _Technical Lead Manager_. In this article, we're going to describe what this role is, why it is needed, and the basics for becoming a technical lead with management responsibilities.

## What is a Technical Lead Manager?

While there is no official definition of this role, in my mind **a Technical Lead Manager (TLM) is a tech lead with minimal engineering management responsibilities.** More specifically, we define "minimal" to mean **no more than 3 direct reports.** If you're brand new to engineering management, a _direct report_ means someone (e.g. a software engineer) who reports to you in your company's organizational hierarchy. In other words, you write code _and_ are the boss of up to 3 people.

### Why not call them a Junior/Associate Engineering Manager?

You could, but the distinction here is that **we expect an engineer with this role to still perform his/her technical duties (i.e. coding, debugging, code review), albeit in a more limited capacity.** Without the _Technical_ adjective, we leave the opportunity to let those technical skills atrophy in certain organizations.

### Why the need to combine both duties? Won't that overwhelm the individual?

At some point in your career, you'll be faced with the decision of considering roles in management. In many organizations, the leap to engineering manager is a large one. Make no mistake: **engineering management is a completely different skillset from software engineering and should be considered hitting the "reset button" on your skills.** With such a stark contrast in roles and responsibilities, the transition to management at the most junior levels could dissuade people from considering this career move in the first place.

Nonetheless, leaders need to groom the next generation of leaders underneath them. To ease the transition and best prepare their future leaders with the necessary skills, consider looking at this hybrid role.

## What does a Technical Lead Manager do?

At its core, a Technical Lead Manager codes _and_ manages the career of up to 3 direct reports. Since management is generally the newer of the two skills, we won't focus too much on how to develop the technical skills. For further reading on this topic, consider [this primer](https://www.getclockwise.com/blog/the-new-tech-leads-survival-guide) if you've never been a tech lead before.

Instead, let's dive into the core skills you're going to have to build from scratch.

### The Four Dials of Engineering Management

When I first became an engineering manager, my CTO gave me this analogy to think of engineering management skills _like knobs on a guitar amp_. For EMs, there are four dials: **people, process, product, and prowess**.

Each dial is turned up somewhere between 0 and 10 depending on the organization you work in. Some companies highly value technical prowess, while other companies see EMs as people managers with a previous technical background. Regardless of your company's organizational design, you're going to want to know what all four knobs are so you can dial in the exact volume of each skill.

#### People

Let me cut right to the chase: **successful engineering management is all about maximizing the effectiveness of your people.** If you take nothing else away from this guide, know that _simply caring about your people will get you 80% of the way there_. To effectively care for your people, this dial itself has 4 core aspects:

##### One-on-ones (O3s)

These are the bread and butter of effective management. **Consider them the most sacred time of your entire week.** O3s build trust, shared understanding, and rapport. From all of the materials I've read on this subject, here's a formula for the most effective O3s:

* Have them every week with all of your directs for 30 minutes.
* If you have a conflict, move your O3 but _never cancel it_.
* Start each O3 with "how are you?" or "what's on your mind?"
* At first, allot all 30 minutes for your directs to say whatever is on their agenda. They may have a lot on their mind in getting to know you so indulge them.
* At some point (around a month in), you'll want to shift it to be 15 minutes for their agenda followed by 15 minutes for your agenda.
* Finally, another month or so in, you'll want to trim a few minutes off of your agenda to bake in coaching & goal setting for your directs' career development.

I still use this formula for all of my O3s and it has served me very well. **Be an [active listener](https://www.youtube.com/watch?v=saXfavo1OQo) during their agenda.** If you're looking for what to ask during your agenda, I've referred to [this list](https://larahogan.me/blog/first-one-on-one-questions/) for seminal O3 questions to ask your direct reports. For coaching and goal setting, keep scrolling. But first, you're going to want to make feedback a central tenant of your agenda.

##### Feedback

People crave feedback. **Being an effective manager means being an astute observer.** There are lots of ways of observing your team without conjuring up an image of the nosey micromanager. You're  probably doing a few of these already:

* Active listening in meetings
* Addressing blockers in stand-ups
* Reviewing code
* Pair programming

Plus countless more. The difference now is you're not just observing with the lens of a software engineer but also as a manager with a chance to provide feedback.

**Feedback is about behavior.** Let me repeat myself: feedback is _not_ about changing people but about changing behaviors. "You're bad at your job" is not feedback. It is an ad hominem attack.

Feedback is either about _encouraging a behavior (positive)_ or _stopping a behavior (constructive)_. Feedback, too, has a formula for being given effectively. The one I like to use is based on [nonviolent communication](https://en.wikipedia.org/wiki/Nonviolent_Communication). The formula, like seemingly all of my formulas, occurs in four steps:

1. **Describe the current behavior.** Example: "When you test your code..."
2. **Describe the outcome of that behavior.** Example: "...it increases the reliability of our product."
3. **Describe the effect that behavior has on you or others.** Example: "This instills trust in our customers that we build software that works."
4. **If the feedback is positive, use encouraging language to reinforce the behavior. If the feedback is constructive, describe how you would like the behavior changed in the future.** Example: "Keep it up!"

Stitching this together, our positive feedback example looks like this:

> When you test your code, it increases the reliability of our code. This instills trust in our customers that we build software that works. Keep it up!

Conversely, an example of constructive feedback would look very similar, with the largest difference being the final fourth step:

> When you are late to meetings, it reduces the time we have to get through all of our agenda items. This inconveniences the team and reduces their trust in you to be reliable and timely. Please be on time from now on, thanks!

Study this last example. There are a few things at play here that you should pay attention to:

* The format for both positive and constructive feedback is the same. This conditions your team to see all feedback as a focus on behavior and not a personal attack on who they fundamentally are as human beings. If you keep all feedback formats the same, they won't cower at the thought of receiving negative feedback, and they won't have to worry about whether you're going to praise them or bring them down.
* For constructive feedback, it's about the future, not the past. We can't change the past, so there is no reason to dwell on it. Instead, accept that the undesired behavior happened; don't dwell on it or berate your teammates for their shortcomings. All we can do is stop undesired behavior from happening again by steering the rudder in the right direction.

If you follow these guidelines, your team will think of all feedback, positive or constructive, as a window into effective reinforcement or behavior change.

**Finally, I would recommend you wait to introduce feedback until you've had about 8 O3s (and start with only positive feedback).** You need enough time to build rapport with your directs before you begin to give them feedback. And even then, you want it to start positive. Add on another 8 weeks (16 in total) before introducing constructive feedback so they are used to your communication style.

##### Coaching

Once you are having regular meetings with your directs and giving them regular feedback, it's time to start coaching them in their careers. Coaching can take many forms, some of which I've already mentioned before:

* Pair programming
* Code review
* Mentoring
* Career development plans

I use all of these tools regularly and you should feel free to use all of them. **The point of coaching is to get more out of your directs.**

O3s build trust. Feedback is the transparency that results from that trust. Once you have transparency, you now know what your directs are capable of. To be a next-level manager, you need to strive to extract more value out of your directs by pushing them to be their best selves.

How do you get more out of your directs? **Reinforce their strengths and bring up their weaknesses.**

Do they write amazing tests? Have them review others' code for sufficient test coverage or run a workshop on effective testing strategies.

Conversely, if they constantly forget to test their code, consider introducing a pull request checklist to remind them to include tests and enforce that all code comes with tests to merge code into production.

You're going to have to get creative at this step because there are [boundless ways to coach someone](https://knowyourteam.com/blog/2020/02/19/how-to-coach-employees-ask-these-1-on-1-meeting-questions/) through their career. With all of this new work to do, how are you going to find time to still code?

##### Delegation

The only way to find time for everything is to lean on your directs. **Delegation is the skill of pushing down responsibility to others.** I like to think of this with an analogy:

Imagine the CEO of your company. He or she is likely the only person in the company with the knowledge of the entire ecosystem of how your company operates. They're also likely the only person who can both sell the product to investors and create a grand vision of how your company can wow your customers. The value of these two skills let's say is $500/hour.

Now imagine a paralegal at your company. Paralegals often review documents, perform research, and write legal papers on behalf of the lawyers they work with. Let's say these skills are valued at $35/hour.

Your CEO may have a law background and is quite capable of performing the duties of the paralegal. But if the CEO can do the legal work _and_ raise money from investors, while the paralegal can only do the legal work, why would you have the CEO ever do legal work? It's in the best interest of the company for the CEO to focus solely on the work he/she is uniquely suited to perform.

**In your case, you are uniquely suited to manage and tech lead. If you are doing _anything_ outside of those two things, you are reducing the effectiveness of your position and should delegate that responsibility to others.**

Delegation is the hardest part about being a TLM (or any manager for that matter). You are naturally going to want to do what you're comfortable doing: coding. Fortunately for you, coding is no longer your highest value skill.

**At every possible moment, you should evaluate whether the task you are performing could be done by someone else.** If it easily can, document the work involved and share it with your team to perform. If it cannot, then it's probably something you need to continue to do yourself. This self-evaluation is the basis for the next dial of engineering management: process.

#### Process

If engineering management centers around people, then Process is the mechanism for making the People dial as efficient as possible.

**Process is all about documentation.** If you have a repeatable, verifiable way of doing something, you should document it for your team or others to perform. Fun fact: this post is a manifestation of my process. Teaching others how to be engineering managers is a process. Reading this post is the process by which engineering managers can learn about how to manage for the first time. I know this is all getting pretty meta, so let's focus on a few ways to institute process:

##### Leverage a wiki tool

Tools like Confluence or GitHub Wikis are a great way to codify processes. The question I always ask myself is:

> Do other people need to know this and will forget it? I should write this down in the wiki.

I mean all of the time. Even the most seemingly mundane tasks are documented in our Confluence at work. **Never assume a task or process is too dumb to spell out in your documentation tool.** As we gain experience in the field we assume a lot of things about our work. Not everyone has the same experiences or background as us, so what may seem trivial to you could be quite complex to others.

That's why it's always a safe bet to write it down in the docs.

At worst, you took some time to become a better writer and communicator. At best, your documentation with the whole company and adds value to everyone around you. So why wouldn't you write it down?

##### Use it as a teaching moment

Looking for ways to incorporate coaching into your workday? Describe a process to a peer or direct. Gather feedback on your process: what works? What doesn't? What could be improved? Processes should help make everyone around you more effective. If you can teach something to others, you're building your skills as a coach and adding value to the organization.

##### Simplify your life

The beauty of processes is once it is set in stone, you pretty much never need to think about it again. Processes turn algorithmically intense into the automatic.

For example, if you didn't have a process for running effective meetings, you probably spent a lot of cognitive effort in figuring out how to set the agenda. Once you have a process, though, planning meetings become quite rudimentary and painless. You have a plan for how you're going to get through them.

Processes make your life easier, and your life is already challenging enough.

#### Product

One of the cool parts of moving into management is the cross-disciplinary skills you'll develop. **Get ready to become best friends with your product manager.**

I naturally consider myself to be a product-minded engineer. If this doesn't come naturally to you, it will be thrust upon you whether you like it or not. As the engineering manager, **you are now the liaison to other departments about the technical capabilities of your team.** Think of yourself as the head consultant and advisor to the rest of the company about your team.

What is the makeup of the skills on your team: are they front-end or back-end focused (or full-stack)? Who loves tackling bug tickets and who loves fixing tech debt? How will you balance tech debt and bugs with new features? Who decides whether a bug is more important than a feature? These and countless other questions will arise when you turn up the Product dial.

**Your best defense in dialing up Product is to be on the offensive, and that means getting to know your product and your product managers.** Just like your directs, set up O3s with your product manager. Get to know them and what makes them tick. Build trust. The only way you can build great software without you and your PM at each other's throats is to create an authentic relationship.

**There will usually be a healthy tension between product and engineering and that's okay.** Sometime you'll want to address mounting tech debt in your end-to-end tests. Meanwhile, your PM will wonder why Widget X wasn't delivered yesterday. You will each have reasonable justifications for why your stance is correct. Guess what? At the end of the day, you're going to have to come to a consensus. **See my earlier points about active listening, nonviolent communication, and building trust.**

Oh, and by the way, this extends far beyond Product. You'll have conflicts to resolve with architecture, design, marketing, sales, legal, and virtually anyone who comes in contact with your team: _including the team itself_. One engineer will say that Vim is the best IDE. Another will say it's Emacs. Spaces vs tabs. Linux vs Mac. It's pronounced "gif" (no it's "jif"). You name it, someone will have an opposing opinion.

**You are mediator first, moderator second, and arbiter last.**

What I mean by this is in any conflict, your first job is to _help each side empathize with the other_. Empathy, often confused with sympathy (which is about feeling someone else's distress), is about walking a mile in another person's shoes. Can you get Person A to see where Person B is coming from, and vice versa? If not, move into the moderation phase.

Moderation is about guiding the discussion to help the two parties address their concerns and air their grievances. This guidance should hopefully lead to automatic mediation: by moderating the discussion, you're allowing both parties to see where the other is coming from in a way you couldn't immediately achieve in your first pass. But what if consensus should not be reached?

As a last resort, it's time to be the judge, the jury, and the executioner. Ideally, people come to their conclusions themselves. The advice I've always latched onto is

> Your best ideas seem like they never came from you at all and instead came from the person you were trying to convince in the first place

Sometimes you don't have that luxury. In that case, you have to make a call for others. An often-quoted maxim of the military is **a poor decision is generally better than indecision.** Dwelling on whether you are making the right call in this argument and coming up with nothing is worse than just picking one side.

This Product dial morphed into conflict resolution but is an important aspect of the natural tension that will arise when you are no longer interacting solely within the discipline of engineering.

#### Prowess

Finally, we can't forget what got you here in the first place: your technical prowess. For TLMs, coding, tech leadership, and review should still be anywhere from 50 to 80% of your duties depending on the organization you are a part of. In my first formal engineering management role, I also played the role of a tech lead and spent nearly 80% of my time coding. The further up the food chain you go, the less you will be coding. **Do whatever you can to keep that number above 0%, even if that means coding after work.**

Why? **No one wants to work for someone who can't remember what it's like to be in the trenches.** The engineering managers I've respected the most are those who can still tango with the code. Like the CEO and paralegal analogy from above, you _could_ code, but you're choosing to delegate some of those tasks because a portion of your time is better spent managing.

I find that my technical prowess is best utilized through code review and coaching. Writing out a folder structure for React components is suboptimal compared to teaching a room full of engineers on how to organize their React projects.

**You only have two hands. Every teaching opportunity is a chance for you to give yourself more hands.**

**Systems Design becomes the primary skill** you'll be building in your technical dial. Now that you can't spend all of your time programming, you need to be thinking at a higher level as to how your systems will connect to operate in an efficient, scalable, and reliable manner.

[I love this Systems Design playlist on YouTube](https://www.youtube.com/watch?v=quLrc3PbuIw&list=PLMCXHnjXnTnvo6alSjVkgxV-VH6EPyvoX). Not only is it a good source of Systems Design interviews to implement in your hiring program, but it will build your skills when you inevitably interview somewhere else and want to demonstrate you know how to logically think about systems. That playlist will help you get the reps in and build this skill quickly. Do yourself a favor and watch all of the videos on this list.

### Other duties of the TLM you should know about

If you stick to mastering the Four Dials of Engineering Management and maintain your skills as a tech lead, you'll find the Technical Lead Manager role to be very rewarding and easy to transition into. Depending on your companies needs, there are a few other duties I want to call out that you should be aware of that EMs also do so you aren't blindsided:

* **Hiring.** When the money is rolling in, you're inevitably going to be growing your team. You're not only going to be on the other side of the interview, but you may not be interviewing for just technical skills. Oftentimes engineering managers assess things like culture fit and soft skills. I'll save this one for another post, but for now, I would just Google how to run a cultural interview because there are plenty of dos and don'ts you need to consider to keep your interview above board.
* **Firing.** With great power comes great responsibility. As much fun as it is to bring in great people, it is equally crushing to let go of underperformers. Performance management is all about retaining great talent and coaching your direct reports to be the best they can be. Sometimes that's just not enough. The key here is to **document everything and ensure you have a strong case for why this person cannot be saved.** This cannot be a gut feeling. You need plenty of documentation of continued underperformance and failure to adopt feedback before you should consider things like PIPs (Performance Improvement Plans). When in doubt, check with your HR or People team on how to handle these scenarios.
* **Performance Reviews, Compensation, and Promotion.** If you have great people and you are following the Four Dials of Engineering Management to get the most out of your team, you're putting them on the path to upskilling them. They will recognize their metamorphosis and demand more pay or a title bump. Like the previous point, these merits cannot be based on gut feelings alone. Instead, _help your directs build an ironclad case for why they should be recognized_. Someone will ask you to justify why your direct deserves a promotion and a bonus. If you can delegate the rationale to your direct, not only will they be able to justify their raise, but so will you.

## Conclusion and Further Reading

This post is long enough (and I'm tired enough) that you should be well on your way to a successful trial at engineering management. I want to thank [this post](https://randsinrepose.com/archives/meritocracy-trailing-indicator/) for inspiring me to consider Technical Lead Managers as a great bridge to delegating management responsibilities to your team.

All of these tools and strategies were tried and tested by me over the last several years as an engineering manager. All of them are backed up by the amazing engineering management books, podcasts, and blogs I've read over the years. An exhaustive reading/listening list can be found [here](https://leadership-library.dev), though I'd like to call out a few pieces I've found especially useful over the years:

* _The Effective Manager_ by Mark Horstman (and the accompanying _Manager Tools_ podcast)
* _Resilient Management_ by Lara Hogan
* _High Output Management_ by Andy Grove
* _The 27 Challenges Managers Face_ by Bruce Tulgan (particularly for conflict resolution)
* _Accelerate_ by Nicole Forsgren, Jez Humble, and Gene Kim
* _An Elegant Puzzle_ by Will Larson
* _Extreme Ownership_ by Jocko Willink (and his _Jocko Podcast_)
* _The Making of a Manager_ by Julie Zhuo
* _The Manager's Path_ by Camille Fournier
* _The Talent Code_ by Daniel Coyle

I've read all of these books and are the basis for how this guide was built. If you want to learn more about anything discussed in this guide, I would encourage you to read the source material above. Let me know what you found most helpful or if you'd like clarity on any of this [on Twitter](https://twitter.com/theadamconrad) and I'd be happy to walk you through any of the finer points.

Best of luck and remember that **management is a skill just like anything else**. With a combination of study and practice, you'll be well on your way to greatness!
