---
title: How to manage remotely and stay sane
description: Managing remotely is a real challenge after the wake of COVID. If you have to continue to manage remotely you might want to check out this advice.
layout: post
date: '2021-06-14 12:34:00'
tags: wfh remote management-strategies
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

The last 14 months have been a grand experiment in how to manage teams remotely. In one sense this has been a great success. Teams have managed to stay productive and companies have shown record profits. On the other hand, there is a concern of an eventual return-to-office culture.

Let's look at a few of the most common issues managers have with managing remote teams.

## Effective communication

Remember these two words: **documentation** and **asynchronous**. Wash, rinse, combine, and repeat. When you communicate with your teams, do so in an asynchronous way; ensure your teammates can view your messages on their time and in their mediums of choice. That means your message will have to be repeated again and again from Slack to email to the wiki.

Speaking of wikis, _if you answer a question, put it in the documentation_. By documenting things in a permanent medium like a wiki, you effectively automate your ability to answer questions without having to repeat yourself. You also create an easy to reference, permanent, searchable document for anyone to access.

The side effect of thinking this way is it **reduces the number of meetings on your calendar**. Pushing towards asynchronous, document-based communication removes the need for meetings on your calendar since you can comment back-and-forth and evolve the document on your own time.

## Video calls

First raised by [the BBC](https://www.bbc.com/worklife/article/20200421-why-zoom-video-chats-are-so-exhausting), the last year has showcased a new phenomenon called "Zoom fatigue." People are sick of seeing little boxes of humans on their laptops and crave real attention from real people. I get that. But that will never solve the problem of your dev who is in Minneapolis and you who are in San Francisco.

First, **level the playing field**. Require that all teammates take calls from their laptops rather than an office camera that projects the entire conference room. This might seem like a trivial/subtle change but it is important for setting context about "us versus them."

You don't want to assume that those who are permanently remote are second-class citizens in your organization. They will feel this way if they see folks in a conference room Zoom and they are on their laptops.

The key difference is in _your attention to others_. If you have a camera right on your face with a laptop in front of you, you tend to focus on the participants in the Zoom. If you have a camera in a conference room you tend to focus on the people in the conference room and the camera becomes background noise. This is good for those in the conference room and bad for those who are remote.

So if you need to abandon the conference camera in favor of all laptops even in the office, you should **consider upgrading your AV hardware**. A recent study suggests that [better audio quality makes you appear smarter](https://tips.ariyh.com/p/good-sound-quality-smarter) so investing in a solid microphone like the Shure SM57 is a great, cheap audio upgrade with an interface like the Focusrite Scarlett 2i2. By extension, getting a better camera (and even lighting) will also improve your perception on Zoom.

Even if you don't have the budget for this at work, it still might be a good investment going forward. If you plan to work remotely for future jobs then these will be investments in your future job prospects as well.

## Whiteboarding

Drawing things on a laptop is notoriously hard. There are few solutions to solving this problem right now. I do have one solution I've found to be useful but it requires investment and practice.

**Buy an iPad with an Apple Pencil.**

I understand this is not a cheap ask. Neither is outfitting a home office with a comfortable, ergonomic chair and desk. However, if you want to commit to working remotely and managing teams then this is a relatively cheap upgrade in favor of a much-improved whiteboarding experience.

I really like the [Microsoft Whiteboard app](https://apps.apple.com/us/app/microsoft-whiteboard/id1352499399) because it is easy to get drawing on the iPad with the Pencil. The other two things I really like about this app are the ability to create concrete shapes from hand-drawn movements and that it is easy to persist old drawings or share on Zoom.

It's not perfect but it's free and I've not yet experienced a limitation using it for work.

## Interviewing

Interviewing remotely is a challenge for sure. You want to create a sense of connection with your candidates.

I've had [this video](https://www.youtube.com/watch?v=DYAUw7NTHJ0) on repeat for the last several weeks because it is jam-packed with advice regardless of whether or not you are conducting interviews remotely. You'll gain a lot of insights on whether or not remote is a limitation for you with hiring.

Specifically to remote interviewing, I think the best thing you can do is **allow the candidate to use their own IDE and share their whole screen**. In other words, use the remote interview as a window into how they would actually do their work on a given day. If you frame an interview with that lens that the question of remote versus in-person hiring is no longer a problem. It is solely focused on providing a great experience and not about whether remote is contributing (or hindering) that experience.

## Strategic work

For some reason, managers believe this is the biggest detriment to remote work. Someone recently commented on a LinkedIn post of mine that "nothing beats getting a bunch of folks together in a room in front of a whiteboard".

People have had to strategize throughout the pandemic. Their companies have done very well financially. While some may prefer in-person to remote I fail to see anything about in-office that is _inherently_ a better experience.

This might be a cop-out answer but I think interweaving many of the suggestions above would solve the strategy problem. Leveraging written communication, improving your setup for video calls, and the right technology makes these kinds of meetings so much easier to express your ideas effectively.

Strategy meetings, to me, revolve around [writing dense documentation](https://lethain.com/strategies-visions/) and may benefit from some whiteboarding with other leaders. A great AV setup with an iPad and a proctor to keep the meeting on track with notes is all you need to have an effective strategy meeting.

## Reading emotions and connecting with people

This last point is perhaps the most difficult to conquer in a remote environment. Body language, by the sheer fact that video cuts out most of the body, is impossible to read remotely. Still, even if you can't see someone's whole body does not mean you can't read emotions or body language at all.

**Not being on Zoom, is, in and of itself, a signal.** Not turning on your video can be a sign of [anxiety or depression](https://www.psychologytoday.com/us/blog/lifetime-connections/202004/dealing-zoom-anxiety), particularly if one usually is on Zoom but has decided not too recently.

If the other party is on Zoom you'll have to be more perceptive about watching visual cues. Again, without obvious body language cues like crossing legs or folding arms, you can still judge emotions through facial expressions. You just have to work harder to find these cues and be more observant. I'm not an FBI agent or a psychologist but there are [books](https://www.amazon.com/Unmasking-Face-Recognizing-Emotions-Expressions/dp/1883536367) on the subject.
