---
title: On business value
description: How do you showcase the business value of the work that your team is doing?
layout: post
date: '2021-05-17 11:04:00'
tags: okrs leadership kpis
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

I did an AMA on Clubhouse a little while ago and there were a lot of awesome, hard-hitting questions on the panel. So for the next several weeks, I'm going to write out my thoughts to each of the questions and how I answered them.

> How do you showcase the business value of the work that your team is doing?

There's something that gives me pause about this question having the adjective `business` in there. If you're building a product, you're adding value to the business. What value is not business value in this context? Or put it another way: **what is valuable that _isn't_ business value for professional code?**.

So I will answer this question with the broader aim to **demonstrate value of the work you do**.

The easiest way to do this is to [showcase your impact](/blog/how-to-demonstrate-impact) on the organization. The best way to automate this is to set up regular OKRs every quarter that relate to business KPIs (i.e. revenue, monthly active users, customer retention). Showing impact with numbers and figures against business targets is the easiest way to bridge the gap with business folks.

The more difficult, laborious way to do this is to compare the value you and your teams have generated against how much [you and your teams' cost](/blog/how-to-justify-your-cost-to-a-client).

Really big companies like Facebook know **how much a developer contributes to the company bottom line** - both in revenue and the number of users they impact. That's a huge draw for some developers: imagine if you alone could make a change that affects _millions_ of people! Now what they don't tell you is that your changes probably amount to altering a button from blue to a slightly more [convincing shade of blue](https://blog.ultimate-uk.com/googles-41-shades-of-blue). So if you want to work on something that will actually move the needle on fixing the planet we live on, [we're hiring](https://indigoag.com/join-us#openings).
