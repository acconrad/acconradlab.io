---
title: On delegation
description: I was recently promoted to an Engineering Manager role. How do I turn off my technical know-how to move to a leadership role?
layout: post
date: '2021-05-03 11:04:00'
tags: delegation leadership manager-skills
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

I did a recent AMA on Clubhouse and there were a lot of awesome, hard-hitting questions on the panel. So for the next several weeks, I'm going to write out my thoughts to each of the questions and how I answered them.

> I was recently promoted to an Engineering Manager role. How do I turn off my technical know-how to move to a leadership role?

First of all: congratulations! It's hard to break into the role of engineering manager so great job and welcome to the club!

Second of all: know that you are not alone. This is a _very common_ refrain from new managers.

In fact, one of the most memorable pieces of writing I've ever seen on the topic was an Ask HackerNews thread about switching to management. The very top comment was warning new managers to **delegate more and code less** and how this will not come instinctively to you as a new manager.

It's really easy to revert to what you know; to roll up your sleeves, fire up your terminal and editor, and get to coding the problem when everyone else cannot. **Resist the urge to code whenever possible.** It's extremely counter-intuitive.

You've trained, possibly for your entire career, to solve problems with code. Now I'm telling you to take all of that training and throw it in the trash. How is that possibly a good idea?

I've made this analogy [before](/blog/technical-lead-management) with a whole section on delegation. To summarize: the higher up the food chain you go, the rarer your skill sets become. A CTO can do all of the things you can do but needs to focus on only the things she can do, like hiring executives and setting executive direction and vision for the engineering teams.

By extension, you as an engineering manager now have a set of skills that separate you from your previous self as a developer. Therefore, let your developers do development things because only you can do certain manager things, like hiring, firing, promoting, setting technical direction, and resolving conflicts.

**Focus on the things that make you valuable and differentiated so others can do what they are best at**. It may be uncomfortable at first but it is _necessary_ for your role as you transition into management.

