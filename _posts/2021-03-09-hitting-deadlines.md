---
title: Hitting deadlines
description: Deadline-driven development is an anti-pattern so here are a few ways to combat it before it becomes a problem.
layout: post
date: '2021-03-09 11:04:00'
tags: productivity scheduling anti-patterns
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

My recent productivity obsession is around [Evidence-Based Scheduling](https://www.joelonsoftware.com/2007/10/26/evidence-based-scheduling/).

Software estimation is hard. But I also want to make sure our stakeholders understand how far we are along in our projects and when we think we can reasonably deliver some software.

While watching _The Last Dance_ this weekend about the '98 Chicago Bulls basketball team, I got to thinking about how great sports teams all review tape from their previous performances.

Who does that in software? I don't think I've seen anyone do that. And yet, if you think about the principles behind the book in _Deep Work_, you have to get as much feedback as possible as to how you are performing at your job.

_Practicing_ estimation is a skill and takes effort and time. In speaking with a few colleagues and friends on how they've implemented this themselves, there are a few things to keep in mind.

## Buy-in and messaging is key

On the surface, this sounds like micromanagement to the _n_ -th degree. Now my boss is tracking my time? **I'm not tracking your time - YOU are**. It's really easy for a framework for this to sound like a way to place the lens of Big Brother over your development teams. That is _not_ the goal at all.

Instead, this is about tracking _yourself_ to see how good you are at estimating your own work. Endless articles have demonstrated that we, as an industry, are _really_ bad at estimating how long our work will take. This is putting that into repeated, frequent practice.

There are no points for completing things early and there is no shame in finishing things way off target. The point is not to grade your performance in matching expectations. **The point is to see what is your pattern for estimation in the first place and account for that.**

Maybe you are the chronic optimist and everything you do actually takes 20% longer than you expect.

Great! Now we can ensure that if you take a ticket we can reliably add 20% to the estimate to predict when any of your work will arrive. The time to complete doesn't matter; all that matters is the message to stakeholders about when we think this will most likely arrive.

## It takes a lot of work upfront

A friend of mine mentioned that his team of 8 took about 8 hours _a week_ in meetings breaking down tickets in grooming and providing granular estimates of the work that was to be delivered. That's a _lot_ of time in meetings per week, particularly for individual contributors.

At first, that seems like the effort is way too high for a team size that small. However, after a few weeks of working through this system, they were able to get these meetings down to about 2.5 hours while also increasing their accuracy and delivery.

Would you spend 5% of your week ensuring your delivery is 90% accurate to stakeholders? I'd take that each and every week without fail!

## Target practice

Remember that all of this is not an excuse to let stakeholders pepper your team with deadlines. **It is an anti-pattern to adopt EBS _so you can enforce deadlines_**. Rather, it is a reaction to an anti-pattern of deadline-driven development so you can get your teams out of the corner you've already backed into.

The end result of adopting evidence-based scheduling should look something like a bull's eye target. This analogy I use time and time again with my team leads when one of their stakeholders is adamant about requiring a specific date for something to arrive.

Imagine you are firing an arrow at a target 100 yards away with a 5' radius. The goal, of course, is to hit the bull's eye in as few shots as possible. There are two ways to do this:

1. **Increase your accuracy**. Each time you fire an arrow (i.e. deliver an estimate) it should get closer to your target. In other words, if you think something will take 2 days and you're off by 1 day this week and then off by 2 days next week, your estimation skills are only getting worse.
2. **Move closer to the target**. It's a lot easier to hit a target from 50 yards away than 100. In software, this is just time moving forward. The closer you get to the projected date of delivery (i.e. target) the clearer the bull's eye becomes. You probably know what you hope to accomplish today but who knows what you're confident to deliver 9 months from now.

So if you practice hitting a target regularly (i.e. practicing evidence-based scheduling) while simultaneously moving closer to the target (i.e. building more software every day), you should be moving away from deadlines towards target dates which just get more _accurate_ and _predictable_ over time.

At the beginning of the quarter, your project will arrive in 3 months with 60% confidence. In 2 months the project will arrive in 3 weeks in that month with 80%. In that week, the project will arrive on that Thursday with 95% confidence. This is how we shift away from deadlines to target dates.

Stakeholders still hear a date that makes them happy and you aren't beholden to a rigid delivery date which makes you happy. Everyone wins. That is why I am so excited to try this system out and if you have any experience with EBS please let me know.
