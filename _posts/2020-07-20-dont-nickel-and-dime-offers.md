---
title: Don't nickel and dime offers
description: Making offers to candidates shouldn't cause a stir in your budget. Don't lose great people over a few thousand dollars.
layout: post
date: '2020-07-20 22:23:58'
tags: organizations engineering-leadership hiring
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

It's so hard to find great engineers that pass all of your interviews with flying colors. So when you do find someone who actually makes that happen, don't let them go because you feel the need to bargain over little bits of money.

One issue is you may lose them because you're perceived as a company that wants to haggle down to the last dime. If they'll haggle over that, how do you know they won't treat you well once you arrive? Do you _want_ to work for a company where they are going to nickel and dime your every interaction?

These are the questions candidates will ask if you can't come to a reasonable negotiated offer quickly. Sure, coming up or down by $10k or more makes sense and is part of every negotiation. I find myself impressed that candidates are doing that more often these days. But if you're closed in on, say, less than $5k in differences, let it be. You won't miss that money.

My gut instinct at first is if a candidate is negotiating hard, you don't want to seem like your organization is soft and that you'll curtail to any demand an engineer asks for. This happened to me today and when I took a step back I thought "well what is the alternative? Do I really want to let this candidate go for a few grand?" The answer was certainly _no flippin' way_.

So remember: negotiation is good. Haggling down to the last few thousand dollars is not. You've probably already invested that money in just assembling the people to interview this candidate in the first place.

Oh, and when you are ready to make an offer, *sync up with your recruiting team to ensure the benefits are up-to-date*. I paid for this one dearly last week; turns out our benefits were _better_ than we advertised and we failed to mention that and lost a candidate. Save yourself that grief and make sure you know your benefits are current so you're putting your best foot forward.
