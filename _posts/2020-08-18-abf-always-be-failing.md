---
title: ABF - Always Be Failing
description: Failure is your greatest gift for growth.
layout: post
date: '2020-08-18 00:19:06'
tags: learning growth opportunity
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

If you want to get ahead in life you need to fail. Failure is growth.

Getting everything right teaches you nothing but to be smug. Failure keeps you humble and feasting for ways to improve.

Here are a few recent examples and what I learned:

## Improving hiring

I _hate_ to lose. So you can imagine how much my day was ruined when last Friday I received an email from a candidate I was really looking forward to hiring that they would not be choosing us.

I thought we had this one in the bag but thankfully the candidate gave us a reason for why they didn't go with us.

If we had landed this candidate I might begin to get complacent. I've had the joy of hiring a lot of great candidates in a short amount of time. I've personally hired nearly two dozen people in just the last year I've been running my teams at Indigo and there is room for me to believe I am good at hiring. This would be a great opportunity for [confirmation bias](https://en.wikipedia.org/wiki/Confirmation_bias) to come out of the woodworks.

Instead, this was a huge hit to my ego. What am I doing wrong? I thought I figured this candidate out; how could I be so blind? Why the hell does this seem to happen to me at crucial points where I need a yes?

I tend to obsess about certain thoughts that ruminate in my brain for too long. But in asking myself so many questions I knew that I had to have follow-ups with teammates. **Post mortems are a great opportunity to learn from failure.** So I chatted with the key folks in this hiring play. What can we learn here? How can we make changes _today_ to fix this for future candidates so we don't lose them?

**None of this questioning happens when you win. You only try to refine and perfect when you lose.**

## Over communication

I've got a lot of new teammates with a lot of awesome energy to make a great first impression. With so many new folks and so many new ideas thrown around it can be difficult to follow which ideas are canon and which are breaking new ground.

So I was surprised to find out that I had a case of a policy a developer thought was adopted that had never existed before.

I won't get into details but this would have been a slippery slope kind of policy if not controlled properly. It has amazing roots but without getting into details it was a good policy that could easily be abused and misinterpreted if not carefully communicated across the organization.

This was an eye opener about the often touted advice to [over communicate](https://adamconrad.dev/blog/technical-lead-management/); share the same message over and over again because you can never guarantee if your message was received in the way you intended it to.

In this case, I was extremely confident that the message was received loud and clear. But this suggested policy was a clear misinterpretation of what I had said. Whose fault is that?

I'm a big believer in [extreme ownership](https://www.amazon.com/Extreme-Ownership-U-S-Navy-SEALs/dp/1250067057) so I immediately wondered what _I_ had done (or not done) to miss the mark communicating my ideas. How could my ideas be interpreted this way to shape policy when they shouldn't have? Why had I not clearly communicated to all parties our stance on our policies in software development?

Again, **I had an inner dialogue with myself asking so many times why I failed** until I got to a root cause. The answer? Suggestions need to be labeled as suggestions. Leaders have influence and people will take your word quite literally. **If you don't specify otherwise your words as a leader may be considered the unequivocal truth.**

Communicate your message. Then reiterate it on Slack. Then say it again in an email one. And maybe say it one more time on your Wiki. Communicate until everyone on your teams not only hears your words but understands their meaning as well.

## Common themes

So from these two examples we can draw some conclusions on failure:

* **Failure leads to questioning.** We don't question when we win. Our brain goes _wild_ when we lose. There's an instinct to reject failure and put the blame elsewhere. But that instinct in a way is a form of questioning: why didn't the other person land that candidate? Why can't my company offer our employees this benefit? These are still questions. And the answers are opportunities to grow.
* **Questioning leads to problem solving.** There's no problem to solve if you succeed. As engineers we love to solve problems so in a way failure is something we crave.
* **Solving problems is fun and educational.** Every time I've failed I've gone through this process of questioning to make sure it never happens again. Sometimes it does but most of the time I don't repeat the same mistakes. That is improvement and accomplishment. Stack enough of those over a career and you have a resilient person with a lot of knowledge and wisdom.

The more failures you have the more growth you have. And the more growth you have the more chances you have to succeed big when opportunity comes knocking.
