---
title: Start today
description: There is nothing special about January 1st. Everything you want to do can happen today.
layout: post
date: '2020-12-31 21:30:00'
tags: motivation goals resolutions
subclass: post tag-test tag-content
categories: other
navigation: 'True'
article: 'True'
---

I don't like when people ask me what my New Year's Resolutions are.

I don't have any and I never will.

If I want to resolve to do something - I just do it. Right then and there. I'm writing this post on the night of the last night of the year not because it was meant to close out one of the worst years in the history of my life but because there is no reason to wait.

Let me repeat that: **there is no reason to wait**.

January 1st doesn't usher in some reset button like how turning 26 magically drops your car insurance rates. January 1st doesn't absolve you of the things you missed yesterday that you know you could have physically, emotionally, or spiritually started yesterday. January 1st doesn't care.

**Resolve to never make arbitrary resolutions again.**

Resolve to take charge of your life and the things you want to accomplish _the moment you want to accomplish them_. Inertia is a powerful thing and that first push is the easiest when you are most motivated to start: **the moment you consider it a possibility**. As soon as you know you can (or want) to achieve something is the time when you have the motivation and inner encouragement to make that a reality.

2021 will be better. But you don't have to wait for tomorrow to start living better today.
