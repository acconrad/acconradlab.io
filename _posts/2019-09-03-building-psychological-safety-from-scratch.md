---
title: Building psychological safety from scratch
description: If trust is such an important factor in high-performance teams then how are you supposed to get that from a team that doesn't know each other?
layout: post
date: '2019-09-03 22:51:57'
tags: leadership teamwork
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

Most people leave their job because of [their manager](https://www.forbes.com/sites/victorlipman/2015/08/04/people-leave-managers-not-companies/#1000172847a9). They leave their manager because they are not engaged in their work. So how do you, as a leader, engage your employees?

You might think it is giving them interesting work. You might think it is giving them lots of money and paying at the top of the industry. You might also think it is free lunch, free massages, and other cool perks you saw some Silicon Valley company offering their employees.

You'd be wrong.

**The single best way to engage your employees is to give them an environment of trust.** This means trusting them from day one, allowing them to be transparent with you, and enabling them to spread that culture to their peers.

Another way of saying this: provide psychological safety. And here's how you start doing it today.

## Inclusion

The vital first step is to make everyone a part of the conversation. It doesn't matter if you are the first engineering hire or your first day on the job. Every person needs equal _and equitable_ footing to speak and share their ideas.

Remote employees tend to get marginalized in this step. You might think you're giving everyone a voice but if you have a headquarters where everyone gathers by a water cooler to talk shop then you are leaving your remote employees out of the conversation.

Junior, less experienced employees are often thought to be included because you might go around the table to ask for people's opinions but they are still neglected because they aren't taken as seriously or with equal gravity to their more senior counterparts. And while there is something to be said for experience driving more sound decisions that shouldn't exclude a great idea simply because someone is brand new to the industry.

Give everyone a voice and make them equally as loud.

## Safety to learn and contribute

Google made a huge stir about 20% time; that you could take a whole day out of your week to do whatever you want to develop your career outside of your regular value-added work to the company.

Since when is software engineering a career where you only need to learn 20% of the time? **Learning should be an integral, continuous part of the software engineer's professional experience**. And the biggest part of learning is trying new things, not getting them right the first time, and persevering to get back up and try again.

I just watched a great [TED talk](https://www.youtube.com/watch?v=9vJRopau0g0) on how just giving children encouragement towards their goals gave them a _16% increase in their chance of success_. How much would you pay for 16% fewer bugs, 16% more output, and 16% more engagement from your employees? Well mail me your checks because you can do it all for free if you simply encourage a culture that **failure is a gift** and not something to be reprimanded for.

We learn when we fail. We also learn when we share that failure (and subsequent success) with our teammates and greater community. Not only do we need a platform to try and fail, we also need a platform to share our findings along the way. Being able to contribute to the greater community is another vital point of psychological safety.

In my teams, **we acknowledge that even updates to documentation are contributions**. I've met plenty of high achievers who are annoyed that they can't contribute to code on day one. I tell them that if the README is checked into the code base, and you find a flaw in our setup README docs, then that is checking in code. **All contributions are welcome.**

When you know that your README change was as valuable to the team as some hardcore algorithmic update to some C++ code, you remove the barrier to entry that only the most senior folks can make the most outstanding contributions. **Outstanding contributions come from outstanding people.**

## Safety to challenge the status quo

No one wants to rock the boat on their first day at the job. That's why new hires are the only ones wearing button down shirts with collars. Everyone _thinks_ they have to dress to impress and start off on the right foot and by the end of the week these same engineers are wearing t-shirts like the rest of us.

What if, like me, they just liked getting dressed up because they enjoy dressing up? Or what if you didn't think that having to be in the office was required for productivity in the first place?

Having radical ideas is the basis for innovation. If you couldn't challenge societal norms then the founders of Airbnb would have never been able to convince people that it's totally okay to sleep on a strangers couch and that everyone would be doing it a decade later. If you asked anyone in 2004 that premise would not only sound absurd but downright dangerous.

Why are only the successful entrepreneurs allowed to innovate? Why can't it be every single person in your group? The only barrier to this hurdle is yourself. There is literally _nothing_ stopping you from having anyone challenging the status quo. All you have to do is _let go_. Control is hard for some folks to relinquish but it is essential if you want to get this part of psychological safety.

## No fear

Which leads me to my last point: no fear. Letting go of control is based on a fear you have that if you don't have control then bad things will happen. A fear-based mentality will only get you so far. It is self-limiting in nature.

A fear-based culture leads to people feeling:

* embarrassed
* marginalized
* threatened
* punished

A fearless culture, meanwhile, leads to people feeling:

* celebrated
* included
* empowered
* rewarded

Which feelings do you want to feel?

I hope I've convinced you that psychological safety is the bedrock for high-performing teams. A fearless culture leads to greater productivity, higher engagement, and increased satisfaction at work. And it costs you nothing but relinquishing a bit of your control freak instincts to trust your group to do the right thing.

If you can take that first fearless leap towards a safe environment then you will be rewarded with all of the benefits that come along with it.
