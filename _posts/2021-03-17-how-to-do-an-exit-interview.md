---
title: Are exit interviews a waste of time?
description: Exit interviews seem like a waste of time...or are they?
layout: post
date: '2021-03-17 11:04:00'
tags: meetings interviews anti-patterns
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

Someone I knew recently asked how to perform an exit interview to elicit an honest response from the employee for the company to improve.

The short answer is **don't bother because you're not going to get honest feedback from those that leave**. The incentives just aren't aligned - what could the employee possibly gain by spilling the beans now that they are out the door? At best, there is a cathartic, vindictive release for Finally telling the company why they screwed up. At worst, it burns bridges with people who might hire you in the future at another company.

A longer answer is that people do leave genuine feedback from time to time. For the same reasons, the same lack of incentives for the employee are often the reason they do leave honest feedback. If you have nothing to lose and your gripe with the company is directly squarely at the company and not your manager then you'll probably throw them a bone and say something constructive to help your former manager's team going forward.

Still, most people leave [because of their manager](https://www.prnewswire.com/news-releases/new-ddi-research-57-percent-of-employees-quit-because-of-their-boss-300971506.html) which means most people have no interest in digging themselves a further hole with the person that caused them to leave. For the rest, its still not a compelling reason to say anything.

## You already know the answers

Nobody just wakes up and quits out of the blue. They think about leaving. They _plan_ for months very carefully on how they will leave.

Look at how wild hiring is now. Most folks need to study and prep for months.

So that means at least one foot has been out the door for a while. If you’re [doing regular 1-1s](/blog/technical-lead-management/) you’d pick up on this stuff.

If other people have left whatever they tell you is probably the same as it is today.

People know what is broken with their company. Everyone knows and one person leaving, unless they are very senior, isn’t going to be enough of a shock to rock the boat and radically change direction. Because if you missed 10 whole things you would know by now. And you would have fixed them so you didn’t lose the candidate in the first place.

These things all play out in a very long time frame.

## What if you have to conduct an exit interview?

I would just keep it pleasant and let the employee speak freely. If they have nothing to say that’s probably expected and fine too.

If you have a trusting, safe relationship with the person leaving then you are more likely to get an honest response. **I just wouldn't bank on it ever**.

Instead, see exit interviews as the last chance to leave a positive impression before they leave a review on Glassdoor or Blind. An amicable leave means a great review and possible referrals. Leaving with a bad taste in their mouth will only spell trouble for your team going forward.
