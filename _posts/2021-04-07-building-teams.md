---
title: Building teams
description: How do you build teams and how do you differentiate from building a team from 0 to 1 vs. 1 to 100?
layout: post
date: '2021-04-07 11:04:00'
tags: team-building hiring org-design
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

I did a recent AMA on Clubhouse and there were a lot of awesome, hard-hitting questions on the panel. So for the next several weeks, I'm going to write out my thoughts to each of the questions and how I answered them.

> How do you build teams and how do you differentiate from building a team from 0 to 1 vs. 1 to 100?

Full transparency: I've never hired a first engineer for a company. But I _have_ hired the first engineer for a team.

Hiring the first engineer is helpful if you **other teams they can join.** Even if that team is building something completely different, just having a sense of **camaraderie** is a huge win for people to not feel isolated when they are the first to do something.

It also helps to hire **a more senior engineer** because they are more capable of handling big, ambiguous projects from scratch. All of my new teams start with the most senior person and work their way down. It also makes it easier for newer, more junior developers to learn from more experienced folks when you are onboarding and training people to learn the ways of your new team(s).

Full transparency, deux: I also have never hired 100 engineers for a company either. I've hired 18 engineers from scratch for my teams and referred another 10 to other teams. I would say not much changes after the first squad of 4-6 is built.

Getting those folks is key and once you have that central unit in place it's just a matter of **maintaining and expanding that culture with the same squad blueprint.** So if I'm looking at budget for the next year I try to **never hire a fractional or incomplete squad**.

Obviously for some teams, budgets are too tight to fit into neat buckets like squads. But if you have the luxury of building out full units of teams then be sure to build out a whole and not a partial unit or else you're going to have trouble giving that squad **a central mission and objective to complete.**

Each squad should have a vision and a purpose. If you don't have a complete squad to assemble then you don't have all the pieces you need to accomplish an objective. This will leave folks floundering between teams just to keep resources utilizes on all of the things your folks still don't have time for on their own.
