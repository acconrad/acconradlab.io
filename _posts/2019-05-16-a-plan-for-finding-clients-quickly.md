---
title: A plan for finding clients quickly
description: Most contracts give you 30 days notice if they terminate early. What do you do then?
layout: post
date: '2019-06-06 21:50:40'
tags: sales marketing process
subclass: post tag-test tag-content
categories: consulting
navigation: 'True'
article: 'True'
---

This time last year I was scrambling.

A month before everything was going great. I had an awesome client, charging (what I thought was) a great rate; no complaints.

And then I got the email.

The client was terminating the project early and I had to wrap up in the next 30 days. It had nothing to do with my personal performance it's just the company decided they couldn't afford to spend this much on software so they cut the funding abruptly.

What was I to do?

Extreme circumstances call for extreme measures. I felt a lot of personal pressure to not have a gap in clients. The result: I landed a new client without any gap in employment. And I can safely say I've never had a gap with clients in my entire time as a full-time freelance consultant developer. How did I do it?

One word: process.

## The client prospecting process

When your back is up against the wall you will do what it takes to make things work. I will admit this is a very aggressive course for grabbing clients but it works if you're willing to put in the work. For every week day in the 30 day window I had, this is what I did:

### Reach out to 5 contacts in my network

The first step is warm outreach. Cold outreach is a dead end in such a short time frame to it is vital to reach out to folks in your network. So I start by asking them if their company needs any contracting work. If they don't, can they recommend 3 people who might even remotely need it _or_ if they ever worked with consultants before and used a firm.

### If there are leads, follow up

After a few days I was able to build up a consistent rotation of 3 leads to prospect. This is where you get to a conversation in person or over the phone as soon as possible. **Any hesitation leads to indecision and ultimately no purchase of your services.** Time decay is a real thing so you must apply that pressure to get on the phone.

### If there are prospects, promise them a 24 hr turnaround

I have to admit that some of my favorite parts of the prospecting process are right after that intro phone call. This is a part of the process that can get you into trouble because you're carrying a lot of momentum and a lot of positive energy without any commitment. And those good vibes can cloud your judgement.

This is why it is crucial to stay focused on getting to closing by not celebrating a good conversation and just **getting things done with a fast estimate**. Your prospecting call should leave you with a bunch of notes to address so get them a statement-of-work/proposal in 24 hours.

Again, time decay is real so every hour you waste is time for the prospect to decide they don't need to go with you. Building trust is about doing what you'll say. You'd be surprised at how many people will not follow through on super simple things like this.

### If there are potential buyers, check in

At this point you're getting real close to a sale. You can taste that sweet signature but you can't slow down. You should have put in your proposal that you'll check in within 48 hours. So make sure **when you deliver your proposal you mark on calendar for 48 hours later to check in with that prospect**.

Do revisions, get on the phone; whatever it takes. The objective is to ensure that **your next client feels special and valued and you want the transaction to be mutually beneficial**.

Knowing you have your clients' best interest at heart is not only important but it is something very few consultants seem to care about anymore. Of course you want to make money and your client wants to generate revenue/save money from the software you're about to give them. But just stating that you want to go the extra mile for them is not a phrase most clients hear or that most consultants say. **You should relish in any opportunity to stand out**.

### Grab that signature - you did it!

At this point you've done everything you can. Like any funnel, most potential clients won't even respond to your initial email or LinkedIn message. Few will respond. Fewer will get on the phone. Almost none will read a contract or proposal. **But it only takes one signature to be in business.**

It kind of reminds me of all of those cheeky jokes like "what do you call someone who graduates at the bottom of their med school class? A doctor." It's true.

You can strike out thousands of times. You could be trailing the entire game. But if the clock reaches zero and you hit a homer to win the game, you're a winner.

I was able to close multiple six figure deals with this strategy and while I heard a lot of rejection at every stage of the process, the fact remains that I never had a gap in clients because I aggressively pursued this strategy. I just hope you don't have to wait until the 11th hour when your contract expires early and you're scrambling to add new clients to your pipeline. Get started on this stuff today so you're not in trouble tomorrow and happy prospecting!
