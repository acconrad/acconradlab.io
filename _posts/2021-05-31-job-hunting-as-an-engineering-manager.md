---
title: Job hunting as an engineering manager
description: How do you decide when and how to leave your company and be confident that you’re making the right decision?
layout: post
date: '2021-05-31 11:04:00'
tags: hiring interviewing interview-prep
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

I did an AMA on Clubhouse a little while ago and there were a lot of awesome, hard-hitting questions on the panel. So for this last installment, I'm going to write out my thoughts to a the questions and how I answered them. The last question from the panel was:

> How do you decide when and how to leave your company and be confident that you’re making the right decision?

As I've mentioned in a previous article, you don't just wake up one day and say to yourself "I think I'll [leave my job](/blog/how-to-do-an-exit-interview) today!"

It's a long, painful process. It's the culmination of a lot of smaller irritations, frustrations, and setbacks that leave you longing for something more on a greener pasture. And that doesn't even cover the interviews.

Interviewing sucks. I won't deny that. But changing your mindset on [how to approach technical interviews](/blog/why-hiring-is-broken-and-how-im-dealing-with-it) can make all of the difference.

Gergely wrote a [great article](https://blog.pragmaticengineer.com/preparing-for-the-systems-design-and-coding-interviews/) recently on what to study and how to study for the range of engineering management interview questions. It's a very complete and exhaustive list of resources that is the exact same references I would list here so there's no reason to duplicate those efforts. Just read that article and actually do all of the stuff he recommends.

Yes, it will take a long time (like, months). And yes, it is difficult, painful, and arguably a giant waste of time. But as I've told lots of people before: interviewing is a skill just like anything else that you can master.

**All that passing an interview proves is that you are good at interviewing**.

I like to compare the technical interview process to the SAT/ACT/IB standardized exams. The best way to "win" at the SATs is to answer a lot of SAT questions. And all that the SATs really prove is that you're good at the SATs. They [don't predict success in college](https://www.forbes.com/sites/nickmorrison/2020/01/29/its-gpas-not-standardized-tests-that-predict-college-success/) and there's an argument to be made for getting rid of them altogether. See the similarities now?

Regardless, both systems aren't going away any time soon. So you have two choices: don't play the game and limit your options and upside, or play the game even if you don't like the rules.

With companies paying [outlandish salaries now](https://www.levels.fyi) for a field that doesn't require formal credentials or schooling, this could not be a better time to get out there and find your [perfect role](https://indigoag.com/join-us#openings). I would argue that even if you don't like the system we have, you're leaving hundreds of thousands of dollars per year on the table by not playing along.

Study hard, get your name out there, and buy me a coffee when this post convinces you to get that job that's going to make you an extra $80k per year.
