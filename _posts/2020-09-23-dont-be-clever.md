---
title: Don't be clever
description: If you're trying to work around your company's systems to make things happen it is probably not going to work in your favor sooner rather than later.
layout: post
date: '2020-09-23 22:09:22'
tags: process retention
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

I made a pretty frustrating mistake recently.

I won't get into the details but essentially I was trying to do the right thing by creating a very clever set of circumstances to achieve a goal. The problem is that this clever setup had a gaping hole in it that was exposed by my People team. When I tried to resolve it with my VP, I realized that I could have solved this problem months ago with a simple conversation.

**Before you get clever, just talk with someone to see if you can get to your resolution in an easier way.** Sometimes all it takes is a phone call or a quick chat to get through the red tape.

I thought I needed to carefully route around all of the red tape to achieve my goal. I created a series of circumstances to check all of the boxes from all of the stakeholders to ensure I was obeying all of the rules we had set up for the problem I was trying to solve. I felt like I needed to fit within these rules to make an exceptional case work.

**The flaw I made was that I identified the exceptional case, and _didn't raise it as an exception_.** If you see an exception, you should _make an exception_. Instead, I tried to fit an exceptional case into the standard workflow by carefully creating all of these circuitous processes to ensure that both the exception was taken care of and I was staying within the rules of the road.

But when you have an exceptional case for something, whether it be hiring, promotions, or organizational design, it is now obvious to me that you need to just face it head-on rather than try to create all of this red tape to make it work within the system.

If we are a stickler for the rules we have setup without taking into account exceptional cases, we are doomed for pain and suffering. Being too dogmatic with our rules as leaders makes things too inflexible and brittle. On the flip side, we also don't want to allow too many exceptions to pass through or else they will, by definition, fail to be exceptional.

Finding the right balance of breaking, bending, and sticking to the rules is vital to ensure that the standards you set for your teams are a healthy addition to the processes you already have in place.
