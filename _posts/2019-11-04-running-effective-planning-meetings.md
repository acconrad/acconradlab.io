---
title: Running Effective Planning Meetings
description: As a technical lead, planning meetings serve as your opportunity to clarify upcoming work with your stakeholders.
layout: post
date: '2019-11-04 13:17:19'
tags: meetings planning tech-lead project-management
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

**As a technical lead, planning meetings serve as your opportunity to clarify upcoming work with your stakeholders. Running them properly will make your meetings more effective both for your teammates and your stakeholders.**

In software, planning meetings serve two primary missions:

1. Clarify what your team needs to do.
2. Give stakeholders confidence in the work you are promising to perform.

How do you approach these meetings to achieve these goals? Follow these steps:

## 1. Revisit work in progress from last week

The first step is to review the work that has carried over from last week. Go through every task with each developer and ask the following questions:

* We didn’t get it done last week - will we get it done this week?
* If so: are the acceptance criteria still clear?
* If not: do we need to break up the ticket? Are there blockers? **What has changed since last week?**

If we didn’t land our commitments then we need to quickly learn **why we missed our commitments and what needs to be done to fix it.** It could be that the requirements were too vague and need clarity. It could be that the requirements were clear, but to implement the functionality, we surfaced additional concerns that will result in additional subtasks and work for the team. **It is essential to follow through on existing work or deprioritize this work before we add new tasks in from the backlog.**

## 2. Pulse check, the first: are we full for the week?

It is possible (though uncommon) that so much is still in progress that it is not wise to add in more tickets. **If you overcommitted last week then the key is to nail those commitments this week.** Software is difficult to estimate; you won’t be perfect and that isn’t the point. The point is to **manage expectations with stakeholders and strive for better estimation every week.**

## 3. Search the backlog for additional work

If your team isn’t fully utilized for the upcoming week then it is time to look into the backlog to find more work for your team to do. How do you know what work should be performed next? Depending on your team, it should look like one of the following scenarios:

1. **What’s at the top of the backlog?** If you have groomed your backlog with a Product Manager, then the backlog is likely prioritized from top to bottom.
2. **What is the highest priority item?** Priority is another ranking mechanism. Perhaps an IT support ticket came in at the last minute, so it is at the bottom of your backlog but surfaces an important production issue. This likely needs more attention than tickets even at the top of your backlog.
3. **What are your team objectives? Is the order or priority not in alignment with certain near-term objectives you’re trying to accomplish?** Are you launching an MVP? Do you have a checklist of things that must launch before the end of the quarter? Quickly scan the backlog for other tasks or stories that may have missed the first two scenarios.

Once you’ve found the work, it’s time to move onto the next step.

## 4. Clarify the work for each task you add from the backlog

Now that you’ve determined there is work to be done, here’s a sample checklist to ensure the work is ready to be performed by your development team:

* Does the title correctly summarize the task to be completed? (If it’s a story, does it follow the format "_As a PERSONA, I want to PERFORM ACTION so that I can ACCOMPLISH GOAL_")
* Is the description filled out with acceptance criteria? What does the Product team/Customer expect will be available when this ticket is complete?
* If this is a story, what the subtasks for engineers to complete to assert the story is done? What is the “definition of done” for this ticket?
* What team will be working on this? API or UI? A product team or a platform team? Is this even on the right Jira board?
* Were any comments or updates left to indicate more information that wasn’t captured?
* If this is a UI task: are there mockups or InVision prototypes? Are those mockups linked appropriately?
* Speaking of linking: is this ticket blocked by another ticket? Follows the completion of another ticket? Is there any kind of connection from this ticket to another?

This is by no means an exhaustive list. This is simply some of the things you should consider when you’re evaluating if **the ticket has all of the information necessary for _any developer on your team to complete the ticket_.**

## 5. Add the ticket to this week’s scope & repeat step 4.

Not much more to say here other than to repeat step 4 until you reach step 6…

## 6. Pulse check, the second: are we full now?

Steps 4 & 5 can get unwieldy if you’re trying to eat clock in the rest of your meeting. Next thing you know you’ve groomed quite a few tickets that may or may not still fit into the work capacity of a given week. So let’s check again: **given all the work you just planned for - is this a sustainable amount of work your team can commit to this week?**

If so, congratulations! You’ve successfully planned your week for you and your team. Everyone is in alignment on:

* How much work we’re committing to achieving by the end of the week
* What steps need to be completed (in as much detail as necessary for your team) so that anyone can grab a ticket and have enough context to complete the task at hand

If you’ve committed too much, simply drop the lowest priority work to reach your ideal work capacity. Conversely, if you don’t have enough, continue to repeat steps 4 & 5 until you reach your ideal work capacity.
