---
title: A quick and dirty guide to Kanban
description: Kanban sounds new and scary - but is it really? No and it's very simple and easy to implement.
layout: post
date: '2020-11-30 16:18:00'
tags: agile productivity
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

I recently saw a question about how to implement Kanban and I realized I could very succinctly describe what is involved in the practice. Lots of people write whole books and courses on Kanban but I think that is completely unnecessary. I propose that you can learn (and implement Kanban) in about 5 minutes with a quick, one-page read.

This guide is purposefully short because I want you to know that **there really isn't all that much to this practice** so I'm going to save you all of the fluff and background that usually accompanies one of my posts.

**Kanban is just a way of organizing your teams' work.** It's all about _continuous improvement_ and the _acknowledgement that priorities change constantly_.

I think **most software teams that don't have hard deadlines would benefit more from Kanban than Scrum**. The reason is that software is inherently complex and often has moving targets. So gaming companies, which do have hard deadlines for when their games are released, probably won't benefit from Kanban and should stick to the Scrum (i.e. waterfall) approach. If you're a SaaS software product building things on-the-fly, consider Kanban. So what is involved?

## 1. Put your tasks on a visual board

So many tools exist that already do this: Trello, Asana, and Jira are three I can think of that already do this aspect of Kanban: **become aware of what you are _actually_ working on**. Just showing everything to your teams helps you understand where the blockers, bottlenecks, and underutilized capacity lies.

## 2. Create a simple workflow to start

I would recommend a 5-step workflow that has scaled to multiple teams on very large products:

1. **Inbox:** The stuff that just came into your backlog that is yet to be prioritized
2. **In scope:** The stuff that you've prioritized and could actively pick up to work on
3. **In progress:** The stuff someone is building with code and tests
4. **In review:** The stuff that has a Pull Request or some other kind of gate to review quality of the work to be delivered
5. **Done:** The stuff that is out on your production servers and released to customers

## 3. Use the workflow and check for blockers

With this workflow you'll see tickets moving from one step to the next. The number of tickets in each column tells you something. When tickets pile up in a given column here is how I investigate them:

1. **Lots in inbox:** Check with product management to find time to prioritize - either lots of new ideas are being sourced (good) or lots of bugs, issues, or debt are accumulating (bad).
2. **Lots in scope:** The sprint just started (not an issue). If the sprint has been going on for a while, either the team committed to too much, too many bugs came into the sprint, or there is a bottleneck further up the workflow.
3. **Lots in progress:** _You should only ever have as many tickets in progress as there are people_. If there are fewer then not everyone is working on something and if there are more then someone is juggling more than one task at a time. **The whole thing with Kanban is that a great workflow means doing one thing to completion.** So don't let anyone work on more than one thing at once (in terms of active development - you could have a ticket in review waiting for PR feedback while you are starting on another ticket)
4. **Lots in review:** Developers are not paying attention to Pull Requests and product/design are not checking the acceptance criteria are being fulfilled. My teams provide a guarantee of 4 hours to review their teammates' code. Make sure you have [code owners set up](https://help.github.com/en/github/creating-cloning-and-archiving-repositories/about-code-owners) and ensure all of the right parties are seeing notifications that they need to review the code.
5. **Lots in done:** Nothing wrong with that.

## 4. Refine your workflow

Start with this template and see what works for your teams. Maybe you have a staging server that needs to be pushed for review before you can push to production so insert a column between steps 4 and 5. Maybe you have separate, distinct steps for Code Review, Design Review, and Product Review - add them in. **What matters is that you take the basics of a Kanban flow and iterate on your workflow to suit the productivity of your teams.** And further, each team may have a different workflow that works for them so **be open to adopting not just one workflow but many**.

## 5. Measure and learn

Okay so you did all of this stuff and now you're running Kanban (all of this can be turned on with a switch [in Trello](https://trello.com/b/C4Awm5lK/kanban-board) or [Jira](https://www.atlassian.com/agile/tutorials/how-to-do-kanban-with-jira-software) or [Asana](https://asana.com/uses/kanban-boards)). How do you know this was the right move? Kanban is great for measuring a few things which correlate really well with [high-velocity teams](https://www.amazon.com/Accelerate-Software-Performing-Technology-Organizations/dp/1942788339?SubscriptionId=AKIAILSHYYTFIVPWUY6Q&tag=duckduckgo-ffab-20&linkCode=xm2&camp=2025&creative=165953&creativeASIN=1942788339):

1. **Cycle time**: How long does it take for completed dev work to actually get into the hands of customers? High-performing teams have a very short feedback cycle for review and release.
2. **Lead time**: How long does it take for a ticket to arrive on the door of the backlog and get it all the way to the done step (i.e. how long does it take for someone to ask a developer to do something and then have it delievered to customers). Cycle time is a subset of lead time but is important to distinguish to help bring to light the things that are in the control of the developer (planning, prioritizing, and working on the product) and out of the control of the developer (the review and release process). Just like cycle time, lead time should be fast and fluid.
3. **Throughput**: How much stuff gets put into `DONE` in a given sprint? Is it trending up or down or stagnating? High-performing teams can get through a lot of stuff quickly (duh).
4. **WIP** (work in progress): Are people actually focusing or trying to juggle a lot? High-performing teams have a low WIP (ideally 1 per developer).

If you can measure these 4 things and trend them in the right direction, this should indicate that it was a good decision to adopt Kanban.
