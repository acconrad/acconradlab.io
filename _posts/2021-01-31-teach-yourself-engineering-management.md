---
title: Teach yourself engineering management
description: Managers never get the training they need. Maybe you can be the first one who does.
layout: post
date: '2021-1-31 21:09:00'
tags: management learning curriculum
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

Engineering management is rarely taught to new managers.

The usual path to management looks something like this: your manager identifies you as someone with potential but there are either (A) no more ranks to climb as an engineer or (B) no good managers they can hire so they put you in the manager slot.

This is troublesome for two reasons:

1. Not everyone wants to be a manager
2. For those that do, they're thrust into the job without any training or help

If you're one of the lucky managers who actually wanted to make this change then you _need_ to learn how to manage effectively.

This curriculum is based on what I taught myself over the last 4 years to climb the ranks from engineering manager to director. This curriculum is also the same one I will use to continue up the ranks to VP and CTO as I believe the advice and needs change as you move higher in an organization.

I'm going to arrange this much like you would a computer science curriculum: start with some core courses (books) and then bridge into more advanced concepts, with optional side tracks for certain specialties (or weaknesses) to focus on as you gain more knowledge.

## Freshmen Core Curriculum (Mandatory Reading)

I consider this to be the base of all learning for engineering managers - from line managers who call themselves "Engineering Manager" all the way up to founders. Anyone can get something great out of these books and they are the ones that are recommended the most often:

### The Effective Manager & The Manager Tools podcast

When I first started out I just wanted someone to walk me through the basics. That's exactly what this book and the podcast do for you. I'll simplify it even further: [listen to the Manager Tools Trinity](https://www.manager-tools.com/2009/10/management-trinity-part-1). This is your 101 intro course.

**If you took nothing else away from this post, listen to the Trinity.**

It is opinionated. It might even be considered controversial. But if you nailed **one-on-ones, feedback, coaching, and delegation** you would be 80% ahead of your peers.

The book expands on these 4 concepts with examples aplenty but I'd recommend you listen to these 3 podcast episodes on repeat until every concept from each episode is drilled into your brain. Nothing has carried me as far into my management career as these three episodes.

### Extreme Ownership

This one is so foundational that I would argue this is for everyone, not just managers. In a way, I could consider this to be the prologue to the management curriculum.

This book was written by a Navy SEAL officer who says that **you own everything in your life.** And I do mean _everything_. Every failure, every win, and every stalemate is attributed to something you have done.

Your developer didn't push code with tests? You own that because you didn't ensure they internalized the policy on tests must be delivered with code.

Your team missed a deadline? You didn't reduce the scope and have an exit strategy for what could happen if the milestones slip.

No matter what the question, it all comes back to _you_. The magical thing is once you internalize this concept and lead by example, _everyone_ on your teams picks up on this and they also share in that ownership. It's an accountability feedback loop that only trends in a positive direction.

### Army Leadership and the Profession (ADP 6-22)

Speaking of military books, here's another leadership book is taken directly from the US Army that you can read for free [here](https://armypubs.army.mil/epubs/DR_pubs/DR_a/pdf/web/ARN20039_ADP%206-22%20C1%20FINAL%20WEB.pdf).

I didn't find this book until I was a director but this is one of the highest-recommended books on leadership I've ever come across. _This_ is an actual Leadership 101 book. It covers everything from mindset to effective communication to how to actually lead and get others to follow.

While _TEM_ is the most tactical 101 book for your needs as an engineering manager, this is the true 101 must-read for any manager in any profession. If the world's most powerful military uses it to train their officers then I would imagine it can be a great foundational read for you as well.

### The Manager's Path

Once you've nailed the tactical basics with the right mindset, only then can you begin to see the hierarchy of how one develops into a manager from a technical lead. This is a bit more high-level than the other two books because it covers the whole path from tech lead all the way up to VP and CTO.

I like this book because it helps you understand the relevant concepts that are most important at each level. I reference this book every once and a while even as a director just so I can refresh my memory on what is most important at my current level and how to start thinking about operating at the next level.

### The 27 Challenges Managers Face

This is your dictionary and thesaurus of engineering management. As the book says, it walks you through how to handle 27 of the most common challenges with the people on your teams. Everything from under-productive developers to personal conflicts, this book walks through each scenario step-by-step.

Because you're never going to memorize, internalize, or encounter all 27 issues when you first start out, this book is best left to skim once to keep in the back of your head so that when you do encounter a situation, you can remember it was covered in this book and to quickly look it up for actionable guidance in your difficult situation.

## Sophomore Curriculum (for Directors)

Honestly, you could stop here and you'd be way ahead of other managers in the field. At some point, you'll move up in your career and you'll need to focus on a different set of things.

Instead of learning how to hire engineers and conduct interviews, you're learning how to create hiring programs. Instead of hiring for a budget, you're drafting the budget. Instead of building a team, you're building many teams and training those managers to run those teams autonomously.

These are the books for your next set of challenges:

### An Elegant Puzzle

This is arguably my favorite engineering management book. Like _The Effective Manager_, it has play-by-play guidance on extremely important topics to managers. The difference is that _TEM_ covers the tactics to manage individual contributors (ICs). _An Elegant Puzzle_, however, covers the tactics to manage and build teams.

As a director, this is my CLRS _Algorithms_ book. It is both my bible and my reference guide for all of the major facets of my job as a director. It covers everything from how to size engineering teams to how to build a hiring funnel.

Given that Will wrote this as a tech leader at Stripe (and is now a CTO at Calm) this book is useful for anyone at the director stage and above. I am sure I will continue to reference this as I transition into senior management roles.

### The Talent Code

This is about how to nurture talent and bring out top performers. I debated whether or not to add this to the Core Curriculum because it is something any manager would benefit from learning more about. However, this is more of an advanced concept in coaching and arguably about identifying top talent as a hiring manager.

Therefore, I just don't see this as a core reading but if you are so inclined to dive more into the coaching aspect of management, this is a great book.

There is also _The Little Book of Talent_ which is like an abridged version of this book with 52 specific tips and traits for fostering talent which is an acceptable substitute for this one if you didn't enjoy this book. Personally, I like _TLBOT_ more since I found the backstories of some of these talent aspects to be ranting diatribes but to each their own.

### Accelerate

This book is not about management. But once you become a director you'll want to start measuring the performance of your teams to help identify bottlenecks, weak points, and performance gaps. _Accelerate_ provides the most robust, scientifically-backed means with which to do so.

The book is the foundation for the [DORA metrics](https://cloud.google.com/blog/products/devops-sre/using-the-four-keys-to-measure-your-devops-performance) (also known as the "Four Keys") so you could theoretically skip the whole book and read just this blog post and you would get the gist of what they are trying to accomplish.

If you do want to read more, the actual meat of the study and the takeaways are in the first half of the book. If you're a real nerd for the scientific method, the second half of the book goes into the details of how the study was conducted and really dives into _how_ they determined the results while the first half focuses on _what_ the results were.

## Junior Curriculum (for VPs)

As I mentioned before - I've never been a VP of Engineering so I can't speak to whether these are good books for VPs, but they are books I have read and plan to revisit when I get there because I found them to be unnecessary for my journey so far.

### High Output Management

To start, this is a great book and is in the engineering management canon for a reason. That reason is it was written by the former CEO of Intel who helped transform the company into the leading semiconductor outfit in the world.

But that's exactly it - it was written by a CEO. The things he needs to think about to run a successful organization are not what you need to worry about as a line manager or even as a director. All of the other books above should come before you read this book. I know this because I read it and I don't remember what I retained from it.

I know it was a great read at the time but the only thing I can remember that applies to my work is that there was a massive 200-something point checklist at the end of the book to evaluate yourself and your organization in areas for improvement. This is exactly the kind of list that is useful for someone who runs an entire engineering organization but is major overkill if you don't.

And even then, this book was written in 1995 so there are a lot of stories and concepts that are both dated and more relevant to the hardware industry (while I primarily cater to software leaders).

I'll definitely reference this for the next company I join but if you read the other books first you'd be in a great spot.

### Trillion Dollar Coach

Warning: I haven't read this book yet. That alone should be a reason why you can understand why it is in the higher-level curriculum than my pay grade. Why?

So far, everyone who has recommended this book to me has either been a CEO or a CTO of a startup. And this book is about engineering startup playbooks. This tells me that it's all about building engineering teams with top-brass leaders in mind.

The title gives that away: the term "trillion-dollar coach" comes from the fact that this management consultant has helped guide the chief executives at Apple, Google, and Intuit, who have a combined market cap well into the trillions.

Browsing the table of contents, terms like "team first" and "envelope of trust" are the kind of visionary, aspirational wordsmithery of those who command a very senior title. Therefore, this is a book I want to get to but I'd rather work through much of my reading list first before I get here.

## Senior Curriculum (for CTOs and Founders)

At this level, you're writing checks or you're pitching to the people who write those checks. If line managers are tactical, directors are strategic, and VPs are visionary, then you are the foundation and the exemplar. Everything stems from you.

You might be a technical co-founder or you're incoming CTO of the entire United States in which case thanks for reading my blog, I have no idea how you got here or why you're taking advice from me.

I have co-founded [a venture of mine](https://gettoknowapp.com/) which is just me and a friend so I can't say these books help in managing humans but they have been useful as the CTO of that outfit.

### Venture Deals

This is a great founder 101 book because it explains all the nuances of setting up a software company, scaling it, and all of the nuances about raising capital and pitching to investors.

While it is definitely framed more for founders than for leaders at established companies, there is some _excellent_ wisdom in here about how to organize your company. There is a great exercise in here about how you split up all of the executive roles based on the number of founders you have and how to complement skill sets based on all of the functions a company needs to operate, such as marketing, sales, and engineering.

What resonates with me as a leader is that **building teams require some combination of redundancy and complement in skill sets.** Know what skills you need to bolster (e.g. front end) and what skills are lacking to round out your teams (e.g. platform engineering) and it will be easier to scale with the needs of your organization.

### The Hard Thing About Hard Things

I'll admit, this is one of those pop non-fiction books that have a lot of cool stories marrying Ben Horowitz's love of technology and hip-hop. I came for the stories and stayed for a few nuggets of sage advice. The biggest one being **nip rumor culture in the bud.**

This one I noticed from a previous startup and when it's bad, it is toxic. Knowing what it looks like first-hand and how to resolve it (thanks to this book), I'm much more aware of how to maintain a strong culture with integrity and honesty now and how information needs to travel from the top down.

The other one I remember is to **take care of your people.** Empathy, trust, and psychological safety are [tops on my list](/blog/technical-lead-management/) of skills to build as a leader and there are a lot of examples where Ben was put in a rough situation and had to either fire someone compassionately or ask way too much from his employees. He knew the only way he could ask so much as if he spent the rest of his time caring for his people.

That kind of caring takes _years_ to build with your teams and the only way you will get people to join your crazy ride is to deeply care about how they are as people.

### Zero to One

This is another pop non-fiction book from Peter Thiel that everyone seems to reference but is really full of stories and lacking in major tactics. You walk away from books like this and _THTAHT_ with like one or two nuggets and appreciate for what it is: a fun read feigned as education.

Thiel really lets his libertarian pride shine in this one. I'm not trying to get political here but the main takeaway I got from this book is that **moats are powerful** and **crush your enemies with a monopoly.**

I think if I were running a venture-backed firm with a bit more fire under my ass this book would have spoken to me more but I found it a bit too cutthroat for me. Your mileage may vary and it is a highly-recommended book so I'm still willing to include it here.

### Traction

Another book that is not about management. But the only way you'll grow big enough to be a manager of people is to build something people want so much that you need to hire folks to build it fast enough.

_Traction_ has a ton of great insights I've used to build my side projects into profitable ventures with tactics on how to get users. I haven't even begun to use all of the tricks in this book but it is absolutely a worthwhile read particularly if you are an early-stage software startup founder.

## Specialty tracks and additional books I've enjoyed

Many college CS programs offer you the opportunity to specialize in certain disciplines within computer science after you learn the basics. These include artificial intelligence, programming languages, and human-computer interaction.

Similarly, engineering management has a few specialties as well and I've read a few books that delve into very particular facets of the management craft:

### Influence, persuasion, and soft skills

_How to Win Friends and Influence People_ is a classic among the classics. I think it is one of the most highly-read non-fiction books of all time.

The title is so to the point you might hide the cover on the subway for fear you might look like a social Luddite. It is not that kind of book. This book is not how to make friends for dummies.

This is a book on getting others to do things you want _without losing your moral compass_. Influence is a very neutral term: to some, it sounds like something slimy a salesman does to convince you to sign on the dotted line. To others, it sounds like the kind of charismatic tactic President Clinton used to win over voters in the presidential debates.

I'd say this book is one of those gentle reminders that **you should be nice to people and that being nice to people makes it much easier to get them to see your side.**

This book just has to be read (and re-read) to be believed. It should be on everyone's reading list for the simple fact it's so widely accessible and in every library that it's basically free.

### Health and productivity

_How Not to Die_ is a mammoth text at over 570 pages. It's not a management book. It's not a software book. It's not even a self-help book by how you would commonly define that genre of book. This is a book on healthy eating.

In a year like we've just had, health has become a front-and-center reality for us all. Healthy habits have [been shown](https://www.cdc.gov/workplacehealthpromotion/model/control-costs/benefits/productivity.html) to increase productivity.

I live by six healthy habits that doctors recommend every day:

1. Eat right
2. Exercise
3. Don't smoke
4. Drink in moderation
5. Reduce stress
6. Get enough sleep

This book tackles habit number 1. It explains how [this doctor's list of a dozen food groupings](https://nutritionfacts.org/video/dr-gregers-daily-dozen-checklist/) help reduce all-cause mortality for all but one of the top 20 reasons people die (the one it can't help are car accidents). From obesity to cardiovascular disease, all of these foods and habits help you improve your health through the basics we've learned all along: a diet rich in whole fruits, vegetables, legumes, and grains is shown to improve health.

There is even an app for both [iOS](https://apps.apple.com/us/app/dr-gregers-daily-dozen/id1060700802) and [Android](https://play.google.com/store/apps/details?id=org.nutritionfacts.dailydozen&hl=en&gl=US) to help you track your daily progress on eating these key foods. I use it and my Apple Watch as one of my key health staples for ensuring I meet these 6 habits every day.

A healthy manager is a productive manager operating a peak performance.

### Giving feedback

_Nonviolent Communication_ spells out a four-step process for delivering feedback. This one was a game-changer for me on how to speak to people in a way to promote behavior change without casting blame or making people feel bad.

If you want the ultimate deep-dive into how to deliver feedback that resonates with your directs, this is a book to read and study carefully.

### Salary negotiation, hiring, and retention

_Never Split the Difference_ is great whether you are looking for a new job or you are trying to hire a key candidate. It was written by an FBI hostage negotiator who takes a pretty hard stance on how to jump into a negotiation.

This is another one of those books that take 300 pages to convey a few key points as most pop self-help books do these days. I thought the book was entertaining so I didn't mind the fluff but the biggest takeaway I got from this book is the phrase **how am I supposed to do that?**

If I recall correctly, the author was put on the spot to negotiate a really tough situation like how to give terrorists millions of dollars or else they will kill the ambassador. Most people who read this panic and have no idea what to do and figure the author is totally trapped into this nearly-impossible scenario. His response is clutch: "how am I supposed to do that?"

In other words, **if the person on the other side of the table is being unreasonable, turn their poor offer back on them through empathy**. Let's say you're making a cool $150K at your current job and the company interviewing you just offered you $110K for acing their interview.

You can reply with "how am I going to be able to take that offer when I'm already making more than that?" You're putting the onus on the hiring manager to come up with a really good reason why they should make the obviously irrational choice of choosing to leave their current job for a worse one with a lower salary. This forces the hiring manager into a situation where they have to come back to the table with a stronger offer.

As a manager, you can use this tactic as well...but I've already said enough about this book. It's great and it goes into a bunch more tactics on negotiation which you can use when looking to hire new members for your team(s).

### Interview process & design

_Cracking the Coding Interview_ is the canonical reference for developers looking to pass the interview at the FAANG companies and land that big salary. The whole purpose of this book is to game the system like one does for studying the MCAT or SATs with practice questions used at real companies.

**I'm _not_ recommending this book so you can just copy questions from here to use in your interviews.** In fact, I am recommending the opposite: this book is a great way to identify which questions are complete _nonsense_.

If you're a web startup you have no business asking JavaScript developers about bit manipulation and bit shifting. Is it useful knowledge? Possibly. Do any of you read this article actually use heavy bit shifting techniques in your code? Extremely unlikely.

The only time I found a bit manipulation interview question praiseworthy was an interview I did many years ago for a hardware company that built IoT devices. They actually used these techniques and the interview question was actually really fun.

For the rest of you, this is a net-negative and just makes you look pretentious and leading to more false negatives than you can stand to lose in the hiring process.

The bonus for reading this book is if you ever decide to become an IC again you'll have no trouble getting a job or grinding LeetCode.

## Books on my list that I haven't organized yet

Finally, here are the books on my reading list as a manager that I haven't read before and I don't know where to bucket them in the curriculum yet. As I read them I'll update this post with where they should fit. For now, here are the books on my reading list  that I hope to get through in 2021:

* _The Art of Leadership_
* _The Charisma Myth_
* _The Coaching Habit_
* _Confessions of an Advertising Man_
* _Content Inc_
* _Deep Work_
* _Difficult Conversations_
* _The Effective Executive_
* _First, Break All the Rules_
* _The Five Dysfunctions of a Team_
* _High Growth Handbook_
* _It's Not How Good You Are, It's How Good You Want To Be_
* _Leadership Step by Step_
* _The Making of a Manager_
* _Managing Humans_
* _Managing the Unmanageable_
* _Measure What Matters_
* _Mindset: The New Psychology of Success_
* _No Hard Feelings_
* _The ONE Thing_
* _Only the Paranoid Survive_
* _The Phoenix Project_
* _Quiet Leadership_
* _Radical Acceptance_
* _Radical Candor_
* _Resilient Management_
* _Righting Software_
* _Running Lean_
* _Software Engineering at Google_
* _Sprint_
* _Start with Why_
* _Strategic Logic_
* _Team of Teams_
* _Team Topologies_
* _Technical Leadership and the Balance with Agility_
* _Technology Strategy Patterns_
* _Thinking in Promises_
* _The Timeless Way of Building_
* _Turn the Ship Around!_
* _The Win Without Pitching Manifesto_
* _Your Code as a Crime Scene_
