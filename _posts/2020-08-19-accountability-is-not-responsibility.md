---
title: Accountability is not responsibility
description: People seem to think accountability and responsibility are interchangeable. They are wrong and here's why...
layout: post
date: '2020-08-19 00:05:00'
tags: ownership leadership
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

I've noticed some of my colleagues seem to use the terms _accountability_ and _responsibility_ as if they were the same.

They are not. I'll tell you a story to illustrate the difference.

When I was a kid my best friend and I were inseparable. In the summers, not a day could go by without one of us going over to each other's house.

One day I was over his house when my mom came over to pick me up with my younger brother. Since my best friend and I were inseparable you can understand that my mom was also friends with my best friend's mom so she didn't just drive up to the driveway to get me. She got out of the car, said "watch your brother" and started chatting with the family.

While our moms were distracted catching up, the boys were focused on a game of hit the ball over the fence. I should mention we were pretty young: I don't think I was much over the age of 10 which means my brother was closer to 5-6. Young enough not to know better. So when I tossed the ball up in the air to knock it out of the park, my brother came fast approaching.

On my back swing before hitting the ball, I instead hit my brother in the jaw with the tip of my metal baseball bat. The thud instantly caught the attention of my mother and she reached to comfort my brother. You know an injury is bad the longer it takes for a child to cry. Bumped your head on the coffee table? Instant tears. Hit with a baseball bat (unintentionally, of course)? There's a good 10 seconds of processing what happened followed by ear-shattering screams.

First thing my mom did was ask me what happened? As the good boy scout I couldn't possibly tell a lie so I told her exactly what you read: I swung the bat backwards, didn't see my brother, and his face met my bat. It was my fault; I didn't look before I swung a giant metal bat.

## What does this teach us about accountability and responsibility?

It was my **responsibility to look out for** my brother. I was responsible for his well-being and thus I was responsible to look around to ensure it was safe to swing a bat.

Meanwhile, I was **accountable for the injury**. The who, what, when, where, why all fell on me: I did the crime, I didn't check my surroundings, and my brother paid the cost.

The easiest way to distinguish the two is to know that **responsibility is for the future and accountability is for the past**.

When you think about how you lead know that you are **responsible for the productivity, effectiveness, and well-being of your teams**. There's a lot of "who" and "when" tied to responsibility: taking great people and helping them achieve things in their future.

When you think about projects you've accomplished or failed then you are **accountable for the result**. As I've seen called out time and time again, you give credit to everyone on your team when you succeed and you take it on the chin yourself when you fail.

## Why do you need to know the difference?

In leading by example and teaching others we need people to understand that **when our future leaders gain responsibility they also gain the outcome of those new duties**. A young and ambitious engineer is promoted to senior with a big pay bump and fresh title. It's easy to get on their high horse and relish in the pay and status.

But a senior title now comes with on-call duty 24/7 for one week every 2 months. Their code is subject to more scrutiny and demands a higher level of quality. Fewer mistakes are tolerated. You have to earn your keep. And now that this engineer can become a tech lead they also have to answer to the boss if the project fails. Before they could hide in the shadows if a project was delayed. Now they are front and center to answer if something is delayed or broken.

Aim to increase your responsibility. Just know that this comes with more things to answer for if things go wrong.

