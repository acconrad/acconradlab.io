---
title: Always have the Beginner Mindset
description: Thinking like a beginner and staying humble will always be an asset in your career.
layout: post
date: '2019-12-13 12:19:06'
tags: learning beginner career-development
subclass: post tag-test tag-content
categories: career
navigation: 'True'
article: 'True'
---

The Beginner's Mindset is about approaching learning with open arms and the understanding that you don't know everything. You intuitively have this approach when you actually are a beginner but if you maintain that approach as an expert it will serve you well.

I still keep this mindset with everything I do because staying humble and thirsty to learn keeps you ahead of your peers and you'll learn faster. Here are a few things I keep in mind when learning with a beginner mindset.

## Learn how to learn

It sounds like a ridiculous premise given that many folks spend much of their childhood in a classroom learning. Unfortunately, most schools teach but that doesn't mean students learn. And like anything, learning is a skill that can be acquired (and is not taught in primary school).

The best way to learn this is to [take this Coursera course](https://www.coursera.org/learn/learning-how-to-learn/) which is really both a class on how to learn and how to read effectively. And while you're at it, you might as well learn how to [speak](https://www.youtube.com/watch?v=Unzc731iCUY), [write](https://www.youtube.com/watch?v=vtIzMaLkCaM), and how to [think critically](https://www.coursera.org/learn/mindware?).

## Throw out your assumptions

If you're starting to practice the beginner's mindset a decade into your career, you'll have a lot of knowledge already built up. This will only bias you into thinking like an expert, which will hinder your progress.

Instead, throw out all of your pre-existing assumptions and **assume you might be wrong**. This opens you up to new possibilities that you likely didn't consider because of all of your inherit biases you've built up over the years. Now you can explore ideas and concepts with a whole new chain of connections that you'd previously cut off.

## Stop jumping to solutions

Another problem with thinking with your experience is when you see a new problem, you rely on your previous solutions to create a novel way to solve this one. Again, if you think like a beginner, you don't have previous solutions to rely on. What you think is a perfect solution is no longer relevant so you should scrap it.

The beginner's mindset means **asking why all of the time**. This is so effective there is an entire [Wikipedia entry on this concept](https://en.wikipedia.org/wiki/5_Whys) that I would encourage you to investigate further.

## Beginners dream

Something happens as we gain experience: we also gain cynicism. We see what has succeeded and what has failed and that hurts our ability to think ideal states. We know the world is shades of gray but why should that hinder us from believing beyond our true potential?

My favorite example of this is Airbnb's [11-star customer rating framework](https://uxdesign.cc/applying-airbnbs-11-star-framework-to-the-candidate-experience-3f0b9c4e68a3?gi=88688bfe0579). Everywhere you go you either see a thumbs-up/thumbs-down rating or a 5-star rating system. Airbnb wanted to know what happens if you break the glass ceiling.

What would a 6-star rating on Airbnb look like? I mean, what does more than 100% look like to you? That must be pretty great.

Well, then what does a 7-star rating look like? Can I even fathom that kind of an amazing experience?

Okay then tell me about a _10-star experience_. Uuh...what are we, the Four Seasons hotel in my tiny Boston apartment? Hey! That's interesting; are there any elements of the famous Four Seasons hotel chain that we can replicate even in the smallest apartments we list on Airbnb?

Beginners don't know where to stop because they've never reached the end before. That means they are free to dream the greatest of dreams without limitations. This unshackling of limitations is not only liberating, but idea-generating. What would an 11-star experience look like for _your_ business?

## Embrace the haters

Another issue with cynicism is you know where you might fail. You anticipate criticism and faults so you are quick to deflect criticism with retorts rather than _embrace_ it.

What if that criticism is advice? What if, what sounds like criticism, is actually a cry for help? And finally, what if the critics are right and you are wrong?

When you think like an expert, you assume you are right from the start. When you are a beginner, you assume you are _wrong_ from the start. People who want to learn and grow will crave feedback and help if they think they don't have all the answers.

Can you see how limiting it might be to think like an expert?

## Teach what you learn to internalize

Finally, just because you learned something doesn't mean you know it. How many TED talks have you listened to that went in one ear and out the other? If you took the above course on how to learn and you're checking back in here, you should know a great way to internalize what you've learned is to revisit the material and ideally write about it. And what do you know, a platform like a blog is a perfect way to teach others and engage your mind with active learning techniques.

Trust me, nothing is more gut-wrenching than trying to write a blog post on a technical topic and posting it to the internet. The best defense against looking stupid is to really know the material. Writing is a great incentive to force yourself to learn what you are talking about so you aren't embarrassed.

The beginners mindset has allowed me to stay humble, stay open to possibilities, and most importantly, stay hungry to learn and grow as I move into the next phase of my career. Hopefully it will do the same for you as well.
