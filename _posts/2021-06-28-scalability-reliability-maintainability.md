---
title: Scalability, Reliability, and Maintainability
description: Ch 1 of my book club review of Designing Data-Intensive Applications by Martin Kleppmann.
layout: post
date: '2021-06-28 15:28:00'
tags: systems-design book-club scalability reliability maintainability
subclass: post tag-test tag-content
categories: systems-design
navigation: 'True'
article: 'True'
---

Like my series on [Algorithms](/blog/why-hiring-is-broken-and-how-im-dealing-with-it/), I've decided that I need to _really_ understand systems design.

I run hiring for multiple teams. Some of our questions revolve around systems design. How can I possibly ask these questions if I couldn't answer them perfectly myself?

It's only fair that I master these concepts. The book that everyone seems to agree with is the best book on this topic is [_Designing Data-Intensive Applications_](https://dataintensive.net/) by Martin Kleppmann.

In the spirit of my last educational series, I will transcribe my raw notes over the next 7 weeks or so. I do this both to teach my readers and also to reinforce the concepts myself. As I recall from my course on [learning how to learn](https://www.coursera.org/learn/learning-how-to-learn), one of the best ways to learn is to teach others.

## The syllabus

I've decided to craft a syllabus for myself as if I were both the teacher and the student to help keep me on track and reinforcing the concepts. If you have a similar learning plan or have questions about mine, please feel free to [reach out](/newsletter).

* Week 1 (this week)
  * Read Chapter 1: Scalability, Reliability, and Maintainability
  * Practice: [Design a scalable system to count events](https://www.youtube.com/watch?v=bUHFg8CZFws)
  * Explore, at a high-level, data tools in the space
* Week 2
  * Read Chapter 2: Data Models & Query Languages
  * Practice: [Top K Problem](https://www.youtube.com/watch?v=kx-XDoPjoHw)
* Week 3
  * Read Chapter 3: Storage & Retrieval
  * Practice: [Distributed cache](https://www.youtube.com/watch?v=iuqZvajTOyA)
* Week 4
  * Read Chapter 5: Replication
  * Practice: [Rate limiter](https://www.youtube.com/watch?v=FU4WlwfS3G0)
* Week 5
  * Read Chapter 6: Partitioning
  * Practice: [Notification service](https://www.youtube.com/watch?v=bBTPZ9NdSk8)
* Week 6
  * Read Chapter 8: Faults & Reliability
  * Practice: [Distributed messaging queue](https://www.youtube.com/watch?v=iJLL-KPqBpM)
  * Read: [Facebook's messaging queue with HBase](http://highscalability.com/blog/2010/11/16/facebooks-new-real-time-messaging-system-hbase-to-store-135.html)
* Week 7
  * Read Chapter 9: Consistency & Consensus
  * Practice: Read the [High](http://highscalability.com/blog/2011/12/19/how-twitter-stores-250-million-tweets-a-day-using-mysql.html) [Scalability](http://highscalability.com/scaling-twitter-making-twitter-10000-percent-faster) [Series](http://highscalability.com/blog/2013/7/8/the-architecture-twitter-uses-to-deal-with-150m-active-users.html) on Twitter and implement it
* Week 8
  * Skim Ch 10 & 11: Stream and Batch Processing
  * More possible practice that leverage stream/batch processing like [Uber](https://www.youtube.com/watch?v=umWABit-wbk)
  * Listen: a [podcast review](https://www.youtube.com/watch?v=grGqCuTcu50) of the book

## Notes on Chapter 1

Chapter one is really an overview chapter. It's easy to read and easy to skim. There aren't too many deep concepts here. I found lots of shortlists/rules of thumb to be all you need to focus on the basics.

### Data storage

When thinking of data storage, consider these 5 buckets:

1. DB engines
  * **Row:** good for <abbr title="Online Transaction Processing">OLTP</abbr>, random reads, random writes, and write-heavy interactions (Postgres, MySQL)
  * **Column:** good for <abbr title="Online Analytical Processing">OLAP</abbr>, heavy reads on few columns, and few writes (HBase, Cassandra, Vertica, Bigtable)
  * **Document:** good for data models with one-to-many relationships such as a <abbr title="Content Management System">CMS</abbr> like a blog or video platform, or when you're cataloging things like e-commerce products (MongoDB, CouchDB)
  * **Graph:** good for data models with many-to-many relationships such as fraud detection between financial and purchase transactions or recommendation engines that associate many relationships to recommend products (Neo4J, Amazon Neptune)
  * **Key-Value:** essentially a giant hash table/dictionary, good for in-memory session storage or caching e-commerce shopping cart data (Redis, DynamoDB)
2. **Search indexes** (ElasticSearch, Solr)
3. **Caches** (Memcached, Redis)
4. **Batch processing frameworks:** good if data delay can be several hours or more and you can store all the data and process it later (Storm, Flink, Hadoop)
5. **Stream processing frameworks:** good if data delay can only be several minutes and you need to store data in aggregate while processing data on-the-fly (Kafka, Samza, Flink)

### Systems design in a nutshell

Approaching systems design falls into 5 steps:

1. **Functional requirements** are designed to get us to think in APIs where we translate sentences into verbs for function names and nouns for input and return values.
2. **Non-functional requirements** describe system qualities like scalability, availability, performance, consistency, durability, maintainability, and cost.
3. **High-level designs** present the inbound and outbound data flow of the system.
4. **Detailed designs** are for the specific components you want to focus on. Focus on the technologies you want to use for the data you want to store, transfer, process, and access.
5. **Bottlenecks & tradeoffs** ensure we know how to find the limits of our designs and how we can balance solutions since there is no one singular correct answer to an architecture.

### Requirements gathering

When asking questions regarding a product spec for a large-scale system, focus on these 5 categories of questions:

1. **Users:** Who are they? What do they do with the data?
2. **Scale:** How many requests/sec? Reads or writes? Where is the bottleneck? How many users are we supporting? How often/fast do users need data?
3. **Performance:** When do things need to be returned/confirmed? What are the tolerance and SLAs for constraints?
4. **Cost:** Are we optimizing for development cost (use OSS) or operational/maintenance cost (use cloud services)?
5. **CAP theorem:** Partitioning is something you know you'll have to account for with highly-scalable systems. So it may be easier to ask what is more valuable: consistency or availability?

**If consistency is most important**, consider an <abbr title="Atomicity Consistency Isolation Durability">ACID</abbr> database like Postgres or even a NewSQL database like CockroachDB or Google Spanner. **If availability is most important**, consider a <abbr title="Basic Availability Soft-state Eventual consistency">BASE</abbr> database like an eventually consistent NoSQL solution such as CouchDB, Cassandra, or MongoDB.

Even better is to [use this diagram](https://blog.nahurst.com/visual-guide-to-nosql-systems) to map your concerns onto a pyramid. Given that you can only ever expect 2 of the 3 parts of the CAP theorem to be satisfied it might actually be better to ask _which property is least important?_ If it's...

* **Consistency** - most NoSQL solutions will work like Cassandra, CouchDB, and Amazon Dynamo
* **Availability** - some NoSQL solutions and some NewSQL solutions like Bigtable, HBase, MongoDB, Google Spanner, and Redis
* **Partition tolerance** - any relational or graph solution like Postgres or Neo4j will work since these are notoriously difficult to partition compared to the other solutions

Though likely everyone [misunderstands the CAP theorem](http://pl.atyp.us/wordpress/?p=2521) so I would read this a few times and internalize the example.

### The three system qualities in 1 line

This chapter can effectively be summarized in 3 sentences:

1. **Scalability** determines if this system can _grow_ with the growth of your product. The best technique for this is _partitioning_.
2. **Reliability** determines if this system produces _correct results_ (nearly) each and every time. The best techniques for this are _replication_ and _checkpointing_.
3. **Maintainability** determines if this system can _evolve_ with your team and is easy to understand, write, and extend.

### Further reading and study

As I said before, this is a pretty simple chapter. I also watched [this systems design walkthrough](https://www.youtube.com/watch?v=bUHFg8CZFws). This video extended these concepts and informed some of these notes. I like to accompany learnings with practice to seed new questions for our own [interview process](https://indigoag.com/join-us#openings).

[This article](http://highscalability.com/youtube-architecture) on YouTube's architecture further reinforces the sample problem on the YouTube video (how meta). You can check your solution against the one that was really used by YouTube.

Finally, you can rifle through a bunch of [these videos](https://www.youtube.com/playlist?list=PLMCXHnjXnTnvo6alSjVkgxV-VH6EPyvoX) fairly quickly as each touches on a small subset of system design techniques.

Check-in next week with a summary of Chapter 2 of the book: _Data Models & Query Languages_!
