---
title: Are middle managers useful?
description: As a middle manager, you have several teams with dedicated managers. If you have good managers, what is your value?
layout: post
date: '2021-05-10 11:04:00'
tags: middle-management leadership director-level
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

I did an AMA on Clubhouse a little while ago and there were a lot of awesome, hard-hitting questions on the panel. So for the next several weeks, I'm going to write out my thoughts to each of the questions and how I answered them.

> As a middle manager, you have several teams with dedicated managers. If you have good managers, what is your value?

As a middle manager, I hope I'm useful! But I see where this question is coming from: _if you have line managers coaching the developers, what do you need a manager of managers for_?

I think this question suffers from an assumption that there are only two kinds of managers; those that lead people and executives who call the shots.

In reality, there are three kinds of managers and we know this to be true because it exists in the military: junior-grade, field-grade, and general-grade officers. To relate this to engineering management:

* Engineering Manager is a base level manager that runs a team (military company). This is a junior officer (i.e. Lieutenant)
* Senior Engineering Manager is an experienced manager that runs a larger (or multiple) teams/companies. This is a senior officer (i.e. Captain)
* Director is a manager of managers (and thus teams). This is a field-grade officer (i.e. Major)
* Senior Director is an experienced director. This is a senior field-grade officer (i.e. Colonel)
* Vice President manages directors (and thus whole divisions). This is a general-grade officer (i.e. Brigadier General/Rear Admiral)
* Senior Vice President is an experienced VP. This is a senior general-grade officer (i.e. Major General/Lieutenant General/Vice Admiral)
* Executive Vice President is a top VP, and is often an interchangeable with CTO. This is the highest-ranked officer (i.e. General/Admiral)

**Middle managers exist to _set direction_ and _coach line managers_.** For larger organizations, the gap is too large between the executives/generals and the line managers/lieutenants. So there needs to be folks who can relate to the junior managers who aren't so busy building visions and setting budgets with many commas after them.

There are [lots of great books](/blog/teach-yourself-engineering-management) on being a great director. The first two that come to mind that I would recommend are [An Elegant Puzzle](https://www.amazon.com/gp/product/B07QYCHJ7V/ref=x_gr_w_glide_sout?caller=Goodreads&callerLink=https://www.goodreads.com/book/show/45303387-an-elegant-puzzle&tag=x_gr_w_glide_sout-20) and [The Manager's Path](https://www.amazon.com/Managers-Path-Leaders-Navigating-Growth/dp/1491973897). The first one has been my playbook as a director while the second book covers the full manager progression all the way up to VP and has a great section on directors.

Check out both of those books and remember to **coach managers** and **set direction for all of your teams** and you will be extremely valuable to any organization!
