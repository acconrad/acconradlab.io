---
title: How to justify your cost to a client
description: Don't let customers have sticker shock when you tell them how much you cost.
layout: post
date: '2019-06-06 21:50:40'
tags: pricing value
subclass: post tag-test tag-content
categories: consulting
navigation: 'True'
article: 'True'
---

> **TLDR: Price yourself in a way that _feels_ low to the client even if it is high in absolute terms.**

If you're new to freelancing or [consulting](https://anonconsulting.com/) the advice that gets passed around a lot is to not charge by the hour because then you're making yourself a commodity. That instead you should use [value-based pricing](https://training.kalzumeus.com/newsletters/archive/consulting_1). In software, this is flawed for 2 reasons:

1. **Most software gigs are longer-term.** Marketers and designers can easily charge value-based pricing because their projects or campaigns are extremely finite. A campaign or a logo are just that - you might iterate a few times to get what you want but once you get the deliverable that's it. Even if you do a shorter 4-6 week engagement as a software developer you will inevitably have a bug or security update that will require maintenance after the engagement is over; it just doesn't "end" when you deliver the code.
2. **Most companies don't price software products; they price developers.** Companies don't hire freelance designers, they buy logos. Companies don't hire marketers, they buy campaigns. But _everyone_ hires software developers. They don't want to buy a SaaS app from you. They want to buy your time. So until the industry moves away from buying your time, the vast majority of clients will still value you as a dollar-per-hour commodity.

I know this having tried to win plenty of clients over with this approach. I also know this now having working at a company that hires lots of contractors.

So let's dive into the 5 Whys to see if we can find a root cause and a solution.

* **Why doesn't value-based consulting work?** Because companies still buy a software developer's _time_ and not what they _produce_.
* **Why can't we sell software and not ourselves?** Because software is a blurred line. Unless you are expected to write the entire application for a client what is your code versus someone else's is very hard for the client to define.
* **Why can't we clearly define the products we build?** Because software is built with teams. If you are trying to find clients chances are you are not their first (or their last) developer. Software is very difficult to get done as a solo project and is rarely done in a vacuum. Software is a team sport.
* **Why is software a team sport?** Because it's super hard!
* **Why is software hard?** Because we are trying to map the decisions of the human mind onto a flat glowing metal screen. How easy could it possibly be to describe the 300 billion neurons of the human brain?

So software is really hard to do on your own and therefore hard to distinguish your software product from the rest of the team you are joining as a contractor. Does that mean you should throw in the towel and give up? **No way!**

Yes, freelancing and consulting is super hard and unless you have the stomach to sell yourself and get lots of rejections you are going to have a bad time.

But there is one thing you can do to overcome the hurdle of a client justifying the cost of going with a contractor: **convert your desired value-based outcome into an hourly rate.**

## Estimates are your saving grace

The key to make this all work is to work backwards from your ideal to a realistic estimate. This is best illustrated with an example.

Suppose you have a project that you think is worth $100,000. You want to tell the client the charge is $100,000 because of blah blah something valuable. Now ask yourself how long do you think this will take? 10 weeks? That's about 400 hours or $250/hour which is pretty dang good.

The problem is that no one is going to pay $250/hour for a contract developer. Unless you work for a prestigious, well-established consulting firm, you are going to have a hard (if not impossible) time finding clients that will hire independent consultants at that rate.

**Here's the key:** if you tell your client you charge $100/hr now they're listening. It just means to get to that rate you would theoretically need to do 1,000 hours worth of work. **But your client doesn't care if it takes you 1,000 hours or 400 hours if they agree to pay you to complete the project in the time you've told them it would take.**

So _if you end up only taking 400 hours to complete the project_ you've upped your hourly rate to the desired rate for your original project _and_ the customer gets their project 60% faster than they thought they would have. You think they are going to complain if their critical project arrives early?

Isn't that [inflating your hours](https://en.wikipedia.org/wiki/Parkinson%27s_law)? On the contrary, there is an [entire law](https://en.wikipedia.org/wiki/Hofstadter%27s_law) devoted to a programmer's hubris in estimating software.

Your ideal hourly rate is probably too optimistic. A heavily-inflated hourly estimate is based on something like Hofstadter's Law to compensate for the things you don't know.

In the end, your actual hours billed in this example will probably fall somewhere in the middle around 700-750 hours because of things you failed to account for. Either way, you're still making more than the hourly rate you billed and your client still gets their project delivered on time or earlier.

Next time your client balks at your price remember Hofstadter's Law and buffer in quite a bit of extra time so you can land the project early, make more per hour, and use the rest of that time to find more clients and bill more hours.
