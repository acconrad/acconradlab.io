---
title: Build your network now
description: Not only is it never too late to start but you don't want to be without it when you need it most.
layout: post
date: '2019-04-02 22:49:10'
tags: networking relationships
subclass: post tag-test tag-content
categories: consulting
navigation: 'True'
article: 'True'
---

My contract got cut short. I've got to assemble another client in the next 30 days or the money stops coming in. Thankfully, I've built a strong network over the last decade I've been in this industry so I think I should be alright.

Would you be okay if this happened to you?

I've heard this phrase so many times it has been drilled into my brain when it comes to networking:

> Dig the well before you get thirsty

Most people think of networking as a _reactionary_ activity. You network with people at meetups because you need something from someone. No wonder people hate to go to meetups!

**Networking should be a _proactive_ activity.** You _offer_ help and connections. You build people up and bring them together _just because you can_.

What does this get you?

If you've asked that question you've already lost. Stop only thinking about yourself. **No one wants to network with selfish people.** The question you should be asking is:

> How can I make the world a better place for my fellow humans?

When you give back, you get more in return. And I don't mean this in the touchy-feely validating kind of way. I mean real benefits when your contract runs out and you need more money NOW kind of benefit.

Imagine your friend is looking to start a company. Imagine your other friend is looking to start a company. You see two people and one opportunity to actualize. In that moment it probably has no material impact on your life. But in the future when you are looking for a job and those two are now hiring, who do you think they are going to ask to fill their top spot? Who do you think will have their own network of folks looking for freelancers to help build the MVPs for their startups?

If you're thinking this imaginative scenario actually happened to me, you'd be right.

I actually helped connect my friends together to start a successful startup. It did nothing for my wallet for years. But eventually I ended up working for their company. And years after that they helped me land some key clients when I went into business for myself. And we're all still friends!

This stuff works. But it only works if you live with an abundance mentality that **it's not all about you**. If you only network when you're desperate and need something, you're going to have a bad time. Which is no wonder why everyone fails at it and thinks it's scummy and no fun.

If, instead, you **make networking about genuine, authentic connection**, you're apt to get everything you could ever want out of networking and way more than you bargained for. I may be a sample size of one but my hope is that you'll add to that list of anecdotes as well.
