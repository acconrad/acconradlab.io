---
title: Working Remotely When You Didn't Plan To
description: So many people are being forced to work remotely thanks to COVID and have no idea where to begin.
layout: post
date: '2020-02-27 23:00:19'
tags: tips remote-work communication
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

If you're reading this article at the time of its publishing, your professional world is probably upended with pleas (or demands) to work from home. I've embraced remote work culture for years and I believe it is the future of work.

Companies that embrace remote-first can increase the size of their candidate pool, reduce costs in lower cost of living areas, increase employee satisfaction by removing one of the worst things about a job (a commute), and hold a key advantage in flexibility that their competitors cannot leverage.

Because of this, my teams at [Indigo](https://indigoag.com) are remote-first and the majority of my team members live outside of the Boston metro area where our headquarters resides. Not only are we ready and able for remote work, we also perform at a very high level within our organization.

How are we able to do this? I'll share **three key strategies that are essential for remote work to succeed in today's workplace**.

## Over communicate

This may sound obvious to certain leaders but this cannot be understated: **digital communication requires overcommunication**. There are a few reasons you need to overcommunicate when moving to a remote organization:

1. Words can be misinterpreted. One's voice does not only communicate words, but tones, inflections, and emotion. This is all removed when your default mode is text rather than speech. Reinforcing this with multiple takes (and multiple mediums) for the same message will ensure no one gets the wrong message.
2. Text can feel restricting. As a rule of thumb, if you don't feel like your message is getting across with 3 attempts, **get on a video call or phone call with the other people.** Just because you are remote doesn't mean you can't fall back on video or audio to emulate face-to-face communication.
3. Not everyone will see your message when you send it. With remote comes an increased risk for asynchronous messaging and reduced priority that your message will be heard. The more times you say the same message, the higher the likelihood that everyone will get what you're saying when you say it.

So **say the same thing over, and over, and over, and over, and over again until you are 100% confident that everyone gets it**. They may not have seen it the first time, or interpreted the way you did, or could use the help of a personal chat with a tool like Zoom or Slack.

## Document everything

As I mentioned in the previous section, your medium has shifted from real-time, synchronous communication, to asynchronous communication. This is both a blessing and a curse.

It is a blessing because you will are now in control of your communication channels. In the office, if someone comes up to your desk and interrupts you, you have no agency as to whether or not this message is worth your time. Some people want to come by and chat. Others need your undivided attention. Now if eight people bug you on Slack, _you_ get to decide when and how to message them back. This is very freeing and allows you to take better control of your time.

It is also a curse because you now need to be on top of the myriad communication channels that exist. Some people love email, others love Slack, and still, others will use text message or a phone call to reach you. Guess what? You're now responsible to check all of them which will only continue to fraction your time with distractions.

To combat these problems, you need to **document any notes, action items, or checklists that could be useful to more than one person**. When you shift your mindset towards asynchronous communication, you are using documentation as your key lever. **Documentation is your leverage so you don't have to repeat yourself**. When you don't have to repeat yourself, your time doesn't have to be fractured answering so many questions on Slack or email.

How to get started with documentation is easy: write stuff down! Hopefully, in a centralized place. Unfortunately, in some organizations, there are many different tools used at once. For example, we use Microsoft OneDrive for Office _and_ Google Drive _and_ Confluence _and_ an internal Facebook wiki.

Yikes.

If possible, try to converge on one shared wiki tool and stick with it. For engineering, we use Confluence within the Atlassian ecosystem. So you know that if I wrote an article, it's on Confluence. **Do your best to write your content on one platform and make it known to help your coworkers reinforce the need for one central system.**

## Trust & transparency

The bedrock that supports all of the above is trust. Another one that seems obvious to many leaders but creeps insidiously on newly-remote organizations. Why? **When you go remote, you lose control over your team's time.**

This may be a hard one for you to hear but if you're used to showing up in the office you're likely preconditioned to assume that "butts in seats" equates to productivity. How will I know if my team is doing work and not slacking off? Are they putting in a full effort if they have a TV and a bed close to their desk?

I hate to break it to you, but you need to `l e t   g o`. That's right, we've got a control complex. And I get it; the American employment system since the Ford days has conditioned us to believe that productivity is correlated with in-office work.

Let me offer you a quick analogy as to how this changed my whole approach to trust with my organization:

### A quick aside on trust over a lifetime

When we were children, the world gave us structure and guidance because we were not capable of making sound decisions for ourselves. School starts at a specific time, you must eat the lunch given to you, and your whole world is regimented to the whims of the adults you're surrounded by.

Once you got to college, that whole paradigm changed. Professors did not care if you did the homework, showed up to class, ate lunch, or really did anything. If you don't do the work, you simply fail. In that sense, all you have to blame is yourself.

And then something curious happened.

You got a job, congratulations! You've now regressed to being treated like a child again: you must show up to work at a specific time, you have a narrow window for lunch, and your boss gives you dirty looks if you leave at 4:58 PM to pick up your kids.

Does it have to be this way?

### Trust means assuming positive intent

The truth is that we can reverse this trend of no trust. **If you assume positive intent, your people will being to trust you and be more compelled to deliver great work.** When you _decide_ that your people are great (because you only hire great people) from the beginning and don't need to be watched over by you to do their best, you are assuming positive intent.

This new world of remote work may mean they take a longer lunch break. Or a break to feed the baby. Or go to the gym. So what? At the end of the day, if they do the work, who cares if they're online at 3 pm or 3 am?

It's certainly helpful to your teammates to know others are online to assist them. It is preferred everyone is online at the same time, but it is not required for success.

**With trust comes accountability.** Now that you are assuming positive intent and trust in your teammates, they need to return the favor by holding themselves accountable. When the boss isn't looking, how do _I_ prove to the world that I'm delivering on my work? How can _I_ demonstrate that I'm worthy of a promotion or raise?

If you've got anxieties like this moving to remote work, the focus has shifted from time to results. _And that's a good thing_. When we all trust each other to get things done, our value is solely reflected on our output and not on inconsequential metrics like attendance or time on the clock. It's liberating; let remote work liberate you!

## We've got a tough road ahead, but we can get through it together

If you keep these three tactics in mind you'll be shocked at how much your teams will begin to embrace remote work culture. **Be sure to balance all of this work communication with social communication**. That means creating fun Slack channels like `#music`, `#video-games`, or `#cute-animals`.

With everyone holed away in their home offices things are going to be quite isolating. As humans, we crave social interaction. Recognize this and don't make it all about business. Lighten up and do your best to support your teammates. We're all going to make it and having a sense of humor will help us all get through this together.

Best of luck!
