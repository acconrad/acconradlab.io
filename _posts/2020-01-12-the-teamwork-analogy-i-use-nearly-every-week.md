---
title: The teamwork analogy I use nearly every week
description: Teamwork is vital and this is a great way to get that message across.
layout: post
date: '2020-01-12 22:06:12'
tags: inspiration leadership teamwork
subclass: post tag-test tag-content
categories: leadership
navigation: 'True'
article: 'True'
---

I remember when I started at my current company there was a departing employee who had a reputation for being exceptionally talented but also quite difficult to work with. This was so pervasive that virtually every person I spoke with had the same thing to say about them. It became very clear when they left how much of a hole they left because of their prowess.

The code was smart and quite clever. In fact, it was so clever that it almost impossible to understand; to the point where we ended up scrapping all of it in favor of something more verbose but much simpler.

Was it by-the-book the best code we could have written? No. But it was code that everyone on the team could understand and contribute to, which made it way more versatile.

I think back to these first few weeks (and this old code) quite a bit in my one-on-ones and onboarding meetings. It made me think of an analogy I find myself using over and over again to show people why teamwork is so important. It goes something like this:

> No one ever partially IPO'd. The NYSE isn't going to have you ring the bell because you're a rock star programmer while the rest of the company fails. Either we all cross the finish line, arm-in-arm, or we all fail to complete the race.

So many people fail to see their petty arguments with their fellow employees are completely fruitless. What is the point of winning your argument if it fractures your team?

Companies are more like baseball than tennis; it's not an individual sport. One or two stellar performers cannot possibly make up for a losing team. That's why I don't hire divas or prima donnas.

Sure, the rock star may be exceptionally talented and even a "10x" programmer. But if their 10x causes everyone else to drop 50% in productivity because their code is exceptionally clever or they are overly critical in code review, then the team overall is still no better off in terms of productivity.

**If you want to be selfish, it's in your best interest to be a team player.** You read that right, if you want to make that sweet IPO money the only way you are going to get there is if your whole company gets there. It's in your best interest, therefore, to help others out so you can all get to that exit event sooner rather than later. You only get there after the last person crosses the finish line, and you're all linked arm-in-arm. _That's_ why you stand to benefit if others benefit.

The next time you're ready to rip someone's throat out for a bad move or you think your marketing team is useless, remember that every piece of the puzzle has to fit if you want to win. Otherwise you'll stay stuck in the same rut and no one wants to work with someone who is difficult and not a team player.
