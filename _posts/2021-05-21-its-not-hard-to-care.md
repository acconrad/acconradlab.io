---
title: It's not hard to care
description: If you treat others like human beings you're apparently ahead of most hiring managers.
layout: post
date: '2021-05-21 11:04:00'
tags: hiring email
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

I handwrite every single email I send to candidates.

Every. Single. One.

Of course it doesn't scale.
Of course it is time consuming.
Of course rejection sucks and I'll probably get rejected by 99% of the people I reach out to.

If hiring is your _most important_ activity, then why would you ever put in a "copy and paste"-level of effort for people that could provide MILLIONS of dollars of value for your organization?

Have you ever received an email that was copy pasted? Have you received an email where they used the wrong name (or reversed your first name with your last name)? I can answer yes to all of those.

It's pure lunacy to me that recruiters and hiring managers don't put any effort into the candidates they're trying to hire.

_Always_ write a personal note.
_Always_ pay attention to their experience, their wants, their needs.
_Always_ be a human first, and a hiring manager second.
