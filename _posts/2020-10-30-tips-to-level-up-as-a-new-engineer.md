---
title: Tips to level up as a new engineer
description: I've noticed I keep suggesting these tips over and over again to new and junior engineers.
layout: post
date: '2020-10-30 18:02:21'
tags: productivity growth
subclass: post tag-test tag-content
categories: programming
navigation: 'True'
article: 'True'
---

My recent one-on-ones with my more junior engineers revolved around the same topic: **how do I level up quickly to be a better aid for my tech lead?**

New engineers seem to deal with this dichotomy that they both want to learn quickly from the more senior engineers while also ensuring they bother them as little as possible because they are so busy and focused on big, important projects. While that is a reasonable line of thinking there are ways of achieving both without having to wait for the right opportunity to reach out to the top brass in engineering.

I've shared these tips over and over again so see if one (or all) of these tips resonate with you.

## Keep a daily log

One of the most common complaints I hear from more senior engineers is that their tips and suggestions for the more junior engineers just don't stick. It's like they are talking to a wall and the newer engineers just keep making the same mistakes over and over again.

This tip is pulled from the course [learning how to learn](https://www.coursera.org/learn/learning-how-to-learn) which I took quite a few years ago and it's all about **writing down everything you learn in your own words.** This serves two purposes: retention and recollection.

If you can summarize in your own words what you just learned then it is more likely you will retain that knowledge because you were able to translate it from someone else's words into your own. Further, if that retention degrades at all you always have your written log to search from which is your first line of defense to check against before you go asking senior engineers for help.

An added bonus for a daily log is that you can use it to catalog all of your successes when doing annual reviews. Skimming the last year's worth of notes will help you quickly recollect the highlights of your accomplishments in the last year and I bet you'll find some great wins you forgot about from the beginning of the year.

**The most important thing is that you start.** Even if it's just one sentence a day of something you found noteworthy. As long as you are creating the habit of maintaining a daily log then you can always fill it out as you add more entries and solidify the ritual.

## Cluster your questions

An old [post](https://signalvnoise.com/archives2/the_science_of_interruptions.php) from the woke productivity gurus at Basecamp mentioned that every time an engineer is interrupted it takes them 25 minutes on average to get back into the groove. That means fewer but more substantial interruptions are a net benefit to small, frequent interruptions.

You may think that just popping in on a Slack DM to a staff engineer to ask a 30 second question is an easy ask. The reality is that 30 second question, by this definition, is really a 25 minute and 30 second ask. Ten of those questions could easily add up to half a day of lost productivity for our poor staff engineer.

So instead of frequent interruptions you would be best served to your senior staff to cluster your questions together and take up more of their time in one swoop. Actually put time on their calendar for 15-20 minutes to just as a barrage of questions.

These questions can be unrelated or they can be part of the [five whys](https://en.wikipedia.org/wiki/Five_whys) of just one line of questioning. The collection honestly doesn't matter; what matters is that you are reducing the amount of 25 minute ramp-up times it takes for the senior engineer to get back to being productive.

The important thing is here is that **one large ask will always be an overall smaller commitment and pain than lots of small, frequent asks**. As a bonus, the cost is even lower if you keep a daily log and can reach for your own answers first before having to reach to someone else first.

## Review your own PRs

Whenever you post a pull request to GitHub, Gitlab, or whatever repository tool you choose, review your own PR first. There's usually a page before you hit that submit button where the scope of the `git diff` is posted in its entirety. That is a great opportunity to **be your own reviewer** and imagine your are your own tech lead ensuring your code looks great. This not only gives you more code reviewer practice but I'm fairly confident you'll find something yourself before you hit that submit button. This will not only save you a review round if the reviewer doesn't approve but it will also improve the perception that the quality of your code is great from the start.

Doing your own work upfront shows your colleagues that you sanity checked your own work and that you're putting your best foot forward. Most importantly, you'll save everyone involved in your review precious time.
