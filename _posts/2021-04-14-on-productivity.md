---
title: On productivity
description: As leaders, how do you calculate the productivity of team members?
layout: post
date: '2021-04-14 11:04:00'
tags: productivity metrics performance-management
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

I did a recent AMA on Clubhouse and there were a lot of awesome, hard-hitting questions on the panel. So for the next several weeks, I'm going to write out my thoughts to each of the questions and how I answered them.

> As leaders, how do you calculate the productivity of team members?

This one is a slippery slope. I've tried using things like the [DORA metrics](https://cloud.google.com/blog/products/devops-sre/using-the-four-keys-to-measure-your-devops-performance). I've tried more insideous calculations like lines of code, pull requests made, or bugs solved, but those all optimize for the wrong thing: more stuff.

More stuff doesn't make you more productive; it just means you're doing more stuff with your day.

In a recent bout of 1-1s I asked every direct this question:

**Who do you think I'm more likely to promote: the person who spent 2 hours making the company $3MM with a single code change or the person who spent all year working hard to get all of our tests to 100% code coverage?**

Everyone picks the first person because they know the real answer on measuring productivity: **in impact**.

Lines of code, PRs merged, bugs squashed...these are all vanity metrics that are easy to track but don't really measure what you're after.

Instead, encourage folks to write something like a [promotion packet](https://staffeng.com/guides/promo-packets/) on the impact they've made over the last year. A bit of self-reflection will make it easy to see whether or not a person is truly productive by any semblance of an objective measure of productivity.

Any other kind of measure of productivity is a red herring and a bad road to travel down that will only lead to micromanagement and missing the point.
