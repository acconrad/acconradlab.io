---
title: Why you should start a newsletter even if you never grow a big audience
description: Newsletters are great for everyone to start regardless of your popularity or influence.
layout: post
date: '2018-10-20 10:28:00'
tags: newsletter marketing personal-branding
subclass: post tag-test tag-content
categories: marketing
navigation: 'True'
article: 'True'
---

When I broke out on my own, I wanted to do everything right. One of the most common pieces of advice is to start a newsletter. I believe that even if your newsletter amounts to nothing (which it won’t), you’ll still get a great deal of value out of it.

## You now have a bookmark collection

I’ve tried a bunch of bookmarking services and never seemed to have stuck with them. Whether it is embedded as a feature in the browser or something like Pocket, I just can’t seem to find use in them. If I want to reference something I saw 6–12 months ago, that’s the worst. I usually can’t remember the name of the article and the search function on these services is pretty bad, so I just end up Googling for the article all over again.

Until I started a newsletter.

Now that I’m taking an** active role** pushing out links to my audience on a weekly basis, it’s helping me focus on two things: high quality articles *and* pieces that I actually read and learn from. And since these newsletter services archive my newsletters, I can quickly search and reference my e-mail blasts for the issue that contained a particular post. This is *much faster* than Googling or Pocket.

## You improve your reading, writing, and retention

Newsletters require captivating headlines, succinct writing, and expert recall to distill the content you want to share down to a sentence or a phrase. This further necessitates a command of the language as well as some mental marketing to convince your readers to click a link based on the way the content is presented.

If marketing isn’t your primary function, you’re now expanding your skill-set, becoming a better communicator, and improving your reading retention all to create content that will hook your audience into coming back for more. This can be a powerful motivator since you now have external pressure to deliver a good product and know what you’re talking about.

Now that I have to publish content regularly, I’m reading with a summary in mind (i.e. “what is the gist of this article?”) as well as any questions I might have to be a launching point for Twitter discussions, future blog posts, and more. I certainly would not be thinking with that mindset if I was passively reading articles with no end goal in mind.

## You increase your discipline through routine

Compared to a lot of tech newsletters that have been around for years, I’m still pretty new to this. But already I’ve had multiple instances where I’ve not wanted to hit that publish button to send out my newsletter. Maybe I had a vacation coming up where I wouldn’t be in the country to post on time, or I just really didn’t like the quality of articles I read in the past week. **No excuse — people are waiting for you to deliver each and every time.**

Newsletters create this social pressure to deliver content on a regular schedule, regardless of whether or not you feel like it or you’re even able to get to a computer. Even a blog can’t offer that since most publications either offer a continuous stream of content (like the bigger blog networks) or some erratic schedule that is often enough for you not to notice (i.e. any individual blogger who has established themselves).

But newsletters seem to have this expectation of routine. **People want email content delivered in a predictable manner or else they will unsubscribe.**That pressure forces you to be creative when things aren’t going perfectly.

For example, I send out my newsletters on Monday mornings, which means I have to create them by Sunday night. I’ve had plenty of Sundays so far where all I want to do is lay on the couch and watch football or I’m out of the house doing something where I won’t be near a computer in the time I need.

So instead of throwing in the towel, I get creative. I write the newsletter on a Saturday or Friday to schedule for Monday. I actively develop the newsletter in a to-do list item so that as I find articles, I write summaries *as I’m done reading them *so that when I’m ready to publish my newsletter at the end of the week, all I have to do is copy-and-paste my summaries instead of having to draft them up when my newsletter is being written each week.*

The point is **I find time to deliver which fosters discipline and willpower**. Self-help articles are oozing today with tips on how to be more productive or increase your limited vessel of willpower. Want to do all of that, and increase your reading and writing capabilities? Start a newsletter.

## Why else have you started a newsletter?

I could offer up a host of other reasons if you actually manage to grow an audience (e.g. you’ll have viewers if you ever launch a product, you’ll increase your blog traffic), but I want to make sure you understand that **you can get value from starting something like a newsletter even if it never achieves the primary goal of growing a big following.**

Are there other reasons I haven’t thought of that motivated you to start a newsletter? Were you convinced after reading this article? Throw me some claps below and let me know how mailing lists have helped you either personally or professionally.
