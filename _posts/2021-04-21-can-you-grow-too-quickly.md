---
title: Can you grow too quickly?
description: If you have a small team with exponential growth, how do you make sure that the quick growth doesn’t kill the product or team?
layout: post
date: '2021-04-21 11:04:00'
tags: growth team scaling-teams
subclass: post tag-test tag-content
categories: management
navigation: 'True'
article: 'True'
---

I did an AMA on Clubhouse a little while ago and there were a lot of awesome, hard-hitting questions on the panel. So for the next several weeks, I'm going to write out my thoughts to each of the questions and how I answered them.

> If you have a small team with exponential growth, how do you make sure that the quick growth doesn’t kill the product or team?

When I think of small engineering teams and hyper-growth companies I think of [Instagram](https://review.firstround.com/how-instagram-co-founder-mike-krieger-took-its-engineering-org-from-0-to-300-people). I've actually been thinking about this company a lot lately.

## A brief aside on premature optimization

We've been struggling with organizational (and architectural) complexity at my job recently. Everything takes too long to build and we have seemingly too many folks tackling a few too many things at once.

One of the ways we were able to convince stakeholders to simplify our architecture (and hopefully simplify our org design) was to reference the story of Instagram's engineering team.

TL,DR: Instagram scaled to 300,000,000 MAUs (monthly active users) and sold to Facebook for $1B with only **six engineers**. That's ludicrous.

That story is now burned into my brain. From this point forward, if anyone tells me we need to scale up or build another microservice I'm going to reference this article on how Instagram could do it on far less.

**So to answer the actual question: the only way I could see a fast-growing, successful product failing is to pre-optimize and scale it before it really needs to**.
