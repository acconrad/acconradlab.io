<?xml version="1.0" encoding="UTF-8" ?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
   
      <title>https://anonconsulting.com</title>
   
   <link>https://www.userinterfacing.com</link>
   <description>User Interfacing is a publication for user interface programming and design.</description>
   <language>en-us</language>
   <managingEditor> Adam C. Conrad</managingEditor>
   <atom:link href="rss" rel="self" type="application/rss+xml" />
   
	<item>
	  <title>How to find excellent refactoring opportunities</title>
	  <link>//how-to-find-excellent-refactoring-opportunities/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2018-06-14T21:13:00-04:00</pubDate>
	  <guid>//how-to-find-excellent-refactoring-opportunities/</guid>
	  <description><![CDATA[
	     <p>I was doing some code review today and I realized after the fact that the refactoring I wrote for the review was a great example of how I iteratively evolved a piece of code into something really great. So here&#39;s a step-by-step approach to finding and performing a code refactoring you can be proud of.</p>

<h1>Prerequisite: Read <em>Refactoring</em></h1>

<p>The first thing you should do is read Martin Fowler&#39;s <em>Refactoring</em>. You&#39;re super in luck too because the <a href="https://martinfowler.com/articles/201803-refactoring-2nd-ed.html">2nd Edition is going to be in JavaScript</a>.</p>

<p>Since you can&#39;t get that now, go read the <a href="https://martinfowler.com/books/refactoringRubyEd.html">Ruby Edition</a>. The Ruby edition is better because it&#39;s easier to relate to in the front-end world since Ruby is a simple scripting language and way less verbose than the original version in Java.</p>

<p>Why do you need to read this book? Because it&#39;s all tactics. This book has something like 40+ techniques for finding code smells and refactoring them into something beautiful. You probably already knew the really simple ones like Extract Method and Extract Class, but the more advanced ones are where it really shines.</p>

<p>One client I worked with had this heavy reliance on using <code>switch/case</code> statements. Those aren&#39;t inherently bad but more often than not you can write something cleaner if you <a href="https://refactoring.com/catalog/replaceConditionalWithPolymorphism.html">replace the conditional with polymorphism</a>. Stuff like this is gold, pure gold!</p>

<p><img class="js-lazy-image" data-src="https://media.giphy.com/media/KdDIGOxbQBAqI/giphy.gif" alt="Refactoring is gold" /></p>

<h1>An example in refactoring</h1>

<p>Here&#39;s the (obfuscated) code I was working with to begin our journey:</p>
<div class="highlight"><pre><code class="language-javascript" data-lang="javascript"><span class="kr">const</span> <span class="nx">addEvents</span> <span class="o">=</span> <span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="nx">label</span><span class="p">,</span> <span class="nx">segment</span><span class="p">)</span> <span class="o">=&gt;</span> <span class="nx">events</span><span class="p">.</span><span class="nx">push</span><span class="p">([</span><span class="nx">label</span><span class="p">,</span> <span class="nx">segment</span><span class="p">]);</span>

<span class="kr">const</span> <span class="nx">main</span> <span class="o">=</span> <span class="p">()</span> <span class="o">=&gt;</span> <span class="p">{</span>
  <span class="kr">const</span> <span class="p">[</span><span class="nx">upcoming</span><span class="p">,</span> <span class="nx">past</span><span class="p">]</span> <span class="o">=</span> <span class="nx">partition</span><span class="p">(</span>
    <span class="nx">arr</span><span class="p">,</span> <span class="nx">date</span> <span class="o">=&gt;</span> <span class="nx">today</span><span class="p">.</span><span class="nx">diff</span><span class="p">(</span><span class="nx">date</span><span class="p">,</span> <span class="s1">&#39;day&#39;</span><span class="p">)</span> <span class="o">&lt;=</span> <span class="mi">0</span>
  <span class="p">);</span>

  <span class="kr">const</span> <span class="nx">events</span> <span class="o">=</span> <span class="p">[]</span>

  <span class="k">if</span> <span class="p">(</span><span class="nx">upcoming</span><span class="p">.</span><span class="nx">length</span> <span class="o">&gt;</span> <span class="mi">0</span><span class="p">)</span> <span class="p">{</span>
    <span class="nx">addEvents</span><span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="s1">&#39;upcoming stuff&#39;</span><span class="p">,</span> <span class="nx">upcoming</span><span class="p">);</span>
  <span class="p">}</span>

  <span class="k">if</span> <span class="p">(</span><span class="nx">past</span><span class="p">.</span><span class="nx">length</span> <span class="o">&gt;</span> <span class="mi">0</span><span class="p">)</span> <span class="p">{</span>
    <span class="kr">const</span> <span class="nx">byMonth</span> <span class="o">=</span> <span class="nx">groupBy</span><span class="p">(</span><span class="nx">past</span><span class="p">,</span> <span class="nx">date</span> <span class="o">=&gt;</span>
      <span class="nx">moment</span><span class="p">(</span><span class="nx">date</span><span class="p">).</span><span class="nx">format</span><span class="p">(</span><span class="s1">&#39;YYYY-MM&#39;</span><span class="p">);</span>
    <span class="p">);</span>
    <span class="kr">const</span> <span class="nx">months</span> <span class="o">=</span> <span class="nb">Object</span><span class="p">.</span><span class="nx">keys</span><span class="p">(</span><span class="nx">byMonth</span><span class="p">).</span><span class="nx">sort</span><span class="p">().</span><span class="nx">reverse</span><span class="p">();</span>
    <span class="nx">months</span><span class="p">.</span><span class="nx">forEach</span><span class="p">((</span><span class="nx">month</span><span class="p">,</span> <span class="nx">idx</span><span class="p">)</span> <span class="o">=&gt;</span> <span class="nx">addEvents</span><span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="err">`</span><span class="nx">$</span><span class="p">{</span><span class="nx">idx</span><span class="p">}</span> <span class="nx">months</span> <span class="nx">ago</span><span class="err">`</span><span class="p">,</span> <span class="nx">month</span><span class="p">);</span>
  <span class="p">}</span>

  <span class="k">return</span> <span class="nx">events</span><span class="p">;</span>
<span class="p">}</span>
</code></pre></div>
<p>This code is pretty simple - I&#39;ve got two arrays of dates: upcoming dates, and past dates (including today). I just need to let the world know what&#39;s coming up and what is in the past, so it&#39;s not a simple push to an array because each chunk is split off by month (except for the future, which is just labeled future stuff), and each section is really a tuple of label and date.</p>

<p>So what immediately got me tweakin&#39; on this code?</p>

<h2>Single Responsibility Principle</h2>

<p>By now the SOLID acronym is engrained in my brain. If you don&#39;t know what it is, thoughtbot wrote a <a href="https://robots.thoughtbot.com/back-to-basics-solid">great intro about what SOLID is</a> so definitely check that out.</p>

<p>The most obvious one that people seem to remember is the first one, Single Responsibility Principle. It&#39;s probably the easiest because the definition really says it all. Your class/function/whatever should do one thing and do it well. In this case, I see a bunch of stuff, so my first thought is to break this function up into multiple functions.</p>

<p>How do I know how to break it up? Stuff like <code>if</code> statements are literal blocks of code. Since everything is essentially scoped to blocks in JavaScript, anything inside of a bracket is a good candidate to break out into its own function.</p>

<p>I&#39;m also going to cheat and skip a step, because <code>if</code> statements also provide another subtle hint: <strong>the <code>if</code> keyword can usually be refactored into a guard clause.</strong> Why? Because it is already guarding some code against being run within your function.</p>

<p>If we rewrite the <code>if</code> in guard clause notation (meaning, exit early before the function has a chance to run), along with the SRP refactoring, we get:</p>
<div class="highlight"><pre><code class="language-javascript" data-lang="javascript"><span class="kr">const</span> <span class="nx">addEvents</span> <span class="o">=</span> <span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="nx">label</span><span class="p">,</span> <span class="nx">segment</span><span class="p">)</span> <span class="o">=&gt;</span> <span class="nx">events</span><span class="p">.</span><span class="nx">push</span><span class="p">([</span><span class="nx">label</span><span class="p">,</span> <span class="nx">segment</span><span class="p">]);</span>

<span class="kr">const</span> <span class="nx">addUpcomingEvents</span> <span class="o">=</span> <span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="nx">upcoming</span><span class="p">)</span> <span class="o">=&gt;</span> <span class="p">{</span>
  <span class="k">if</span> <span class="p">(</span><span class="nx">upcoming</span><span class="p">.</span><span class="nx">length</span> <span class="o">===</span> <span class="mi">0</span><span class="p">)</span> <span class="k">return</span><span class="p">;</span>
  <span class="nx">addEvents</span><span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="s1">&#39;upcoming stuff&#39;</span><span class="p">,</span> <span class="nx">upcoming</span><span class="p">);</span>
<span class="p">};</span>

<span class="kr">const</span> <span class="nx">getPastMonths</span> <span class="o">=</span> <span class="nx">past</span> <span class="o">=&gt;</span> <span class="p">{</span>
    <span class="k">return</span> <span class="nx">groupBy</span><span class="p">(</span><span class="nx">past</span><span class="p">,</span> <span class="nx">date</span> <span class="o">=&gt;</span> <span class="nx">moment</span><span class="p">(</span><span class="nx">date</span><span class="p">).</span><span class="nx">format</span><span class="p">(</span><span class="s1">&#39;YYYY-MM&#39;</span><span class="p">));</span>
<span class="p">};</span>

<span class="kr">const</span> <span class="nx">addPastEvents</span> <span class="o">=</span> <span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="nx">past</span><span class="p">)</span> <span class="o">=&gt;</span> <span class="p">{</span>
  <span class="k">if</span> <span class="p">(</span><span class="nx">past</span><span class="p">.</span><span class="nx">length</span> <span class="o">===</span> <span class="mi">0</span><span class="p">)</span> <span class="k">return</span><span class="p">;</span>
  <span class="kr">const</span> <span class="nx">months</span> <span class="o">=</span> <span class="nb">Object</span><span class="p">.</span><span class="nx">keys</span><span class="p">(</span><span class="nx">getPastMonths</span><span class="p">(</span><span class="nx">past</span><span class="p">)).</span><span class="nx">sort</span><span class="p">().</span><span class="nx">reverse</span><span class="p">();</span>
  <span class="nx">months</span><span class="p">.</span><span class="nx">forEach</span><span class="p">((</span><span class="nx">month</span><span class="p">,</span> <span class="nx">idx</span><span class="p">)</span> <span class="o">=&gt;</span> <span class="nx">addEvents</span><span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="err">`</span><span class="nx">$</span><span class="p">{</span><span class="nx">idx</span><span class="p">}</span> <span class="nx">months</span> <span class="nx">ago</span><span class="err">`</span><span class="p">,</span> <span class="nx">month</span><span class="p">);</span>
<span class="p">};</span>

<span class="kr">const</span> <span class="nx">main</span> <span class="o">=</span> <span class="p">()</span> <span class="o">=&gt;</span> <span class="p">{</span>
  <span class="kr">const</span> <span class="p">[</span><span class="nx">upcoming</span><span class="p">,</span> <span class="nx">past</span><span class="p">]</span> <span class="o">=</span> <span class="nx">partition</span><span class="p">(</span>
    <span class="nx">arr</span><span class="p">,</span> <span class="nx">date</span> <span class="o">=&gt;</span> <span class="nx">today</span><span class="p">.</span><span class="nx">diff</span><span class="p">(</span><span class="nx">date</span><span class="p">,</span> <span class="s1">&#39;day&#39;</span><span class="p">)</span> <span class="o">&lt;=</span> <span class="mi">0</span>
  <span class="p">);</span>

  <span class="kr">const</span> <span class="nx">events</span> <span class="o">=</span> <span class="p">[];</span>

    <span class="nx">addUpcomingEvents</span><span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="nx">upcoming</span><span class="p">);</span>
    <span class="nx">addPastEvents</span><span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="nx">past</span><span class="p">);</span>

  <span class="k">return</span> <span class="nx">events</span><span class="p">;</span>
<span class="p">}</span>
</code></pre></div>
<h2>Mutation is ugly</h2>

<p>Already this looks way better. <code>main()</code>&#39;s single responsibility is populating the array of <code>events</code> and returning them. Each extracted function has one responsibility based on its function definition. So I guess we can call it a day, right? The next smell I see is this pattern:</p>
<div class="highlight"><pre><code class="language-javascript" data-lang="javascript"><span class="kr">const</span> <span class="nx">myPoorFriendArray</span> <span class="o">=</span> <span class="p">[];</span>

<span class="c1">// a world of hurt, warping our pristine, empty friend</span>
<span class="nx">blackBoxFunction</span><span class="p">(</span><span class="nx">myPoorFriendArray</span><span class="p">);</span>
<span class="nx">moreTorture</span><span class="p">(</span><span class="nx">myPoorFriendArray</span><span class="p">);</span>
<span class="nx">theGauntlet</span><span class="p">(</span><span class="nx">myPoorFriendArray</span><span class="p">);</span>

<span class="c1">// we chew him up and spit him out, never knowing the horrors he experienced</span>
<span class="k">return</span> <span class="nx">myPoorFriendArray</span><span class="p">;</span>
</code></pre></div>
<p>Ya feel me on this one? Our boy <code>myPoorFriendArray</code> is now an honorary X-Man: mutated from his original, empty form, into some chimera of various parts of the code. <a href="https://www.youtube.com/watch?v=fSJoDuU328k">Kanye said it better than I can</a>, but this is the stuff I <em>DON&#39;T LIKE</em>.</p>

<p>Maybe you come from an OO world like Java and mutation is just a regular, everyday occurrence. If you&#39;re woke and you come from a functional background like Elm, you can see that mutants are bad. We don&#39;t want no X-Men fighting for us on the coding battlegrounds (sorry Gambit, love you).</p>

<p>So what&#39;s the fix here?</p>

<ol>
<li><strong>Send the empty array in as an argument to our first add function</strong></li>
<li><strong>The function will take in the empty array and make a copy</strong></li>
<li><strong>The copy is filled out and returned</strong></li>
</ol>

<p>The argument is not mutated, and we remove state dependencies. If you&#39;ve done any functional programming, terms like <em>stateless</em> and <em>immutability</em> are everyday terms, and that&#39;s what we&#39;re striving for.</p>

<p>Our stateless code doesn&#39;t have <a href="https://hackernoon.com/code-smell-side-effects-of-death-31c052327b8b">side effects</a>, a nasty code smell that makes bugs more difficult to track down because state persists across functions. Without state, what you put into it is what you get out of it each and every time. Making your functions predictable, and thus less likely to include errors you didn&#39;t test for.</p>

<p>Here&#39;s what this change looks like:</p>
<div class="highlight"><pre><code class="language-javascript" data-lang="javascript"><span class="kr">const</span> <span class="nx">addEvents</span> <span class="o">=</span> <span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="nx">label</span><span class="p">,</span> <span class="nx">segment</span><span class="p">)</span> <span class="o">=&gt;</span> <span class="nx">events</span><span class="p">.</span><span class="nx">push</span><span class="p">([</span><span class="nx">label</span><span class="p">,</span> <span class="nx">segment</span><span class="p">]);</span>

<span class="c1">// we didn&#39;t touch the middle functions...</span>

<span class="kr">const</span> <span class="nx">main</span> <span class="o">=</span> <span class="p">()</span> <span class="o">=&gt;</span> <span class="p">{</span>
  <span class="kr">const</span> <span class="p">[</span><span class="nx">upcoming</span><span class="p">,</span> <span class="nx">past</span><span class="p">]</span> <span class="o">=</span> <span class="nx">partition</span><span class="p">(</span>
    <span class="nx">arr</span><span class="p">,</span> <span class="nx">date</span> <span class="o">=&gt;</span> <span class="nx">today</span><span class="p">.</span><span class="nx">diff</span><span class="p">(</span><span class="nx">date</span><span class="p">,</span> <span class="s1">&#39;day&#39;</span><span class="p">)</span> <span class="o">&lt;=</span> <span class="mi">0</span>
  <span class="p">);</span>

  <span class="k">return</span> <span class="nx">addPastEvents</span><span class="p">(</span><span class="nx">addUpcomingEvents</span><span class="p">([],</span> <span class="nx">upcoming</span><span class="p">),</span> <span class="nx">past</span><span class="p">);</span>
<span class="p">}</span>
</code></pre></div>
<p>Now we&#39;re getting somewhere! But for the savvy reader, something should still seem off. Can you see what&#39;s still wrong?</p>

<h2>Negate the state</h2>

<p><strong>We didn&#39;t really remove the state from the add functions.</strong> Yeah, we gave it a starting point in our <code>main()</code> function, but the <code>addEvents()</code> function is still adding events to a singular <code>events</code> array which is getting passed around like a rag doll. How do I know this?</p>

<p><code>Array.prototype.push</code> is a mutable function. It provides an interface to add stuff to an array and returns the number of elements in the enlarged array. How do we do this in an immutable way?</p>

<p><code>Array.prototype.concat</code> is the immutable equivalent of <code>push()</code>. It takes an array, combines it with another array, and those two are combined into a newly-allocated array. So we can modify the above slightly to be:</p>
<div class="highlight"><pre><code class="language-javascript" data-lang="javascript"><span class="kr">const</span> <span class="nx">addEvents</span> <span class="o">=</span> <span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="nx">label</span><span class="p">,</span> <span class="nx">segment</span><span class="p">)</span> <span class="o">=&gt;</span> <span class="p">{</span>
  <span class="k">if</span> <span class="p">(</span><span class="o">!</span><span class="nx">label</span><span class="p">)</span> <span class="k">return</span> <span class="nx">events</span><span class="p">;</span>
  <span class="k">return</span> <span class="nx">events</span><span class="p">.</span><span class="nx">concat</span><span class="p">([[</span><span class="nx">label</span><span class="p">,</span> <span class="nx">segment</span><span class="p">]]);</span>
<span class="p">};</span>
</code></pre></div>
<p>Now we never touch our inputs. <code>events</code> remains as pristine as it was the day it arrived in this humble function, and instead we use <code>concat()</code> to combine <code>events</code> with the new array we&#39;ve made, which is the array of our tuple (a point of clarification: tuples don&#39;t exist in JavaScript, but I&#39;m using that term to describe our 2-item array to make the distinction clearer here).</p>

<p>Now I&#39;m in audit mode: where else do I need to follow this pattern of not touching the arguments and ensuring I return an <code>Array</code> type?</p>

<h2>Provide type safety</h2>

<p>If you aren&#39;t using TypeScript, this is a great way to practice some proactive type safety to ensure that your function returns objects of the same type regardless of the output. That means don&#39;t return an <code>Array</code> if you have stuff, but return <code>undefined</code> or <code>null</code> if you exit early (like in a guard clause). Oh crap, I&#39;m doing that. Looks like another opportunity to refactor!</p>
<div class="highlight"><pre><code class="language-javascript" data-lang="javascript"><span class="c1">// unchanged but illustrating that all functions, including this one, always return an Array</span>
<span class="kr">const</span> <span class="nx">addEvents</span> <span class="o">=</span> <span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="nx">label</span><span class="p">,</span> <span class="nx">segment</span><span class="p">)</span> <span class="o">=&gt;</span> <span class="p">{</span>
  <span class="k">if</span> <span class="p">(</span><span class="o">!</span><span class="nx">label</span><span class="p">)</span> <span class="k">return</span> <span class="nx">events</span><span class="p">;</span>
  <span class="k">return</span> <span class="nx">events</span><span class="p">.</span><span class="nx">concat</span><span class="p">([[</span><span class="nx">label</span><span class="p">,</span> <span class="nx">segment</span><span class="p">]]);</span>
<span class="p">};</span>

<span class="kr">const</span> <span class="nx">addUpcomingEvents</span> <span class="o">=</span> <span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="nx">upcoming</span><span class="p">)</span> <span class="o">=&gt;</span> <span class="p">{</span>
  <span class="k">if</span> <span class="p">(</span><span class="nx">upcoming</span><span class="p">.</span><span class="nx">length</span> <span class="o">===</span> <span class="mi">0</span><span class="p">)</span> <span class="k">return</span> <span class="nx">events</span><span class="p">;</span>
    <span class="k">return</span> <span class="nx">addEvents</span><span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="s1">&#39;upcoming stuff&#39;</span><span class="p">,</span> <span class="nx">upcoming</span><span class="p">);</span>
<span class="p">};</span>

<span class="kr">const</span> <span class="nx">getPastMonths</span> <span class="o">=</span> <span class="nx">past</span> <span class="o">=&gt;</span> <span class="p">{</span>
    <span class="k">return</span> <span class="nb">Object</span><span class="p">.</span><span class="nx">keys</span><span class="p">(</span><span class="nx">groupBy</span><span class="p">(</span><span class="nx">past</span><span class="p">,</span> <span class="nx">date</span> <span class="o">=&gt;</span> <span class="nx">moment</span><span class="p">(</span><span class="nx">date</span><span class="p">).</span><span class="nx">format</span><span class="p">(</span><span class="s1">&#39;YYYY-MM&#39;</span><span class="p">))).</span><span class="nx">sort</span><span class="p">().</span><span class="nx">reverse</span><span class="p">();</span>
<span class="p">};</span>

<span class="kr">const</span> <span class="nx">addPastEvents</span> <span class="o">=</span> <span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="nx">past</span><span class="p">)</span> <span class="o">=&gt;</span> <span class="p">{</span>
  <span class="k">if</span> <span class="p">(</span><span class="nx">past</span><span class="p">.</span><span class="nx">length</span> <span class="o">===</span> <span class="mi">0</span><span class="p">)</span> <span class="k">return</span> <span class="nx">events</span><span class="p">;</span>
  <span class="k">return</span> <span class="nx">getPastMonths</span><span class="p">(</span><span class="nx">past</span><span class="p">).</span><span class="nx">reduce</span><span class="p">((</span><span class="nx">acc</span><span class="p">,</span> <span class="nx">month</span><span class="p">,</span> <span class="nx">idx</span><span class="p">)</span> <span class="o">=&gt;</span> <span class="p">{</span>
      <span class="k">return</span> <span class="nx">addEvents</span><span class="p">(</span><span class="nx">acc</span><span class="p">,</span> <span class="err">`</span><span class="nx">$</span><span class="p">{</span><span class="nx">idx</span><span class="p">}</span> <span class="nx">months</span> <span class="nx">ago</span><span class="err">`</span><span class="p">,</span> <span class="nx">month</span><span class="p">),</span> <span class="nx">events</span><span class="p">);</span>
  <span class="p">};</span>
<span class="p">};</span>
</code></pre></div>
<p>Lots to unpack here. So in <code>addUpcomingEvents()</code> all we did was make sure we return an <code>Array</code> and not <code>undefined</code> (ending with just <code>return;</code> is shorthand for returning an <code>undefined</code> object). We do this because <code>concat()</code> returns an array, so we want to make sure all <code>return</code> statements provide the same type of object in any given function.</p>

<p>Next I did some refactoring of <code>getPastMonths()</code> to handle the sorting an reversing, because the <code>groupBy</code> function <em>technically</em> returns an <code>Object</code>, and a way for it to return an <code>Array</code> is to grab the <code>Keys</code> (which is an <code>Array</code>) and do our necessary transformations to that array object.</p>

<p>Finally, <code>addPastEvents()</code> starts out the same as the upcoming function by ensuring our guard clause returns an <code>Array</code> type. The next part is a bit wilder. Originally we were taking an array and iterating over it using <code>Array.prototype.forEach</code>. The problem is that this iterator doesn&#39;t return an <code>Array</code> like we need it to. It simply gives us a platform to view every item in our array.</p>

<p>We also know that in the end, we want one array object that adds in all of the past events. <strong>When you think about needing to combine things into one and return that combined object, think of using <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce"><code>Array.prototype.reduce</code></a>.</strong></p>

<p>In this case, I knew I needed to add the events (by month) into our <code>events</code> array, <em>and</em> return that newly combined array, using <code>events</code> as the starting point. The reduce function takes two arguments, a <code>callback</code> on how to combine stuff, and an optional <code>initial</code> object to begin with.</p>

<p>The <code>callback</code> is probably the most confusing part - what is this nested <code>return</code> trying to do? The <code>reduce()</code> <code>callback</code> argument has two arguments of its own: an <code>accumulator</code> object, which is initialized to our <code>initial</code> object (if you leave it out, it defaults to <code>undefined</code>), and the <code>current</code> item that is being iterated on. There are two additional optional arguments: the <code>index</code> of the item you are iterating on, and the original <code>array</code> object you called <code>reduce()</code> on. Since we need the <code>index</code> argument to label our months, I added that in.</p>

<p>So with all of that said, the <code>reduce()</code> function is basically saying:</p>

<blockquote>
<p>For each past month in my array, add it (with its label) onto my accumulated array, which is the cumulation of every previous iteration, starting with my initial <code>events</code> array.</p>
</blockquote>

<h2>The final result</h2>

<p><img src="/assets/images/blur-close-up-code-546819.jpg" alt="Refactored code is good code"></p>

<p>It was at this point I called it a day and was satisfied with my refactoring for code review I mentioned in the beginning. The final product looks like this:</p>
<div class="highlight"><pre><code class="language-javascript" data-lang="javascript"><span class="kr">const</span> <span class="nx">addEvents</span> <span class="o">=</span> <span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="nx">label</span><span class="p">,</span> <span class="nx">segment</span><span class="p">)</span> <span class="o">=&gt;</span> <span class="p">{</span>
  <span class="k">if</span> <span class="p">(</span><span class="o">!</span><span class="nx">label</span><span class="p">)</span> <span class="k">return</span> <span class="nx">events</span><span class="p">;</span>
  <span class="k">return</span> <span class="nx">events</span><span class="p">.</span><span class="nx">concat</span><span class="p">([[</span><span class="nx">label</span><span class="p">,</span> <span class="nx">segment</span><span class="p">]]);</span>
<span class="p">};</span>

<span class="kr">const</span> <span class="nx">addUpcomingEvents</span> <span class="o">=</span> <span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="nx">upcoming</span><span class="p">)</span> <span class="o">=&gt;</span> <span class="p">{</span>
  <span class="k">if</span> <span class="p">(</span><span class="nx">upcoming</span><span class="p">.</span><span class="nx">length</span> <span class="o">===</span> <span class="mi">0</span><span class="p">)</span> <span class="k">return</span> <span class="nx">events</span><span class="p">;</span>
    <span class="k">return</span> <span class="nx">addEvents</span><span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="s1">&#39;upcoming stuff&#39;</span><span class="p">,</span> <span class="nx">upcoming</span><span class="p">);</span>
<span class="p">};</span>

<span class="kr">const</span> <span class="nx">getPastMonths</span> <span class="o">=</span> <span class="nx">past</span> <span class="o">=&gt;</span> <span class="p">{</span>
    <span class="k">return</span> <span class="nb">Object</span><span class="p">.</span><span class="nx">keys</span><span class="p">(</span><span class="nx">groupBy</span><span class="p">(</span><span class="nx">past</span><span class="p">,</span> <span class="nx">date</span> <span class="o">=&gt;</span> <span class="nx">moment</span><span class="p">(</span><span class="nx">date</span><span class="p">).</span><span class="nx">format</span><span class="p">(</span><span class="s1">&#39;YYYY-MM&#39;</span><span class="p">))).</span><span class="nx">sort</span><span class="p">().</span><span class="nx">reverse</span><span class="p">();</span>
<span class="p">};</span>

<span class="kr">const</span> <span class="nx">addPastEvents</span> <span class="o">=</span> <span class="p">(</span><span class="nx">events</span><span class="p">,</span> <span class="nx">past</span><span class="p">)</span> <span class="o">=&gt;</span> <span class="p">{</span>
  <span class="k">if</span> <span class="p">(</span><span class="nx">past</span><span class="p">.</span><span class="nx">length</span> <span class="o">===</span> <span class="mi">0</span><span class="p">)</span> <span class="k">return</span> <span class="nx">events</span><span class="p">;</span>
  <span class="k">return</span> <span class="nx">getPastMonths</span><span class="p">(</span><span class="nx">past</span><span class="p">).</span><span class="nx">reduce</span><span class="p">((</span><span class="nx">acc</span><span class="p">,</span> <span class="nx">month</span><span class="p">,</span> <span class="nx">idx</span><span class="p">)</span> <span class="o">=&gt;</span> <span class="p">{</span>
      <span class="k">return</span> <span class="nx">addEvents</span><span class="p">(</span><span class="nx">acc</span><span class="p">,</span> <span class="err">`</span><span class="nx">$</span><span class="p">{</span><span class="nx">idx</span><span class="p">}</span> <span class="nx">months</span> <span class="nx">ago</span><span class="err">`</span><span class="p">,</span> <span class="nx">month</span><span class="p">),</span> <span class="nx">events</span><span class="p">);</span>
  <span class="p">};</span>
<span class="p">};</span>

<span class="kr">const</span> <span class="nx">main</span> <span class="o">=</span> <span class="p">()</span> <span class="o">=&gt;</span> <span class="p">{</span>
  <span class="kr">const</span> <span class="p">[</span><span class="nx">upcoming</span><span class="p">,</span> <span class="nx">past</span><span class="p">]</span> <span class="o">=</span> <span class="nx">partition</span><span class="p">(</span><span class="nx">arr</span><span class="p">,</span> <span class="nx">date</span> <span class="o">=&gt;</span> <span class="nx">today</span><span class="p">.</span><span class="nx">diff</span><span class="p">(</span><span class="nx">date</span><span class="p">,</span> <span class="s1">&#39;day&#39;</span><span class="p">)</span> <span class="o">&lt;=</span> <span class="mi">0</span><span class="p">);</span>
  <span class="k">return</span> <span class="nx">addPastEvents</span><span class="p">(</span><span class="nx">addUpcomingEvents</span><span class="p">([],</span> <span class="nx">upcoming</span><span class="p">),</span> <span class="nx">past</span><span class="p">);</span>
<span class="p">}</span>
</code></pre></div>
<p>To summarize, the major things we covered in this refactoring are:</p>

<ol>
<li><strong>Simplify functions down to a single responsibility</strong></li>
<li><strong>Eliminate side effects by using immutable functions and not modifying arguments</strong></li>
<li><strong>Ensure type safety by verifying all return statements provide the same type of object</strong></li>
</ol>

<p>This gives us code that is easier to read, less prone to bugs, and easier to test.</p>

<p>Is there anything else you would refactor? Did I miss something? <a href="http://twitter.com/theadamconrad">Hit me up on Twitter</a> and let me know what else could make this code better.</p>

	  ]]></description>
	</item>


</channel>
</rss>
