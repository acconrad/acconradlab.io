<?xml version="1.0" encoding="UTF-8" ?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
   
      <title>https://anonconsulting.com</title>
   
   <link>https://www.userinterfacing.com</link>
   <description>User Interfacing is a publication for user interface programming and design.</description>
   <language>en-us</language>
   <managingEditor> Adam C. Conrad</managingEditor>
   <atom:link href="rss" rel="self" type="application/rss+xml" />
   
	<item>
	  <title>How to Improve on Naming Contexts in Domain-Driven Design</title>
	  <link>//how-to-improve-on-naming-contexts-in-domain-driven-design/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2018-05-30T23:25:00-04:00</pubDate>
	  <guid>//how-to-improve-on-naming-contexts-in-domain-driven-design/</guid>
	  <description><![CDATA[
	     <p>I&#39;ve been using <a href="https://phoenixframework.org">Elixir&#39;s Phoenix framework</a> for a few years but most of my projects were on the older MVC style. Phoenix 1.3 introduced the concept of contexts (commonly known as bounded contexts). This changed the way you architect your applications.</p>

<p>I was <em>really</em> confused by this at first (naming is, after all, is <a href="https://skeptics.stackexchange.com/a/39178">one of the hardest things in programming</a>). After a bunch of exploration, I&#39;ve got a surefire way to improve how you name these things.</p>

<h2>What are bounded contexts, exactly?</h2>

<p>A <a href="https://martinfowler.com/bliki/BoundedContext.html">bounded context</a> is a conceptual grouping of a set of data models. The conceptual piece is key here because it means <em>you</em> decide on how you want to split up your models. This freedom is also a crutch because it also means there is no right or wrong way to create a context. For example, the name of these two contexts...</p>

<p><img src="/assets/images/bounded-context-intro.png" alt="Bounded context example"></p>

<p>...is just as valid as these...</p>

<p><img src="/assets/images/bounded-context-intro-alt.png" alt="Bad bounded context example"></p>

<p>But hopefully, this will post will show you how to get closer to the first example and further from the second.</p>

<h2>What is domain-driven design, and how does it relate to bounded contexts?</h2>

<p>Now that we know what a bounded context is, it makes sense to define <a href="https://airbrake.io/blog/software-design/domain-driven-design">domain-driven design (DDD)</a> as well. DDD is the practice of <strong>managing complexity of software applications by relating their underlying data models to domain logic</strong>. That&#39;s a mouthful; can we break it down further?</p>

<p>The <strong>domain</strong> is an <a href="https://en.wikipedia.org/wiki/Ontology_(information_science)">ontology</a>, meaning how things are grouped in the world. For example, the word <em>anesthesia</em> has a very specific connection to the domain of medicine. Another word, like <em>Mike</em>, can belong to multiple domains, such as the domain of Biblical names, or in the domain of politics as it relates to the NATO alphabet.</p>

<p>When the design is <strong>domain-driven</strong>, it means we place the <strong>model</strong> of our domain (e.g. a <code>PlayingCard</code> in the domain of <code>Poker</code>) in a <strong>context</strong> (e.g. the contextual grouping, such as a <code>Game</code>) to help manage the complexity.</p>

<p><img src="/assets/images/domain-driven-design.png" alt="The elements of domain-driven design"></p>

<h2>Can&#39;t I just have one context and call it a day?</h2>

<p><strong>Yes!</strong> In fact, that is preferred for very small applications. What do you name your one and only context for your small application? The application! If you&#39;re struggling with naming and you&#39;re just getting started with say, a script with one model, <strong>the context of your application can be named <em>your application</em></strong>. So if we took the above example and gave it a real context and model, it could very well look like this:</p>

<p><img src="/assets/images/simple-ddd.png" alt="Simple domain-driven design example"></p>

<h2>So what happens when my app becomes huge?</h2>

<p>This is where DDD is useful. Domain-driven design is specifically for handling the complexity of growing applications as they add more and more models. It doesn&#39;t <em>really</em> make sense for an app with one model. <strong>Once you get to about 4 models, that&#39;s a good time to start looking at binding your models to multiple contexts</strong>.  This isn&#39;t a hard-and-fast rule, so don&#39;t feel like you <em>have to</em> break out into multiple contexts, but once you get above 4 models, those contextual groupings will begin to surface.</p>

<h2>Naming contexts by example</h2>

<p>I&#39;ve always felt the best way to figure things out is to learn by example. I&#39;m going to illustrate 3 extremely simplified versions of popular applications, and we&#39;ll reveal how we can organize the data models of these applications into contexts. The main thing to remember is that <strong>naming contexts boils down to how you would describe the set of models to a friend in one word</strong>.</p>

<h3>Example 1: Quora</h3>

<p>Quora is a forum where users of the site can have their questions answered by the community. Questions can be upvoted by the community that are deemed intelligent or thought-provoking, helping it gain a larger audience for people to participate. Similarly, popular answers can be upvoted to help bring the best answers to the top, logically grouping the best question with the answer.</p>

<p><img src="/assets/images/quora-no-ddd.png" alt="Quora domain modeling example"></p>

<p>Looking at this setup a natural hierarchy emerges. Is there a signal here that we can start creating separate contexts? The first thing I see is that <code>Question</code> and <code>Answer</code> operate on the same level - they have equal precedence in the total domain hierarchy of this application. Now taking my advice from above, I have to ask myself:</p>

<blockquote>
<p>How would I describe <code>Question</code> and <code>Answer</code> to a friend in one word?</p>
</blockquote>

<p>First of all, we can look at the description I gave in the beginning. In addition, we remember that contexts are nouns, so when I&#39;m parsing those first descriptive sentences, I see words like <strong>forum</strong> and <strong>site</strong>.</p>

<p>Another technique is to bring these models into reality. Where do you see Q&amp;A in the real world? At tech talks, for example, it&#39;s usually at the end of a presentation where the speaker has a discussion with an audience. Using the noun technique in the previous sentence, potential context ideas are <strong>presentation</strong> and <strong>discussion</strong>. Now we can apply the domain-driven part of our design to ask ourselves:</p>

<blockquote>
<p>What domain does this belong to, and are there any conflicts or ambiguities we need to worry about?</p>
</blockquote>

<p>For presentation, this belongs to the domain of things like public speaking and business. The problem is, presentations are also conflated with the MVP (Model-View-Presenter) pattern. It is also a specific format for PowerPoint. Given that this word has specific connotations with programming jargon, we can rule this one out. That leaves us with <strong>discussion</strong> and <strong>forum</strong> as our context for our Q&amp;A models. Honestly, either will work here, but one hint is that forum relates more to this particular domain than discussion does, so I think we&#39;ll go with forum.</p>

<p>Now we can repeat the process for <code>User</code> and <code>Organization</code>. Since these aren&#39;t on the same level, we could argue that they shouldn&#39;t be in the same context, but we have to give them <em>a</em> context because there really isn&#39;t a point in making a 1-to-1 mapping of models to contexts for an application this small. But we also see a natural mapping here: people belong to organizations all of the time, whether it&#39;s a company or a volunteering outfit.</p>

<p>How do we describe people in an organization? We say they&#39;re organized by some hierarchy and are usually grouped into teams or divisions. That gives me a few nouns: <strong>hierarchy</strong>, <strong>grouping</strong>, <strong>team</strong>, and <strong>division</strong>. I can already see that team and division are immediately out since that is an example of how organizations are split and don&#39;t fully encompass the user-organization relationship. Grouping is good, but the word <em>group</em> itself does creep a bit too close to computer terms such as <code>GROUP BY</code> or the C# <code>grouping</code> keyword. So I&#39;ll go with <strong>hierarchy</strong>.</p>

<p><img src="/assets/images/quora-ddd.png" alt="Quora domain modeling example with contexts"></p>

<p>Nice work! We have a clear designation between two contexts that make sense. One thing to point out is that <strong>contexts can be renamed or changed later</strong>. It&#39;s not important that we anticipate the use of these contexts as we add models. Sure, <code>Hierarchy</code> might limit our flexibility, as it could be a stretch to add in helper models like <code>Address</code> into this context, but you can worry about refactoring later.</p>

<h3>Example 2: Twitter</h3>

<p>Twitter is a microblogging service that allows people to express their thoughts in a very limited space. All messages (called &quot;tweets&quot;) have a 280 character limit and are broadcast to followers. Followers can then interact with their connection&#39;s messages by liking or sharing (via &quot;retweeting&quot;) content that they think is valuable to their own networks.</p>

<p><img src="/assets/images/twitter-no-ddd.png" alt="Twitter domain modeling example"></p>

<p>What did you come up with?</p>

<p>Let&#39;s again apply the same heuristics from above to decide what our contexts will be. The first thing that stands out to me is the intimate tie between <code>User</code> and <code>Tweet</code>. It&#39;s the absolute core of this platform. Simplifying the noun technique, my first thought was that this <em>is</em> the <strong>microblog</strong>. However, given that Twitter has increased its word count from 140 to 280 characters, I feel like the <em>micro</em> portion of this is basically irrelevant, but the <strong>blog</strong> portion aptly contextualizes this relationship. Plus, it&#39;s a natural context for future potential models like <code>Comment</code>.</p>

<p>Next, there are the user-related interactions: following/followers is largely abstracted away since it&#39;s a <code>User</code> to <code>User</code> relationship. <code>Like</code> and <code>Share</code> are new models, and do have similar mechanisms that feel like they could be grouped. What did I just call these things? <strong>Interaction</strong>, that&#39;s one (in fact, I also used it in the opening paragraph). I also used <strong>network</strong> which would describe the following/follower relationship, but again, networking is computer jargon and that could collide with a lot of other concepts we are working with. I think <strong>interaction</strong> is a good one:</p>

<p><img src="/assets/images/twitter-ddd.png" alt="Twitter domain modeling example with contexts"></p>

<p>This one was a bit easier given our previous look, but again, I really want to stress that <strong>there is no right answer</strong>. All we are attempting to do here is make it easy for you to figure out the naming, but at the end of the day, there&#39;s no actual penalty in naming or grouping these however you like. In Phoenix, there is certainly some cost with talking between contexts, but it&#39;s not an insurmountable issue.</p>

<h3>Example 3: Google Drive</h3>

<p>Google Drive is a cloud file storage provider. You can access files from anywhere in the world on any device, provided you have a Google account. In addition to reading files, you can collaboratively write to files with other users who have permission to read (and write) these files. Further, you can collaborate via tools like comments and sticky notes to provide feedback even if you cannot directly write to the file. Google Drive does not provide universal file capabilities and is mainly focused on reading/writing for documents, spreadsheets, and slideshow presentations.</p>

<p><img src="/assets/images/google-drive-no-ddd.png" alt="Google Drive domain modeling example"></p>

<p>This one is a bit more complex. We have quite a few more models here with a bit more ambiguity for how we can split things up. Let&#39;s start with the easiest one that all exist on the same level: <code>Document</code>, <code>Spreadsheet</code>, and <code>Presentation</code>. How did I group those in the above description? I called them all <strong>files</strong>. Normally we&#39;d red-flag this context because it&#39;s too computer-specific of a domain, but remember, this service is <em>very</em> computer focused by its very nature, so it&#39;s actually okay that we give this a context like this.</p>

<p>The next layer up we have our <code>IO</code> which handles permissions for file read and write access controls and <code>Comment</code> which is a special kind of write. Finally, we have the <code>User</code> at the very top, which touches everything. Now your gut might tell you that reading, writing, and commenting are <strong>interactions</strong> just like in the Twitter example. But we can&#39;t do that for a few reasons:</p>

<ol>
<li>The methods for determining read and write access are encapsulated within <code>IO</code> - they are permission grants to unlock certain functionality, but <code>IO</code> in of itself is not an interaction</li>
<li>Remember, <code>User</code> has access to all of these models. Even though this UML diagram lays everything out in a hierarchy, it&#39;s not a simple three-tiered relationship, so in a way, we <em>interact</em> with our files as well by virtue of owning them.</li>
</ol>

<p>Let&#39;s offer a different narrative. We mentioned earlier that we can collaborate with other users to perform actions. The noun here is <strong>collaboration</strong>. Additionally, those actions are governed by <strong>permissions</strong>. Which do we choose?</p>

<p>This is probably the toughest one of all. Collaboration implies an innate connection with others, even though you can manipulate a file without any other users. Whereas permission makes sense with both user interaction and <code>IO</code> but seems to leave <code>Comment</code> out in the cold. The key clue we have is that <strong><code>Comment</code> is a subclass of <code>IO</code>, so we can lower the priority of making <code>Comment</code> work for our context</strong>. In other words, since we know permission is a sensible context for both <code>User</code> and <code>IO</code>, it stands to reason that <strong>permission</strong> will be a good context name because <code>Comment</code> is a type of <code>IO</code> in this domain.</p>

<p><img src="/assets/images/google-drive-ddd.png" alt="Google Drive domain modeling example with contexts"></p>

<p>This provides lots of ability to expand with both file types and ways of handling things centered around the <code>User</code>, such as authorization and authentication. Both contexts also pretty cleanly separate the levels of hierarchy between data models.</p>

<h2>Wrapping up</h2>

<p>Let&#39;s summarize our heuristic into three simple steps:</p>

<ol>
<li><strong>Map the domain.</strong> We used UML diagrams to lay out the hierarchy of our models to provide visual clues for possible contexts.</li>
<li><strong>Horizontal or vertical alignment provides clues.</strong> If something falls along the same plane or area of your diagram, you can bet it&#39;s a context.</li>
<li><strong>Describe that alignment in one word, preferring the noun with the more specific domain.</strong> This is the hardest part, but the idea here is to take the description of our application and pick out the nouns (and sometimes verbs) that associate our related models. From there, we simply prune the word that has the clearest message with the least ambiguity.</li>
</ol>

<p>Naming contexts is hard. It can be made easier if you follow these guidelines the next time you write an application using domain-driven design.</p>

<p>Make sure you don&#39;t stop with just your first iteration. Growing applications require consistent refinement. Sometimes you&#39;ll need to split off a context into multiple contexts, while other times you&#39;ll need to consolidate contexts. Don&#39;t be afraid to go in either direction. Finally, don&#39;t be afraid to be wrong, because there is no definitive right answer either. Good luck and happy naming!</p>

	  ]]></description>
	</item>


</channel>
</rss>
