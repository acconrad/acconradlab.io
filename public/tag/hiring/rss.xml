<?xml version="1.0" encoding="UTF-8" ?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
   
      <title>https://anonconsulting.com</title>
   
   <link>https://www.userinterfacing.com</link>
   <description>User Interfacing is a publication for user interface programming and design.</description>
   <language>en-us</language>
   <managingEditor> Adam C. Conrad</managingEditor>
   <atom:link href="rss" rel="self" type="application/rss+xml" />
   
	<item>
	  <title>Why hiring is broken and how I'm dealing with it</title>
	  <link>//why-hiring-is-broken-and-how-im-dealing-with-it/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2018-08-28T10:28:00-04:00</pubDate>
	  <guid>//why-hiring-is-broken-and-how-im-dealing-with-it/</guid>
	  <description><![CDATA[
	     <p>Programming interviews are a major source of anxiety and angst among the software community. Every few months you see a post on HackerNews saying that we need to seriously rethink how we should be interviewing candidates in the software development profession. The primary reason has to do with how programming interviews are structured today.</p>

<h2>The typical interview process in 2018</h2>

<p>If you&#39;ve ever interviewed at one of the big FAANG (Facebook Amazon Apple Netflix Google) companies you&#39;ve noticed a trend in how candidates are interviewed, regardless of what kind of position you&#39;re interviewing for. The format looks something like this:</p>

<ul>
<li><strong>A recruiter reaches out asking if you want to interview with [FAANG_COMPANY]</strong></li>
<li><strong>30-45 min phone screener</strong></li>
<li><strong>4-5 in-person interviews with a combination of fellow software engineers, engineering managers, and HR</strong></li>
</ul>

<p>The real nugget of frustration lies in the type of interview questions they ask. Whether you&#39;re an infrastructure engineer or a front-end developer, <strong>these companies love to ask data structure and algorithm questions on a whiteboard</strong>. If you aren&#39;t prepared, you can get <em>rocked</em> by them. There are two approaches people seem to take in response:</p>

<ol>
<li><strong>Interview where this doesn&#39;t happen.</strong> Don&#39;t want to be subjected to arbitrary interview questions from your CS classes with things you never do anymore? Don&#39;t play the game!</li>
<li><strong>Embrace the flawed process and game the system.</strong> In many ways, today&#39;s interviews are like studying for the SAT. They aren&#39;t an accurate reflection of what you&#39;ll actually be doing, but they do have a predictable format that you can study to maximize your chances of success.</li>
</ol>

<p>But is there a third, better way?</p>

<h2>Why hiring is the way it is today</h2>

<p>For a while, I was in the first camp and believed that these tests were contrived, led to too many false negatives (missing good candidates who simply didn&#39;t excel in these superficial conditions), and didn&#39;t actually measure programming aptitude under real conditions. I&#39;ve conducted probably over 100 interviews in my career and I will admit I have done both non-whiteboard and whiteboard interview questions with mixed results in both.</p>

<p>But then I realized something.</p>

<p>To preface my hypothesis, I will state right away that this isn&#39;t backed up by anything factual since I have never worked at either Facebook or Google. However, given these are highly-desirable companies to work for, I have now come around to understanding why I think hiring is in the tumultuous state it is in:</p>

<p><strong>Google and Facebook&#39;s core businesses revolve around algorithmic problems of large, connected sparse graphs. Therefore, they ask these questions to judge if their candidates can operate on all programming aspects of their business. As a consequence of their success, other companies have followed suit even if their core business is not solving a graph problem.</strong></p>

<p>Whether it is a network of web pages or a network of people&#39;s digital profiles, these businesses, at their core, are about efficiently managing <em>billions</em> of nodes in a connected graph. Problems at this kind of scale cannot be managed without at least some knowledge of graph data structures and algorithms for efficiently interacting with each other.</p>

<p>When you look at it this way, it starts to make sense as to why a company like Google would prefer to ask questions like this. The problem is that other companies have seen Google and Facebook&#39;s breakout success and have decided to emulate them without carefully understanding their own business and what is required of their engineers to perform well.</p>

<p>One analogy that resonates with me is when young teenagers read a bodybuilding magazine and decide that if they want to look like Arnold Schwarzenegger, they have to train like him. So the publication outlines Arnold&#39;s winning workout that leads him to victory at Mr. Olympia.</p>

<p>Do you see the flaw there? They&#39;re showcasing advanced athletes with their advanced workouts. Instead, what you really want to see is <strong>what the successful athletes did as beginners to get to the next stage on their path to ultimate success</strong>.</p>

<p>Put it another way: you don&#39;t want to emulate Airbnb&#39;s or Stripe&#39;s homepage today if you want to look at how to market a successful startup. Instead, you want to look at Airbnb&#39;s homepage in 2008, or Stripe&#39;s in 2010.</p>

<p><strong>It is far better to understand success through the <em>whole journey</em>, and not just at the end when success has already been attained.</strong></p>

<p>Getting back to hiring, I think businesses suffer from this same issue, and emulate the FAANG with its current success, rather than look at their earlier hiring practices, or at least take a step back and critically evaluate <em>why</em> they chose to hire the way the did. Perhaps with that insight, we might have businesses who hire to truly reflect the capabilities required of the jobs they are paying for.</p>

<h2>So then why are you doing this?</h2>

<p>&quot;Okay&quot;, you might say, &quot;so even with all of that said, why are you still in favor of learning data structures and algorithms after being in the business for 10 years?&quot;</p>

<p>Good question! It comes down to four primary reasons:</p>

<h3>If you can do this, you can pretty much handle any interview</h3>

<p>As I mentioned earlier, the second camp of folks has embraced that this is our reality in hiring right now. There are <a href="https://www.amazon.com/Elements-Programming-Interviews-Python-Insiders/dp/1537713949/ref=sr_1_1?ie=UTF8&qid=1526172412&sr=8-1&keywords=elements+of+programming+interviews+python">lots</a> of great <a href="https://www.amazon.com/Cracking-Coding-Interview-Programming-Questions/dp/0984782850">books</a> that give you the background plus targeted practice to essentially guarantee entry into the coveted FAANG community.</p>

<p>I personally know 4 people in the last year who used one of those two books and put in roughly 80-100 hours of study and landed jobs at Google, Facebook, and Amazon.</p>

<p>This stuff works, plain and simple. Put another way: if you knew that with just 100 hours of work, 2.5 weeks of full-time study, you could <em>drastically</em> increase your chances of earning well into the six figures of income, would you put in the time?</p>

<p>Even if you know that you&#39;re &quot;above these games,&quot; or that &quot;your work speaks for itself,&quot; is it really <em>that</em> much to ask to take what amounts to one semester of college coursework to <strong>have the peace of mind that whether it&#39;s Google or some small startup, you&#39;ll always pass a tech interview and get a great job?</strong></p>

<p>Once I looked at it like this, it was hard <em>not to</em> give this a shot, especially considering I never took a dedicated algorithms class in college.</p>

<h3>I enjoy teaching people</h3>

<p>One of my favorite jobs of the past few years was acting as an engineering manager. I was fortunate to still spend quite a bit of my time coding, but I was also able to mentor junior engineers. I get as much back as I give when I&#39;m teaching someone something for the first time.</p>

<p>I believe that anyone can do this thing called programming. My hope is that, given the right approach and materials, this can be taught in a practical way that isn&#39;t subject to the academic rigidity of a textbook or classroom. To be fair, I will be using a textbook and college course to guide my notes, but my hope is that my interpretation will clear up some of the murky confusion that often arises from overly dense courses.</p>

<p><strong>If you want a new way of learning the fundamentals and nothing else has worked, maybe this will.</strong></p>

<h3>Teaching reinforces learning</h3>

<p>If you&#39;ve heard of the renowned <a href="https://pt.coursera.org/learn/learning-how-to-learn">learning how to learn course</a> you know that the best way to learn something is to teach it to someone else. So yes, as selfish motivation for me to permanently learn this material, I will be employing many of that course&#39;s techniques (e.g. chunking, spaced repetition, teaching, alternative notetaking methods) to solidify my understanding of this topic.</p>

<h3>This is part of something even bigger</h3>

<p>Lastly, one of my prime motivations for doing all of this is because <strong>I think this is necessary background before one can start to appreciate the fundamentals of front-end UI development.</strong></p>

<p>Eventually, I&#39;d like to extend this to a second series on the fundamentals of UI frameworks like Angular and React. Both of these frameworks are deeply rooted in understanding how the DOM works.</p>

<p>But before you can truly understand how the DOM works, you need to understand how a tree works. And to understand how a tree works, it helps to understand how data structures like lists and arrays work. When I kept peeling back the layers, I realized it just made more sense to start with the fundamentals of computer science and programming.</p>

<h2>How this is going to work</h2>

<p>It is nearing the end of summer and college classes are starting up again. It turns out one of those courses is <a href="http://www3.cs.stonybrook.edu/%7Eskiena/373/">Steven Skiena&#39;s CSE 373 Analysis of Algorithms class</a>. Skiena is famous for his <a href="http://www.algorist.com/">Algorithm Design Manual</a>, a book considered by many to be the most approachable introduction to data structures and algorithms. Even Steve Yegge, the famous blogger and Xoogler, <a href="https://steve-yegge.blogspot.com/2008/03/get-that-job-at-google.html">recommends the book</a> above other canonical texts like <em>Cracking the Coding Interview</em> or the mega textbook, <em>Algorithms</em>.</p>

<p>Given I&#39;ve tried my hand at <em>The Elements of Programming Interviews</em>, <em>Cracking the Coding Interview</em>, <em>Algorithms</em>, and <em>The Competitive Programmer&#39;s Handbook</em>, all without success, I thought I&#39;d give this one a try. In addition to owning a copy of the book, I figured the combination of the book plus the actual live lectures and course materials might help fill in the gaps from just studying a textbook on your own.</p>

<p>In other words, <strong>I&#39;m auditing Skiena&#39;s algorithms course and this series will be my notes as I attempt to learn data structures and algorithms once and for all.</strong> And hopefully, I&#39;ll help others along the way.</p>

<h3>Structure</h3>

<p>As this is a college course, I imagine many of the exercises and sections are simply not applicable to why you might follow along with this series: to ace a technical interview. So if I see something in the course that you&#39;ll likely never see in an interview, I&#39;ll acknowledge it was covered, but I won&#39;t say too much so we only need to learn exactly what will be useful to us.</p>

<h4>Each post will follow each lecture</h4>

<p>Because I&#39;m going to follow along with CSE 373, I&#39;m going to be taking notes as if I were a student in the course. Those notes will then be transcribed and presented in a teachable format to both reinforce my learning of the material but also to provide a useful filter for my fellow professionals.</p>

<h4>Each post will cover the publically-available exercises</h4>

<p>In case this somehow gets picked up enough to warrant the attention of actual students taking the class, <strong>no material will be published before it becomes publicly available.</strong> This means that if there are homework problems assigned, I will not be publishing my attempts at a solution until the course has published solutions (if ever).</p>

<p>As much as I&#39;d truly love to give you a deep dive into every insight of someone taking the course, I don&#39;t think it&#39;s fair to the instructor or the students to turn this series into a cheater&#39;s reference guide to getting through the course (and hey, I might be wrong on a few things).</p>

<h4>Each post will provide additional commentary where appropriate</h4>

<p>Finally, my hope is to add insight into questions I might have or relevant connections to other material that might help bolster everyone&#39;s learning of the material. I certainly won&#39;t have all of the answers as I take this course, but my hope is this can be a launching point into more discussion <a href="https://twitter.com/theadamconrad">on Twitter</a> or other forms of social media so we can figure this all out together.</p>

	  ]]></description>
	</item>


</channel>
</rss>
