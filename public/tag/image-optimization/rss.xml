<?xml version="1.0" encoding="UTF-8" ?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
   
      <title>https://anonconsulting.com</title>
   
   <link>https://www.userinterfacing.com</link>
   <description>User Interfacing is a publication for user interface programming and design.</description>
   <language>en-us</language>
   <managingEditor> Adam C. Conrad</managingEditor>
   <atom:link href="rss" rel="self" type="application/rss+xml" />
   
	<item>
	  <title>The Fastest Way to Increase Your Site's Performance Now</title>
	  <link>//the-fastest-way-to-increase-your-sites-performance-now/</link>
	  <author>Adam C. Conrad</author>
	  <pubDate>2018-04-25T04:25:00-04:00</pubDate>
	  <guid>//the-fastest-way-to-increase-your-sites-performance-now/</guid>
	  <description><![CDATA[
	     <p>The web is getting bigger. It is also getting slower. The funny thing is, every single day I&#39;m visiting new sites to try out a new product or read a blog, and I find the same performance issues over and over again. All I have to do is check out DevTools to see that most images are way too large.</p>

<p>What baffles me is how easy it is to fix this. <em>The</em> fastest way to increase the performance of any site is to <strong>losslessly compress images</strong> that are being displayed. In fact, it&#39;s so easy, anyone on your team, regardless of coding experience, can make a dent in improving performance.</p>

<h2>Compressing images should be automatic</h2>

<p>The <a href="https://www.machmetrics.com/speed-blog/average-page-load-times-websites-2018/">average page size</a> is 1.88 MB, with the majority of that weight coming from images. So if the bulk of a site is images, we should be focusing on image optimization before we focus on things like database indexing, caching, or other more advanced forms of performance optimization.</p>

<p><a href="https://royal.pingdom.com/2018/03/07/web-performance-top-100-e-commerce-sites-in-2018/"><img src="/assets/images/top-100-performance.png" alt="Performance of top 100 e-commerce sites via Pingdom"></a></p>

<h2>The 4 levels of image optimization</h2>

<p>There are four major levels of effort required to reduce the size of image data. Each level builds off of the previous one, so be sure to complete level 1 before moving onto level 2.</p>

<p>The first level requires the least amount of work (which can be accomplished in minutes). The fourth level requires all of the previous steps as well as its own set of tasks. This can take hours, if not days, depending on how image-heavy your content is.</p>

<p>Each level provides diminishing returns. Simply completing one level will do more for your performance than the next level. Which is why it&#39;s important not to skip any steps.</p>

<h3>Level 1: Lossless compression</h3>

<p>The first thing you should do is grab a copy of <a href="https://imageoptim.com/">ImageOptim</a>. This tiny application is your new best friend. ImageOptim will scan every image within a folder you select, and automatically compress the image, removing all of the useless data.</p>

<p>In the context of this blog, useless data refers to anything that doesn&#39;t contribute to the visual quality of the image being displayed. In fact, the term lossless refers to <a href="https://en.wikipedia.org/wiki/Lossless_compression">the ability to reconstruct the original data from the compressed data</a> without losing any information needed to render that data.</p>

<p>For example, did you know that a photo can store the name of the author of that picture? That comment might be useful in protecting the copyright of the photographer, but it has zero effect on what your application will look like to the world. That metadata is <strong>lossless</strong>, and if you were to run lossless compression on that JPG photograph, it would strip out this and <a href="https://www.photometadata.org/META-Resources-Field-Guide-to-Metadata">dozens</a> of other metadata fields.</p>

<p>What you may not realize is that this kind of metadata makes up a very large part of the image&#39;s total size. This is exactly why lossless compression is so important. <strong>Removing lossless data does not affect the visual quality of your images, but it can drastically reduce their size</strong>. And the smaller the image, the less your browser has to download to retrieve from the server where the image is hosted.</p>

<p>That is why running ImageOptim should be a no-brainer. It only takes a few minutes to run and provides potentially big savings in size without a reduction in quality.</p>

<h3>Level 2: Targeted lossy compression</h3>

<p>The next level requires a little more work, but even more savings. At this point, you&#39;ll have to make specific fixes for each image type. We&#39;ll break this down for the 3 major image types (and if you aren&#39;t using one of these three, make sure you check out the next step):</p>

<p><img src="/assets/images/image-optim-metadata.png" alt="Toggle metadata in ImageOptim preferences"></p>

<h4>JPG</h4>

<p>If you&#39;ve already run a JPG through ImageOptim, you&#39;ll have to download another piece of software. There are additional options for JPGs in ImageOptim preferences, such as removing metadata, but I&#39;ve found that removing color profile metadata actually colorizes images to include a bit more greens and yellows.</p>

<p>So which should you choose? <a href="http://www.jpegmini.com/app">JpegMini</a> handles both lossless and lossy compression and allows you to resize your images.</p>

<p>With regards to image sizing, <strong>use the correct image size</strong>. If you have an image that is 512x512 pixels, but the image is only being used in a 64x64 canvas, scale down that image.  Worried about Retina displays? Check out level 4 for more details.</p>

<p><strong>Another tip: Reduce image quality for JPGs down to 80% by default</strong>. JPG is a lossy format in general, but a very <a href="http://regex.info/blog/lightroom-goodies/jpeg-quality">in-depth analysis of JPG compression quality</a> shows that there is effectively zero difference in visual quality, but <em>significant savings in file size</em>.</p>

<h4>PNG</h4>

<p>Before you need to download anything, you should strip PNG metadata in ImageOptim. This is safe to do for PNGs and I&#39;ve yet to see a PNG image that looks off by removing metadata.</p>

<p><a href="http://optipng.sourceforge.net/">OptiPNG</a> is the next level PNG optimizer after ImageOptim. It has far more algorithms for compressing PNGs and tries far more options to get a truly optimal PNG file size.</p>

<p><img src="/assets/images/color-table.png" alt="PNG color table"></p>

<p>In addition to compression, PNGs are far more sensitive to <strong>using the correct color profiles</strong>. If you have a black-and-white PNG, using a colorless profile will save a ton of space. From there, <strong>choose the correct color palette size</strong> to restrict the number of colors to an acceptable range. Applications like Photoshop will show you a preview when saving your image, so feel free to play with color palette sizes until you see something that reduces the number of colors without reducing the image quality.</p>

<h4>GIF</h4>

<p>Again, ImageOptim will do a solid job optimizing all image types, including GIFs. However, for a more customized compression technique, take a look at <a href="https://nikkhokkho.sourceforge.io/static.php?page=FileOptimizer">FileOptimizer</a> with the additional plugins for <a href="http://www.lcdf.org/gifsicle/">GifSicle</a> and <a href="https://kornel.ski/lossygif">GifSicle Lossy</a>. These combined seem to do a good job of maintaining image quality while drastically reducing the size of the file.</p>

<h3>Level 3: Convert to the correct image type</h3>

<p><img src="/assets/images/jpegxr-logo.svg" alt="JPEG XR logo" width="100">
<img src="/assets/images/jpeg2000-logo.svg" alt="JPEG 2000 logo" width="100">
<img src="/assets/images/webplogo.png" alt="WebP logo" width="100"></p>

<p>At this point, you&#39;ve already exhausted even the most advanced compression optimization techniques. And while these programs do all of the work for you in a matter of seconds, the truth is you may need to scrap your images altogether if they are in the wrong image format.</p>

<p>How do you know if they&#39;re in the right format? Follow this simple questionnaire and then save your image into the new file format (and then repeat levels 1 and 2 to compress your new images!).</p>

<h4>Does it look like a photograph? Use a JPG</h4>

<p>JPGs were meant to be used with photographs. If you have avatars of your team, photographs of your office, or other real-life images, make sure they are in the JPG format.</p>

<h4>Does it look like a computer-generated graphic or drawing? Use a PNG</h4>

<p>Everything else can be a PNG. Other formats may provide better quality, but if you&#39;re serving a web or image application, a PNG will do and is universally read by every kind of application. The one exception to this would be icons...</p>

<h4>Does it look like an icon? Use an SVG</h4>

<p>Icons are also computer-generated, but the difference is that icons are generally used alongside typography. If you think the image will be used in a menu, on a button, or is meant to symbolize something, it&#39;s probably an icon, and icons will benefit from being SVGs because they can be scaled along with your type and lose 0 fidelity. They will look just as crisp as your fonts and will be much smaller as SVGs as well.</p>

<h4>Are you supporting the latest browsers and don&#39;t care about Firefox? Use WebP, JPEG 2000, and JPEG XR</h4>

<p>Finally, there is a push for next-generation image formats. JPG and PNG have been around for more than two decades, and it&#39;s about time we have some new formats that innovate to maintain image quality without bloating our applications. Even better, they don&#39;t require you decide between image type. For example, WebP works great for both photographs and computer-generated images.</p>

<p>The downside is that support is fragmented across devices and browsers. <a href="https://developers.google.com/speed/webp/">WebP</a> was made by Google, so it&#39;s naturally designed only for Chrome and Chrome mobile. JPG also has evolved formats, but <a href="https://jpeg.org/jpeg2000/index.html">JPEG2000</a> is only supported by Apple (Safari and Safari Mobile), while <a href="https://jpeg.org/jpegxr/index.html">JPEG XR</a> is only supported by Microsoft (IE and IE Edge).</p>

<p>What about Firefox? There is no next-gen format for this browser, but they do have a <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1294490">2-year-old bug ticket</a> to implement WebP and it is assigned, but who knows when this will land.</p>

<h3>Level 4: Source sets &amp; fallbacks</h3>

<p>If you&#39;ve chosen the correct image and you&#39;ve compressed the heck out of it, you&#39;ll want to make sure it&#39;s being served on the right device in the right aspect ratio. I alluded to this back in Level 2, but if you have concerns about Retina displays like MacBook Pros, or even x3 quality for the newest iPhones, you&#39;ll want to ake multiple copies of your image in all of these formats.</p>

<p>Going back to the previous example, if you serve avatars in a 64x64 JPG format, you&#39;ll also want to make copies of dimension 128x128 for Retina <em>and</em> 192x192 for Retina x3. The best way to do this is to <strong>start with a larger image and scale down, rather than scale up</strong>. You know those crime dramas where they ask the hacker to &quot;<em>Zoom! Enhance!</em>&quot;? We all know that doesn&#39;t work in real life, and that same thing holds true for your images - you can&#39;t add clarity where there was none in the first place.</p>

<p>Instead, start with the original source image (say, 512x512) and scale down to 192, save a copy, then 128, save another copy, then 64, and save that last copy. This will result in a less blurry, albeit still lossy (because you are removing information in the form of pixel fidelity) set of images.</p>

<p>With all of these duplicate, scaled images, how do we tie this all together? The image attribute known as <code>srcset</code> comes to the rescue:</p>
<div class="highlight"><pre><code class="language-text" data-lang="text">&lt;img src=&quot;avatar.jpg&quot; srcset=&quot;avatar-sm.jpg 1x, avatar-md.jpg 2x, avatar-lg.jpg 3x&quot; alt=&quot;Your responsive photo avatar&quot;&gt;
</code></pre></div>
<p><code>Srcset</code> is pretty amazing. It will always default to the original <code>1x</code> magnifier like a normal image tag would. However, if it does find the other images, it will apply them to the given aspect ratios for your device. In other words, if you are viewing the photo on a 2015 MacBook Pro, the browser will select <code>avatar-md.jpg</code>, but if you are on an iPhone X, it will select <code>avatar-lg.jpg</code>. And if you&#39;re in a military bunker using IE8, it will fall back to <code>avatar-sm.jpg</code>.</p>

<p><code>Sizes</code> is another property for responsive images but it relies on the width of the device rather than the pixel density. The format is the same:</p>
<div class="highlight"><pre><code class="language-text" data-lang="text">&lt;img src=&quot;bg-slim.png&quot;  srcset=&quot;bg-slim.png 320w, bg-wide.png 900w&quot; sizes=&quot;(max-width: 600px) 320px, 50vw&quot; alt=&quot;Your responsive background&quot;&gt;
</code></pre></div>
<p>You specify an image, space, the size the image should be displayed, with a <code>w</code> descriptor for the <code>srcset</code>, and then, using the <code>sizes</code> attribute, specify the media queries at which point the various sources should be used.</p>

<p>The only downside? <strong><code>Srcset</code> is not supported in IE</strong>. It <em>is</em> supported in IE Edge, and the <code>sizes</code> attribute is supported everywhere. In my honest opinion, this isn&#39;t something to worry about because all of the Retina devices using IE are already new enough to support IE Edge. Anything that still requires IE11 and down likely doesn&#39;t have a Retina display anyway (unless it is connected to an hi-density external monitor) so you likely won&#39;t run into this problem being a real blocker for you.</p>

<h2>Something is better than nothing</h2>

<p>This is not an exhaustive list of image optimization techniques nor is it meant to be a prescriptive formula. <strong>Even if you only run ImageOptim on all of your images in your application, you could be saving upwards of 60-80% of what your users have to download.</strong></p>

<p>Images, audio, and video, <em>not</em> your source code, will comprise the largest space for your application by far. Effectively choosing, compressing, and displaying your images will have a marked impact on both application size and performance for your end user experience. The best part is it only takes a few downloads and a few more seconds to run your images through a very easy set of tools that will provide an instant improvement without sacrificing quality.</p>

	  ]]></description>
	</item>


</channel>
</rss>
